from enum import IntEnum

class Op(IntEnum):
    ADD = 1
    MULTIPLY = 2
    INPUT = 3
    PRINT = 4
    JUMP_IF_TRUE = 5
    JUMP_IF_FALSE = 6
    LESS_THAN = 7
    EQUAL = 8
    DONE = 99

def get_opcode_modes(opcode_modes):
    modes = []
    opcode = opcode_modes % 100
    opcode_modes //= 100
    while opcode_modes > 0:
        modes.append(opcode_modes % 10)
        opcode_modes //= 10
    n_modes = 1
    if opcode in (Op.JUMP_IF_TRUE, Op.JUMP_IF_FALSE):
        n_modes = 2
    elif opcode in (Op.ADD, Op.MULTIPLY, Op.LESS_THAN, Op.EQUAL):
        n_modes = 3
    while len(modes) < n_modes:
        modes.append(0)
    return opcode, modes

def get_value(opcodes, position, mode):
    if mode == 0:
        return opcodes[opcodes[position]]
    return opcodes[position]

def run_intcode(opcodes):
    position = 0
    opcode = 0
    while opcode != Op.DONE:
        # print(opcodes)
        opcode, modes = get_opcode_modes(opcodes[position])
        if opcode == Op.DONE:
            continue
        elif opcode == Op.ADD:
            a = get_value(opcodes, position + 1, modes[0])
            b = get_value(opcodes, position + 2, modes[1])
            opcodes[opcodes[position + 3]] = a + b
            position += 4
        elif opcode == Op.MULTIPLY:
            a = get_value(opcodes, position + 1, modes[0])
            b = get_value(opcodes, position + 2, modes[1])
            opcodes[opcodes[position + 3]] = a * b
            position += 4
        elif opcode == Op.INPUT:
            opcodes[opcodes[position + 1]] = int(input('give me a number: '))
            position += 2
        elif opcode == Op.PRINT:
            print(opcodes[opcodes[position + 1]])
            position += 2
        elif opcode == Op.JUMP_IF_TRUE:
            if get_value(opcodes, position + 1, modes[0]) != 0:
                position = get_value(opcodes, position + 2, modes[1])
            else:
                position += 3
        elif opcode == Op.JUMP_IF_FALSE:
            if get_value(opcodes, position + 1, modes[0]) == 0:
                position = get_value(opcodes, position + 2, modes[1])
            else:
                position += 3
        elif opcode == Op.LESS_THAN:
            a = get_value(opcodes, position + 1, modes[0])
            b = get_value(opcodes, position + 2, modes[1])
            opcodes[opcodes[position + 3]] = 1 if a < b else 0
            position += 4
        elif opcode == Op.EQUAL:
            a = get_value(opcodes, position + 1, modes[0])
            b = get_value(opcodes, position + 2, modes[1])
            opcodes[opcodes[position + 3]] = 1 if a == b else 0
            position += 4
        else:
            print('unknown opcode')
            return [0] # in this case [0] is not a valid result... but we should trigger an error
    return opcodes

with open('input.txt', 'r') as f:
    values = [int(i) for i in f.readline().rstrip().split(',')]

# print(values)

if __name__ == "__main__":
    # if run_intcode([1002, 4, 3, 4, 33]) != [1002, 4, 3, 4, 99]:
        # print('fail')
    # print(run_intcode([1002, 4, 3, 4, 33]))

    # values = [3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9]
    # values = [3,9,7,9,10,9,4,9,99,-1,8] # is input < 8?
    # values = [3,9,8,9,10,9,4,9,99,-1,8] # is input == 8?
    # values = [3,3,1108,-1,8,3,4,3,99] # is input == 8?
    run_intcode(values)
    # 1: 13547311
    # 2: 236453

    # opcode, modes = get_opcode_modes(1002)
    # print(opcode)
    # print(modes)
