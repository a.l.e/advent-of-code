paths = []
with open('input.txt', 'r') as f:
    for line in f:
        paths.append([(move[0], int(move[1:])) for move in line.rstrip().split(',')])

# paths = [
#     [('R', 8), ('U', 5), ('L', 5), ('D', 3)],
#     [('U', 7), ('R', 6), ('D', 4), ('L', 4)]
# ]

movements = {
    'U': (0, 1),
    'R': (1, 0),
    'D': (0, -1),
    'L': (-1, 0),
}

def get_trace(pos, direction, count):
    return [(pos[0] + movements[direction][0] * i, pos[1] + movements[direction][1] * i)
        for i in range(1, count + 1)]

def first():
    wires = []
    for p in paths:
        wire = []
        for m in p:
            wire += get_trace(wire[len(wire) - 1] if len(wire) > 0 else (0, 0), m[0], m[1])
        wires.append(sorted(wire, key=lambda c: (c[1], c[0])))

    intersections = set(wires[0]).intersection(set(wires[1]))
    print(intersections)

    manhattan = min(list(intersections)[1:], key=lambda x: abs(x[0]) + abs(x[1]))
    print(manhattan)
    print(abs(manhattan[0]) + abs(manhattan[1]))

def get_trace_2(pos, direction, count):
    if direction == 'U':
        return [(pos[0], pos[1] + i, pos[2] + i) for i in range(1, count + 1)]
    if direction == 'R':
        return [(pos[0] + i, pos[1], pos[2] + i) for i in range(1, count + 1)]
    if direction == 'D':
        return [(pos[0], pos[1] - i, pos[2] + i) for i in range(1, count + 1)]
    if direction == 'L':
        return [(pos[0] - i, pos[1], pos[2] + i) for i in range(1, count + 1)]

def second():
    wires = []
    for p in paths:
        wire = []
        for m in p:
            wire += get_trace_2(wire[len(wire) - 1] if len(wire) > 0 else (0, 0, 0), m[0], m[1])
        wires.append(sorted(wire, key=lambda c: (c[1], c[0])))

    # brute force solution times out...

    # intersections = []
    # for a in wires[0]:
    #     for b in wires[1]:
    #         if a[0] == b[0] and a[1] == b[1]:
    #             intersections.append((a[0], a[1], a[2] + b[2]))
    #         
    # print(intersections)
    # manhattan = min(list(intersections), key=lambda x: x[2])
    # print(manhattan)

    intersections = []
    wire_a = iter(wires[0])
    wire_b = iter(wires[1])
    a = next(wire_a)
    b = next(wire_b)
    while True:
        try:
            if a[1] > b[1]:
                b = next(wire_b)
            elif a[1] < b[1]:
                a = next(wire_a)
            else:
                if a[0] > b[0]:
                    b = next(wire_b)
                elif a[0] < b[0]:
                    a = next(wire_a)
                else:
                    intersections.append((a[0], a[1], a[2] + b[2]))
                    a = next(wire_a)
                    b = next(wire_b)
        except StopIteration:
            break

    # print(intersections)
    distance = min(list(intersections), key=lambda x: x[2])
    print(distance)
        


if __name__ == "__main__":
    # first()
    second()
