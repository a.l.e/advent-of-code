from dataclasses import dataclass
import itertools
import copy

@dataclass
class Moon:
    x: int
    y: int
    z: int
    dx: int = 0
    dy: int = 0
    dz: int = 0

    # def __eq__(self, other):
    #     if not isinstance(other, Moon):
    #         return NotImplemented
    #     return self.x == other.x and \
    #         self.y == other.y and \
    #         self.z == other.z


    @staticmethod
    def compare(a, b):
        if a < b:
            return 1
        elif a > b:
            return -1
        return 0

    def apply_gravity(self, other):
        self.dx += Moon.compare(self.x, other.x)
        self.dy += Moon.compare(self.y, other.y)
        self.dz += Moon.compare(self.z, other.z)

    def apply_velocity(self):
        self.x += self.dx
        self.y += self.dy
        self.z += self.dz

    def kinetic_energy(self):
        return sum([abs(self.x), abs(self.y), abs(self.z)]) \
            * sum([abs(self.dx), abs(self.dy), abs(self.dz)])
    def get_tuple(self):
        return (self.x, self.y, self.z, self.dx, self.dy, self.dz)

def first(moons, demo):
    for i in range(10 if demo else 1000):
        for pair in itertools.combinations(moons, 2):
            pair[0].apply_gravity(pair[1])
            pair[1].apply_gravity(pair[0])

        for moon in moons:
            moon.apply_velocity()
    # print(moons)

    print(sum([moon.kinetic_energy() for moon in moons]))

def second_b(moons, demo):
    past = set()
    while (moon.get_tuple() for moon in moons) not in past:
        past.add((moon.get_tuple() for moon in moons))
        for pair in itertools.combinations(moons, 2):
            pair[0].apply_gravity(pair[1])
            pair[1].apply_gravity(pair[0])

        for moon in moons:
            moon.apply_velocity()

        if len(past) % 10000 == 0:
            print(len(past))
    print(len(past))
        
def second(moons, demo):
    past = set()
    start = set(moon.get_tuple() for moon in moons)
    iteration = 0
    while iteration == 0 or set(moon.get_tuple() for moon in moons) != start:
        iteration += 1
        for pair in itertools.combinations(moons, 2):
            pair[0].apply_gravity(pair[1])
            pair[1].apply_gravity(pair[0])

        for moon in moons:
            moon.apply_velocity()

        if iteration % 10000 == 0:
            print(iteration)
    print(iteration)

if __name__ == '__main__':

    TODO: part is too slow!!!!!!!!!! no result

 
    demo = False
    path = 'input-demo.txt' if demo else 'input.txt'

    moons = []
    with open(path, 'r') as f:
        for line in f:
            moons.append(Moon(*[int(var.split('=')[1])
                for var in line.rstrip()[1:-1].split(', ')]))

    first(copy.deepcopy(moons),demo)
    second(copy.deepcopy(moons), demo)
