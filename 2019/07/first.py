from enum import IntEnum
from itertools import permutations

class Op(IntEnum):
    ADD = 1
    MULTIPLY = 2
    INPUT = 3
    PRINT = 4
    JUMP_IF_TRUE = 5
    JUMP_IF_FALSE = 6
    LESS_THAN = 7
    EQUAL = 8
    DONE = 99

def get_opcode_modes(opcode_modes):
    modes = []
    opcode = opcode_modes % 100
    opcode_modes //= 100
    while opcode_modes > 0:
        modes.append(opcode_modes % 10)
        opcode_modes //= 10
    n_modes = 1
    if opcode in (Op.JUMP_IF_TRUE, Op.JUMP_IF_FALSE):
        n_modes = 2
    elif opcode in (Op.ADD, Op.MULTIPLY, Op.LESS_THAN, Op.EQUAL):
        n_modes = 3
    while len(modes) < n_modes:
        modes.append(0)
    return opcode, modes

def get_value(opcodes, position, mode):
    if mode == 0:
        return opcodes[opcodes[position]]
    return opcodes[position]

def run_intcode(opcodes, *args):
    args = list(args)
    result = []
    position = 0
    opcode = 0
    while opcode != Op.DONE:
        # print(opcodes)
        opcode, modes = get_opcode_modes(opcodes[position])
        if opcode == Op.DONE:
            continue
        elif opcode == Op.ADD:
            a = get_value(opcodes, position + 1, modes[0])
            b = get_value(opcodes, position + 2, modes[1])
            opcodes[opcodes[position + 3]] = a + b
            position += 4
        elif opcode == Op.MULTIPLY:
            a = get_value(opcodes, position + 1, modes[0])
            b = get_value(opcodes, position + 2, modes[1])
            opcodes[opcodes[position + 3]] = a * b
            position += 4
        elif opcode == Op.INPUT:
            # opcodes[opcodes[position + 1]] = int(input('give me a number: '))
            opcodes[opcodes[position + 1]] = args.pop(0)
            position += 2
        elif opcode == Op.PRINT:
            # print(opcodes[opcodes[position + 1]])
            result.append(opcodes[opcodes[position + 1]])
            position += 2
        elif opcode == Op.JUMP_IF_TRUE:
            if get_value(opcodes, position + 1, modes[0]) != 0:
                position = get_value(opcodes, position + 2, modes[1])
            else:
                position += 3
        elif opcode == Op.JUMP_IF_FALSE:
            if get_value(opcodes, position + 1, modes[0]) == 0:
                position = get_value(opcodes, position + 2, modes[1])
            else:
                position += 3
        elif opcode == Op.LESS_THAN:
            a = get_value(opcodes, position + 1, modes[0])
            b = get_value(opcodes, position + 2, modes[1])
            opcodes[opcodes[position + 3]] = 1 if a < b else 0
            position += 4
        elif opcode == Op.EQUAL:
            a = get_value(opcodes, position + 1, modes[0])
            b = get_value(opcodes, position + 2, modes[1])
            opcodes[opcodes[position + 3]] = 1 if a == b else 0
            position += 4
        else:
            print('unknown opcode')
            raise Exception('unknown opcode')
    return result

with open('input.txt', 'r') as f:
    values = [int(i) for i in f.readline().rstrip().split(',')]

# print(values)

if __name__ == "__main__":
    # values = [3, 15, 3, 16, 1002, 16, 10, 16, 1, 16, 15, 15, 4, 15, 99, 0, 0]
    # values = [3, 23, 3, 24, 1002, 24, 10, 24, 1002, 23, -1, 23, 101, 5, 23, 23, 1, 24, 23, 23, 4, 23, 99, 0, 0]
    max_thruster = (0, [])
    for p in permutations([0, 1, 2, 3, 4]):
        result = 0
        for i in p:
            result = run_intcode(values, i, result)[0]
        if result > max_thruster[0]:
            max_thruster = (result, p)
print(max_thruster)
# 73384 too low
