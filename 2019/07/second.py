from enum import IntEnum
from itertools import permutations
from itertools import cycle

class Op(IntEnum):
    ADD = 1
    MULTIPLY = 2
    INPUT = 3
    OUTPUT = 4
    JUMP_IF_TRUE = 5
    JUMP_IF_FALSE = 6
    LESS_THAN = 7
    EQUAL = 8
    DONE = 99

class Amplifier:

    def __init__(self, opcodes):
        self.opcodes = opcodes
        self.position = 0
        self.done = False

    @staticmethod
    def get_opcode_modes(opcode_modes):
        modes = []
        opcode = opcode_modes % 100
        opcode_modes //= 100
        while opcode_modes > 0:
            modes.append(opcode_modes % 10)
            opcode_modes //= 10
        n_modes = 1
        if opcode in (Op.JUMP_IF_TRUE, Op.JUMP_IF_FALSE):
            n_modes = 2
        elif opcode in (Op.ADD, Op.MULTIPLY, Op.LESS_THAN, Op.EQUAL):
            n_modes = 3
        while len(modes) < n_modes:
            modes.append(0)
        return opcode, modes

    @staticmethod
    def get_value(opcodes, position, mode):
        if mode == 0:
            return opcodes[opcodes[position]]
        return opcodes[position]

    def run_intcode(self, *args):
        args = list(args)
        opcode = 0
        while opcode != Op.DONE:
            # print(opcodes)
            opcode, modes = Amplifier.get_opcode_modes(self.opcodes[self.position])
            if opcode == Op.DONE:
                self.done = True
                return 0 # should be ignored
            elif opcode == Op.ADD:
                a = self.get_value(self.opcodes, self.position + 1, modes[0])
                b = self.get_value(self.opcodes, self.position + 2, modes[1])
                self.opcodes[self.opcodes[self.position + 3]] = a + b
                self.position += 4
            elif opcode == Op.MULTIPLY:
                a = self.get_value(self.opcodes, self.position + 1, modes[0])
                b = self.get_value(self.opcodes, self.position + 2, modes[1])
                self.opcodes[self.opcodes[self.position + 3]] = a * b
                self.position += 4
            elif opcode == Op.INPUT:
                if args:
                    self.opcodes[self.opcodes[self.position + 1]] = args.pop(0)
                    self.position += 2
                else:
                    return

            elif opcode == Op.OUTPUT:
                # print(self.opcodes[self.opcodes[self.position + 1]])
                result = self.opcodes[self.opcodes[self.position + 1]]
                self.position += 2
                return result
            elif opcode == Op.JUMP_IF_TRUE:
                if self.get_value(self.opcodes, self.position + 1, modes[0]) != 0:
                    self.position = self.get_value(self.opcodes, self.position + 2, modes[1])
                else:
                    self.position += 3
            elif opcode == Op.JUMP_IF_FALSE:
                if self.get_value(self.opcodes, self.position + 1, modes[0]) == 0:
                    self.position = self.get_value(self.opcodes, self.position + 2, modes[1])
                else:
                    self.position += 3
            elif opcode == Op.LESS_THAN:
                a = self.get_value(self.opcodes, self.position + 1, modes[0])
                b = self.get_value(self.opcodes, self.position + 2, modes[1])
                self.opcodes[self.opcodes[self.position + 3]] = 1 if a < b else 0
                self.position += 4
            elif opcode == Op.EQUAL:
                a = self.get_value(self.opcodes, self.position + 1, modes[0])
                b = self.get_value(self.opcodes, self.position + 2, modes[1])
                self.opcodes[self.opcodes[self.position + 3]] = 1 if a == b else 0
                self.position += 4
            else:
                print('unknown opcode')
                raise Exception('unknown opcode')

def second():
    # values = [3, 26, 1001, 26, -4, 26, 3, 27, 1002, 27, 2, 27, 1, 27, 26, 27, 4, 27, 1001, 28, -1, 28, 1005, 28, 6, 99, 0, 0, 5]
    # values = [3, 52, 1001, 52, -5, 52, 3, 53, 1, 52, 56, 54, 1007, 54, 5, 55, 1005, 55, 26, 1001, 54,  -5, 54, 1105, 1, 12, 1, 53, 54, 53, 1008, 54, 0, 55, 1001, 55, 1, 55, 2, 53, 55, 53, 4,  53, 1001, 56, -1, 56, 1005, 56, 6, 99, 0, 0, 0, 0, 10]

    # p = [9, 8, 7, 6, 5]
    # p = [9, 7, 8, 5, 6]

    max_thruster = (0, [])
    for p in permutations([5, 6, 7, 8, 9]):
        amplifiers = []
        for i in range(5):
            amplifiers.append(Amplifier(values.copy()))

        for a, i in zip(amplifiers, p):
            a.run_intcode(i)

        result = 0
        k = 0
        for a in cycle(amplifiers):
            if k > 100:
                break
            k += 1
            output = a.run_intcode(result)
            if a.done:
                break
            result = output
        if result > max_thruster[0]:
            max_thruster = (result, p)
    print(max_thruster)

with open('input.txt', 'r') as f:
    values = [int(i) for i in f.readline().rstrip().split(',')]

# print(values)

if __name__ == "__main__":
    second()

