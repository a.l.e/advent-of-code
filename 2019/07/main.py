from enum import IntEnum
from itertools import permutations

class Op(IntEnum):
    ADD = 1
    MULTIPLY = 2
    INPUT = 3
    OUTPUT = 4
    JUMP_IF_TRUE = 5
    JUMP_IF_FALSE = 6
    LESS_THAN = 7
    EQUAL = 8
    DONE = 99

class Amplifier:

    def __init__(self, opcodes):
        self.opcodes = opcodes
        self.position = 0
        self.done = False

    @staticmethod
    def get_opcode_modes(opcode_modes):
        modes = []
        opcode = opcode_modes % 100
        opcode_modes //= 100
        while opcode_modes > 0:
            modes.append(opcode_modes % 10)
            opcode_modes //= 10
        n_modes = 1
        if opcode in (Op.JUMP_IF_TRUE, Op.JUMP_IF_FALSE):
            n_modes = 2
        elif opcode in (Op.ADD, Op.MULTIPLY, Op.LESS_THAN, Op.EQUAL):
            n_modes = 3
        while len(modes) < n_modes:
            modes.append(0)
        return opcode, modes

    @staticmethod
    def get_value(opcodes, position, mode):
        if mode == 0:
            return opcodes[opcodes[position]]
        return opcodes[position]

    def run_intcode(self, *args):
        args = list(args)
        opcode = 0
        while opcode != Op.DONE:
            # print(opcodes)
            opcode, modes = Amplifier.get_opcode_modes(self.opcodes[self.position])
            if opcode == Op.DONE:
                self.done = True
                return result
            elif opcode == Op.ADD:
                a = self.get_value(self.opcodes, self.position + 1, modes[0])
                b = self.get_value(self.opcodes, self.position + 2, modes[1])
                self.opcodes[self.opcodes[self.position + 3]] = a + b
                self.position += 4
            elif opcode == Op.MULTIPLY:
                a = self.get_value(self.opcodes, self.position + 1, modes[0])
                b = self.get_value(self.opcodes, self.position + 2, modes[1])
                self.opcodes[self.opcodes[self.position + 3]] = a * b
                self.position += 4
            elif opcode == Op.INPUT:
                # self.opcodes[self.opcodes[self.position + 1]] = int(input('give me a number: '))
                self.opcodes[self.opcodes[self.position + 1]] = args.pop(0)
                self.position += 2
            elif opcode == Op.OUTPUT:
                # print(self.opcodes[self.opcodes[self.position + 1]])
                result.append(self.opcodes[self.opcodes[self.position + 1]])
                self.position += 2
            elif opcode == Op.JUMP_IF_TRUE:
                if self.get_value(self.opcodes, self.position + 1, modes[0]) != 0:
                    self.position = self.get_value(self.opcodes, self.position + 2, modes[1])
                else:
                    self.position += 3
            elif opcode == Op.JUMP_IF_FALSE:
                if self.get_value(self.opcodes, self.position + 1, modes[0]) == 0:
                    self.position = self.get_value(self.opcodes, self.position + 2, modes[1])
                else:
                    self.position += 3
            elif opcode == Op.LESS_THAN:
                a = self.get_value(self.opcodes, self.position + 1, modes[0])
                b = self.get_value(self.opcodes, self.position + 2, modes[1])
                self.opcodes[self.opcodes[self.position + 3]] = 1 if a < b else 0
                self.position += 4
            elif opcode == Op.EQUAL:
                a = self.get_value(self.opcodes, self.position + 1, modes[0])
                b = self.get_value(self.opcodes, self.position + 2, modes[1])
                self.opcodes[self.opcodes[self.position + 3]] = 1 if a == b else 0
                self.position += 4
            else:
                print('unknown opcode')
                raise Exception('unknown opcode')

def first():
    # values = [3, 15, 3, 16, 1002, 16, 10, 16, 1, 16, 15, 15, 4, 15, 99, 0, 0]
    # values = [3, 23, 3, 24, 1002, 24, 10, 24, 1002, 23, -1, 23, 101, 5, 23, 23, 1, 24, 23, 23, 4, 23, 99, 0, 0]
    max_thruster = (0, [])
    for p in permutations([0, 1, 2, 3, 4]):
        result = 0
        for i in p:
            result = run_intcode(values, i, result)[0]
        if result > max_thruster[0]:
            max_thruster = (result, p)
    print(max_thruster)

def second():
    values = [3, 26, 1001, 26, -4, 26, 3, 27, 1002, 27, 2, 27, 1, 27, 26, 27, 4, 27, 1001, 28, -1, 28, 1005, 28, 6, 99, 0, 0, 5]
    amplifiers = []
    for i in range(5):
        amplifiers.append(Amplifier(values))
    print(amplifiers)
    # result = 0
    # for p in permutations([5, 6, 6, 7, 8, 9]):
    #     result = run_intcode(values, 0, result)[0]

with open('input.txt', 'r') as f:
    values = [int(i) for i in f.readline().rstrip().split(',')]

# print(values)

if __name__ == "__main__":
    second()
