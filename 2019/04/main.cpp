#include <vector>
#include <iostream>
#include <algorithm>

using Digits = std::vector<int>;

Digits get_digits(int n)
{
    Digits result{};
    while (n > 0) {
        result.push_back(n % 10);
        n = n / 10;
    }
    std::reverse(result.begin(), result.end());
    return result;
}

bool check(Digits digits)
{
    if (!std::is_sorted(digits.begin(), digits.end())) {
        return false;
    }
    for (int d: digits) {
        if (std::count(digits.begin(), digits.end(), d) == 2) {
            return true;
        }
    }
    return false;
}
    
int main()
{
    int start = 172851;
    int end = 675869;
    int count = 0;
    for (int i = start; i < end; i++) {
        count += check(get_digits(i));
    }
    std::cout << count << std::endl;
}
