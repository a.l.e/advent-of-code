start = 172851
end = 675869

def check(pwd):
    pwd = str(pwd)
    if (sorted(pwd) != list(pwd)):
        return False
    for c in pwd:
        # if pwd.count(c) >= 2:
        if pwd.count(c) == 2:
            return True
    return False

print(sum(check(n) for n in range(start, end)))
