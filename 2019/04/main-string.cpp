#include <string>
#include <iostream>
#include <algorithm>

bool check(std::string digits)
{
    if (!std::is_sorted(digits.begin(), digits.end()))
        return false;
    for (int d: digits)
        if (std::count(digits.begin(), digits.end(), d) == 2)
            return true;
    return false;
}
    
int main()
{
    int start = 172851;
    int end = 675869;
    int count = 0;
    for (int i = start; i < end; i++)
        count += check(std::to_string(i));
    std::cout << count << std::endl;
}

/*
// more compact but less readable
int main()
{
    int count = 0;
    for (int i = 172851; i < 675869; i++) {
        auto digits = std::to_string(i);
        if (!std::is_sorted(digits.begin(), digits.end()))
            continue;
        count += std::any_of(digits.begin(), digits.end(), [&](auto d) {
            return std::count(digits.begin(), digits.end(), d) == 2; });
    }
    std::cout << count << std::endl;
}
*/
