#include <iostream>
#include <iterator>
#include <numeric>
#include <cmath>

#include <sstream>

#include <vector>
#include <queue>

#include <string>

std::ostream& operator<<(std::ostream& os, const std::vector<int>& v)
{
    return os << "{" << std::accumulate(v.begin() + 1, v.end(), std::to_string(*v.begin()),
        [](std::string a, int i) { return a + ", " + std::to_string(i); }) << "}";
}

using World = std::vector<std::vector<int>>;

struct Coord {
    int i, j;
};

std::string to_string(Coord p) {
    return "(" + std::to_string(p.i) + ", " + std::to_string(p.j) + ")";
}

std::ostream& operator<<(std::ostream& os, const std::vector<Coord>& v)
{
    return os << "{" << std::accumulate(v.begin() + 1, v.end(), to_string(*v.begin()),
        [](std::string a, Coord c) { return a + ", " + to_string(c); }) << "}";
}

std::vector<Coord> get_neighbors(World& world, Coord p) {
    std::vector<Coord> neighbors{};

    for (int i = std::max(0, p.i - 1); i <= std::min((int) world.size() - 1, p.i + 1); i++) {
        for (int j = std::max(0, p.j - 1); j <= std::min((int) world.at(0).size() - 1, p.j + 1); j++) {
            if (i == p.i && j == p.j) {
                continue;
            }
            neighbors.push_back({i, j});
        }
    }
    return neighbors;
}

int a(bool stop_at_flash = false)
{
    int flashes{0};

    World world{};
    {
        std::string line;
        while (std::getline(std::cin, line)) {
            std::vector<int> row{};
            for (char c: line) {
                row.push_back(c - '0');
            }
            world.push_back(row);
        }
    }
    for (auto row: world) {
        std::cout << row << std::endl;
    }

    for (int step = 0; stop_at_flash || step < 100; step++) {
        std::for_each(world.begin(), world.end(), [](std::vector<int>& r) {
            std::for_each(r.begin(), r.end(), [](int& v) {
                v++;
            });
        });

        for (int i = 0; i < world.size(); i++) {
            for (int j = 0; j < world.size(); j++) {
                std::queue<Coord> might_flash{{}};
                might_flash.push({i, j});
                while (!might_flash.empty()) {
                    const auto pos = might_flash.front();
                    might_flash.pop();
                    if (world.at(pos.i).at(pos.j) > 9) {
                        flashes++;
                        world.at(pos.i).at(pos.j) = 0;
                        for (const auto& n: get_neighbors(world, pos)) {
                            if (world.at(n.i).at(n.j) > 0) {
                                world.at(n.i).at(n.j)++;
                                might_flash.push(n);
                            }
                        }
                    }
                }
            }
        }

        if (all_of(world.begin(), world.end(), [](const auto& row) {
            return all_of(row.begin(), row.end(), [](int v) {
                return v == 0;
            });

        })) {
            return step + 1;
        }

    }
    
    return flashes;
}


int main()
{
    if (false) {
        std::cout << a() << std::endl;
    } else {
        std::cout << a(true) << std::endl;
    }
}
