#include <iostream>
#include <sstream>
#include <iterator>

#include <cmath>
#include <numeric>
#include <algorithm>

#include<limits>

#include <vector>
#include <deque>

#include <string>

std::ostream& operator<<(std::ostream& os, const std::vector<std::string>& v)
{
    return os << "{" << std::accumulate(v.begin() + (v.empty() ? 0 : 1), v.end(), v.empty() ? "" : *v.begin(),
        [](std::string a, std::string v) { return a + ", " + v; }) << "}";
}

struct Coord {
    int i, j;
};

bool operator==(const Coord& l, const Coord& r)
{
        return l.i == r.i && l.j == r.j;
}

std::ostream& operator<<(std::ostream& os, const Coord& c)
{
    return os << c.i << ", " << c.j;
}

using Cave = std::vector<std::vector<int>>;
using Path = std::deque<Coord>;

auto get_cave() {
    Cave cave;
    std::string line;
    for (std::string line; std::getline(std::cin, line); ) {
        std::vector<int> row{};
        for (char c: line) {
            row.push_back(c - '0');
        }
        cave.push_back(row);
    }
    return cave;
}

bool contains(const Path& path, const Coord& coord) {
    return std::find(path.begin(), path.end(), coord) != path.end();
}

auto get_neighbors(const Cave& cave, Coord c, const Path& path) {
    std::vector<Coord> neighbors{
        {c.i + 1, c.j},
        {c.i, c.j + 1},
        {c.i - 1, c.j},
        {c.i, c.j - 1},
    };
    int n = cave.size();
    int m = cave.at(0).size();
    // std::cout << "size " << neighbors.size() << std::endl;
    neighbors.erase(std::remove_if(neighbors.begin(), neighbors.end(), [path, m, n](const auto& v) {
        return v.i < 0 || v.j < 0 || v.i > n - 1 || v.j > m - 1 || contains(path, v);
    }), neighbors.end());
    // std::cout << "size 2 " << neighbors.size() << std::endl;
    return neighbors;
}


void go(const Cave& cave, const Coord item, Path& path, int risk, int& min) {
    int n = cave.size();
    int m = cave.at(0).size();

    // std::cout << item << std::endl;

    if (item != Coord{0, 0}) {
        // ignore the start element
        risk += cave.at(item.i).at(item.j);
    }
    path.push_back(item);

    if (risk >= min) {
        path.pop_back();
        return;
    }

    if (item.i == n - 1 && item.j == m - 1) {
        min = risk;
        std::cout << min << std::endl;
        path.pop_back();
        return;
    }

    for (const auto& p: get_neighbors(cave, item, path)) {
        go(cave, p, path, risk, min);
    }
    path.pop_back();
}

int a() {
    auto cave = get_cave();
    Path path{};
    int min = 0; // initialize with the diagonal (shortest path)
    // use the diagonal (except the start point) as the first min
    Coord pos{0, 0};
    while (pos.i < cave.size() - 1 && pos.j < cave.at(0).size() - 1) {
        pos.i++;
        min += cave.at(pos.i).at(pos.j);
        pos.j++;
        min += cave.at(pos.i).at(pos.j);
    }
    go(cave, {0, 0}, path, 0, min);
    return min; // 1104 is too high
}

int b() {
    return 0;
}

int main()
{
    if (true) {
        std::cout << a() << std::endl;
    } else {
        std::cout << b() << std::endl;
    }
}
