#include <iostream>
#include <vector>
#include <unordered_set>
#include <string>
#include <bitset>
#include <numeric>

#include <cassert>

struct Coord {
    int i, j;
};

bool operator==(const Coord& l, const Coord& r)
{
        return l.i == r.i && l.j == r.j;
}

bool operator<(const Coord& l, const Coord& r)
{
        return l.i < r.i || (l.i == r.i && l.j < r.j);
}

std::ostream& operator<<(std::ostream& os, const Coord& c)
{
    return os << c.i << ", " << c.j;
}

inline size_t hash_combine( size_t lhs, size_t rhs ) {
    lhs^= rhs + 0x9e3779b9 + (lhs << 6) + (lhs >> 2);
    return lhs;
}

struct CoordHash {
    size_t operator()(const Coord& c) const {
        return hash_combine(c.i, c.j);
    }
};

using Filter = std::string;
using Image = std::unordered_set<Coord, CoordHash>;

void print(const Image& image) {
    Coord min = *image.begin();
    Coord max = *image.begin();
    for (const auto& pixel: image) {
        min.i = std::min(min.i, pixel.i);
        min.j = std::min(min.j, pixel.j);
        max.i = std::max(max.i, pixel.i);
        max.j = std::max(max.j, pixel.j);
    }
    for (int i = min.i; i < max.i + 1; i++) {
        for (int j = min.j; j < max.j + 1; j++) {
            std::cout << (image.contains({i, j}) ? '#' : '.');
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

Filter get_filter() {
    Filter filter{};
    std::string line;
    int i = 0;
    while (true) {
        std::getline(std::cin, line);
        if (line == "") {
            break;
        }
        filter += line;
    }
    return filter;
}

Image get_image() {
    Image image;
    std::string line;
    int i = 0;
    while (std::getline(std::cin, line)) {
        for (int j = 0; j < line.size(); j++) {
            if (line.at(j) == '#') {
                image.insert({i, j});
            }
        }
        i++;
    }
    return image;
}

using Neighborhood = std::vector<Coord>;

/**
 * Get all pixels around the current one, in a specific distance.
 * The current pixel is included.
 * The image being of infinite size, there no boundaries are checked.
 */
Neighborhood get_neighborhood(const Coord& pixel, int distance) {
    Neighborhood neighborhood{};
    for (int i = pixel.i - distance; i <= pixel.i + distance; i++) {
        for (int j = pixel.j - distance; j <= pixel.j + distance; j++) {
            neighborhood.push_back({i, j});
        }
    }
    return neighborhood;
}

int get_filter_position(const Image& image, const Coord& pixel, bool inverted) {
    const int N_BITS = 9;
    std::bitset<N_BITS> value{};
    int i = 0;
    for (const auto& neighbor: get_neighborhood(pixel, 1)) {
        bool bit = image.contains(neighbor);
        if (inverted) {
            bit = !bit;
        }
        value.set(N_BITS - 1 - i, bit);
        i++;
    }
    // std::cout << value << std::endl;
    return value.to_ulong();
}

Image get_next_image(const Image& image, const Filter& filter, int generation) {
    Image next{};
    Image neighbors{};
    // find all unique neighbors
    for (const auto& pixel: image) {
        for (const auto& neighbor: get_neighborhood(pixel, 2)) {
            neighbors.insert(neighbor);
        }
    }
    bool inverted = filter.at(0) == '#' && (generation % 2) == 0;
    // bool inverted = filter.at(0) == '#';
    for (const auto& pixel: neighbors) {
        int filter_position = get_filter_position(image, pixel, inverted);
        // if (filter.at(filter_position) == (inverted ? '.' : '#')) {
        if (filter.at(filter_position) == '#') {
            next.insert(pixel);
        }
    }
    return next;
}

int a() {
    auto filter = get_filter();
    auto image = get_image();
    // print(image);
    // assert(get_filter_position(image, {2, 2}) == 34);
    for (int generation = 0; generation < 2; generation++) {
        image = get_next_image(image, filter, generation);
        // print(image);
    }
    return image.size(); // 5786 is too high // it's 5395
}

int b() {
    return 0;
}

int main() {
    if (true) {
        std::cout << a() << std::endl;
    } else {
        std::cout << b() << std::endl;
    }
}
