#include <iostream>
#include <sstream>
#include <iterator>

#include <numeric>

#include <vector>
#include <algorithm>

#include <optional>

using Numbers = std::vector<int>;

using BoardCell = std::pair<int, bool>;
using BoardRow = std::vector<BoardCell>;
struct Board {
    bool won{false};
    std::vector<BoardRow> board{};
    auto begin() { return board.begin(); }
    auto end() { return board.end(); }
    void push_back(BoardRow& v) { board.push_back(v); }
};
using Boards = std::vector<Board>;

std::ostream& operator<<(std::ostream& os, const std::vector<int>& v)
{
    return os << "{" << std::accumulate(v.begin() + 1, v.end(), std::to_string(*v.begin()),
        [](std::string a, int i) { return a + ", " + std::to_string(i); }) << "}";
}

Numbers get_numbers() {
    Numbers numbers{};

    std::string line;
    std::getline(std::cin, line);

    std::istringstream iss(line);
    std::string token;
    while(std::getline(iss, token, ',')) {
        numbers.push_back(std::stoi(token));
    }

    return numbers;
}

Boards get_boards() {
    Boards boards{};

    std::string line;
    while (std::getline(std::cin, line)) {
        Board board{};
        for (int i = 0; i < 5; i++) {
            std::getline(std::cin, line);
            // std::cout << line << std::endl;
            std::istringstream iss{line};
            std::vector<int> numbers{ std::istream_iterator<int>(iss), {} };
            // std::cout << numbers << std::endl;
            BoardRow row{};
            for (auto v: numbers) {
                row.push_back(BoardCell{v, false});
            }
            board.push_back(row);
        }
        boards.push_back(board);
    }
    return boards;
}

void set_drawn_number(Board& board, int number) {
    for (auto& row: board) {
        for (auto& cell: row) {
            if (cell.first == number) {
                cell.second = true;
            }
        }
    }
}

bool is_winning(Board board) {
    // return true if any row has all trues
    for (auto& row: board) {
        if (std::all_of(row.begin(), row.end(), [](auto cell) { return cell.second; })) {
            return true;
        }
    }
    // return true if any column has all trues
    for (int i = 0; i < 5; i++) {
        if (std::all_of(board.begin(), board.end(), [i](auto row) { return row.at(i).second; })) {
            return true;
        }
    }
    return false;
}

int count_points(Board board, int number) {
    return number * std::accumulate(board.begin(), board.end(), 0, [](int a, auto& row) {
        return a + std::accumulate(row.begin(), row.end(), 0, [](int a, auto& cell) {
            return a + (cell.second ? 0 : cell.first);
        });
    });
}

int a()
{

    Numbers numbers = get_numbers();
    Boards boards = get_boards();

    std::optional<Board> winner{std::nullopt};
    std::optional<int> winning_number{std::nullopt};
    for (int number: numbers) {
        for (auto& board: boards) {
            set_drawn_number(board, number);
        }
        for (auto& board: boards) {
            if (is_winning(board)) {
                winner = board;
                winning_number = number;
                break;
            }
        }
        if (winner) {
            std::cout << "winner at " << number << std::endl;
            break;
        }
    }

    return count_points(*winner, *winning_number);
}

int b()
{
    Numbers numbers = get_numbers();
    Boards boards = get_boards();

    std::optional<Board> winner{std::nullopt};
    std::optional<int> winning_number{std::nullopt};
    for (int number: numbers) {
        for (auto& board: boards) {
            if (board.won) {
                continue;
            }
            set_drawn_number(board, number);
        }
        for (auto& board: boards) {
            if (board.won) {
                continue;
            }
            if (is_winning(board)) {
                board.won = true;
                winner = board;
                winning_number = number;
            }
        }
    }
    if (winner) {
        std::cout << "winner at " << *winning_number << std::endl;
    }

    return count_points(*winner, *winning_number);
}

int main()
{
    if (false) {
        std::cout << a() << std::endl;
    } else {
        std::cout << b() << std::endl;
    }
}
