#include <iostream>
#include <vector>
#include <variant>
#include <tuple>

struct Number;
struct Number {
    std::vector<std::variant<Number, int>> number;
};

Number get_reduced(Number number) {
    return number;
}

Number get_homework() {
    Number homework{};
    return homework;
}

int get_number_length(const std::string& line) {
    int l = 0;
    int level = 0;
    for (int i = 0; i < line.size(); i++) {
        char c = line.at(0);
        if (c == '[') {
            level++;
        } else if (level == 0) {
            return i;
        } else if (c == ']') {
            level--;
        }
    }
    return line.size() - 1;
}

Number get_line(const std::string& line) {
    std::cout << line << std::endl;
    auto inner_line = line.substr(1, line.size() - 2);
    if (inner_line.at(0) == '[') {
        int l = get_number_length(inner_line);
        std::cout << "length: " << l << std::endl;
    }
    std::cout << " " << inner_line << std::endl;
    return {};
}

int a() {
    // auto a = get_reduced({{{1}, {2}}});
    // std::cout << std::get<int>(a.number.at(0)) << std::endl;
    // std::cout << std::get<int>(a.number.at(1)) << std::endl;
    auto b = get_line("[[[[[9,8],1],2],3],4]");
    return 0;
}

int b() {
    return 0;
}

int main() {
    if (true) {
        std::cout << a() << std::endl;
    } else {
        std::cout << b() << std::endl;
    }
}
