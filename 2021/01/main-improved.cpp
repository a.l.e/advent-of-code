#include <iostream>
#include <iterator>
#include <numeric>
#include <algorithm>

#include<limits>

#include <vector>
#include <deque>

/**
 * Improvement: we don't need to keep track of the sum, just compare the item being removed with
 * the one being added
 */

int a()
{
    int prec = std::numeric_limits<int>::max();
    return std::count_if(std::istream_iterator<int>{std::cin}, {}, [&prec](int v) {
        bool r = v > prec;
        prec = v;
        return r;
    });
}

int b()
{
    std::deque<int> window{};
    return std::count_if(std::istream_iterator<int>{std::cin}, {}, [&](int v) {
        window.push_back(v);
        if (window.size() > 2) {
            int old_value = window.front();
            window.pop_front();
            return v > old_value;
        }
        return false;
    });
}

int main()
{
    if (false) {
        std::cout << a() << std::endl;
    } else {
        std::cout << b() << std::endl;
    }
}
