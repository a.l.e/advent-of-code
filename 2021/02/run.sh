#!/usr/bin/env bash
out="${PWD##*/}"
g++ -std=c++20 -o $out main.cpp
cat test.txt | ./$out
cat input.txt | ./$out
