#include <iostream>
#include <vector>
#include <variant>
#include <tuple>

int a() {
    return 0;
}

int b() {
    return 0;
}

int main() {
    if (true) {
        std::cout << a() << std::endl;
    } else {
        std::cout << b() << std::endl;
    }
}
