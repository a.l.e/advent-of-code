#include <iostream>
#include <bitset>

#include <vector>
#include <unordered_map>

std::string hex_to_bin(std::string hex) {
    std::string bin;
    std::unordered_map<char, std::string> map{
        {'0',"0000"}, {'1',"0001"}, {'2',"0010"}, {'3',"0011"},
        {'4',"0100"}, {'5',"0101"}, {'6',"0110"}, {'7',"0111"},
        {'8',"1000"}, {'9',"1001"}, {'A',"1010"}, {'B',"1011"},
        {'C',"1100"}, {'D',"1101"}, {'E',"1110"}, {'F',"1111"},
    };
    for (const char c: hex) {
        bin += map[c];
    }
    return bin;
}

// cc by Michael Goldshteyn, https://stackoverflow.com/a/18132260/5239250
// bitset needs a static length in bits... use this instead when the number of bits is variable
int bin_to_dec(std::string bin) {
    int num = 0;
    for (char c : bin)
          num = (num << 1) |  // Shift the current set of bits to the left one bit
                  (c - '0');    // Add in the current bit via a bitwise-or
    return num;
}

struct Packet {
    int version;
    int id;
    long long int value;
    std::vector<Packet> packets;
};

using Packets = std::vector<Packet>;

std::ostream& operator<<(std::ostream& os, const Packet& p)
{
    return os << "{version: " << p.version << ", id: " << p.id << ", value: " << p.value << ", packets: " << p.packets.size() << "}";
}

auto get_litteral_value(const std::string& bin, int i = 0) {
    bool next = true;
    std::string value;
    while (next) {
        next = bin.at(i) == '1';
        // std::cout << "next: " << next << std::endl;
        i += 1;
        value += bin.substr(i, 4);
        i += 4;
    }
    return std::make_tuple(bin_to_dec(value), i);
}

std::tuple<Packet, int> get_packet(const std::string& bin, int i);

auto get_packets(const std::string& bin) {
    std::vector<Packet> packets{};
    int i = 0;
    while (i < bin.size()) {
        Packet packet{};
        std::tie(packet, i) = get_packet(bin, i);
        packets.push_back(packet);
    }
    return std::make_tuple(packets, i);
}



std::tuple<Packet, int> get_packet(const std::string& bin, int i = 0) {
    Packet packet{};
    packet.version = std::bitset<3>{bin.substr(i, 3)}.to_ulong();
    i += 3;
    packet.id = std::bitset<3>{bin.substr(i, 3)}.to_ulong();
    i += 3;
    if (packet.id == 4) {
        std::tie(packet.value, i) = get_litteral_value(bin, i);
    } else {
        bool length_in_bits = bin.at(i) == '0';
        i += 1;
        if (length_in_bits) {
            // std::cout << bin.substr(i, 15) << std::endl;
            int length = std::bitset<15>{bin.substr(i, 15)}.to_ulong();
            i += 15;
            // std::cout << "length: " << length << std::endl;
            std::tie(packet.packets, std::ignore) = get_packets(bin.substr(i, length));
            i += length;

            // for (const auto& p: packet.packets) {
            //     std::cout << p << std::endl;
            // }
        } else {
            // std::cout << bin.substr(i, 11) << std::endl;
            int length = std::bitset<11>{bin.substr(i, 11)}.to_ulong();
            i += 11;
            // std::cout << "length: " << length << std::endl;
            for (int j = 0; j < length; j++) {
                Packet p{};
                std::tie(p, i) = get_packet(bin, i);
                // std::cout << p << std::endl;
                packet.packets.push_back(p);
            }

        }
        if (packet.id == 0) { // sum
            for (const auto& p: packet.packets) {
                packet.value += p.value;
            }
        } else if (packet.id == 1) { // mult
            packet.value = 1;
            for (const auto& p: packet.packets) {
                packet.value *= p.value;
            }
        } else if (packet.id == 2) { // min
            packet.value = std::min_element(packet.packets.begin(), packet.packets.end(), [](const auto& a, const auto& b) {return a.value < b.value;})->value;
        } else if (packet.id == 3) { // max
            packet.value = std::max_element(packet.packets.begin(), packet.packets.end(), [](const auto& a, const auto& b) {return a.value < b.value;})->value;
        } else if (packet.id == 5) { // gt
            packet.value = packet.packets.at(0).value > packet.packets.at(1).value;
        } else if (packet.id == 6) { // lt
            packet.value = packet.packets.at(0).value < packet.packets.at(1).value;
        } else if (packet.id == 7) { // eq
            packet.value = packet.packets.at(0).value == packet.packets.at(1).value;
        }
    }
    return std::make_tuple(packet, i);
}

int get_sum_versions(Packet packet) {
    int sum = packet.version;
    for (const auto& p: packet.packets) {
        sum += get_sum_versions(p);
    }
    return sum;
}

int a(const std::string& hex) {
    auto bin = hex_to_bin(hex);
    // std::cout << bin << std::endl;
    Packet packet{};
    std::tie(packet, std::ignore) = get_packet(bin);
    // std::cout << packet << std::endl;
    return get_sum_versions(packet);
}

long long int b(const std::string& hex) {
    auto bin = hex_to_bin(hex);
    // std::cout << bin << std::endl;
    Packet packet{};
    std::tie(packet, std::ignore) = get_packet(bin);
    // std::cout << packet << std::endl;
    return packet.value;
}

int main()
{
    if (false) {
        // std::cout << a("D2FE28") << std::endl;
        // std::cout << a("38006F45291200") << std::endl;
        // std::cout << a("EE00D40C823060") << std::endl;
        // std::cout << a("8A004A801A8002F478") << std::endl;
        // std::cout << a("620080001611562C8802118E34") << std::endl;
        // std::cout << a("C0015000016115A2E0802F182340") << std::endl;
        std::string input;
        std::cin >> input;
        // std::cout << input << std::endl;
        std::cout << a(input) << std::endl;
    } else {
        // std::cout << b("C200B40A82") << std::endl;
        // std::cout << b("04005AC33890") << std::endl;
        // std::cout << b("880086C3E88112") << std::endl;
        // std::cout << b("CE00C43D881120") << std::endl;
        // std::cout << b("D8005AC2A8F0") << std::endl;
        // std::cout << b("F600BC2D8F") << std::endl;
        // std::cout << b("9C005AC2F8F0") << std::endl;
        // std::cout << b("9C0141080250320F1802104A08") << std::endl;
        std::string input;
        std::cin >> input;
        std::cout << b(input) << std::endl;
        // 760417912 is too low
        // 206918848120 is too low
        // 206918848170 long long int
        // 208082311228 unsigned long long int

    }
}
