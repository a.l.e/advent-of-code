#!/usr/bin/env bash
out="${PWD##*/}"
g++ -std=c++20 -o $out main.cpp
./$out < input.txt
# g++ -std=c++20 -o $out -g main.cpp
# gdb -ex 'set args < input.txt' ./$out

# g++ -std=c++20 -o $out-xxx main-state-machine-copied.cpp
