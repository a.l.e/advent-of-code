def a():
    with open('input.txt', 'r') as f:
        input = f.read()

    counter = 0
    for line in input.strip().split('\n'):
        for digit in line.split(" | ")[1].split(' '):
            if len(digit) in [2, 3, 4, 7]:
                counter += 1
    print(counter)

# read the left and right part of the line into words, with their chars sorted
def get_leds(line):
    left, right = line.split(' | ')
    return [[''.join(sorted(x)) for x in left.split(' ')], [''.join(sorted(x)) for x in right.split(' ')]]

def get_by_length(leds, length):
    result = [x for x in leds if len(x) == length]
    return result

def b():
    counter = 0;

    with open('test.txt', 'r') as f:
        input = f.read()

    for line in input.strip().split('\n'):
        patterns, output = get_leds(line)

        one = get_by_length(patterns, 2)[0]
        seven = get_by_length(patterns, 3)[0]
        four = get_by_length(patterns, 4)[0]
        eight = get_by_length(patterns, 7)[0]

        for pattern in get_by_length(patterns, 5):
            if len(set(one).intersection(pattern)) == 2:
                three = pattern
            elif len(set(four).intersection(pattern)) == 2:
                two = pattern
            else:
                five = pattern

        for pattern in get_by_length(patterns, 6):
            if len(set(five).intersection(pattern)) != len(five):
                zero = pattern
            elif len(set(one).intersection(pattern)) == 1:
                six = pattern
            else:
                nine = pattern

        numbers = {
            zero: '0', one: '1', two: '2',
            three: '3', four: '4', five: '5',
            six: '6', seven: '7', eight: '8',
            nine: '9'
        }

        counter += int(''.join(numbers[led] for led in output))

    print(counter)

a()
b()
