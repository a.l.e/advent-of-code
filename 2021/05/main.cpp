#include <iostream>
#include <sstream>
#include <iterator>

#include <unordered_map>
#include <algorithm>
#include <numeric>

struct Line
{
    int x1, y1, x2, y2;
};

std::istream& operator>>(std::istream& is, Line& line)
{
    char c;
    std::string s;
    is >> line.x1 >> c >> line.y1 >> s >> line.x2 >> c >> line.y2;
    return is;
}

std::ostream& operator<<(std::ostream& os, const Line& l)
{
    return os << l.x1 << "," << l.y1 << " " << l.x2 << "," << l.y2;
}

inline size_t hash_combine( size_t lhs, size_t rhs ) {
    lhs^= rhs + 0x9e3779b9 + (lhs << 6) + (lhs >> 2);
    return lhs;
}

struct IntPairHash {
    size_t operator()(const std::pair<int, int>& p) const {
        return hash_combine(p.first, p.second);
    }
};

using Map = std::unordered_map<std::pair<int, int>, int, IntPairHash>;

void display(Map& map) {
    for (int y = 0; y < 10; y++) {
        for (int x = 0; x < 10; x++) {
            std::cout << map[{x, y}] << " ";
        }
        std::cout << std::endl;
    }
}

std::vector<int> get_range(int a, int b) {
    std::vector<int> range(std::abs(a - b) + 1);
    std::iota(range.begin(), range.end(), std::min(a, b));
    if (b > a) {
        std::reverse(range.begin(), range.end());
    }
    return range;
};

// https://stackoverflow.com/a/18771618/5239250 cc by sa aaronman
template <typename Iterator>
    void advance_all (Iterator & iterator) {
        ++iterator;
    }
template <typename Iterator, typename ... Iterators>
    void advance_all (Iterator & iterator, Iterators& ... iterators) {
        ++iterator;
        advance_all(iterators...);
    } 
template <typename Function, typename Iterator, typename ... Iterators>
    Function zip (Function func, Iterator begin, 
            Iterator end, 
            Iterators ... iterators)
    {
        for(;begin != end; ++begin, advance_all(iterators...))
            func(*begin, *(iterators)... );
        //could also make this a tuple
        return func;
    }

int a()
{
    Map ventsMap{};
    std::for_each(std::istream_iterator<Line>{std::cin}, {},
        [&ventsMap](auto l) {
            // std::cout << l << std::endl;
            if (l.x1 == l.x2) {
                auto y = get_range(l.y1, l.y2);
                std::for_each(y.begin(), y.end(), [&ventsMap, l](int y) {
                    ventsMap[{l.x1, y}]++;
                });
            } else if (l.y1 == l.y2) {
                auto x = get_range(l.x1, l.x2);
                std::for_each(x.begin(), x.end(), [&ventsMap, l](int x) {
                    ventsMap[{x, l.y1}]++;
                });
            }
    });
    // display(ventsMap);
    return std::count_if(ventsMap.begin(), ventsMap.end(), [](const Map::value_type& p) {
        return p.second >= 2;
    });
}

int b()
{
    Map ventsMap{};
    std::for_each(std::istream_iterator<Line>{std::cin}, {},
        [&ventsMap](auto l) {
            // std::cout << l << std::endl;
            if (l.x1 == l.x2) {
                auto y = get_range(l.y1, l.y2);
                std::for_each(y.begin(), y.end(), [&ventsMap, l](int y) {
                    ventsMap[{l.x1, y}]++;
                });
            } else if (l.y1 == l.y2) {
                auto x = get_range(l.x1, l.x2);
                std::for_each(x.begin(), x.end(), [&ventsMap, l](int x) {
                    ventsMap[{x, l.y1}]++;
                });
            } else { // diagonal
                auto x = get_range(l.x1, l.x2);
                auto y = get_range(l.y1, l.y2);
                zip([&ventsMap](int x, int y) {
                    // std::cout << "x y " << x << ", " << y <<  std::endl;
                    ventsMap[{x, y}]++;
                }, x.begin(), x.end(), y.begin());
            }
    });
    // display(ventsMap);
    return std::count_if(ventsMap.begin(), ventsMap.end(), [](const Map::value_type& p) {
        return p.second >= 2;
    });
}

int main()
{
    if (false) {
        std::cout << a() << std::endl;
    } else {
        std::cout << b() << std::endl;
    }
}
