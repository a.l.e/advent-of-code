#!/usr/bin/env bash
out="${PWD##*/}"
g++ -std=c++20 -O2 -o $out main.cpp
./$out < test.txt
./$out < input.txt
