#include <iostream>
#include <sstream>
#include <iterator>

#include <cmath>
#include <numeric>
#include <algorithm>

#include <vector>
#include <unordered_map>


#include <string>

std::ostream& operator<<(std::ostream& os, const std::vector<std::string>& v)
{
    return os << "{" << std::accumulate(v.begin() + (v.empty() ? 0 : 1), v.end(), v.empty() ? "" : *v.begin(),
        [](std::string a, std::string v) { return a + ", " + v; }) << "}";
}

using Rules = std::unordered_map<std::string, char>;
using Counter = std::unordered_map<char, uint64_t>;

auto get_instructions() {
    std::string polymer{};
    Rules rules{};

    std::getline(std::cin, polymer);

    std::string line;
    std::getline(std::cin, line);

    std::string left, separator;
    char right;
    while (std::getline(std::cin, line)) {
        std::istringstream iss(line);
        iss >> left >> separator >> right;
        rules[left] = right;
    }

    return std::make_tuple(polymer, rules);
}

uint64_t a()
{
    auto [polymer, rules] = get_instructions();
    for (int i = 0; i < 10; i++) {
        std::string new_polymer{};
        for (long long int j = 0; j < polymer.size() - 1; j++) {
            std::string key = std::string() + polymer.at(j) + polymer.at(j + 1);
            // std::cout << key << " > " << rules[key] << std::endl;
            new_polymer += std::string() + polymer.at(j) + rules[key];
        }
        polymer = new_polymer + polymer.at(polymer.size() - 1);
        // std::cout << "new: " << polymer << std::endl;
        std::cout << i << ": " << polymer.size() << std::endl;
    }
    Counter counter{};
    for (char c: polymer) {
        counter[c]++;
    }
    const auto [min, max] = std::minmax_element(counter.begin(), counter.end(), [](const auto& a, const auto&b) {
        return a.second < b.second;
        });
    std::cout << max->second << " - " << min->second << std::endl;
    return max->second - min->second;
}

// NN[40] = NCN = NC[39] + CN[39]
// NC -> 1: {
// NCN -> NC CN

using Tree = std::unordered_map<std::string, std::vector<Counter>>;

uint64_t b()
{
    auto [polymer, rules] = get_instructions();

    Tree tree{};

    int i = 0;
    for (const auto& rule: rules) {
        tree[rule.first].push_back({});
        tree[rule.first][i][rule.second]++;
        tree[rule.first][i][rule.first[0]]++;
        tree[rule.first][i][rule.first[1]]++;
    }

    // CN[0] = CCN = {C:2, N:1}
    // NC[0] = NBC = {C:1, N:1, B:1}
    // CC[0] = CNC = {C:2, N:1}
    // CC[1] = CN NC = CN[0] + NC[0] = {C:3, N:2, B1}
    // 2 > CC: {B: 4, C: 5, N: 3, }
    // NB[0] = NBB = {B:2, N:1}
    // BC[0] = BBC = {B:2, C:1}
    // CN[1] = CC[0] CN[0] = {C:2, N1} + {C:2, N:1}
    // NC[1] = NB[0] BC[0] = {B:2, N:1} + {B:2, C:1}
    // CC[2] = CN[1] NC[1] = CC[0] + CN[0] + NB[0] + BC[0] = {C:5, N:3, B:4}
    // CC CNC CCNBC CNC
    //

    for (int i = 1; i < 40; i++) {
        for (const auto& rule: rules) {
            tree[rule.first].push_back({});
            std::vector<std::string> tokens = {std::string() + rule.first[0] + rule.second, std::string() + rule.second + rule.first[1]};
            // std::cout << rule.first << ": " << tokens << std::endl;
            for (const auto& token: tokens) {
                for (const auto& letter: tree[token][i - 1]) {
                    tree[rule.first][i][letter.first] += letter.second;
                }
                // remove the second letter of the token, since it's also the first of the next token
                tree[rule.first][i][token[1]]--;
            }
            // readd the last second letter, since the reis no next
            tree[rule.first][i][tokens.back()[1]]++;
        }
    }
    // i = 39;
    // std::cout << "result " << i << std::endl;
    // for (const auto& rule: tree) {
    //     std::cout << rule.first << ": {";
    //     for (const auto& letter: rule.second.at(i)) {
    //         std::cout << letter.first << ": " << letter.second << ", ";
    //     }
    //     std::cout << "}" << std::endl;;
    // }

    Counter counter;
    for (int i = 0; i < polymer.size() - 1; i++) {
        std::string token = std::string() + polymer.at(i) + polymer.at(i + 1);
        // std::cout << "token: " << token << std::endl;
        // for (const auto letter: tree[token].at(1)) {
        for (const auto letter: tree[token].back()) {
            // std::cout << "-> " << letter.first << ": " << letter.second << std::endl;
            counter[letter.first] += letter.second;
        }
    }

    // std::cout << "result" << std::endl;
    // for (const auto& letter: counter) {
    //     std::cout << letter.first << ": " << letter.second << std::endl;
    // }

    const auto [min, max] = std::minmax_element(counter.begin(), counter.end(), [](const auto& a, const auto&b) {
        return a.second < b.second;
    });
    std::cout << max->second << " - " << min->second << std::endl;
    std::cout << polymer << std::endl;
    return max->second - min->second; // 12271437788527 is too low
    // by inserting my data in
    // https://paiv.github.io/aoc2021/day/14/
    // i got that my values are wrong by 3: the max is too big by one and the min too big by four.
    // 12464638059775 - 193200271245 = 12271437788530
    // i guess that i give up...
    // 18,446,744,073,709,551,615
    // 12,271,437,788,527
    // 9,223,372,036,854,775,807
}

int main()
{
    if (false) {
        std::cout << a() << std::endl;
    } else {
        std::cout << b() << std::endl;
    }
}
