# advent of code 2021

It will be in c++

My latest AOC in c++ was 2018.

```sh
g++ -std=c++20 -o 01 main.cpp
cat input.txt | ./01
./01 < input.txt
```

## Day 1: Sonar Sweep

Read a file with one int per line and count the number of sliding windows that match a condition.

- `std::istream_iterator` reads each line of the file.
- `std::count_if`.
- `std::deque`.

Ideas:

- It's not necessary to keep the sum, it's enough to compare the element that is removed from the window with the one being added to tell if the window gets bigger or smaller (the problem is easier to solve with a `for int i`)

## Day 2: Dive!

Read a file with two fields (string, int) per line into a struct and move around in a 2D space.

- `std::istream_iterator` reads each line of the file into a `struct`.
- `std::accumulate.
- `std::cout` for a custom `struct`.

## Day 3: Binary Diagnostic

Read binary numbers from a file. Filter the rows by the binary values.

- the `n_bits` constant needs to be set to 5 for the test data.
- `bitset`
- `remove_if` and `erase`
- failed try to output a `std::array` to `cout`

      ```cpp
      template <T, unsigned int N>
      std::ostream& operator<<(std::ostream& os, const T (&a)[N])
      {
          return os << "{" << std::accumulate(a.begin() + 1, a.end(), std::to_string(*a.begin()),
              [](std::string a, int i) { return a + ", " + std::to_string(i); }) << "}";
      }
      ```

## Day 4: Giant Squid

Read a file with comma separated values on the first line and 5 x 5 matrices on the other lines.  
Play bingo by checking the drawn numbers in the boards and checking for winning boards.

- print (debug) the content of a `vector<int>`
- `getline` splitting with a custom separator.
- `using` for simple custom data types.

## Day 5: Hydrothermal Venture

Read a file with a list of lines (coordinates from and to), put the on a map and count the overlaps.

- read a complex line into a `struct` without using a regex.
- use coordinates as key for a map
- get an iterator on a map (and `count_if`)
- implement a _python-like_ `range` function using `iota`.
- use a `zip` function in a `for_each`

## Day 6: Lanternfish

Fish that give birth to new fishes after n days... part 2 cannot be brute forced but solution the solution is not that hard...

I've used a map, but cycling in an array is almost simpler...

## Day 7: The Treachery of Whales

There is a mathematical solution for each part... but brute forcing also works very well.

## Day 8: Seven Segment Search

Find the real encoding for a row of LED displays.

- split a line by | and spaces.
- intersection and difference between strings.

## Day 9: Smoke Basin

In a grid of numbers, compare cells to the neighbors and find areas in the grid.

- use a queue for exploring the surroundings.

## Day 10: Syntax Scoring

Parse code composed by opening and closing braces

- `cout` empty `std::vector`
- stack and maps

## Day 11: Dumbo Octopus

Octopuses on a 2D int grid flash when they get over 9 level of energy.

- Read a _packed> 2D vector of ints (no space between values).
- Neighborgs in all 8 directions.
- Recursively explore the neighborhood.
§
## Day 12: Passage Pathing

A graph of ways between caves: find all paths that match specific criteria.

- `deque` is a bit like a `stack` but can be traversed...
- split a string by a comma.

## Day 13: Transparent Origami

Take a transparent map with dots and fold until an ASCII art text appears.

- compare and hash `Coord` to insert it in a `unordered_set`.
- read from `cin` into a string and then split it in int variables by using `>>` (only works with a separator of a different type)

## Day 14: Extended Polymerization

Create a polymer by following an insertion rule for adding new molecules inbetween two existing ones.

Part 1 can be solved by creating the final string and counting the occurrences.

For part 2 I went the other way round and calculated step wise the number of letter for each pair, after each step.

- `std::cout << std::string`
- fill and print a `std::unordered_map`
