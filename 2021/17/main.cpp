#include <iostream>

#include <numeric>

#include <vector>

#include <chrono>
#include <thread>


struct Coord {
    int x, y;
    Coord operator+=(const Coord rhs) const {
        x + rhs.x;
        y + rhs.y;
        return *this;
    }
};

std::ostream& operator<<(std::ostream& os, const Coord& c)
{
    return os << "{" << c.x << ", " << c.y << "}";
}

struct Area {
    Coord top, bottom;
    bool contains(Coord c) const {
        return c.x >= top.x &&
               c.x <= bottom.x &&
               c.y <= top.y &&
               c.y >= bottom.y;
    }
};


struct Frame {
    Coord position;
};

using Frames = std::vector<Frame>;

auto get_next(Coord position, Coord velocity) {
    position.x += velocity.x;
    position.y += velocity.y;
    if (velocity.x > 0) {
        velocity.x--;
    } else if (velocity.x < 0) {
        velocity.x++;
    }
    velocity.y--;
    return std::make_tuple(position, velocity);
}

void display(const Frames& frames, const Area& target) {
    const auto& [min, max] = std::accumulate(frames.begin(), frames.end(), std::make_tuple(frames.begin()->position, frames.begin()->position), [target](auto a, const auto& v) {
        std::get<0>(a).x = std::min(std::get<0>(a).x, v.position.x);
        std::get<0>(a).y = std::min(std::get<0>(a).y, v.position.y);
        std::get<1>(a).x = std::max(std::get<1>(a).x, v.position.x);
        std::get<1>(a).y = std::max(std::get<1>(a).y, v.position.y);
        return a;
    });
    // std::cout << min << std::endl;
    // std::cout << max << std::endl;
    for (const auto& frame: frames) {
        std::cout << "\x1B[2J\x1B[H";
        // std::cout<< u8"\033[2J\033[1;1H";
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        for (int y = max.y; y >= min.y; y--) {
            for (int x = min.x; x <= max.x; x++) {
                if (frame.position.x == x && frame.position.y == y) {
                    std::cout << "x";
                } else if (target.contains({x, y})) {
                    std::cout << "o";
                } else {
                    std::cout << ".";
                }
            }
            std::cout << std::endl;
        }
    }
}

int get_max_y(Coord velocity, const Area& target) {
    Coord position{0, 0};
    Frames frames{};
    frames.push_back(Frame{position});
    auto max_y = position.y;
    bool found = false;
    while (position.y >= target.bottom.y && position.x <= target.bottom.x) {
        std::tie(position, velocity) = get_next(position, velocity);
        frames.push_back(Frame{position});
        max_y = std::max(max_y, position.y);
        if (target.contains(position)) {
            found = true;
            break;
        }
    }
    // display(frames, target);
    return found ? max_y : -1;
}

int a(const Area& target) {
    // Coord velocity{7, 2};
    // Coord velocity{6, 3};
    // Coord velocity{9, 0};
    // Coord velocity{17, -4};
    int max = -99999;
    for (int i = 0; i < 216; i++) {
        for (int j = 0; j < 1000; j++) {
            max = std::max(max, get_max_y({i, j}, target));
        }
    }
    return max;
    // return get_max_y({6, 9}, target);
}

long long int b(const Area& target) {
    long long int counter = 0;
    for (int i = 0; i < 216; i++) {
        for (int j = -200; j < 1000; j++) {
            if (get_max_y({i, j}, target) >= 0) {
                counter++;
            }
        }
    }
    return counter;
}

int main()
{
    if (false) {
        // target area: x=20..30, y=-10..-5
        // std::cout << a({{20, -5}, {30, -10}}) << std::endl;
        // target area: x=155..215, y=-132..-72
        std::cout << a({{155, -72}, {215, -132}}) << std::endl;
        // 8646 is too high with brute forcing
    } else {
        // std::cout << b({{20, -5}, {30, -10}}) << std::endl;
        std::cout << b({{155, -72}, {215, -132}}) << std::endl;
    }
}
