#include <iostream>
#include <iterator>
#include <numeric>
#include <cmath>

#include <sstream>

#include <vector>
#include <deque>
#include <unordered_map>
#include <unordered_set>
#include <set>

#include <string>

std::ostream& operator<<(std::ostream& os, const std::deque<std::string>& v)
{
    return os << "{" << std::accumulate(v.begin() + (v.empty() ? 0 : 1), v.end(), v.empty() ? "" : *v.begin(),
        [](std::string a, std::string v) { return a + ", " + v; }) << "}";
}

struct Cave {
    std::vector<std::string> next;
    bool visited{false};
};

using Caves = std::unordered_map<std::string, Cave>;

Caves get_caves() {
    Caves caves{};

    std::string line;
    while (std::getline(std::cin, line)) {
        std::istringstream iss(line);
        std::string left;
        std::getline(iss, left, '-');
        std::string right;
        std::getline(iss, right, '-');
        // std::cout << left << " <-> " << right << std::endl;
        caves[left].next.push_back(right);
        caves[right].next.push_back(left);
    }

    return caves;
}

bool isupper(const std::string& string) {
    return std::all_of(string.begin(), string.end(), [](const char c) {
        return isupper(c);
    });
}

bool islower(const std::string& string) {
    return std::all_of(string.begin(), string.end(), [](const char c) {
        return islower(c);
    });
}

bool contains(const std::vector<std::string>& haystack, const std::string& needle) {
     return std::find(haystack.begin(), haystack.end(), needle) != haystack.end();
}

bool contains(const std::deque<std::string>& haystack, const std::string& needle) {
     return std::find(haystack.begin(), haystack.end(), needle) != haystack.end();
}

bool has_small_duplicates(const std::vector<std::string>& haystack) {
    int n = 0;
    std::unordered_set<std::string> set;
    for (const auto& item: haystack) {
        if (islower(item)) {
            n++;
            set.insert(item);
        }
    }
    return n != set.size();
}

bool has_small_duplicates(const std::deque<std::string>& haystack) {
    int n = 0;
    std::unordered_set<std::string> set;
    for (const auto& item: haystack) {
        if (islower(item)) {
            n++;
            set.insert(item);
        }
    }
    return n != set.size();
}

int go_b(const Caves& caves, const std::string& item, std::deque<std::string>& path) {
     if (
         isupper(item) || !contains(path, item) ||
         (item != "start" && !has_small_duplicates(path))
     ) {
         path.push_back(item);
         if (item == "end") {
             // std::cout << path << std::endl;
             path.pop_back();
             return 1;
         }
         int path_counter = 0;
         for (const auto& next: caves.at(item).next) {
             path_counter +=  go_b(caves, next, path);
         }
         path.pop_back();
         return path_counter;
     }
     return 0;
}

int b()
{
    auto caves = get_caves();
    std::deque<std::string> path{};
    return go_b(caves, "start", path);
}

int main()
{
    std::cout << b() << std::endl;
}
