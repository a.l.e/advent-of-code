#!/usr/bin/env bash
out="${PWD##*/}"
g++ -std=c++20 -O2 -o $out main.cpp
./$out < test.txt
# ./$out < test-02.txt
./$out < input.txt
g++ -std=c++20 -O2 -o "$out-stack" main-stack.cpp
./"$out-stack" < test.txt
./"$out-stack" < input.txt
