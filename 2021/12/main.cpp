#include <iostream>
#include <iterator>
#include <numeric>
#include <cmath>

#include <sstream>

#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <set>

#include <string>

std::ostream& operator<<(std::ostream& os, const std::vector<std::string>& v)
{
    return os << "{" << std::accumulate(v.begin() + (v.empty() ? 0 : 1), v.end(), v.empty() ? "" : *v.begin(),
        [](std::string a, std::string v) { return a + ", " + v; }) << "}";
}

struct Cave {
    std::vector<std::string> next;
};

using Caves = std::unordered_map<std::string, Cave>;

Caves get_caves() {
    Caves caves{};

    std::string line;
    while (std::getline(std::cin, line)) {
        std::istringstream iss(line);
        std::string left;
        std::getline(iss, left, '-');
        std::string right;
        std::getline(iss, right, '-');
        // std::cout << left << " <-> " << right << std::endl;
        caves[left].next.push_back(right);
        caves[right].next.push_back(left);
    }

    return caves;
}

bool isupper(const std::string& string) {
    return std::all_of(string.begin(), string.end(), [](const char c) {
        return isupper(c);
    });
}

bool islower(const std::string& string) {
    return std::all_of(string.begin(), string.end(), [](const char c) {
        return islower(c);
    });
}

bool contains(const std::string& haystack, const std::string& needle) {
     return haystack.find(needle) != std::string::npos;
}

bool contains(const std::vector<std::string>& haystack, const std::string& needle) {
     return std::find(haystack.begin(), haystack.end(), needle) != haystack.end();
}

int go(const Caves& caves, const std::string& item, std::vector<std::string> path) {
     if (isupper(item) || !contains(path, item)) {
         path.push_back(item);
         if (item == "end") {
             return 1;
         }
         int path_counter = 0;
         for (const auto& next: caves.at(item).next) {
             path_counter +=  go(caves, next, path);
         }
         return path_counter;
     }
     return 0;
}

bool has_small_duplicates(const std::vector<std::string>& haystack) {
    int n = 0;
    std::unordered_set<std::string> set;
    for (const auto& item: haystack) {
        if (islower(item)) {
            n++;
            set.insert(item);
        }
    }
    return n != set.size();
    // std::unordered_set<std::string> set(haystack.begin(), haystack.end());
    // return std::count_if(haystack.begin(), haystack.end(), [](const auto& v) { return islower(v); }) !=
    //     std::count_if(set.begin(), set.end(), [](const auto& v) { return islower(v); });
}

int go_b(const Caves& caves, const std::string& item, std::vector<std::string> path) {
     if (
         isupper(item) || !contains(path, item) ||
         (item != "start" && !has_small_duplicates(path))
     ) {
         path.push_back(item);
         if (item == "end") {
             // std::cout << path << std::endl;
             return 1;
         }
         int path_counter = 0;
         for (const auto& next: caves.at(item).next) {
             path_counter +=  go_b(caves, next, path);
         }
         return path_counter;
     }
     return 0;
}

int a()
{
    auto caves = get_caves();
    std::vector<std::string> path{};
    return go(caves, "start", path);
}

int b()
{
    auto caves = get_caves();
    std::vector<std::string> path{};
    return go_b(caves, "start", path);
}

int main()
{
    if (false) {
        std::cout << a() << std::endl;
    } else {
        std::cout << b() << std::endl;
    }
}
