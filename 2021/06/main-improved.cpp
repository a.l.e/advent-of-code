#include <iostream>

#include <array>
#include <algorithm>
#include <numeric>

using Fish_counter = std::array<unsigned long long int, 9>;

unsigned long long int b(std::array<int, 300> fishes)
{
    // std::cout << fishes << std::endl;
    Fish_counter fish_counter{};
    for (auto fish: fishes) {
        fish_counter[fish]++;
    }
    for (int i = 0; i < 256; i++) {
        // rotate the items in the array
        unsigned long long int zero = fish_counter[0];
        for (int i = 1; i <= 8; i++) {
            fish_counter[i - 1] = fish_counter[i];
        }
        fish_counter[8] = zero;
        // add the left most to the last and to the sixth
        fish_counter[6] += zero;
    }
    return std::accumulate(fish_counter.begin(), fish_counter.end(), 0, [](unsigned long long int a, int v) {
        return a + v;
    });
}

int main()
{
    // std::array<int, 5> test = {3,4,3,1,2};
    std::array<int, 300> input = {5,1,4,1,5,1,1,5,4,4,4,4,5,1,2,2,1,3,4,1,1,5,1,5,2,2,2,2,1,4,2,4,3,3,3,3,1,1,1,4,3,4,3,1,2,1,5,1,1,4,3,3,1,5,3,4,1,1,3,5,2,4,1,5,3,3,5,4,2,2,3,2,1,1,4,1,2,4,4,2,1,4,3,3,4,4,5,3,4,5,1,1,3,2,5,1,5,1,1,5,2,1,1,4,3,2,5,2,1,1,4,1,5,5,3,4,1,5,4,5,3,1,1,1,4,5,3,1,1,1,5,3,3,5,1,4,1,1,3,2,4,1,3,1,4,5,5,1,4,4,4,2,2,5,5,5,5,5,1,2,3,1,1,2,2,2,2,4,4,1,5,4,5,2,1,2,5,4,4,3,2,1,5,1,4,5,1,4,3,4,1,3,1,5,5,3,1,1,5,1,1,1,2,1,2,2,1,4,3,2,4,4,4,3,1,1,1,5,5,5,3,2,5,2,1,1,5,4,1,2,1,1,1,1,1,2,1,1,4,2,1,3,4,2,3,1,2,2,3,3,4,3,5,4,1,3,1,1,1,2,5,2,4,5,2,3,3,2,1,2,1,1,2,5,3,1,5,2,2,5,1,3,3,2,5,1,3,1,1,3,1,1,2,2,2,3,1,1,4,2};

    // std::cout << b(test) << std::endl;
    std::cout << b(input) << std::endl;
}
