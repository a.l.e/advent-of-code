#!/usr/bin/env bash
out="${PWD##*/}"
g++ -std=c++20 -o $out main.cpp
./$out
g++ -std=c++20 -o $out main-improved.cpp
./$out
