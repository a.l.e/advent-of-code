#include <iostream>
#include <sstream>
#include <iterator>

#include <cmath>
#include <numeric>

#include <vector>
#include <unordered_set>


#include <string>

struct Coord {
    int x, y;
};

bool operator==(const Coord& l, const Coord& r)
{
        return l.x == r.x && l.y == r.y;
}

std::ostream& operator<<(std::ostream& os, const Coord& c)
{
    return os << c.x << ", " << c.y;
}

inline size_t hash_combine( size_t lhs, size_t rhs ) {
    lhs^= rhs + 0x9e3779b9 + (lhs << 6) + (lhs >> 2);
    return lhs;
}

struct CoordHash {
    size_t operator()(const Coord& c) const {
        return hash_combine(c.x, c.y);
    }
};

using Sheet = std::unordered_set<Coord, CoordHash>;

Sheet get_folded(const Sheet& sheet, Coord fold) {
    Sheet result{};
    if (fold.x > 0) {
        for (auto& coord: sheet) {
            if (coord.x > fold.x) {
                result.insert({fold.x - (coord.x - fold.x), coord.y});
            } else if (coord.x < fold.x) {
                result.insert(coord);
            }
        }
    } else {
        for (const auto& coord: sheet) {
            if (coord.y > fold.y) {
                result.insert({coord.x, fold.y - (coord.y - fold.y)});
            } else if (coord.y < fold.y) {
                result.insert(coord);
            }
        }
    }
    return result;
}

void print(const Sheet& sheet) {
    const auto max = std::accumulate(sheet.begin(), sheet.end(), Coord{0, 0}, [](const auto& a, const auto& v) {
        return a.x < v.x || a.y < v.y ? Coord{std::max(a.x, v.x), std::max(a.y, v.y)} : a;
    });
    for (int i = 0; i < max.y + 1; i++) {
        for (int j = 0; j < max.x + 1; j++) {
            std::cout << (sheet.contains({j, i}) ? '#' : '.');
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

using Folds = std::vector<Coord>;

auto get_manual() {
    Sheet sheet{};
    Folds folds{};

    std::string line;
    while (std::getline(std::cin, line)) {
        if (line == "") {
            break;
        }
        std::istringstream iss(line);
        int x, y;
        char c;
        iss >> x >> c >> y;
        sheet.insert({x, y});
    }

    while (std::getline(std::cin, line)) {
        line = line.substr(11);
        // std::cout << line << std::endl;
        std::istringstream iss(line);
        char axis, c;
        int value;
        iss >> axis >> c >> value;
        if (axis == 'x') {
            folds.push_back({value, 0});
        } else {
            folds.push_back({0, value});
        }
    }

    return std::make_tuple(sheet, folds);
}


int a()
{
    auto [sheet, folds] = get_manual();
    // print(sheet);
    for (const auto& fold: folds) {
        sheet = get_folded(sheet, fold);
        // print(sheet);
        break;
    }
    return sheet.size();
}

int b()
{
    auto [sheet, folds] = get_manual();
    for (const auto& fold: folds) {
        sheet = get_folded(sheet, fold);
    }
    print(sheet);
    return 0;
}

int main()
{
    if (false) {
        std::cout << a() << std::endl;
    } else {
        std::cout << b() << std::endl;
    }
}
