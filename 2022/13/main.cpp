#include <iostream>
#include <string>
#include <vector>

#include <variant>
#include <numeric>
#include <algorithm>
#include <cassert>

struct Value;
struct Value {
    std::variant<int, std::vector<Value>> value;
};

std::string fromValue(Value value) {
    if (std::holds_alternative<int>(value.value)) {
        return std::to_string(std::get<int>(value.value));
    }
    auto vector = std::get<std::vector<Value>>(value.value);
    if (vector.empty()) {
        return "[]";
    }

    return "[" + std::accumulate(vector.begin() + 1, vector.end(), fromValue(*vector.begin()),
        [](std::string a, Value v) { return a + "," + fromValue(v); }) + "]";
}

Value get_parsed_line(const std::string& line) {
    std::vector<Value> result;
    std::string digits;
    // i = 1: the first char of line must be a [ and we ignore it
    for (int i = 1; i < line.size(); i++) {
        char c = line.at(i);
        if (c == ']') {
            if (digits.size() > 0) {
                result.push_back({std::stoi(digits)});
            }
            return {result};
        } else if (c == ',') {
            if (digits.size() > 0) {
                result.push_back({std::stoi(digits)});
                digits = "";
            }
        } else if (c == '[') {
            auto inner = get_parsed_line(line.substr(i));
            result.push_back(inner);
            i += fromValue(inner).size() - 1;
        } else {
            digits += c;
        }
    }
    return {result}; // it should never get down here
}

// smaller: -1, equal: 0, bigger: 1
int compare(Value a, Value b) {
    // if both are int, just compare them
    if (std::holds_alternative<int>(a.value) && std::holds_alternative<int>(b.value)) {
        if (std::get<int>(a.value) < std::get<int>(b.value)) {
            return -1;
        } else if (std::get<int>(a.value) > std::get<int>(b.value)) {
            return +1;
        } else {
            return 0;
        }
    }

    // otherwise make sure that both a and b are now vectors of Value
    auto a_vec = std::holds_alternative<std::vector<Value>>(a.value) ?
        std::get<std::vector<Value>>(a.value) :
        std::vector<Value>{a};
    auto b_vec = std::holds_alternative<std::vector<Value>>(b.value) ?
        std::get<std::vector<Value>>(b.value) :
        std::vector<Value>{b};

    // while both vectors have items, compare each of them until until they don't match, or ...
    for (int i = 0; i < std::min(a_vec.size(), b_vec.size()); i++) {
        int cmp = compare(a_vec.at(i), b_vec.at(i));
        if (cmp == 0) {
            continue;
        }
        return cmp;
    }

    // ... the size of the vectors differ
    if (a_vec.size() < b_vec.size()) {
        return -1;
    }
    if (a_vec.size() > b_vec.size()) {
        return 1;
    }

    return 0;
}

int a() {
    int i = 1;
    int result = 0;
    for (std::string line; std::getline(std::cin, line); ) {
        auto left = get_parsed_line(line);
        // std::cout << fromValue(left) << "\n";
        // assert(line == fromValue(left));

        std::getline(std::cin, line);
        auto right = get_parsed_line(line);
        // std::cout << "line: " << line << "\n";
        // assert(line == fromValue(right));
        
        if (compare(left, right) < 0) {
            result += i;
        }

        // consume the empty line
        std::getline(std::cin, line);

        i += 1;
    }
    return result;
}

int b() {
    std::vector<Value> values{get_parsed_line("[[2]]"), get_parsed_line("[[6]]")};

    for (std::string line; std::getline(std::cin, line); ) {
        if (line.size() == 0) {
            continue;
        }
        values.push_back(get_parsed_line(line));
    }

    std::sort(values.begin(), values.end(), [](const auto& a, const auto& b) {
        return compare(a, b) < 0;
    });

    int result = 1;
    int i = 1;
    for (const auto& value: values) {
        auto line = fromValue(value);
        if (line == "[[2]]" || line == "[[6]]") {
            result *= i;
        }
        i += 1;
    }

    return result;
}

void tests() {
    // std::cout << fromValue(get_parsed_line("[1,1,3,1,1]")) << "\n";
    assert(fromValue(get_parsed_line("[1,1,3,1,1]")) == "[1,1,3,1,1]");
    assert(fromValue(get_parsed_line("[[1],[2,3,4]]")) == "[[1],[2,3,4]]");
    assert(fromValue(get_parsed_line("")) == "");
}

int main(int argc, char *argv[]) {
    std::string part = argc == 3 && std::string(argv[1]) == "-part" ? argv[2] : "";

    if (part == "a") {
        std::cout << "a:\n" << a() << "\n";
    } else if (part == "b") {
        std::cout << "b:\n" << b() << "\n";
    } else if (part == "t") {
        tests();
    } else {
        std::cout << "Usage: " << argv[0] << " -part a|b|t [<< input.txt]\n";
    }
}
