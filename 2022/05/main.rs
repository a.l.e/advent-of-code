use std::env;

use std::io::{self, BufRead};
use std::collections::VecDeque;

#[derive(Debug)]
struct Rearrangement {
    count: usize,
    from: usize,
    to: usize,
}

impl Rearrangement {
    fn from(l: String) -> Rearrangement {
        let v = l.trim().split(' ')
            .filter_map(|v| v.parse::<usize>().ok())
            .collect::<Vec<usize>>();
        Rearrangement {
                count: v[0], from: v[1], to: v[2]
        }
    }
}

fn a() -> String {
    let stdin = io::stdin();
        
    type Crates = Vec<VecDeque<char>>;
    let mut crates: Crates = Crates::new();
    for line in stdin.lock().lines() {
        let crate_line = line.unwrap();
        let crate_chars = crate_line.chars().collect::<Vec<char>>();

        if crate_chars.len() == 0 {
            break;
        }
        if crate_chars[1] == '1' {
            continue;
        }

        let n = (crate_chars.len() + 1) / 4;
        for i in 0..n {
            while crates.len() <= i {
                crates.push(VecDeque::new());
            }
            if crate_chars[i * 4 + 1] == ' ' {
                continue;
            }
            crates[i].push_back(crate_chars[i * 4 + 1]);
        }
    }

    stdin.lock()
        .lines()
        .map(|l| Rearrangement::from(l.unwrap()))
        .for_each(|r| {
            for _i in 0..r.count {
                let c = crates[r.from - 1].pop_front().unwrap();
                crates[r.to - 1].push_front(c);
            }
        });

    crates
        .iter()
        .map(|c| c.front().unwrap())
        .collect::<String>()
}

fn b() -> String {
    "99".to_string()
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() == 1 || args[1] == "a" {
        println!("a: {}", a());
    } else if args[1] == "b" {
        println!("b: {}", b());
    } else {
        println!("Usage: {} [a|b] < input.txt", args[0]);
    }
}
