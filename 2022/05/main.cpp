#include <iostream>
#include <string>
#include <vector>
#include <numeric>

#include <sstream>
#include <iterator>

#include <deque>

#include <algorithm>

std::ostream& operator<<(std::ostream& os, const std::vector<std::string>& v)
{
    return os << "{" << std::accumulate(v.begin() + (v.empty() ? 0 : 1), v.end(), v.empty() ? "" : *v.begin(),
        [](std::string a, std::string v) { return a + ", " + v; }) << "}";
}

struct Rearrangement {
    int count;
    int from;
    int to;
};

std::istream& operator>>(std::istream& is, Rearrangement& r)
{
    std::string s;
    is >> s >> r.count >> s >> r.from >> s >> r.to;
    return is;
}

std::ostream& operator<<(std::ostream& os, const Rearrangement& r)
{
    return os << r.count << ": " << r.from << " -> " << r.to;
}

using Crates = std::vector<std::deque<char>>;

using namespace std::string_literals;

void read_crates_from_cin(Crates& crates) {
    std::string line;
    while (std::getline(std::cin, line)) {
        if (line == "") {
            break;
        }
        if (line.at(1) == '1') {
            continue;
        }
        for (int i = 0, j = 0; i < line.size(); i += 4, j += 1) {
            char c = line.at(i + 1);
            if (c == ' ') {
                continue;
            }
            while (crates.size() < j + 1) {
                crates.push_back({});
            }
            crates.at(j).push_back(c);
        }
    }
}

std::string a() {
    Crates crates;
    read_crates_from_cin(crates);

    std::for_each(std::istream_iterator<Rearrangement>{std::cin}, {},
        [&crates](const auto& r) {
            // std::cout << r << "\n";
            for (int i = 0; i < r.count; i++) {
                char crate = crates.at(r.from - 1).front();
                crates.at(r.from - 1).pop_front();
                crates.at(r.to - 1).push_front(crate);
            }
        }
    );
    
    return std::accumulate(crates.begin(), crates.end(), ""s, [](const std::string& a, const std::deque<char>& stack) {
        return a + stack.front();
    });

}

std::string b() {
    Crates crates;
    read_crates_from_cin(crates);

    std::for_each(std::istream_iterator<Rearrangement>{std::cin}, {},
        [&crates](const auto& r) {
            std::string moving_crates{};
            for (int i = 0; i < r.count; i++) {
                moving_crates = crates.at(r.from - 1).front() + moving_crates;
                crates.at(r.from - 1).pop_front();
            }
            for (const char c: moving_crates) {
                crates.at(r.to - 1).push_front(c);
            }
        });

    return std::accumulate(crates.begin(), crates.end(), ""s, [](const std::string& a, const std::deque<char>& stack) {
        return a + stack.front();
    });
}

int main(int argc, char *argv[]) {
    std::string part = argc == 3 && std::string(argv[1]) == "-part" ? argv[2] : "";

    if (part == "") {
        std::cout << "Usage: " << argv[0] << " -part a|b [<< input.txt]\n";
    }

    if (part != "b") {
        std::cout << "a:\n" << a() << "\n";
    } else {
        std::cout << "b:\n" << b() << "\n";
    }
}
