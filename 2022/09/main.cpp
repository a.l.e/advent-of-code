#include <iostream>
#include <vector>
#include <string>

#include <numeric>
#include <istream>
#include <iterator>
#include <algorithm>
#include <unordered_set>
#include <cmath>
#include <cassert>

struct Command {
    char direction;
    int distance;
};

std::istream &operator>>(std::istream& is, Command& c) {
    is >> c.direction >> c.distance;
    return is;
}

struct Position {
    int x;
    int y;
};

std::ostream& operator<<(std::ostream& os, const Position& c)
{
    return os << c.x << ", " << c.y;
}

inline bool operator==(const Position& lhs, const Position& rhs) {
    return lhs.x == rhs.x && lhs.y == rhs.y;
}

inline size_t hash_combine( size_t lhs, size_t rhs ) {
    lhs^= rhs + 0x9e3779b9 + (lhs << 6) + (lhs >> 2);
    return lhs;
}

struct PositionHash {
    size_t operator()(const Position& p) const {
        return hash_combine(p.x, p.y);
    }
};

void move_tail(Position& tail, const Position& head) {
    // one on top of the other
    if (tail.x == head.x && tail.y == head.y) {
        return;
    }
    // vertical move if more than 1 apart
    if (tail.x == head.x) {
        if (std::abs(tail.y - head.y) == 2) {
            tail.y += tail.y < head.y ? 1 : -1;
        }
        return;
    }
    // horizontal  move if more than 1 apart
    if (tail.y == head.y) {
        if (std::abs(tail.x - head.x) == 2) {
            tail.x += tail.x < head.x ? 1 : -1;
        }
        return;
    }
    // diagonal move if more than 1 apart
    if (std::abs(tail.x - head.x) == 2 || std::abs(tail.y - head.y) == 2) {
        tail.x += tail.x < head.x ? 1 : -1;
        tail.y += tail.y < head.y ? 1 : -1;
    }
}

void move_head(Position& head, const Command& c) {
        if (c.direction == 'R') {
            head.x += 1;
        } else if (c.direction == 'U') {
            head.y += 1;
        } else if (c.direction == 'D') {
            head.y -= 1;
        } else if (c.direction == 'L') {
            head.x -= 1;
        }
}

int a() {
    using Footpaths = std::unordered_set<Position, PositionHash>;
    Footpaths footpaths;

    Position head{0,0};
    Position tail{0,0};

    std::for_each(std::istream_iterator<Command>{std::cin}, {}, [&head, &tail, &footpaths](Command c) {
        // std::cout << c.direction << " -> " << c.distance << std::endl;
        for (int i = 0; i < c.distance; i++) {
            move_head(head, c);
            move_tail(tail, head);
            footpaths.insert(tail);
        }
        // std::cout << "H " << head << std::endl;
        // std::cout << "T " << tail << std::endl;
    });
    return footpaths.size();
}

int b() {
    using Footpaths = std::unordered_set<Position, PositionHash>;
    Footpaths footpaths;

    Position head{0,0};
    std::vector<Position> tails(9, {0,0});

    std::for_each(std::istream_iterator<Command>{std::cin}, {}, [&head, &tails, &footpaths](Command c) {
        for (int i = 0; i < c.distance; i++) {
            move_head(head, c);
            move_tail(tails.at(0), head);
            for (int i = 1; i < tails.size(); i++) {
                move_tail(tails.at(i), tails.at(i - 1));
            }
            footpaths.insert(tails.back());
        }
    });
    return footpaths.size();
}

int main(int argc, char *argv[]) {
    std::string part = argc == 3 && std::string(argv[1]) == "-part" ? argv[2] : "";

    if (part == "") {
        std::cout << "Usage: " << argv[0] << " -part a|b [<< input.txt]\n";
    }

    if (part != "b") {
        std::cout << "a:\n" << a() << std::endl;
    } else {
        std::cout << "b:\n" << b() << std::endl;
    }
}
