#include <iostream>
#include <string>
#include <vector>
#include <cassert>

#include <numeric>

using namespace std::string_literals;

using Forest = std::vector<std::vector<int>>;

std::ostream& operator<<(std::ostream& os, const std::vector<int>& v)
{
    return os << "{" << std::accumulate(v.begin() + (v.empty() ? 0 : 1), v.end(), v.empty() ? "" : std::to_string(*v.begin()),
        [](std::string a, int v) { return a + ", " + std::to_string(v); }) << "}";
}

std::ostream& operator<<(std::ostream& os, const std::vector<bool>& v)
{
    return os << "{" << std::accumulate(v.begin() + (v.empty() ? 0 : 1), v.end(), v.empty() ? "" : (*v.begin() ? "1"s : "0"s),
        [](std::string a, bool v) { return a + ", " + (v ? "1"s : "0"s); }) << "}";
}

void read_forest(Forest& forest) {
    std::string line;
    for (std::string line; std::getline(std::cin, line); ) {
        std::vector<int> row{};
        for (char c: line) {
            row.push_back(c - '0');
        }
        forest.push_back(row);
    }
}

int a() {
    Forest forest;
    read_forest(forest);
    int n = forest.size();
    using Visible = std::vector<std::vector<bool>>;
    Visible visible(n, std::vector<bool>(n));

    // for (const auto& line: forest) {
    //     std::cout << line << "\n";
    // }

    for (int i = 0; i < n; i++) {
        int max_left = std::numeric_limits<int>::min();
        int max_right = std::numeric_limits<int>::min();
        for (int j = 0; j < n; j++) {
            if (forest.at(i).at(j) > max_left) {
                visible.at(i).at(j) = true;
            }
            // std::cout << i << ", " << n - 1 - j << " (" << max_right << ")" << "\n";
            if (forest.at(i).at(n - 1 - j) > max_right) {
                visible.at(i).at(n - 1 - j) = true;
            }

            max_left = std::max(max_left, forest.at(i).at(j));
            max_right = std::max(max_right, forest.at(i).at(n - 1 - j));
        }
    }

    for (int j = 0; j < n; j++) {
        int max_top = std::numeric_limits<int>::min();
        int max_bottom = std::numeric_limits<int>::min();
        for (int i = 0; i < n; i++) {
            if (forest.at(i).at(j) > max_top) {
                visible.at(i).at(j) = true;
            }
            if (forest.at(n - 1 - i).at(j) > max_bottom) {
                visible.at(n - 1 - i).at(j) = true;
            }
            max_top = std::max(max_top, forest.at(i).at(j));
            max_bottom = std::max(max_bottom, forest.at(n - 1 - i).at(j));
        }
    }

    // std::cout << "======" << "\n";
    // for (const auto& line: visible) {
    //     std::cout << line << "\n";
    // }

    return std::accumulate(visible.begin(), visible.end(), 0,
        [](int a, std::vector<bool> row) {
            return a + std::accumulate(row.begin(), row.end(), 0,
                [] (int a, bool v){ return a + (v ? 1 : 0); });
        });
}

// learn to read the specs! this find the distant trees, the elves don't care about...
int one_if_visible(const Forest& forest, int v, int i, int j, int& max) {
    if (forest.at(i).at(j) < max) {
        return 0;
    }
    if (forest.at(i).at(j) == max && v <= max) {
        return 0;
    }
    max = std::max(max, forest.at(i).at(j));
    return 1;
}

int b() {
    Forest forest;
    read_forest(forest);
    int n = forest.size();
    using Visible = std::vector<std::vector<int>>;
    Visible visible(n, std::vector<int>(n));
    
    // for (const auto& line: forest) {
    //     std::cout << line << "\n";
    // }

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            // to the right
            int v = forest.at(i).at(j);
            int right = 0;
            // int max = std::numeric_limits<int>::min();
            for (int jj = j + 1; jj < n; jj++) {
                right += 1;
                if (forest.at(i).at(jj) >= v) {
                    break;
                }
                // right += one_if_visible(forest, v, i, jj, max);
            }
            // to the left
            int left = 0;
            for (int jj = j - 1; jj >= 0; jj--) {
                left += 1;
                if (forest.at(i).at(jj) >= v) {
                    break;
                }
            }
            // downwards
            int down = 0;
            for (int ii = i + 1; ii < n; ii++) {
                down += 1;
                if (forest.at(ii).at(j) >= v) {
                    break;
                }
            }
            // upwards
            int up = 0;
            for (int ii = i - 1; ii >= 0; ii--) {
                up += 1;
                if (forest.at(ii).at(j) >= v) {
                    break;
                }
            }
            // std::cout << i << ", " << j << ": " << up << "/" << right << "/" << down << "/" << left << "\n";
            visible.at(i).at(j) = right * left * up * down;
        }
    }
    
    // std::cout << "======" << "\n";
    // for (const auto& line: visible) {
    //     std::cout << line << "\n";
    // }

    int max = 0;
    for (const auto& row: visible) {
        for (const int value: row) {
            max = std::max(max, value);
        }
    }
    return max;
}

int main(int argc, char *argv[]) {
    std::string part = argc == 3 && std::string(argv[1]) == "-part" ? argv[2] : "";

    if (part == "") {
        std::cout << "Usage: " << argv[0] << " -part a|b [<< input.txt]\n";
    }

    if (part != "b") {
        std::cout << "a:\n" << a() << "\n";
    } else {
        std::cout << "b:\n" << b() << "\n";
    }
}
