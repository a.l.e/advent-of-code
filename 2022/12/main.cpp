#include <iostream>
#include <iomanip>
#include <vector>
#include <numeric>
#include <queue>
#include <unordered_set>
#include <unordered_map>

using World = std::vector<std::vector<int>>;

std::ostream& operator<<(std::ostream& os, const std::vector<int>& v)
{
    return os << "{" << std::accumulate(v.begin() + (v.empty() ? 0 : 1), v.end(), v.empty() ? "" : std::to_string(*v.begin()),
        [](std::string a, int v) { return a + ", " + std::to_string(v); }) << "}";
}

struct Position {
    int x;
    int y;

    friend Position operator+(Position lhs, const Position& rhs) {
        lhs.x += rhs.x;
        lhs.y += rhs.y;
        return lhs;
    }
};

std::ostream& operator<<(std::ostream& os, const Position& p)
{
    return os << p.x << ", " << p.y;
}

inline bool operator==(const Position& lhs, const Position& rhs) {
    return lhs.x == rhs.x && lhs.y == rhs.y;
}

inline size_t hash_combine( size_t lhs, size_t rhs ) {
    lhs^= rhs + 0x9e3779b9 + (lhs << 6) + (lhs >> 2);
    return lhs;
}

struct PositionHash {
    size_t operator()(const Position& p) const {
        return hash_combine(p.x, p.y);
    }
};

using Distances = std::unordered_map<Position, int, PositionHash>;
using Visiting = std::unordered_set<Position, PositionHash>;

void read(World& world, Position& start, Position& end) {
    std::string line;
    for (std::string line; std::getline(std::cin, line); ) {
        std::vector<int> row{};
        for (char c: line) {
            Position position{(int) row.size(), (int) world.size()};
            if (c == 'S') {
                row.push_back(0);
                start = position;
            } else if (c == 'E') {
                end = position;
                row.push_back('z' - 'a');
            } else {
                row.push_back(c - 'a');
            }
        }
        world.push_back(row);
    }
}

// all items have an infinite distance
void init_distances(Distances& distances, const World& world) {
    for (int i = 0; i < world.size(); i++) {
        for (int j = 0; j < world.at(i).size(); j++) {
            distances.insert({{j, i}, std::numeric_limits<int>::max()});
        }
    }
}

void display_distances(const World& world, const Distances& distances) {
    {
        for (int i = 0; i < world.size(); i++) {
            for (int j = 0; j < world.at(i).size(); j++) {
                std::cout << std::setw(2) << distances.at({j, i}) << " ";
            }
            std::cout << "\n";
        }
    }
}

// all items are unvisited
void init_unvisited(Visiting& unvisited, const World& world) {
    for (int i = 0; i < world.size(); i++) {
        for (int j = 0; j < world.at(i).size(); j++) {
            unvisited.insert({j, i});
        }
    }
}

const std::vector<Position> directions = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};

// a neighbur is close to position but cannot have climb higher than 1
std::vector<Position> get_neighbours(const Position& position, const World& world) {
    std::vector<Position> result;
    for (const auto& direction: directions) {
        auto neighbour = position + direction;
        if (neighbour.x >= 0 && neighbour.y >= 0 &&
            neighbour.x < world.at(0).size() && neighbour.y < world.size()) {
                if (world.at(neighbour.y).at(neighbour.x) - world.at(position.y).at(position.x) < 2) {
                    result.push_back(neighbour);
                }
        }
    }
    return result;
}

// https://en.wikipedia.org/wiki/Shortest_Path_Faster_Algorithm + unvisited from dijkstra
int a() {
    World world;
    Position start;
    Position end;
    read(world, start, end);

    Distances distances;
    init_distances(distances, world);
    distances[start] = 0;

    // std::cout << "S: " << start << "\n";
    // std::cout << "E: " << end << "\n";
    // for (const auto& row: world) {
    //     std::cout << row << "\n";
    // }

    
    std::queue<Position> processing;
    processing.push(start);

    Visiting unvisited;
    init_unvisited(unvisited, world);

    while (!processing.empty()) {
        Position position = processing.front();
        processing.pop();
        for (const auto& neighbour: get_neighbours(position, world)) {
            if (distances[position] < distances[neighbour] + 1) {
                distances[neighbour] = distances[position] + 1;
                if (!unvisited.contains(neighbour)) {
                    continue;
                }
                unvisited.erase(neighbour);
                if (neighbour == end) {
                    std::queue<Position>().swap(processing); // clear processing
                    break;
                }
                processing.push(neighbour);
            }
        }
    }

    // display_distances(world, distances);
    return distances[end];
}

// https://stackoverflow.com/a/15602044
struct PriorityNode {
    Position position;
    int distance;
    bool operator<(const PriorityNode& rhs) const {
        return distance < rhs.distance;
    }
};

// trying to do dijkstra, too?
// it's incomplete
int a_dijkstra() {
    // https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm

    World world;
    Position start;
    Position end;
    read(world, start, end);

    Distances distances;
    init_distances(distances, world);
    distances[start] = 0;

    using Previous = std::unordered_set<Position, PositionHash>;
    Previous previous;

    // 1.
    Visiting unvisited;
    init_unvisited(unvisited, world);

    // TODO: how to get the distance to update once the node is in the queue?
    // is it needed?
    std::priority_queue<PriorityNode> processing;
    PriorityNode node{start, 0};
    processing.push(node);

    // 2.
    Position current = start;
    for (const auto& neighbour: get_neighbours(current, world)) {
            if (distances[current] < distances[neighbour] + 1) {
            }
    }

    return distances[end];
}

int b() {
    World world;
    Position start; // this value is not used... but we get it for free in read()
    Position end;
    read(world, start, end);

    int distance = std::numeric_limits<int>::max(); // we look for the min distance

    for (int i = 0; i < world.size(); i++) {
        for (int j = 0; j < world.at(i).size(); j++) {
            // for each cell that is a 0 height ('a')
            if (world.at(i).at(j) != 0) {
                continue;
            }

            start = {j, i};

            Distances distances;
            init_distances(distances, world);
            distances[start] = 0;

            Visiting unvisited;
            init_unvisited(unvisited, world);

            std::queue<Position> processing;
            processing.push(start);

            while (!processing.empty()) {
                Position position = processing.front();
                processing.pop();
                for (const auto& neighbour: get_neighbours(position, world)) {
                    if (world.at(neighbour.y).at(neighbour.x) == 'a') {
                        continue;
                    }
                    if (distances[position] < distances[neighbour] + 1) {
                        distances[neighbour] = distances[position] + 1;
                        if (!unvisited.contains(neighbour)) {
                            continue;
                        }
                        unvisited.erase(neighbour);
                        if (neighbour == end) {
                            distance = std::min(distance, distances[end]);
                            std::queue<Position>().swap(processing); // clear processing
                            break;
                        }
                        processing.push(neighbour);
                    }
                }
            }
        }
    }

    return distance;
}

int main(int argc, char *argv[]) {
    std::string part = argc == 3 && std::string(argv[1]) == "-part" ? argv[2] : "";

    if (part == "") {
        std::cout << "Usage: " << argv[0] << " -part a|b [<< input.txt]\n";
    }

    if (part != "b") {
        std::cout << "a:\n" << a() << "\n";
        // std::cout << "a:\n" << a_dijkstra() << "\n";
    } else {
        std::cout << "b:\n" << b() << "\n";
    }
}
