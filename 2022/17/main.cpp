#include <iostream>
#include <string>
#include <vector>
#include <bitset>

using Chamber = std::vector<std::bitset<7>>;
using Rock = std::vector<std::bitset<7>>;

// the rocks are inverted: its row 0 is the bottom
const std::vector<Rock> rocks {
    {
        {0b0011110}, // ####
    },
    {
        {0b0001000}, // .#.
        {0b0011100}, // ###
        {0b0001000}, // .#.
    },
    {
        {0b0011100},// ### 
        {0b0000100},// ..# 
        {0b0000100},// ..# 
    },
    {
        {0b0010000}, // # 
        {0b0010000}, // # 
        {0b0010000}, // # 
        {0b0010000}, // # 
    },
    {
        {0b0011000}, // ##
        {0b0011000}, // ##
    },
};

void shift_left(Rock& rock) {
    for (const auto& slice: rock) {
        if (slice.test(slice.size() - 1)) {
            return;
        }
    }
    for (auto& slice: rock) {
        slice <<= 1;
    }
}
void shift_right(Rock& rock) {
    for (const auto& slice: rock) {
        if (slice[0] == 1) {
            return;
        }
    }
    for (auto& slice: rock) {
        slice >>= 1;
    }
}

int a() {
    std::string gas_pushes;
    std::getline(std::cin, gas_pushes);

    Chamber chamber;

    int i_push = 0;
    int n = 2022;
    // int n = 4;
    for (int i = 0; i < n; i++) {
        auto rock = rocks.at(i % 4);
        // free fall from four lines above the bottom or highest rock
        for (int j = 0; j < 4; j++) {
            if (gas_pushes.at(i_push % gas_pushes.size()) == '<') {
                shift_left(rock);
            } else {
                shift_right(rock);
            }
            std::cout << "i<<" << std::endl;
            for (const auto& slice: rock) {
                std::cout << "b " << slice << std::endl;
            }
            i_push += 1;
        }
        std::cout << ">>" << std::endl;
        // before checking for intersections, it will fit at the top of the chamber
        int fitting = chamber.size();
        // check if the rock can go further down
        int top_i = chamber.size() - 1;
        for (int i = top_i; i >= 0; i--) {
            bool fits = true;
            for (int j = 0; j <= top_i - i; j++) {
                if ((chamber.at(i - j) & rock.at(j)).any()) {
                    fits = false;
                    break;
                }
            }
            if (!fits) {
                break;
            }
            fitting = i;
        }
        // add the rock at the fitting line
        for (int i = 0; i < rock.size(); i++) {
            if (fitting - i >= chamber.size()) {
                chamber.push_back(rock.at(i));
            } else {
                chamber.at(fitting - i) |= rock.at(i);
            }
        }
        for (const auto& slice: chamber) {
            std::cout << "w " << slice << std::endl;
        }
    }

    return chamber.size();
}

int b() {
    return 2;
}

void tests() {
}

int main(int argc, char *argv[]) {
    std::string part = argc == 3 && std::string(argv[1]) == "-part" ? argv[2] : "";

    if (part == "a") {
        std::cout << "a:\n" << a() << "\n";
    } else if (part == "b") {
        std::cout << "b:\n" << b() << "\n";
    } else if (part == "t") {
        tests();
    } else {
        std::cout << "Usage: " << argv[0] << " -part a|b|t [<< input.txt]\n";
    }
}
