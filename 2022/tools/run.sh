#!/usr/bin/env sh

# compile main.cpp into a file with the same name as the directory
# the run part a or b with, optionally, a text file as the input

if [ $# -lt 1 ]; then
    printf "Usage: run.sh a [test.txt]\n"
    exit 0
fi

part="$1"

file=""
if [ $# -eq 2 ]; then
    file="$PWD/$2"

    if [ ! -f "$file" ]; then
        printf "$file does not exist\n"
        exit 0
    fi
fi

out="${PWD##*/}"

g++ -std=c++20 -o $out main.cpp
# g++ -std=c++20 -pedantic -Wall -Wextra -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wdisabled-optimization -Wformat=2 -Winit-self -Wlogical-op -Wnoexcept -Wold-style-cast -Woverloaded-virtual -Wredundant-decls -Wshadow -Wsign-conversion -Wsign-promo -Wstrict-null-sentinel -Wstrict-overflow=5 -Wswitch-default -Wundef -Werror -Wno-unused -o $out main.cpp

if [ $? -ne 0 ]; then
    exit 0
fi

if [ -z "$file" ]; then
    ./$out -part "$part"
else
    ./$out -part "$part" < "$file"
fi
