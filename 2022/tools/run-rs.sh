#!/usr/bin/env sh

# compile main.cpp into a file with the same name as the directory
# the run part a or b with, optionally, a text file as the input

if [ $# -lt 1 ]; then
    printf "Usage: ${$0} a [test.txt]\n"
    exit 0
fi

part="$1"

file=""
if [ $# -eq 2 ]; then
    file="$PWD/$2"

    if [ ! -f "$file" ]; then
        printf "$file does not exist\n"
        exit 0
    fi
fi

out="${PWD##*/}-rs"

rustc -o $out main.rs

if [ $? -ne 0 ]; then
    exit 0
fi

if [ -z "$file" ]; then
    ./$out "$part"
else
    ./$out "$part" < "$file"
fi
