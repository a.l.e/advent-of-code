/**
 * This version uses "vector" diamonds
 */

#include <iostream>
#include <algorithm>
#include <iterator>
#include <string>
#include <utility>
#include <unordered_set>
#include <unordered_map>
#include <vector>

struct Position {
    int x;
    int y;
};

inline bool operator==(const Position& lhs, const Position& rhs) {
    return lhs.x == rhs.x && lhs.y == rhs.y;
}

inline size_t hash_combine( size_t lhs, size_t rhs ) {
    lhs^= rhs + 0x9e3779b9 + (lhs << 6) + (lhs >> 2);
    return lhs;
}

struct PositionHash {
    size_t operator()(const Position& p) const {
        return hash_combine(p.x, p.y);
    }
};

std::ostream& operator<<(std::ostream& os, const Position& p) {
    return os << p.x << ", " << p.y;
}

struct SensorData {
    Position sensor;
    Position beacon;
};

std::istream& operator>>(std::istream& is, SensorData& data) {
    std::string line;
    for (int i = 0; i < 10; i++) {
        is >> line;
        if (line.size() == 0) {
            break;
        }
        switch (i) {
            case 2:
                data.sensor.x = {std::stoi(line.substr(2, -1))};
                break;
            case 3:
                data.sensor.y = {std::stoi(line.substr(2))};
                break;
            case 8:
                data.beacon.x = {std::stoi(line.substr(2, -1))};
                break;
            case 9:
                data.beacon.y = {std::stoi(line.substr(2))};
                break;
        }
    }
    return is;
}

struct Diamond {
    Position sensor;
    int radius;
};

inline bool operator<(const Diamond& lhs, const Diamond& rhs) {
    return lhs.sensor.x - lhs.radius < rhs.sensor.x - lhs.radius;
}

std::ostream& operator<<(std::ostream& os, const Diamond& d) {
    return os << "(" << d.sensor << ") +- " << d.radius;
}

using Diamonds = std::vector<Diamond>;
using Beacons = std::unordered_set<Position, PositionHash>;

void read_sensor_data(Diamonds& diamonds, Beacons beacons) {
    std::for_each(std::istream_iterator<SensorData>{std::cin}, {}, [&diamonds, &beacons](const auto& data) {
        int radius = std::abs(data.sensor.x - data.beacon.x) + std::abs(data.sensor.y - data.beacon.y);
        diamonds.push_back({data.sensor, radius});
        beacons.insert(data.beacon);
    });
}

int a() {
    Diamonds diamonds{};
    Beacons beacons{};
    read_sensor_data(diamonds, beacons);

    std::unordered_set<int> row{};
    // std::cout << diamonds.size() << "\n";
    int y = diamonds.size() == 14 ? 10 : 2000000; // test or input?

    for (const auto& diamond: diamonds) {
        if (diamond.sensor.y - diamond.radius > y && diamond.sensor.y + diamond.radius < y) {
            // y does not cut the diamond
            continue;
        }
        // if (diamond.sensor == Position{8, 7}) {
        // std::cout << "diamond: " << diamond << "\n";
        // use the manhattan distance
        int radius = (diamond.radius + (y - diamond.sensor.y) * (y > diamond.sensor.y ? -1 : 1));
        int start_x = diamond.sensor.x - radius;
        int end_x = diamond.sensor.x + radius;
        for (int i = start_x; i <= end_x; i++) {
            if (!beacons.contains({i, y})) {
            row.insert(i);
            }
        }
        // }
    }

    return row.size();
}

struct Span {
    int x0;
    int x1;
};

inline bool operator<(const Span& lhs, const Span& rhs) {
    return lhs.x0 < rhs.x0;
}

std::ostream& operator<<(std::ostream& os, const Span& s) {
    return os << s.x0 << ", " << s.x1;
}

unsigned long long b() {
    Diamonds diamonds{};
    Beacons beacons{};
    read_sensor_data(diamonds, beacons);

    int distance = diamonds.size() == 14 ? 20 : 4000000; // test or input?


    for (int i = 0; i < distance; i++) {
        Span segment_span = {0, distance};
        std::sort(diamonds.begin(), diamonds.end(), [i](const Diamond& lhs, const Diamond& rhs) {
            // sort by the left most position on the line i
            int lhs_radius = (lhs.radius + (i - lhs.sensor.y) * (i > lhs.sensor.y ? -1 : 1));
            int rhs_radius = (rhs.radius + (i - rhs.sensor.y) * (i > rhs.sensor.y ? -1 : 1));
            return lhs.sensor.x - lhs_radius < rhs.sensor.x - rhs_radius;
        });
        for (const auto& diamond: diamonds) {
            int radius = (diamond.radius + (i - diamond.sensor.y) * (i > diamond.sensor.y ? -1 : 1));
            int x0 = diamond.sensor.x - radius;
            int x1 = diamond.sensor.x + radius;
            if (x0 <= segment_span.x1 && x1 >= segment_span.x0) {
                if (x0 > segment_span.x0 + 1) {
                    // we're there: it's a hole!
                    return (segment_span.x0 + 1) * 4000000ull + i;
                }
                if (x1 >= segment_span.x1) {
                    // the full segment is covered
                    break;
                }
                segment_span.x0 = x1;
            }
        }
    }

    return 2; // should never get here
}

void tests() {
}

int main(int argc, char *argv[]) {
    std::string part = argc == 3 && std::string(argv[1]) == "-part" ? argv[2] : "";

    if (part == "a") {
        std::cout << "a:\n" << a() << "\n";
    } else if (part == "b") {
        std::cout << "b:\n" << b() << "\n";
    } else if (part == "t") {
        tests();
    } else {
        std::cout << "Usage: " << argv[0] << " -part a|b|t [<< input.txt]\n";
    }
}
