#include <iostream>
#include <algorithm>
#include <iterator>
#include <string>
#include <utility>
#include <unordered_set>
#include <unordered_map>
#include <vector>

struct Position {
    int x;
    int y;
};

inline bool operator==(const Position& lhs, const Position& rhs) {
    return lhs.x == rhs.x && lhs.y == rhs.y;
}

inline size_t hash_combine( size_t lhs, size_t rhs ) {
    lhs^= rhs + 0x9e3779b9 + (lhs << 6) + (lhs >> 2);
    return lhs;
}

struct PositionHash {
    size_t operator()(const Position& p) const {
        return hash_combine(p.x, p.y);
    }
};

std::ostream& operator<<(std::ostream& os, const Position& p) {
    return os << p.x << ", " << p.y;
}

struct SensorData {
    Position sensor;
    Position beacon;
};

std::istream& operator>>(std::istream& is, SensorData& data) {
    std::string line;
    for (int i = 0; i < 10; i++) {
        is >> line;
        if (line.size() == 0) {
            break;
        }
        switch (i) {
            case 2:
                data.sensor.x = {std::stoi(line.substr(2, -1))};
                break;
            case 3:
                data.sensor.y = {std::stoi(line.substr(2))};
                break;
            case 8:
                data.beacon.x = {std::stoi(line.substr(2, -1))};
                break;
            case 9:
                data.beacon.y = {std::stoi(line.substr(2))};
                break;
        }
    }
    return is;
}

struct Span {
    int x0;
    int x1;
};

inline bool operator<(const Span& lhs, const Span& rhs) {
    return lhs.x0 < rhs.x0;
}

std::ostream& operator<<(std::ostream& os, const Span& s) {
    return os << s.x0 << ", " << s.x1;
}

using Segments = std::unordered_map<int, std::vector<Span>>;
using Beacons = std::unordered_set<Position, PositionHash>;

void read_sensor_data(Segments& segments, Beacons& beacons) {
    std::for_each(std::istream_iterator<SensorData>{std::cin}, {}, [&segments, &beacons](const auto& data) {
        int radius = std::abs(data.sensor.x - data.beacon.x) + std::abs(data.sensor.y - data.beacon.y);
        // if (data.sensor == Position{8, 7}) {
        for (int i = 0; i <= radius; i++) {
            Span span{data.sensor.x - radius + i, data.sensor.x + radius - i};
            // std::cout << data.sensor.y + i << ": " << span << "\n";
            segments[data.sensor.y + i].push_back(span);
            if (i == 0) {
                continue;
            }
            // std::cout << data.sensor.y - i << ": " << span << "\n";
            segments[data.sensor.y - i].push_back(span);
        }
        beacons.insert(data.beacon);
        // }
    });
}

int a() {
    Segments segments{};
    Beacons beacons{};

    read_sensor_data(segments, beacons);

    std::unordered_set<int> row{};
    // std::cout << segments.size() << "\n";
    int y = segments.size() == 37 ? 10 : 2000000; // test or input?
    for (const auto& span: segments[y]) {
        for (int i = span.x0; i <= span.x1; i++) {
            if (!beacons.contains({i, y})) {
                row.insert(i);
            }
        }
    }

    return row.size();
}

unsigned long long int b() {
    Segments segments{};
    Beacons beacons{};

    read_sensor_data(segments, beacons);

    int distance = segments.size() == 37 ? 20 : 4000000; // test or input?
    for (int i = 0; i <= distance; i++) {
        std::sort(segments.at(i).begin(), segments.at(i).end());
        Span segment_span = {0, distance};
        for (const auto& span: segments.at(i)) {
            if (span.x0 <= segment_span.x1 && span.x1 >= segment_span.x0) {
                if (span.x0 > segment_span.x0 + 1) {
                    // we're there: it's a hole!
                    // std::cout << ">>> " << Position{segment_span.x0 + 1, i} << "\n";
                    return (segment_span.x0 + 1) * 4000000ull + i;
                }
                if (span.x1 >= segment_span.x1) {
                    // the full segment is covered
                    break;
                }
                segment_span.x0 = span.x1;
                // std::cout << ". " << segment_span << "\n";
            }
        }
    }
    return 2; // should never get here
}

void tests() {
}

int main(int argc, char *argv[]) {
    std::string part = argc == 3 && std::string(argv[1]) == "-part" ? argv[2] : "";

    if (part == "a") {
        std::cout << "a:\n" << a() << "\n";
    } else if (part == "b") {
        std::cout << "b:\n" << b() << "\n";
    } else if (part == "t") {
        tests();
    } else {
        std::cout << "Usage: " << argv[0] << " -part a|b|t [<< input.txt]\n";
    }
}
