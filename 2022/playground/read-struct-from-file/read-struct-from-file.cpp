#include <iostream>
#include <filesystem>
#include <fstream>
#include <iterator>
#include <algorithm>

struct Command {
    char direction;
    int distance;
};

std::istream &operator>>(std::istream& is, Command& c) {
    is >> c.direction >> c.distance;
    return is;
}

std::ostream& operator<<(std::ostream& os, const Command& c)
{
    return os << c.direction << ", " << c.distance;
}

// directly reading the file into the command
int a(std::string filename) {
    std::ifstream input_file;
    input_file.open(filename);
    // if (!input_file.is_open()) {
    //     std::cout << "Cannot find the file " << filename << "\n";
    // }
    std::istream_iterator<Command> input_stream(input_file);

    std::for_each(input_stream, {}, [](Command c) {
        std::cout << c << "\n";
    });
    return 1;
}

// read the line as string and, then, the fields in it
int b(std::string filename) {
    std::ifstream input_file;
    input_file.open(filename);
    std::string line;
    Command c;
    while (getline(input_file, line)) {
        std::istringstream iss(line);
        iss >> c;
        std::cout << c << "\n";
    }
    return 2;
}

int main(int argc, char *argv[]) {
    if (argc <= 2) {
        std::cout << "Usage: " << argv[0] << "a|b [<< input.txt]\n";
    }
    std::string part = argv[1];

    std::string filename = argc == 3 ? argv[2] : "";
    if (filename != "") {
        namespace fs = std::filesystem;
        fs::path f{filename};
        if (!fs::exists(f)) {
            std::cout << "Cannot find the file " << filename << "\n";
            return 1;
        }
        if ((fs::status(filename).permissions() & fs::perms::owner_read) == fs::perms::none) {
            std::cout << "Cannot read the file " << filename << "\n";
            return 1;
        }
    }

    if (part == "a") {
        std::cout << a(filename) << "\n";
    } else {
        std::cout << b(filename) << "\n";
    }
}
