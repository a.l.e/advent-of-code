# Reading a struct from a file

For the Advent of code, I normally read the input from the standard input.

I've started doing so a few years ago and cannot really recall the exact reasons for it, except: I'm convinced, it's easier to do so.

Having been asked, how to "easily" convert a line of a text file into a struct, I dived into it again, and the result has not made me change my minde: it's (much) easier to read from the standard input...

But it was an interesting experience to dive into the file reading!

You can compile and run `read-struct-from-file.cpp` with:

```sh
g++ -std=c++20 -o read-struct-from-file read-struct-from-file.cpp && ./read-struct-from-file a test.txt
```

The code:

- `main()` checks the optional filename from the command line and checks that the file exists and is readable by the user.
- `a()`:
  - uses one of the functions from `algorithm` (or `numeric` for `accumulate()` that processes an iterator and
  - directly reads the data into the `Command` struct.
- `b():
  - reads each line as a string and then
  - creates a stream from the string and fills the struct from it.

Remarks:

- In `a()`, I've left the (commented out) check for the file being open: it's probably not necessary, because the checks are already being done in `main()`.
- The `test.txt` file is from day 9 of the Advent of code 2022.
