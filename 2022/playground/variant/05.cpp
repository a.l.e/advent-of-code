// Add a function that converts the `Value` to a string.
#include <variant>
#include <vector>
#include <iostream>

struct Value;
struct Value {
    std::variant<int, std::vector<Value>> value;
};

std::string fromValue(Value value) {
    if (std::holds_alternative<int>(value.value)) {
        return std::to_string(std::get<int>(value.value));
    }
    auto vector = std::get<std::vector<Value>>(value.value);
    if (vector.empty()) {
        return "[]";
    }

    std::string text = "[" + fromValue(vector.at(0));
    for (int i = 1; i < vector.size(); i++) {
        text += "," + fromValue(vector.at(i));
    }
    return text + "]";
}

int main() {
    Value value{std::vector<Value>{{1}}}; // -> [1]
    std::cout << fromValue(value) << "\n";
    value = {std::vector{value}}; // -> [[1]]
    std::cout << fromValue(value) << "\n";
    value = {std::vector{Value{1}, {std::vector<Value>{{2}}}}}; // -> [1,[2]]
    std::cout << fromValue(value) << "\n";
}
