// Create a variant variable that can contain integers or strings.
#include <variant>
#include <string>

int main() {
    std::variant<int, std::string> value;
    value = 1;
    value = "abcd";
}
