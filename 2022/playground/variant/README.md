# Variant and recursive data structures

On December 13, I have used `std::variant` to create a recursive data structure that contains numbers and lists of (list of...) numbers.

In C++, if you want to create a recursive data structure, you need to combine `std::variant` with a container (that will terminate the recusion).

You can compile the example below with:

```cpp
g++ -o 01 -std=c++20 01.cpp && ./01
```

- [01](01.cpp): Create a variant variable that can contain integers or strings.
- [02](02.cpp): Get the value of variant as int or string.
- [03](03.cpp): Before using it, check that the variant contains the wanted type.
- [04](04.cpp): Create a recursive data structure (`Value`) that can contain an int or a vector of ints.
- [05](05.cpp): Add a function that converts the `Value` to a string.
- [06](06.cpp): Add a function that parses a string and creates a recursive `Value` data structure.
