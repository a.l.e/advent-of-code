// Add a function that parses a string and creates a recursive `Value` data structure.
#include <variant>
#include <vector>
#include <iostream>

struct Value;
struct Value {
    std::variant<int, std::vector<Value>> value;
};

std::string fromValue(Value value) {
    if (std::holds_alternative<int>(value.value)) {
        return std::to_string(std::get<int>(value.value));
    }
    auto vector = std::get<std::vector<Value>>(value.value);
    if (vector.empty()) {
        return "[]";
    }

    std::string text = "[" + fromValue(vector.at(0));
    for (int i = 1; i < vector.size(); i++) {
        text += "," + fromValue(vector.at(i));
    }
    return text + "]";
}

Value get_parsed_line(const std::string& line, int& i) {
    std::vector<Value> result;
    std::string digits;
    // i = i + 1: the first char of line must be a [ and we ignore it
    for (i = i + 1; i < line.size(); i++) {
        char c = line.at(i);
        if (c == ']') {
            if (digits.size() > 0) {
                result.push_back({std::stoi(digits)});
            }
            return {result};
        } else if (c == ',') {
            if (digits.size() > 0) {
                result.push_back({std::stoi(digits)});
                digits = "";
            }
        } else if (c == '[') {
            result.push_back(get_parsed_line(line, i));
        } else {
            digits += c;
        }
    }
    return {result}; // it should never get down here
}

Value get_parsed_line(const std::string& line) {
    int start = 0;
    return get_parsed_line(line, start);
}

int main() {
    std::cout << fromValue(get_parsed_line("[1,1,5,1,1]")) << "\n";
    std::cout << fromValue(get_parsed_line("[[1],[2,3,4]]")) << "\n";
    std::cout << fromValue(get_parsed_line("[[[]]]")) << "\n";
}
