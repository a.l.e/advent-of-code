// Create a recursive data structure (`Value`) that can contain an int or a vector of ints.
#include <variant>
#include <vector>
#include <iostream>

struct Value;
struct Value {
    std::variant<int, std::vector<Value>> value;
};

int main() {
    Value value{std::vector<Value>{{1}}}; // -> [1]
    value = {std::vector{value}}; // -> [[1]]
    value = {std::vector{Value{1}, {std::vector<Value>{{2}}}}}; // -> [1,[2]]
}
