// Get the value of variant as int or string.

#include <iostream>
#include <variant>
#include <string>

int main() {
    std::variant<int, std::string> value;

    value = 1;
    std::cout << std::get<int>(value) << "\n";;

    value = "abcd";
    std::cout << std::get<std::string>(value) << "\n";
}

