// Before using it, check that the variant contains the wanted type.
#include <iostream>
#include <variant>
#include <string>

int main() {
    std::variant<int, std::string> value;

    value = 1;
    if (std::holds_alternative<int>(value)) {
        std::cout << std::get<int>(value) << "\n";;
    }

    value = "abcd";
    if (std::holds_alternative<std::string>(value)) {
        std::cout << std::get<std::string>(value) << "\n";
    }
}

