import sys

dirs = {}
pwd = []
for line in sys.stdin:
    command = line.strip().split(" ");
    if command[0] == "$":
        if command[1] == "cd":
            if command[2] == "/":
                pwd = [""]
            elif command[2] == "..":
                pwd = pwd[0:-1]
            else:
                pwd.append(command[2])
        elif command[0] == "ls":
            pass
    elif command[0] == "dir":
        pass
    else:
        path = pwd
        while path:
            dirname = '/'.join(path)
            dirs[dirname] = dirs.get(dirname, 0) + (int) (command[0])
            path = path[0:-1]
# print(dirs)

print('a:')
print(sum([size for size in dirs.values() if size < 100000]))

print('b:')
used_space = 70000000 - dirs[""]
target_space = 30000000 - used_space 

print(min([size for size in dirs.values() if size > target_space]))
# print(min([item for item in dirs.items() if item[1] > target_space], key=lambda x: x[1]))
