use std::env;

fn a() -> u32{
    99
}

fn b() -> u32 {
    99
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() == 1 || args[1] == "a" {
        println!("a: {}", a());
    } else if args[1] == "b" {
        println!("b: {}", b());
    } else {
        println!("Usage: {} [a|b] < input.txt", args[0]);
    }
}
