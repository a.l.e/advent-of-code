#include <iostream>
#include <string>

int a() {
    return 1;
}

int b() {
    return 2;
}

void tests() {
}

int main(int argc, char *argv[]) {
    std::string part = argc == 3 && std::string(argv[1]) == "-part" ? argv[2] : "";

    if (part == "a") {
        std::cout << "a:\n" << a() << "\n";
    } else if (part == "b") {
        std::cout << "b:\n" << b() << "\n";
    } else if (part == "t") {
        tests();
    } else {
        std::cout << "Usage: " << argv[0] << " -part a|b|t [<< input.txt]\n";
    }
}
