#include <iostream>
#include <string>

#include <iostream>
#include <iterator>
#include <numeric>

struct Hand {
    char opponent; // ABC
    char player; // XYZ
};

std::istream &operator>>(std::istream& is, Hand& h) {
    is >> h.opponent >> h.player;
    return is;
}

int winning_move(int a) {
    return (a + 1) % 3;
}

int losing_move(int a) {
    return (a + 2) % 3;
}

// 0 = rock, 1 = paper, 2 = scissor
int winning_points(int a, int b) {
    if (a == b) {
        return 3;
    } else if (a == winning_move(b)) {
        return 6;
    } else {
        return 0;
    }
}

// 0 = lose, 1 = draw, 2 = win
// 0 = rock, 1 = paper, 2 = scissor
int goal_points(int a, int b) {
    switch (a) {
        case 1: return b;
        case 2: return winning_move(b);
        default: return losing_move(b);
    }
}

int a() {
    return std::accumulate(std::istream_iterator<Hand>{std::cin}, {}, 0, 
        [](int a, Hand h) {
            return a +
                (h.player - 'W') +
                winning_points(h.player - 'X', h.opponent - 'A');
        }
    );
}

int b() {
    return std::accumulate(std::istream_iterator<Hand>{std::cin}, {}, 0, 
        [](int a, Hand h) {
            return a +
                (h.player - 'X') * 3 +
                goal_points(h.player - 'X', h.opponent - 'A') + 1;
        }
    );
}

int main(int argc, char *argv[]) {
    std::string part = argc == 3 && std::string(argv[1]) == "-part" ? argv[2] : "";

    if (part == "") {
        std::cout << "Usage: " << argv[0] << " -part a|b [<< input.txt]\n";
    }

    if (part != "b") {
        std::cout << a() << "\n";
    } else {
        std::cout << b() << "\n"; // 8925
    }
}
