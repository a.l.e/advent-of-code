#include <iostream>
#include <string>

#include <cassert>
#include <unordered_set>

int get_start_of_packet(std::string buffer, int n =4) {
    for (int i = 0; i < buffer.size() - n; i++) {
        std::unordered_set<char> unique{buffer.begin() + i, buffer.begin() + i + n};
        if (unique.size() == n) {
            // std::cout << ":::" << i << "\n";
            return i + n;
        }
    }
    return buffer.size();
}

int a() {
    std::string line;
    std::getline(std::cin, line);
    return get_start_of_packet(line);
}

int b() {
    std::string line;
    std::getline(std::cin, line);
    return get_start_of_packet(line, 14);
}

int main(int argc, char *argv[]) {
    std::string part = argc == 3 && std::string(argv[1]) == "-part" ? argv[2] : "";

    if (part == "") {
        std::cout << "Usage: " << argv[0] << " -part a|b [<< input.txt]\n";
    }

    if (part != "b") {
        assert(get_start_of_packet("mjqjpqmgbljsphdztnvjfqwrcgsmlb") == 7);
        assert(get_start_of_packet("bvwbjplbgvbhsrlpgdmjqwftvncz") ==  5);
        assert(get_start_of_packet("nppdvjthqldpwncqszvftbrmjlhg") == 6);
        assert(get_start_of_packet("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg") == 10);
        assert(get_start_of_packet("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw") == 11);

        std::cout << "a:\n" << a() << "\n";
    } else {
        std::cout << "b:\n" << b() << "\n";
    }
}
