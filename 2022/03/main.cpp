#include <iostream>
#include <istream>
#include <iterator>
#include <string>
#include <numeric>
#include <algorithm>
#include <ctype.h>

auto get_intersection(const std::string& a, const std::string& b) {
    std::string a_and_b{};
    std::set_intersection(a.begin(), a.end(), b.begin(), b.end(), std::back_inserter(a_and_b));
    return a_and_b;
}

int a() {
    return std::accumulate(std::istream_iterator<std::string>{std::cin}, {}, 0, [](const int a, const std::string& rucksack) {
        auto left = rucksack.substr(0, rucksack.size() / 2);
        auto right = rucksack.substr(rucksack.size() / 2, rucksack.size());
        std::sort(left.begin(), left.end());
        std::sort(right.begin(), right.end());
        auto common = get_intersection(left, right);
        int priority = std::islower(common.at(0)) ? 1 + common.at(0) - 'a' : 27 + common.at(0) - 'A';
        // std::cout << rucksack << " -> " << left << " / " << right << " == " << common.at(0) << "." << priority << std::endl;
        return a + priority;
    });
}

int b() {
    return std::accumulate(std::istream_iterator<std::string>{std::cin}, {}, 0, [](const int a, const std::string rucksack) {
        std::string common = rucksack;
        std::sort(common.begin(), common.end());
        for (int i = 0; i < 2; i++) {
            std::string line;
            std::getline(std::cin, line);
            std::sort(line.begin(), line.end());
            common = get_intersection(common, line);
        }
        return a + std::islower(common.at(0)) ? 1 + common.at(0) - 'a' : 27 + common.at(0) - 'A';
    });
}

int main(int argc, char *argv[]) {
    std::string part = argc == 3 && std::string(argv[1]) == "-part" ? argv[2] : "";

    if (part == "") {
        std::cout << "Usage: " << argv[0] << " -part a|b [<< input.txt]\n";
    }

    if (part != "b") {
        std::cout << a() << "\n";
    } else {
        std::cout << b() << "\n";
    }
}
