use std::env;

use std::io::{self, BufRead};
use std::collections::HashSet;
use std::iter::FromIterator;

fn char_to_priority(c: char) -> u32 {
    c as u32 -
        if c.is_ascii_lowercase() {
            'a' as u32 - 1
        } else {
            'A' as u32 - 27
        }
}

fn a() -> u32 {
    let mut priority = 0;
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        let mut right = line.unwrap().trim().to_string();
        let left = right.split_off(right.len() / 2);
        let common = HashSet::<_>::from_iter(left.chars())
            .intersection(&HashSet::from_iter(right.chars()))
            .into_iter().map(|a| *a)
            .next().unwrap();
        priority += char_to_priority(common);
    }
    priority
}

fn b() -> u32 {
    let mut priority = 0;
    let mut lines = io::stdin().lock().lines();
    while let Some(line) = lines.next() {
        let mut common: HashSet<char> = line.unwrap().chars().collect();
        for _ in 0..2 {
            // intersect two further lines with the set
            let line = lines.next().unwrap().unwrap();
            common = common.intersection(&HashSet::<_>::from_iter(line.chars())).map(|a| *a).collect();
        }
        priority += char_to_priority(common.iter().next().cloned().unwrap());
    }
    priority
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() == 1 || args[1] == "a" {
        println!("a: {}", a());
    } else if args[1] == "b" {
        println!("b: {}", b());
    } else {
        println!("Usage: {} [a|b] < input.txt", args[0]);
    }
}
