use std::env;

use std::io::{self, BufRead};

struct Assignment {
    a: (u32, u32), b: (u32, u32)
}
impl Assignment {
    fn from(v: Vec<u32>) -> Assignment {
        Assignment {
            a: (v[0], v[1]), b: (v[2], v[3])
        }
    }
}

fn a() -> usize{
    io::stdin().lock()
        .lines()
        .map(|l| Assignment::from(
            l.unwrap().trim()
                .split(&['-', ','])
                .map(|v| v.parse::<u32>().unwrap())
                .collect::<Vec<u32>>()))
        .filter(|v| 
            v.a.0 >= v.b.0 && v.a.1 <= v.b.1 ||
                v.b.0 >= v.a.0 && v.b.1 <= v.a.1)
        .count()
}

fn b() -> usize {
    io::stdin().lock()
        .lines()
        .map(|l| Assignment::from(
            l.unwrap().trim()
                .split(&['-', ','])
                .map(|v| v.parse::<u32>().unwrap())
                .collect::<Vec<u32>>()))
        .filter(|v| !(v.a.1 < v.b.0 || v.b.1 < v.a.0))
        .count()
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() == 1 || args[1] == "a" {
        println!("a: {}", a());
    } else if args[1] == "b" {
        println!("b: {}", b());
    } else {
        println!("Usage: {} [a|b] < input.txt", args[0]);
    }
}
