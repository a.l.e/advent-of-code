#include <iostream>
#include <algorithm>
#include <iterator>

struct Assignment
{
    int a0, a1, b0, b1;
};

std::istream& operator>>(std::istream& is, Assignment& a)
{
    char c;
    is >> a.a0 >> c >> a.a1 >> c >> a.b0 >> c >> a.b1;
    return is;
}

std::ostream& operator<<(std::ostream& os, const Assignment& a)
{
    return os << a.a0 << "-" << a.a1 << ", " << a.b0 << "-" << a.b1;
}

int a() {
    return std::count_if(std::istream_iterator<Assignment>{std::cin}, {},
        [](const auto& a) {
            // std::cout << a << "\n";
            return a.a0 >= a.b0 && a.a1 <= a.b1 ||
                   a.b0 >= a.a0 && a.b1 <= a.a1;
        }
    );
}

int b() {
    return std::count_if(std::istream_iterator<Assignment>{std::cin}, {},
        [](const auto& a) {
            return a.a0 <= a.b0 && a.a1 >= a.b0 ||
                   a.b0 <= a.a0 && a.b1 >= a.a0;
        }
    );
}

int main(int argc, char *argv[]) {
    std::string part = argc == 3 && std::string(argv[1]) == "-part" ? argv[2] : "";

    if (part == "") {
        std::cout << "Usage: " << argv[0] << " -part a|b [<< input.txt]\n";
    }

    if (part != "b") {
        std::cout << "a:\n" << a() << "\n";
    } else {
        std::cout << "b:\n" << b() << "\n";
    }
}
