
def line_to_array(line):
    return [[int(i) for i in a.split('-')] for a in line.split(',')]

def is_included(a):
    return a[0][0] <= a[1][0] and a[0][1] >= a[1][1] or a[0][0] >= a[1][0] and a[0][1] <= a[1][1]

def a(text):
    numberOfOverlaps = 0
    for line in text.split('\n'):
        assignment = line_to_array(line)
        if is_included(assignment):
            numberOfOverlaps += 1
    
    print(numberOfOverlaps)

def main():

	text = """
	2-4,6-8
	2-3,4-5
	5-7,7-9
	2-8,3-7
	6-6,4-6
	2-6,4-8
	"""

	a(text.strip())
   
main()
