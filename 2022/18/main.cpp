/**
 * Status: the current code gives the right result for the test data.
 * For the real input, the result is wrong. No idea why.
 */
#include <iostream>
#include <cassert>
#include <string>
#include <iterator>
#include <algorithm>
#include <unordered_map>
#include <map>
#include <set>
#include <unordered_set>
#include <array>
#include <numeric>

struct Position {
    int x;
    int y;
    int z;

    friend Position operator+(Position lhs, const Position& rhs) {
        lhs.x += rhs.x;
        lhs.y += rhs.y;
        lhs.z += rhs.z;
        return lhs;
    }
};

inline bool operator<(const Position& lhs, const Position& rhs) {
    if (lhs.x < rhs.x) {
        return true;
    }
    if (lhs.x == rhs.x) {
        if (lhs.y < rhs.y) {
            return true;
        }
        if (lhs.y == rhs.y) {
            return (lhs.z < rhs.z);
        }
    }
    return false;
}

struct PositionByY {
    bool operator()(const Position& lhs, const Position& rhs) const { 
        if (lhs.y < rhs.y) {
            return true;
        }
        if (lhs.y == rhs.y) {
            if (lhs.x < rhs.x) {
                return true;
            }
            if (lhs.x == rhs.x) {
                return lhs.z < rhs.z;
            }
        }
        return false;
    }
};

struct PositionByZ {
    bool operator()(const Position& lhs, const Position& rhs) const { 
        if (lhs.z < rhs.z) {
            return true;
        }
        if (lhs.z == rhs.z) {
            if (lhs.x < rhs.x) {
                return true;
            }
            if (lhs.x == rhs.x) {
                return lhs.y < rhs.y;
            }
        }
        return false;
    }
};

/**
 * like upper_bound (first element bigger) but but for less (first element smaller)
 *
 * inspired by https://stackoverflow.com/a/71280448
 */
template <typename T, typename U>
std::set<U>::iterator first_lesser_than(const T& set, const U& value) {
    if (set.size() == 0) {
        return set.end();
    }
    auto it = set.lower_bound(value);
    // value was found and is the first item in the set
    if (it == set.begin()) {
        return set.end();
    }
    return --it;
}

const std::istream& operator>>(std::istream& is, Position& position) {
    char c;
    std::string s;
    is >> position.x >> c >> position.y >> c >> position.z;
    return is;
}

inline bool operator==(const Position& lhs, const Position& rhs) {
    return lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z;
}

inline size_t hash_combine( size_t lhs, size_t rhs ) {
    lhs^= rhs + 0x9e3779b9 + (lhs << 6) + (lhs >> 2);
    return lhs;
}

struct PositionHash {
    size_t operator()(const Position& p) const {
        return hash_combine(hash_combine(p.x, p.y), p.z);
    }
};

std::ostream& operator<<(std::ostream& os, const Position& c) {
    return os << c.x << ", " << c.y << ", " << c.z;
}

using Cubes = std::unordered_map<Position, int, PositionHash>;
using Air_pockets = std::unordered_map<Position, int, PositionHash>;

using Cubes_by_x = std::set<Position>;
using Cubes_by_y = std::set<Position, PositionByY>;
using Cubes_by_z = std::set<Position, PositionByZ>;

struct Cubes_by {
    Cubes_by_x by_x;
    Cubes_by_y by_y;
    Cubes_by_z by_z;
};

const std::array<Position, 6> directions {{
    {0, 1, 0}, {1, 0, 0}, {0, -1, 0}, {-1, 0, 0},
    {0, 0, 1}, {0, 0, -1}
}};

int attach_to_neighbours(Cubes& cubes, const Position& p) {
    int neighbours_count = 0;
    for (const auto& direction: directions) {
        auto neighbour = p + direction;
        if (!cubes.contains(neighbour)) {
            continue;
        }
        cubes[neighbour] += 1;
        neighbours_count += 1;
    }
    return neighbours_count;
}

bool is_air_trapped(Cubes& cubes, const Cubes_by& cubes_by, const Position& p) {
    {
        // right
        const auto hitting = cubes_by.by_x.upper_bound(p);
        if (hitting == cubes_by.by_x.end() || hitting->x != p.x) {
            return false;
        }
        if (!(hitting->y == p.y || hitting->z == p.z)) {
            return false;
        }
    }
    {
        // left
        const auto hitting = first_lesser_than(cubes_by.by_x, p);
        if (hitting == cubes_by.by_x.end() || hitting->x != p.x) {
            return false;
        }
        if (!(hitting->y == p.y || hitting->z == p.z)) {
            return false;
        }
    }
    {
        // up
        const auto hitting = cubes_by.by_y.upper_bound(p);
        if (hitting == cubes_by.by_y.end() || hitting->y != p.y) {
            return false;
        }
        if (!(hitting->x == p.x || hitting->z == p.z)) {
            return false;
        }
    }
    {
        // down
        const auto hitting = first_lesser_than(cubes_by.by_y, p);
        if (hitting == cubes_by.by_y.end() || hitting->y != p.y) {
            return false;
        }
        if (!(hitting->x == p.x || hitting->z == p.z)) {
            return false;
        }
    }
    {
        // z + 1
        const auto hitting = cubes_by.by_z.upper_bound(p);
        if (hitting == cubes_by.by_z.end() || hitting->z != p.z) {
            return false;
        }
        if (!(hitting->x == p.x || hitting->y == p.y)) {
            return false;
        }
    }
    {
        // z - 1
        const auto hitting = first_lesser_than(cubes_by.by_z, p);
        if (hitting == cubes_by.by_z.end() || hitting->z != p.z) {
            return false;
        }
        if (!(hitting->x == p.x || hitting->y == p.y)) {
            return false;
        }
    }

    return true;
}

void process_air_pockets(Cubes& cubes, Air_pockets& air_pockets, const Cubes_by& cubes_by, const Position& p) {
    for (const auto& direction: directions) {
        auto neighbour = p + direction;
        if (cubes.contains(neighbour)) {
            continue;
        }
        if (air_pockets.contains(neighbour)) {
            continue;
        }

        air_pockets[neighbour] = 0;

        if (!is_air_trapped(cubes, cubes_by, neighbour)) {
            continue;
        }

        for (const auto direction: directions) {
            if (cubes.contains(neighbour + direction)) {
                air_pockets[neighbour] += 1;
            }
        }
        // std::cout << "n " << air_pockets.at(neighbour) << "\n";
    }
}

int a() {
    Cubes cubes;
    std::for_each(std::istream_iterator<Position>{std::cin}, {},
        [&cubes](const auto& p) {
            // std::cout << p << "\n";
            int neighbours_count = attach_to_neighbours(cubes, p);
            cubes[p] = neighbours_count;
        }
    );
    return cubes.size() * 6 - std::accumulate(cubes.begin(), cubes.end(), 0, [](const int a, const auto& v) {
        return a + v.second;
    });
}

int b() {
    Cubes cubes;
    Cubes_by cubes_by;

    std::for_each(std::istream_iterator<Position>{std::cin}, {},
        [&cubes, &cubes_by](const auto& p) {
            // std::cout << p << "\n";
            int neighbours_count = attach_to_neighbours(cubes, p);
            cubes[p] = neighbours_count;
            cubes_by.by_x.insert(p);
            cubes_by.by_y.insert(p);
            cubes_by.by_z.insert(p);
        }
    );

    Air_pockets air_pockets;
    std::for_each(cubes.begin(),cubes.end(), [&cubes, &air_pockets, &cubes_by](const auto& cube) {
            process_air_pockets(cubes, air_pockets, cubes_by, cube.first);
        }
    );

    return cubes.size() * 6 -
        std::accumulate(cubes.begin(), cubes.end(), 0, [](const int a, const auto& v) {
            return a + v.second;
        }) -
        std::accumulate(air_pockets.begin(), air_pockets.end(), 0, [](const int a, const auto& v) {
            return a + v.second;
        });
    // 4280 is too high
    // 2474 is too low
}

void tests() {
    // assert((Position{1, 1, 2} < Position{1, 1, 3}));
    // assert(!(Position{1, 1, 2} < Position{1, 1, 1}));
    // assert((Position{2, 1, 2} < Position{3, 1, 2}));
    // assert(!(Position{3, 1, 2} < Position{2, 1, 2}));
    // assert((Position{2, 1, 4} < Position{2, 1, 5}));

    // {
    //     std::set<Position> set;
    //     set.insert({2, 1, 5});
    //     set.insert({2, 1, 4});
    //     for (const auto& item: set) {
    //         std::cout << item << "\n";
    //     }
    // }

    {
        std::set<Position> set;
        set.insert({2, 1, 5});
        std::cout << *(set.lower_bound({2, 1, 4})) << "\n";
        std::cout << (set.lower_bound({2, 1, 4}) == set.end()) << "\n";
        std::cout << (first_lesser_than(set, Position({2, 1, 4})) == set.end()) << "\n";
    }
    // {
    //     std::set<Position> set;
    //     set.insert({2, 1, 4});
    //     std::cout << *(set.lower_bound({2, 1, 5})) << "\n";
    //     std::cout << (set.lower_bound({2, 1, 5}) == set.end()) << "\n";
    //     std::cout << (set.upper_bound({2, 1, 5}) == set.end()) << "\n";
    // }
    {
        // std::set<Position> set;
        // std::cout << (set.lower_bound({2, 1, 4}) == set.end()) << "\n";
        // std::cout << (set.upper_bound({2, 1, 4}) == set.end()) << "\n";
    }
}

int main(int argc, char *argv[]) {
    std::string part = argc == 3 && std::string(argv[1]) == "-part" ? argv[2] : "";

    if (part == "a") {
        std::cout << "a:\n" << a() << "\n";
    } else if (part == "b") {
        std::cout << "b:\n" << b() << "\n";
    } else if (part == "t") {
        tests();
    } else {
        std::cout << "Usage: " << argv[0] << " -part a|b|t [<< input.txt]\n";
    }
}
