#include <iostream>
#include <stdexcept>
#include <vector>
#include <string>
#include <tuple>
#include <numeric>

using Monitor = std::vector<std::string>;

std::ostream& operator<<(std::ostream& os, const Monitor& v)
{
    return os << std::accumulate(v.begin() + (v.empty() ? 0 : 1), v.end(), v.empty() ? "" : *v.begin(),
        [](std::string a, std::string v) { return a + "\n" + v; }) << "\n";
}

std::tuple<int, int> get_line() {
    int increment = 0;
    int processing = 0;
    std::string line;
    if (std::getline(std::cin, line)) {
        if (line != "noop") {
            increment = std::stoi(line.substr(5));
            processing = 1;
        }
    } else {
        throw std::length_error("Unexpected end of file");
    }
    return std::make_tuple(increment, processing);
}

int a() {
    int x = 1;
    int increment = 0;
    int processing = 0;
    int signal = 0;
    for (int i = 1; i <= 220; i++) {
        if (processing == 0) {
            x += increment;
            std::tie(increment, processing) = get_line();
            // std::cout << i << " :" << x << " (" << increment << ")" << "\n";
        } else {
            processing -= 1;
        }
        if ((i - 20) % 40 == 0) {
            signal += i * x;
        }
    }
    return signal;
}

/**
 * sprite_width = 3
 * x register is the position of the middle of the sprite
 * crt_size = (40, 6), 0..39
 * crt draws 1 pixel per clock cycle
 * crt draws # if its x is on the sprie
 */
Monitor b() {
    Monitor monitor(6, "");
    int cpu_x = 1;
    int increment = 0;
    int processing = 0;
    for (int crt_y = 0; crt_y < 6; crt_y++) {
        for (int crt_x = 0; crt_x < 40; crt_x++) {
            if (processing == 0) {
                cpu_x += increment;
                std::tie(increment, processing) = get_line();
            } else {
                processing -= 1;
            }
            if (crt_x >= cpu_x - 1 && crt_x <= cpu_x + 1) {
                monitor.at(crt_y) += '#';
            } else {
                monitor.at(crt_y) += '.';
            }
        }
    }
    return monitor;
}

int main(int argc, char *argv[]) {
    std::string part = argc == 3 && std::string(argv[1]) == "-part" ? argv[2] : "";

    if (part == "") {
        std::cout << "Usage: " << argv[0] << " -part a|b [<< input.txt]\n";
    }

    if (part != "b") {
        std::cout << "a:\n" << a() << "\n";
    } else {
        std::cout << "b:\n" << b() << "\n";
    }
}
