# Advent of code 2022

- 01: go through a list of numbers and find the groups (separated by empty lines) with the highest sum.
  - it should be ok to solve for anyone.
  - in rust one can use itertools to avoid looping through the lines.
- 02: scissor, rock, paper. 
  - beginners should first implement a _scissor, rock, paper_ game.
  - doable for anyone.
- 03: find a common character between strings.
  - not trivial for beginners, but should be doable (with some effort):
    - _convert_ a char to a number
    - find the common chars (intersection) between two strings (there are ready made implementations, but one should be able to create a _slow_ naive implementation)
- 05: find the overlapping spans
  - an easy one
  - reading the input follows a known pattern (split? regex? into a structure?)
  - a beginner can focus on the logic (which is not trivial but not that hard either)

## C++

### Reading the input

This is a summary of the type of input from the previous years and how to read it in c++:

### One value per line, processed as soon as read from standard input

```
12
1
56
```

Count the items that are bigger then the previous number

```cpp
#include <iostream>
#include <iterator>
#include <limits>
#include <algorithm>

int prec = std::numeric_limits<int>::max();
return std::count_if(std::istream_iterator<int>{std::cin}, {}, [&prec](int v) {
    bool r = v > prec;
    prec = v;
    return r;
});
```

If the values are in sections separated by empty lines:

```
12
1

5
3
56
```

```cpp
#include <iostream>
#include <algorithm>

// int max = std::numeric_limits<int>::min();

int max_value = 0;
int section_value = 0;
std::string line;
while (std::getline(std::cin, line)) {
    if (line == "") {
        max_value = std::max(max_value, section_value);
        section_value = 0;
    } else {
        section_value += std::stoi(line);
    }
}
max_value = std::max(max_value, section_value);
```

#### Two values per line, space separted, processed as soon as read from standard input

```
forward 3
down 4
forward 3
```
Calculating the current (final) position, according to a movement and a counter:

```cpp
#include <numeric>
#include <istream>
#include <iterator>

struct Command {
    std::string direction;
    int distance;
};

std::istream &operator>>(std::istream& is, Command& c) {
    is >> c.direction >> c.distance;
    return is;
}

struct Position {
    int x;
    int y;
};

Position p = std::accumulate(std::istream_iterator<Command>{std::cin}, {}, Position{0, 0}, [](Position a, Command c) {
    // std::cout << c << std::endl;
    if (c.direction == "forward") {
        a.x += c.distance;
    } else if (c.direction == "up") {
        a.y -= c.distance;
    } else if (c.direction == "down") {
        a.y += c.distance;
    }
    return a;
});
```

#### Read one column of values into a vector

```
12
1
56
```

```cpp
std::vector<int> data((std::istream_iterator<int>{std::cin}),
                       std::istream_iterator<int>{});
```

#### Read one row of comma separated values into a vector

```
12,1,56
```

```cpp
#include <sstream>

Numbers numbers{};

std::string line;
std::getline(std::cin, line);

std::istringstream iss(line);
std::string token;
while(std::getline(iss, token, ',')) {
    numbers.push_back(std::stoi(token));
}
```

### Read lines of digits

```
123
321
097
```

```cpp
using Matrix = std::vector<std::vector<int>>;

Matrix matrix;
for (std::string line; std::getline(std::cin, line); ) {
    std::vector<int> row{};
    for (char c: line) {
        row.push_back(c - '0');
    }
    matrix.push_back(row);
}
```

### Read lines of numbers

```cpp
for (std::string line; getline(input, line); ) {
    std::istringstream iss{line};
    int value;
    while (iss >> value) {
        std::cout << value << "\n";
    }
}
```

#### Read 5 x 5 boards of integers separated by spaces

```
67 97 50 51  1
47 15 77 31 66
24 14 55 70 52
76 46 19 32 73
34 22 54 75 17
```

```cpp
#include <sstream>
#include <iterator>

using BoardCell = std::pair<int, bool>;
using BoardRow = std::vector<BoardCell>;
struct Board {
    bool won{false};
    std::vector<BoardRow> board{};
    auto begin() { return board.begin(); }
    auto end() { return board.end(); }
    void push_back(BoardRow& v) { board.push_back(v); }
};
using Boards = std::vector<Board>;

Boards boards{};

std::string line;
while (std::getline(std::cin, line)) {
    Board board{};
    for (int i = 0; i < 5; i++) {
        std::getline(std::cin, line);
        std::istringstream iss{line};
        std::vector<int> numbers{ std::istream_iterator<int>(iss), {} };
        BoardRow row{};
        for (auto v: numbers) {
            row.push_back(BoardCell{v, false});
        }
        board.push_back(row);
    }
    boards.push_back(board);
}
```

#### Read rows of values with separators

read the four integer values and _ignore_ the `,` and `->` separators.

```
452,244 -> 452,303
958,109 -> 958,639
```

```cpp
#include <iostream>
#include <algorithm>
#include <iterator>

struct Line {
    int x1, y1, x2, y2;
};

std::istream& operator>>(std::istream& is, Line& line) {
    char c;
    std::string s;
    is >> line.x1 >> c >> line.y1 >> s >> line.x2 >> c >> line.y2;
    return is;
}

std::for_each(std::istream_iterator<Line>{std::cin}, {},
    [](const auto& line) {
    }
);
```

#### Read rows split by a char

```
ecdbfag deacfb acdgb cdg acdbf gdfb efacdg gd cagdbf beacg | cdg dcebgaf gbdf bdacg
fadecg gdbecaf agbfd fgdcb gab ebagdf feabcg deab gdefa ab | adfbg ab fcgdbae bfgecda
```

```cpp
#include <sstream>

for (std::string line; std::getline(std::cin, line); ) {
    std::string left, right;
    {
        std::istringstream iss(line);
        std::getline(iss, left, '|');
        std::getline(iss, right);
    }
}
```

### cout and cin

#### Reading from and printing a custom struct

```
struct Command {
    std::string direction;
    int distance;

};

std::istream &operator>>(std::istream& is, Command& c) {
    is >> c.direction >> c.distance;
    return is;
}

std::ostream& operator<<(std::ostream& os, const Command& c) {
    return os << c.direction << " / " << c.distance;
}
```

#### Print a vector of ...

```cpp
#include <numeric>

std::ostream& operator<<(std::ostream& os, const std::vector<std::string>& v) {
    return os << "{" << std::accumulate(v.begin() + (v.empty() ? 0 : 1), v.end(), v.empty() ? "" : *v.begin(),
        [](std::string a, std::string v) { return a + ", " + v; }) << "}";
}

std::ostream& operator<<(std::ostream& os, const std::vector<int>& v) {
    return os << "{" << std::accumulate(v.begin() + (v.empty() ? 0 : 1), v.end(), v.empty() ? "" : std::to_string(*v.begin()),
        [](std::string a, int v) { return a + ", " + std::to_string(v); }) << "}";
}
```

### Padding the numeric output

```cpp
#include <iomanip>
std::cout << std::setw(2) << distances.at({j, i}) << " ";
```

### Data structures and functions

#### Map of pairs with comparator

```cpp
inline size_t hash_combine( size_t lhs, size_t rhs ) {
    lhs^= rhs + 0x9e3779b9 + (lhs << 6) + (lhs >> 2);
    return lhs;
}

struct IntPairHash {
    size_t operator()(const std::pair<int, int>& p) const {
        return hash_combine(p.first, p.second);
    }
};

using Map = std::unordered_map<std::pair<int, int>, int, IntPairHash>;
```

With a custom structure, you need to implement `==`, too:

```cpp
struct Position {
    int x;
    int y;
};

inline bool operator==(const Position& lhs, const Position& rhs) {
    return lhs.x == rhs.x && lhs.y == rhs.y;
}

inline size_t hash_combine( size_t lhs, size_t rhs ) {
    lhs^= rhs + 0x9e3779b9 + (lhs << 6) + (lhs >> 2);
    return lhs;
}

struct PositionHash {
    size_t operator()(const Position& p) const {
        return hash_combine(p.x, p.y);
    }
};

using Map = std::unordered_map<Position, int, PositionHash>;

// for an "ordered" set
inline bool operator<(const Position& lhs, const Position& rhs) {
      if (lhs.x < rhs.x) {
          return true;
      }
      if (lhs.x == rhs.x) {
          return lhs.y < rhs.y;
      }
      return false;
}

using World = std::set<Position>;
```

#### Zipping n iterators

```cpp
// https://stackoverflow.com/a/18771618/5239250 cc by sa aaronman
template <typename Iterator>
    void advance_all (Iterator & iterator) {
        ++iterator;
    }
template <typename Iterator, typename ... Iterators>
    void advance_all (Iterator & iterator, Iterators& ... iterators) {
        ++iterator;
        advance_all(iterators...);
    } 
template <typename Function, typename Iterator, typename ... Iterators>
    Function zip (Function func, Iterator begin, 
            Iterator end, 
            Iterators ... iterators)
    {
        for(;begin != end; ++begin, advance_all(iterators...))
            func(*begin, *(iterators)... );
        //could also make this a tuple
        return func;
    }
```

Usage:

```cpp
zip([](int a, int b) {
        // do something, mostly with a captured variable
    }, a_list.begin(), a_list.end(), b.begin());
```

#### Split string by char
```
1,2,3,4
```

```cpp
#include <sstream>

std::vector<int> split(std::string text, char delimiter) {
    std::vector<int> tokens{};
    std::istringstream iss(text);
    std::string token;
    while(std::getline(iss, token, delimiter)) {
        tokens.push_back(std::stoi(token));
    }
    return tokens;
}
```

#### Split a string by string

```
1, 2, 3, 4
```

```cpp
#include <vector>
// c++ 20
#include <ranges>
#include <string_view>

std::vector<std::string_view> split(std::string_view text, std::string_view delimiter) {
    std::vector<std::string_view> tokens{};
    for (const auto token: std::views::split(text, delimiter)) {
        tokens.push_back(std::string_view{token.begin(), token.end()});
    }
    return tokens;
}

for (const auto& token: split(line, " -> ")) {
}

#include <charconv>

std::vector<int> split_int(std::string_view text, std::string_view delimiter) {
    std::vector<int> tokens{};
    for (const auto token: std::views::split(text, delimiter)) {
        int token_int;
        std::from_chars(token.data(), token.data() + token.size(), token_int);
        tokens.push_back(token_int);
    }
    return tokens;
}
```

#### Split a string into words

```cpp
#include <sstream>

std::istringstream iss(text);
counter +=  std::for_each(std::istream_iterator<std::string>{iss}, {}, [](std::string v) {
});
```

#### String contains char

```cpp
bool str_contains(std::string s, char c) {
    return s.find(c) != std::string::npos;
}
```

#### Intersection of strings

The strings must be sorted:

```cpp
std::sort(a.begin(), b.end());
```


```cpp
#include <algorithm>
#include <iterator>

auto get_intersection(const std::string& a, const std::string& b) {
    std::string a_and_b{};
    std::set_intersection(a.begin(), a.end(), b.begin(), b.end(), std::back_inserter(a_and_b));
    return a_and_b;
}
```

#### Intersection of strings

```cpp
auto get_difference(const std::string& a, const std::string& b) {
    std::string a_and_b{};
    std::set_difference(a.begin(), a.end(), b.begin(), b.end(), std::back_inserter(a_and_b));
    return a_and_b;
}
```

#### Using litteral strings

```cpp
using namespace std::string_literals;

auto s = "abc"s;
```
### Check for integer overflow

```cpp
// https://stackoverflow.com/a/1514309
// adding
if (item > std::numeric_limits<unsigned long long>::max() - operation_value) {
    std::cout << "+ overflow" << "\n";
}
// multiplication
if (item > std::numeric_limits<unsigned long long>::max() / operation_value) {
    std::cout << "* overflow" << "\n";
}
```

### Gdb

```
$ gdb ./16
(gdb) run -part a < test.txt
(gdb) bt
```

## Rust

```sh
rustc -o 03 main.rs && ./03r a < test.txt
```

For using Cargo:

- First copy the `00/main.rs` file into the current directory.
- `cargo init` will try to name the project after the directory, but will fail because its name starts with a digit.
- run `cargo init --name r##` (where ## is the name of the directory) to make the project name valid and the binary with the same name different than the one created by C++.

### Read a file line by line

```
abc
def
ghi
```

```rs
use std::io::{self, BufRead};

let stdin = io::stdin();
for line in stdin.lock().lines() {
}
```

Read ints from a file, line by line

```
1
2
3
4
```

```rs
let stdin = io::stdin();
for line in stdin.lock().lines() {
    elf_calories = match line.unwrap().trim().parse::<u32>() {
        Ok(num) => num,
        Err(_) => continue,
    };
}
```

or

```rs
io::stdin().lock()
    .lines()
    .map(|v| v.unwrap().parse::<u32>().ok())
    .sum()
```
        
### Read a matrix of integers

```
123
456
789
```

```rs
use std::io::{self, BufRead};

let matrix: Vec<Vec<u32>> = io::stdin().lock()
    .lines()
    .map(|r| r.unwrap().trim().chars()
        .map(|v| v.to_digit(10).unwrap()).collect::<Vec<u32>>())
    .collect();
println!("{:?}", matrix);
```

### Read a file with groups of lines

```rs
// cargo add itertools
use std::io::{self, BufRead};
use itertools::Itertools;

io::stdin().lock()
    .lines()
    .map(|v| v.unwrap().parse::<u32>().ok())
    .batching(|it| {
        let mut sum = None;
        while let Some(Some(v)) = it.next() {
            sum = Some(sum.unwrap_or(0) + v);
        }
        sum
    })
    .max()
    .unwrap_or(0)
```

### Read a file into a struct

```
2-4,6-8
2-3,4-5
5-7,7-9
```

```rs
#[derive(Debug)]
struct Assignment {
    a: (u32, u32), b: (u32, u32)
}

impl Assignment {
    fn from(v: Vec<u32>) -> Assignment {
        Assignment {
            a: (v[0], v[1]), b: (v[2], v[3])
        }
    }
}

fn a() -> usize{
    io::stdin().lock()
        .lines()
        .map(|l| Assignment::from(
            l.unwrap().trim()
                .split(&['-', ','])
                .map(|v| v.parse::<u32>().unwrap())
                .collect::<Vec<u32>>()))
        .filter(|v| 
            v.a.0 >= v.b.0 && v.a.1 <= v.b.1 ||
                v.b.0 >= v.a.0 && v.b.1 <= v.a.1)
        .count()
}
```

Reading the ints and ignoring the strings

```
move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
```

```rs
struct Rearrangement {
    fn from(l: String) -> Rearrangement {
        let v = l.trim().split(' ')
            .filter_map(|v| v.parse::<usize>().ok())
            .collect::<Vec<usize>>();
        Rearrangement {
                count: v[0], from: v[1], to: v[2]
        }
    }
}
```
### Aliasing a type

```rs
type Matrix = Vec<Vec<u32>>;
```

### Running tests

Create a test function anywhere in the file (at the bottom?)

```rs
#[test]
fn it_works() {
    assert_eq!(2 + 2, 4);
}
```

run

```
cargo test
```
