#include <iostream>
#include <iomanip>
#include <string>

#include <ranges>
#include <string_view>
#include <vector>
#include <array>
#include <charconv>
#include <set>
#include <algorithm>

struct Position {
    int x;
    int y;
};

inline bool operator<(const Position& lhs, const Position& rhs)
{
      if (lhs.x < rhs.x) {
          return true;
      }
      if (lhs.x == rhs.x) {
          return lhs.y < rhs.y;
      }
      return false;
}

std::ostream& operator<<(std::ostream& os, const Position& p)
{
    return os << p.x << ", " << p.y;
}

std::vector<std::string_view> split(std::string_view text, std::string_view delimiter) {
    std::vector<std::string_view> tokens{};
    for (const auto token: std::views::split(text, delimiter)) {
        tokens.push_back(std::string_view{token.begin(), token.end()});
    }
    return tokens;
}

std::vector<int> split_int(std::string_view text, std::string_view delimiter) {
    std::vector<int> tokens{};
    for (const auto token: std::views::split(text, delimiter)) {
        int token_int;
        std::from_chars(token.data(), token.data() + token.size(), token_int);
        tokens.push_back(token_int);
    }
    return tokens;
}

using World = std::set<Position>;

void render(const World& world) {
    const auto minmax_x = std::minmax_element(world.begin(), world.end(),
        [](const auto& a, const auto& b) {
            return a.x < b.x;
        });
    const auto minmax_y = std::minmax_element(world.begin(), world.end(),
        [](const auto& a, const auto& b) {
            return a.y < b.y;
        });
    // std::cout << std::get<0>(minmax_x)->x << ", " << std::get<1>(minmax_x)->x << "\n";
    for (int y = std::get<0>(minmax_y)->y; y <= std::get<1>(minmax_y)->y; y++) {
        std:: cout << std::setw(3)  << y << " ";
        for (int x = std::get<0>(minmax_x)->x; x <= std::get<1>(minmax_x)->x; x++) {
            std::cout << (world.contains({x, y}) ? '#' : ' ');
        }
        std::cout << "\n";
    }
}

void read_world(World& world) {
    for (std::string line; std::getline(std::cin, line); ) {
        std::vector<Position> path{};
        for (const auto& token: split(line, " -> ")) {
            const auto& position = split_int(token, ",");
            path.push_back(Position{position.at(0), position.at(1)});
        }
        Position p0 = path.at(0);
        for (int i = 1; i < path.size(); i++) {
            Position p1 = path.at(i);
            for (int y = std::min(p0.y, p1.y); y <= std::max(p0.y, p1.y); y++) {
                for (int x = std::min(p0.x, p1.x); x <= std::max(p0.x, p1.x); x++) {
                    world.insert({x, y});
                }
            }
            p0 = p1;
        }
    }
}

int a() {
    World world;
    read_world(world);

    int i = 0;
    bool found = false;
    while (!found) {
        Position start{500, 0};
        Position end = start;
        bool stuck = false;
        while (!stuck) {
            const auto hitting = world.lower_bound({start.x, start.y});
            if (hitting == world.end()  || hitting->x > start.x) {
                found = true;
                break;
            }
            // std::cout << "h " << *hitting << "\n";

            if (!world.contains({start.x - 1, hitting->y})) {
                start = {start.x - 1, hitting->y};
                continue;
            }
            if (!world.contains({start.x + 1, hitting->y})) {
                start = {start.x + 1, hitting->y};
                continue;
            }

            end = {hitting->x, hitting->y - 1};
            stuck = true;
        }
        if (!found) {
            world.insert(end);
            i++;
        }
    }

    // render(world);

    return i;
}

int b() {
    World world;
    read_world(world);

    const auto max_y = std::max_element(world.begin(), world.end(),
        [](const auto& a, const auto& b) {
            return a.y < b.y;
        });
    int bottom = max_y->y + 2;
    // std::cout << bottom << "\n";

    int i = 0;
    bool found = false;
    while (!found) {
        Position start{500, 0};
        Position end = start;
        bool stuck = false;
        while (!stuck) {
            const auto hitting_it = world.lower_bound({start.x, start.y});

            const auto hitting = hitting_it == world.end() || hitting_it->x > start.x ?
                Position{start.x, bottom} :
                *hitting_it;
            // std::cout << "h " << hitting << "\n";

            if (hitting.y == bottom) {
                end = {hitting.x, hitting.y - 1};
                break;
            }

            if (!world.contains({start.x - 1, hitting.y})) {
                start = {start.x - 1, hitting.y};
                continue;
            }
            if (!world.contains({start.x + 1, hitting.y})) {
                start = {start.x + 1, hitting.y};
                continue;
            }

            if (hitting.y == 1) {
                found = true;
                break;
            }

            end = {hitting.x, hitting.y - 1};
            stuck = true;
        }
        world.insert(end);
        i++;

        // if (i >= 100) {
        //     break;
        // }
    }

    // render(world);

    return i;
}

void tests() {
}

int main(int argc, char *argv[]) {
    std::string part = argc == 3 && std::string(argv[1]) == "-part" ? argv[2] : "";

    if (part == "a") {
        std::cout << "a:\n" << a() << "\n";
    } else if (part == "b") {
        std::cout << "b:\n" << b() << "\n";
    } else if (part == "t") {
        tests();
    } else {
        std::cout << "Usage: " << argv[0] << " -part a|b|t [<< input.txt]\n";
    }
}
