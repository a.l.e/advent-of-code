#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <functional>
#include <utility>
#include <sstream>
#include <optional>
#include <algorithm>
#include <numeric>

using namespace std::string_literals;

struct Monkey {
   unsigned long long inspected;
   std::queue<unsigned long long> items; 
   char operation_op;
   std::optional<int> operation_value;
   int test_divisible;
   std::pair<int, int> targets; // false = 0, true = 1
};

#include <ranges>
#include <string_view>
#include <charconv>


std::queue<unsigned long long> split_items(std::string_view text, std::string_view delimiter) {
    std::queue<unsigned long long> tokens{};
    for (const auto token: std::views::split(text, delimiter)) {
        unsigned long long token_int;
        std::from_chars(token.data(), token.data() + token.size(), token_int);
        tokens.push(token_int);
    }
    return tokens;
}

void read_monkeys(std::vector<Monkey>& monkeys) {
    std::string line;
    for (std::string line; std::getline(std::cin, line); ) {
        std::getline(std::cin, line);
        auto items = split_items(line.substr("  Starting items: "s.size()), ", ");
        std::getline(std::cin, line);
        line = line.substr("  Operation: new = old "s.size());
        const auto operation_op = line.at(0);
        line = line.substr(2);
        std::optional<int> operation_value = line == "old" ? std::nullopt : std::make_optional(std::stoi(line));
        std::getline(std::cin, line);
        int test_divisible = std::stoi(line.substr("  Test: divisible by "s.size()));
        std::getline(std::cin, line);
        int test_target_true = std::stoi(line.substr("    If true: throw to monkey "s.size()));
        std::getline(std::cin, line);
        int test_target_false = std::stoi(line.substr("    If false: throw to monkey "s.size()));
        monkeys.push_back(Monkey{
           0,
           items,
           operation_op,
           operation_value,
           test_divisible,
           std::make_pair(test_target_false, test_target_true)
        });
        std::getline(std::cin, line);
    }
}

int a() {
    std::vector<Monkey> monkeys{};
    read_monkeys(monkeys);
    for (int i = 0; i < 20; i++) {
        for (auto& monkey: monkeys) {
            monkey.inspected += monkey.items.size();
            while (!monkey.items.empty()) {
                auto item = monkey.items.front();
                monkey.items.pop();
                int operation_value = monkey.operation_value.has_value() ? monkey.operation_value.value() : item;
                if (monkey.operation_op == '+') {
                    item += operation_value;
                } else if (monkey.operation_op == '*') {
                    item *= operation_value;
                }
                item /= 3;
                int target = item % monkey.test_divisible == 0 ?
                    std::get<1>(monkey.targets) :
                    std::get<0>(monkey.targets);
                // std::cout << target << ": " << item << "\n";
                monkeys.at(target).items.push(item);
            }
        }
    }
    std::sort(monkeys.begin(), monkeys.end(), [](const auto& a, const auto b) {
        return a.inspected > b.inspected;
    });
    return std::accumulate(monkeys.begin(), monkeys.begin() + 2, 1, [](int a, const auto monkey) {
        return a * monkey.inspected;
    });
}

unsigned long long  b() {
    std::vector<Monkey> monkeys{};
    read_monkeys(monkeys);

    const unsigned long long lcm = std::accumulate(monkeys.begin(), monkeys.end(), 1ull, [](auto a, const auto& v) {
        return a * v.test_divisible;
    });
    // std::cout << "lcm " << lcm << "\n";

    for (int i = 0; i < 10000; i++) {
        for (auto& monkey: monkeys) {
            monkey.inspected += monkey.items.size();
            while (!monkey.items.empty()) {
                auto item = monkey.items.front();
                monkey.items.pop();
                int operation_value = monkey.operation_value.has_value() ? monkey.operation_value.value() : item;
                if (monkey.operation_op == '+') {
                    if (item > std::numeric_limits<unsigned long long>::max() - operation_value) {
                        std::cout << "+ overflow" << "\n";
                    }
                    item += operation_value;
                } else if (monkey.operation_op == '*') {
                        if (item > std::numeric_limits<unsigned long long>::max() / operation_value) {
                            std::cout << "* overflow" << "\n";
                        }
                        item *= operation_value;
                }

                item %= lcm; // <- keep the number small!

                int target = item % monkey.test_divisible == 0 ?
                    std::get<1>(monkey.targets) :
                    std::get<0>(monkey.targets);
                monkeys.at(target).items.push(item);
            }
        }
    }
    // for (const auto& monkey: monkeys) {
    //     std::cout << monkey.inspected << "\n";
    // }
    std::sort(monkeys.begin(), monkeys.end(), [](const auto& a, const auto b) {
        return a.inspected > b.inspected;
    });
    return std::accumulate(monkeys.begin(), monkeys.begin() + 2, 1ull, [](auto a, const auto& monkey) {
        return a * monkey.inspected;
    });
}

int main(int argc, char *argv[]) {
    std::string part = argc == 3 && std::string(argv[1]) == "-part" ? argv[2] : "";

    if (part == "") {
        std::cout << "Usage: " << argv[0] << " -part a|b [<< input.txt]\n";
    }

    if (part != "b") {
        std::cout << "a:\n" << a() << "\n";
    } else {
        std::cout << "b:\n" << b() << "\n";
    }
}
