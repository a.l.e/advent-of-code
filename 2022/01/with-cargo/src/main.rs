use std::env;

use std::io::{self, BufRead};
use itertools::Itertools;

// adapter from https://fasterthanli.me/series/advent-of-code-2022/part-1
fn a() -> u32{
    // using itertool's bachting to sum groups of lines
    io::stdin().lock()
        .lines()
        .map(|v| v.unwrap().parse::<u32>().ok())
        // https://docs.rs/itertools/0.10.5/itertools/trait.Itertools.html#method.batching
        .batching(|it| {
            let mut sum = None;
            while let Some(Some(v)) = it.next() {
                sum = Some(sum.unwrap_or(0) + v);
            }
            sum
        })
        .max()
        .unwrap_or(0)
}

fn b() -> u32 {
    // using itertool's bachting to sum groups of lines
    io::stdin().lock()
        .lines()
        .map(|v| v.unwrap().parse::<u32>().ok())
        .batching(|it| {
            let mut sum = None;
            while let Some(Some(v)) = it.next() {
                sum = Some(sum.unwrap_or(0) + v);
            }
            sum
        })
        .sorted_by_key(|&v| std::cmp::Reverse(v))
        .take(3)
        .sum()
}

fn main() {
    let args: Vec<String> = env::args().collect();
    // dbg!(&args);
    if args.len() == 1 || args[1] == "a" {
        println!("a: {}", a());
    } else if args[1] == "b" {
        println!("b: {}", b());
    } else {
        println!("Usage: {} [a|b] < input.txt", args[0]);
    }
}
