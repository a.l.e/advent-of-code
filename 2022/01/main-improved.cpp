#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <numeric>

int a() {
    return 0;
}

int b() {
    std::vector<int> elves{0};
    std::string line;
    while (std::getline(std::cin, line)) {
        if (line == "") {
            elves.push_back(0);
        } else {
            elves.back() += std::stoi(line);
        }
    }
    std::partial_sort(elves.begin(), elves.begin() + 3, elves.end(), std::greater<int>{});
    return std::accumulate(elves.begin(), elves.begin() + 3, 0);
}

int main(int argc, char *argv[]) {
    std::string part = argc == 3 && std::string(argv[1]) == "-part" ? argv[2] : "";

    if (part == "") {
        std::cout << "Usage: " << argv[0] << " -part a|b [<< input.txt]\n";
    }

    if (part != "b") {
        std::cout << a() << "\n";
    } else {
        std::cout << b() << "\n";
    }
}

