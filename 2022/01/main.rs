use std::env;

use std::io::{self, BufRead};
use std::cmp;
use std::collections::BinaryHeap;
use std::cmp::Reverse;

fn a() -> u32{
    let stdin = io::stdin();
    let mut max_calories = 0;
    let mut elf_calories = 0;
    for line in stdin.lock().lines() {
        elf_calories = match line.unwrap().trim().parse::<u32>() {
            Ok(num) => elf_calories + num,
            Err(_) => {
                max_calories = cmp::max(max_calories, elf_calories);
                0
            },
        };
    }
    cmp::max(max_calories, elf_calories)
}

fn add_to_best_3(best_3: &mut BinaryHeap::<Reverse<u32>>, value: u32) {
    best_3.push(Reverse(value));
    if best_3.len() > 3 {
        best_3.pop();
    }
}

fn b() -> u32 {
    let stdin = io::stdin();
    let mut best_3 = BinaryHeap::new();
    let mut elf_calories = 0;
    for line in stdin.lock().lines() {
        elf_calories = match line.unwrap().trim().parse::<u32>() {
            Ok(num) => elf_calories + num,
            Err(_) => {
                add_to_best_3(&mut best_3, elf_calories);
                0
            },
        };
    }
    add_to_best_3(&mut best_3, elf_calories);
    // println!("{:?}", best_3);
    // Reverse being a tuple struct with a single public field x.0 copies the value of the field out of it
    best_3.iter().map(|v| v.0).sum()
}

fn main() {
    let args: Vec<String> = env::args().collect();
    // dbg!(&args);
    if args.len() == 1 || args[1] == "a" {
        println!("{}", a());
    } else if args[1] == "b" {
        println!("{}", b());
    } else {
        println!("Usage: {} [a|b] < input.txt", args[0]);
    }
}
