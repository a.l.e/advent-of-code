#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <numeric>

std::ostream& operator<<(std::ostream& os, const std::vector<int>& v)
{
    return os << "{" << std::accumulate(v.begin() + (v.empty() ? 0 : 1), v.end(), v.empty() ? "" : std::to_string(*v.begin()),
        [](std::string a, int v) { return a + ", " + std::to_string(v); }) << "}";
}

int a() {
    int max_food = 0;
    int elf_food = 0;
    std::string line;
    while (std::getline(std::cin, line)) {
        if (line == "") {
            max_food = std::max(max_food, elf_food);
            elf_food = 0;
        } else {
            elf_food += std::stoi(line);
        }
    }
    max_food = std::max(max_food, elf_food);
    return max_food;
}

int b() {
    std::vector<int> elves{};
    std::string line;
    int elf_food = 0;
    while (std::getline(std::cin, line)) {
        if (line == "") {
            elves.push_back(elf_food);
            elf_food = 0;
        } else {
            elf_food += std::stoi(line);
        }
    }
    if (elf_food > 0) {
        elves.push_back(elf_food);
    }
    std::sort(elves.begin(), elves.end(), std::greater<int>());
    // std::cout << elves << "\n";
    return std::accumulate(elves.begin(), elves.begin() + 3, 0,
        [](int a, int v) { return a + v; });
}

int main(int argc, char *argv[]) {
    std::string part = argc == 3 && std::string(argv[1]) == "-part" ? argv[2] : "";

    if (part == "") {
        std::cout << "Usage: " << argv[0] << " -part a|b [< input.txt]\n";
    }

    if (part != "b") {
        std::cout << a() << "\n";
    } else {
        std::cout << b() << "\n";
    }
}
