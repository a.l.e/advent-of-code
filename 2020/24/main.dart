import 'dart:io';
import 'package:quiver/core.dart';
import 'package:trotter/trotter.dart';

const demo = false;
// using boundaries, first gets the min and max
// and then checks all possible cells inside of the boundaries:
// it takes about 20 minutes on my computer.
// checking the neighbourhood of the living cells takes about 10 seconds...
const use_boundaries = false;

class Coord {
  final int x;
  final int y;
  final int z;

  Coord(this.x, this.y, this.z);

  clone() => Coord(x, y, z);

  @override
  toString() => '{$x, $y, $z}';

  @override
  bool operator ==(o) => x == o.x && y == o.y && z == o.z;

  // https://stackoverflow.com/questions/20577606/whats-a-good-recipe-for-overriding-hashcode-in-dart
  @override
  int get hashCode => hash3(x.hashCode, y.hashCode, z.hashCode);

  @override
  Coord operator +(o) => Coord(x + o.x, y + o.y, z + o.z);

  @override
  List<Object> get props => [x, y, z];
}

// https://stackoverflow.com/questions/2459402/hexagonal-grid-coordinates-to-pixel-coordinates
final movement = {
  'e'  : () => Coord( 1, -1,  0),
  'se' : () => Coord( 1,  0, -1),
  'sw' : () => Coord( 0,  1, -1),
  'w'  : () => Coord(-1,  1,  0),
  'nw' : () => Coord(-1,  0,  1),
  'ne' : () => Coord( 0, -1,  1)
};

final directions = Permutations(3, [-1, 0, 1])()
  .map((e) => Coord(e[0], e[1], e[2])); // trotter

count_neighbours(world, coord) {
  return directions
    .fold(0, (a, e) => a + (world.contains(coord + e) ? 1 : 0));
}

get_neighbours(coord) {
  return directions
    .fold(Set<Coord>(), (a, e) {
      a.add(coord + e);
      return a;
    });
}

get_world_boundaries(world) {
  return world
    .fold(List<Coord>.from([world.first.clone(), world.first.clone()]), (a, e) {
      a[0] = Coord(
        (a[0].x < e.x) ? a[0].x : e.x,
        (a[0].y < e.y) ? a[0].y : e.y,
        (a[0].z < e.z) ? a[0].z : e.z);
      a[1] = Coord(
        (a[1].x > e.x) ? a[1].x : e.x,
        (a[1].y > e.y) ? a[1].y : e.y,
        (a[1].z > e.z) ? a[1].z : e.z);
      return a;
    });
}

next_world(world) {
  var next = Set<Coord>();
  var inactives = Set<Coord>(); // speed up the resurection
  for (final c in world) {
    final n = count_neighbours(world, c);
    if (n == 1 || n == 2) {
      next.add(c.clone());
    }
    if (!use_boundaries) {
      for (final neighbour in get_neighbours(c)) {
        if (!inactives.contains(neighbour) && !world.contains(neighbour) && !next.contains(neighbour)) {
          final n = count_neighbours(world, neighbour);
          if (n == 2) {
            next.add(neighbour.clone());
          } else {
            inactives.add(neighbour.clone());
          }
        }
      }
    }
  }
  final boundaries = get_world_boundaries(world);
  if (use_boundaries) {
    for (var z = boundaries[0].z - 1; z <= boundaries[1].z + 1; z++) {
      for (var y = boundaries[0].y - 1; y <= boundaries[1].y + 1; y++) {
        for (var x = boundaries[0].x - 1; x <= boundaries[1].x + 1; x++) {
          
          final c = Coord(x, y, z);
          if (!world.contains(c) && count_neighbours(world, c) == 2) {
            next.add(c);
          }
        }
      }
    }
  }
  return(next);
}

main() {
  const path = 'input${demo ? '-demo' : ''}.txt';
  final List<List<String>> movements = File(path)
    .readAsLinesSync()
    .map((l) => l.split('')
      .fold<List<String>>([], (a, c) {
        if (a.length > 0 && (a.last == 's' || a.last == 'n')) {
          a.last += c;
        } else {
          a.add(c);
        }
        return a;
      })
    ).toList();
  // print(movements);

  var tiles = Set<Coord>();

  for (final flipping in movements) {
    var pos = Coord(0, 0, 0);
    for (final direction in flipping) {
      pos = pos + movement[direction]();
    }
    if (tiles.contains(pos)) {
      tiles.remove(pos);
    } else {
      tiles.add(pos);
    }
  }
  print('part 1: ${tiles.length}');

  const n = 100;
  for (int i = 0; i < n; i++) {
    print(i);
    tiles = next_world(tiles);
  }
  // print(tiles);
  print('part 2: ${tiles.length}');
}
