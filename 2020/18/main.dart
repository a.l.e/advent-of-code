import 'dart:io';

const demo = true;
const part_1 = false;

final operate = {
  '+': (a, e) => a + e,
  '*': (a, e) => a * e,
};

List<int> calculate(String line) {
  var result = part_1 ? 0 : 1;
  var buffer = 0;
  var operator = '+';
  for (var i = 0; i < line.length; i++) {
    final c = line[i];
    if (c == '(') {
      final done = calculate(line.substring(i + 1));
      result = operate[operator](result * buffer, done[0]);
      buffer = 0;
      i += done[1];
    } else if (c == ')') {
      print('r ${result * buffer}');
      return [result * buffer, i + 1];
    } else if (c == ' ') {
    } else if (c == '+') {
      operator = '+';
    } else if (c == '*') {
      operator = '*';
    } else {
      final value = int.parse(c);
      if (part_1) {
        result = operate[operator](result, value);
      } else {
        if (operator == '+') {
          buffer += value;
        } else {
          result *= buffer;
          buffer = value;
        }
      }
      print('$operator v: $value b: $buffer = $result');
    }
    // stdout.write(c);
  }
  // print('');
  return [result * buffer, line.length];
}

main() {
  const path = 'input${demo ? '-demo' : ''}.txt';
  final lines = File(path)
    .readAsLinesSync();

  for (final line in lines.sublist(1)) {
    print(line);
    final n = calculate(line);
    print(n[0]);
    break;
  }

  // print(lines.
  //   fold<int>(0, (a, e) => a + calculate(e)[0]));

}
