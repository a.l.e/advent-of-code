import 'dart:io';
import 'package:trotter/trotter.dart';
import 'dart:collection';
import 'dart:math';

main() {
  final demo = false;
  final path = 'input${demo ? '-demo' : ''}.txt';

  final data = File(path)
    .readAsLinesSync()
    .map((e) => int.parse(e))
    .toList();

  final preamble_n = demo ? 5 : 25;

  var invalid = 0;

  for (var i = preamble_n; i < data.length; i++) {
    var n = Combinations(2, data.sublist(i - preamble_n, i))()
      .where((e) => e[0] + e[1] == data[i])
      .length;
    if (n == 0) {
      invalid = data[i];
      break;
    }
  }
  print('step 1: $invalid');

  LOOP_I: for (var i = 2; i < data.length; i++) {
    var sample = ListQueue<int>.from(data.sublist(0, i));
    for (var j = i; j < data.length; j++) {
      var sum = sample.
        reduce((a, e) => a + e);
      if (sum == invalid) {
        var weakness = sample.reduce(min) + sample.reduce(max);
        print('step 2: $weakness');
        break LOOP_I;
      }
      sample.removeFirst();
      sample.add(data[j]);
    }
  }
}
