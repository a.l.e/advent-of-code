import itertools
import math

filename = 'input.txt' if False else 'input-demo.txt'

# expenses = [1721, 979, 366, 299, 675, 1456]

with open(filename) as f:
    expenses = [int(line.rstrip()) for line in f]

# result = [a * b  if a + b == 2020 for a, b in itertools.combinations(expenses, 2)]

for a, b in itertools.combinations(expenses, 2):
    if a + b == 2020:
        print(a * b)

print(next((a * b for a, b in itertools.combinations(expenses, 2) if a + b == 2020), None))

for a, b, c in itertools.combinations(expenses, 3):
    if a + b + c == 2020:
        print(a * b * c)

print(next((math.prod(i) for i in itertools.combinations(expenses, 3) if sum(i) == 2020), None))

print(list(itertools.combinations(expenses, 3)))
