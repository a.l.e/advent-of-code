import 'dart:io';

const demo = false;
const part_1 = true;

main() {
  const path = 'input${demo ? '-demo' : ''}${part_1 || !demo ? '' : '-02'}.txt';
  final List<List<Set<String>>> lines = File(path)
    .readAsLinesSync()
    .map((e) => e.substring(0, e.length - 1)
      .split(' (contains ')
      .map((e) => e.replaceAll(',', '').split(' ').toSet()).toList()
    ).toList();

  // print(lines);

  var ingredients = Set();
  var allergens = Set();
  for (final line in lines) {
    ingredients = ingredients.union(line[0]);
    allergens = allergens.union(line[1]);
  }

  // print('ingredients: $ingredients');
  // print('allergens: $allergens');

  final singles = lines
    .where((e) => e[1].length == 1)
    // .map((e) => e[1].first)
    .toList();
  print('singles: $singles');

  for (final single in singles) {
    // print(single[1].first);
    final intersections = lines
      .where((e) => e[1].length > 0)
      .where((e) => e[0] != single[0] && e[1].contains(single[1].first))
      .map((e) => e[0].intersection(single[0]))
      .where((e) => e.length == 1)
      // .map((e) => e.first)
      .toList();
    // print('∩: $intersections');
  }

  print('---');


  var others = Map<String, Set<String>>();

  for (final single in singles) {
    // print(single[1].first);
    var other = single[0];
    lines
      .where((e) => e[1].length > 0)
      // TODO: do not intersect with self
      .where((e) => e[0] != single[0] && e[1].contains(single[1].first))
      .forEach((e) {
        other = other.intersection(e[0]);
      });
    // print('${single[1].first}: $other');
    others[single[1].first] = other;
  }

  print('others: $others');

  var ingredients_with_allergens = Map<String, String>();
  var newly_detected_ingredients = List<String>();
  var newly_detected_allgergens = List<String>();

  others
    .forEach((allergen, ingredients) {
      if (ingredients.length == 1) {
        print('$allergen → ${ingredients.first}');
        newly_detected_allgergens.add(allergen);
        newly_detected_ingredients.add(ingredients.first);
      }
    });
  others.removeWhere((k, v) => newly_detected_allgergens.contains(k));
  others
    .forEach((allergen, ingredients) {
      ingredients.removeWhere((e) => newly_detected_ingredients.contains(e));
    });
  print('others: $others');
}
