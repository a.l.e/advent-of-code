import 'dart:async';
import 'dart:io';
import 'dart:convert';

extension Range on num {
  bool isIn(num from, num to) {
    return from <= this && this <= to;
  }
}

void main() async {
  // final part_1 = true;
  final path = false ? 'input.txt': 'input-demo.txt';

  final pattern = RegExp(r'^(\d+?)-(\d+?) (\w): (.+)$');

  // read the file line by line and split into four fields
  final passwords = await File(path)
    .openRead()
    .map(utf8.decode)
    .transform(LineSplitter())
    .map((e) => pattern.firstMatch(e).groups([1, 2, 3, 4]))
    .toList();

    // does the password [3] contain at least [0] and at most [1] occurences of [2]?
    print(
        passwords.where((e) => e[2].allMatches(e[3]).length.isIn(int.parse(e[0]), int.parse(e[1])))
        // .toList()
        .length
    );

    // does the password [3] have the char [2] at [0] xor at at [1]?
    print(
        passwords.where((e) => ((e[3][int.parse(e[0]) - 1] == e[2]) ^ (e[3][int.parse(e[1]) - 1] == e[2])))
        // .toList()
        .length
    );
}
