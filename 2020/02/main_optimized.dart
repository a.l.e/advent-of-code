import 'dart:async';
import 'dart:io';
import 'dart:convert';

extension Range on num {
  bool isIn(num from, num to) {
    return from <= this && this <= to;
  }
}

void main() async {
  // final part_1 = true;
  final path = true ? 'input.txt': 'input-demo.txt';

  final pattern = RegExp(r'^(\d+?)-(\d+?) (\w): (.+)$');

  // read the file line by line and split into four fields... filter and count
  final count = File(path)
    .openRead()
    .map(utf8.decode)
    .transform(LineSplitter())
    .map((e) => pattern.firstMatch(e).groups([1, 2, 3, 4]))
    // part 1: does the password [3] contain at least [0] and at most [1] occurences of [2]?
    // .where((e) => e[2].allMatches(e[3]).length.isIn(int.parse(e[0]), int.parse(e[1])))
    // part 2: has password [3] the char [2] at [0] xor at [1]?
    .where((e) => ((e[3][int.parse(e[0]) - 1] == e[2]) ^ (e[3][int.parse(e[1]) - 1] == e[2])))
    .length;

    print(await count);
}
