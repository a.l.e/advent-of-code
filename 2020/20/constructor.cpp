#include <iostream>
class Abc {
public:
    Abc(int a) :
    a{a}, b{a * 2}, c{b * 3} {}

    int a, b, c;
};

int main()
{
    Abc abc{2};
    std::cout << abc.c << std::endl;
}
