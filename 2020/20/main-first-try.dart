import 'dart:io';

const demo = true;
const part_1 = true;

main() {
  const path = 'input${demo ? '-demo' : ''}${part_1 || !demo ? '' : '-02'}.txt';
  final input = Map<int, List<String>>.fromEntries(File(path)
    .readAsStringSync()
    .split('\n\n')
    .map((e) {
      final tile = e.split(':\n');
      return MapEntry<int, List<String>>(
        int.parse(tile[0].split(' ')[1]),
        tile[1].trim().split('\n'));
    }));
  // print(input);

  final borders = input.map((k, v) {
    final value = v.map((e) => e.replaceAll('#', '1').replaceAll('.', '0')).toList();
    final top = int.parse(value.first, radix: 2);
    final bottom = int.parse(value.last, radix: 2);
    final left = int.parse(value.map((e) => e[0]).join(''), radix: 2);
    final right = int.parse(value.map((e) => e[e.length - 1]).join(''), radix: 2);
    print('k top: $k ${top.toRadixString(2)}');
    print('k bottom: $k ${bottom.toRadixString(2)}');
    return MapEntry(k, [top, right, bottom, left]);
  });
  print('borders: $borders');
  final matches = borders.map((k, v) {
    final top = borders.keys.fold<List<int>>([], (a, e) =>
      v[0] == borders[e][2] ? a + [e] : a);
    final right = borders.keys.fold<List<int>>([], (a, e) =>
      v[1] == borders[e][3] ? a + [e] : a);
    final bottom = borders.keys.fold<List<int>>([], (a, e) =>
      v[2] == borders[e][0] ? a + [e] : a);
    final left = borders.keys.fold<List<int>>([], (a, e) =>
      v[3] == borders[e][1] ? a + [e] : a);
    return MapEntry(k, [top, right, bottom, left]);
  });
  print('matches 1427: ${matches[1427]}');
  final top_left = matches.keys.fold<List<int>>([], (a, e) => 
    matches[e][0].length == 0 && matches[e][3].length == 0 ? a + [e] : a);
  final top_right = matches.keys.fold<List<int>>([], (a, e) => 
    matches[e][0].length == 0 && matches[e][1].length == 0 ? a + [e] : a);
  final bottom_right = matches.keys.fold<List<int>>([], (a, e) => 
    matches[e][2].length == 0 && matches[e][1].length == 0 ? a + [e] : a);
  final bottom_left = matches.keys.fold<List<int>>([], (a, e) => 
    matches[e][2].length == 0 && matches[e][3].length == 0 ? a + [e] : a);

 print('corners: ${[top_left, top_right, bottom_right, bottom_left]}');
}
