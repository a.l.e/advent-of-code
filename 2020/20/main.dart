import 'dart:io';
import 'package:trotter/trotter.dart';

const demo = true;
const part_1 = true;


extension on String {
  String reversed() => this.split('').reversed.join('');
}

// from quiver
Iterable<List<T>> zip<T>(Iterable<Iterable<T>> iterables) sync* {
  if (iterables.isEmpty) return;
  final iterators = iterables.map((e) => e.iterator).toList(growable: false);
  while (iterators.every((e) => e.moveNext())) {
    yield iterators.map((e) => e.current).toList(growable: false);
  }
}

class Tile {
  final List<String> raw;
  final List<String> bin;
  Map<String, List<String>> borders;
  Tile(this.raw) : 
    bin = raw.map((e) => e.replaceAll('#', '1').replaceAll('.', '0')).toList()
  {
    List<String> b = [
      bin.first,
      bin.map((e) => e[e.length - 1]).join(''),
      bin.last,
      bin.map((e) => e[0]).join('')];
    List<String> r = b
      .map((e) => e.reversed())
      .toList();

    //  →
    // ↓ ↓
    //  →
    borders = {
      'rotated0': b,
      'rotated90':  [r[3], b[0], r[1], b[2]],
      'rotated180': [r[2], r[3], r[0], r[1]],
      'rotated270': [b[1], r[2], b[3], r[0]],
      'flipped_h':  [r[0], b[3], r[2], b[1]],
      'flipped_v':  [b[2], r[1], b[0], r[3]]
    };
  }

  @override
  toString() => 'Tile {$bin}';
}

main() {
  const path = 'input${demo ? '-demo' : ''}${part_1 || !demo ? '' : '-02'}.txt';
  final input = Map<int, List<String>>.fromEntries(File(path)
    .readAsStringSync()
    .split('\n\n')
    .map((e) {
      final tile = e.split(':\n');
      return MapEntry<int, List<String>>(
        int.parse(tile[0].split(' ')[1]),
        tile[1].trim().split('\n'));
    }));
  // print(input);
  print(input.length);

  final tiles = input.map((k, v) => MapEntry(k, Tile(v)));
  // print(tiles);

  final orientation = Permutations(2, tiles.entries.first.value.borders.keys.toList())();
  

  var matches = Map<int, List<int>>();
  for (final combo in Combinations(2, tiles.keys.toList())()) {
    // print('combo: $combo');
    // print(orientation);
    final the_one = tiles[combo[0]];
    final the_other = tiles[combo[1]];
    for (final o in orientation) {
      print('o ${the_one.borders[o[0]]} ${o[0]}'); 
			// TODO: no, we have to match top to bottom, ... oder doch nicht...
			final any_equal = zip([the_one.borders[o[0]], the_other.borders[o[1]]]).fold<bool>(false, (a, e) => a || (e[0] == e[1]));
      if (any_equal) {
        combo.forEach((e) {if (!matches.containsKey(e)) matches[e] = [];});
        matches[combo[0]].add(combo[1]);
        matches[combo[1]].add(combo[0]);
      }
    }
  }
  print(matches);

  // final borders = input.map((k, v) {
  //   final value = v.map((e) => e.replaceAll('#', '1').replaceAll('.', '0')).toList();
  //   final top = int.parse(value.first, radix: 2);
  //   final bottom = int.parse(value.last, radix: 2);
  //   final left = int.parse(value.map((e) => e[0]).join(''), radix: 2);
  //   final right = int.parse(value.map((e) => e[e.length - 1]).join(''), radix: 2);
  //   print('k top: $k ${top.toRadixString(2)}');
  //   print('k bottom: $k ${bottom.toRadixString(2)}');
  //   return MapEntry(k, [top, right, bottom, left]);
  // });

  // print('borders: $borders');

  // final matches = borders.map((k, v) {
  //   final top = borders.keys.fold<List<int>>([], (a, e) =>
  //     v[0] == borders[e][2] ? a + [e] : a);
  //   final right = borders.keys.fold<List<int>>([], (a, e) =>
  //     v[1] == borders[e][3] ? a + [e] : a);
  //   final bottom = borders.keys.fold<List<int>>([], (a, e) =>
  //     v[2] == borders[e][0] ? a + [e] : a);
  //   final left = borders.keys.fold<List<int>>([], (a, e) =>
  //     v[3] == borders[e][1] ? a + [e] : a);
  //   return MapEntry(k, [top, right, bottom, left]);
  // });
  // print('matches 1427: ${matches[1427]}');
  // final top_left = matches.keys.fold<List<int>>([], (a, e) => 
  //   matches[e][0].length == 0 && matches[e][3].length == 0 ? a + [e] : a);
  // final top_right = matches.keys.fold<List<int>>([], (a, e) => 
  //   matches[e][0].length == 0 && matches[e][1].length == 0 ? a + [e] : a);
  // final bottom_right = matches.keys.fold<List<int>>([], (a, e) => 
  //   matches[e][2].length == 0 && matches[e][1].length == 0 ? a + [e] : a);
  // final bottom_left = matches.keys.fold<List<int>>([], (a, e) => 
  //   matches[e][2].length == 0 && matches[e][3].length == 0 ? a + [e] : a);
  // print('corners: ${[top_left, top_right, bottom_right, bottom_left]}');
}
