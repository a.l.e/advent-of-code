const demo = false;
const data = demo ? [0, 3, 6] : [0, 3, 1, 6, 7, 5];


function logSpokenNumberAtPositionRefactored(startSequence, position) {
	const startSequenceMinusLast = startSequence.slice(0, startSequence.length - 1)
	let obj = new Map();
	startSequenceMinusLast.forEach((x, i) => (obj.set(x, i + 1)))
	let lastNumber = startSequence[startSequence.length - 1]
	for (let i = startSequence.length; i < position; i++) {
    const lastInstanceOfLastNumber = obj.get(lastNumber);
		obj.set(lastNumber, i)
		if (lastInstanceOfLastNumber === undefined) {
			lastNumber = 0
		} else {
			lastNumber = i - lastInstanceOfLastNumber
		}
	}
  console.log("spoken number: ", lastNumber)
}
console.time("logSpokenNumberAtPositionRefactored")
logSpokenNumberAtPositionRefactored(data, 2020)
console.timeEnd("logSpokenNumberAtPositionRefactored")

/* PART 2 */
console.time("logSpokenNumberAtPositionRefactored")
logSpokenNumberAtPositionRefactored(data, 30000000)
console.timeEnd("logSpokenNumberAtPositionRefactored")
