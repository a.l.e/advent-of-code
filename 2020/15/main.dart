import 'dart:io';

// https://stackoverflow.com/a/57371764/5239250
extension ExtendedIterable<E> on Iterable<E> {
  Iterable<T> mapIndexed<T>(T f(E e, int i)) {
    var i = 0;
    return this.map((e) => f(e, i++));
  }

  void forEachIndexed(void f(E e, int i)) {
    var i = 0;
    this.forEach((e) => f(e, i++));
  }
}

main() {
  final demo = false;
  final part_2 = true;
  final numbers = demo ? [0, 3, 6] : [0, 3, 1, 6, 7, 5];

  var memory = Map.fromEntries(numbers
    .mapIndexed((e, i) => MapEntry<int, int>(e, i + 1)));

  final n = part_2 ? 30000000: 2020 ;

  var number_spoken = 0;
  for (int i = numbers.length + 1; i < n; i++) {
    if (memory.containsKey(number_spoken)) {
      final next_number_spoken = i - memory[number_spoken];
      memory[number_spoken] = i;
      number_spoken = next_number_spoken;
    } else {
      memory[number_spoken] = i;
      number_spoken = 0;
    }
  }
  
  print('The ${n}th number spoken is $number_spoken');
}
