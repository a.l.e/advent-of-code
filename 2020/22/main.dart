import 'dart:io';
import 'dart:collection';

const demo = false;

// https://stackoverflow.com/a/57371764/5239250
extension ExtendedIterable<E> on Iterable<E> {
  Iterable<T> mapIndexed<T>(T f(E e, int i)) {
    var i = 0;
    return this.map((e) => f(e, i++));
  }

  void forEachIndexed(void f(E e, int i)) {
    var i = 0;
    this.forEach((e) => f(e, i++));
  }
}

List<ListQueue<int>> play_game(List<List<int>> decks) {
  var deck_a =ListQueue<int>.from(decks[0]);
  var deck_b =ListQueue<int>.from(decks[1]);

  while (deck_a.isNotEmpty && deck_b.isNotEmpty) {
    final a = deck_a.removeFirst();
    final b = deck_b.removeFirst();
    if (a > b) {
      deck_a.add(a);
      deck_a.add(b);
    } else {
      deck_b.add(b);
      deck_b.add(a);
    }
  }
  return [deck_a, deck_b];
}

List<ListQueue<int>> play_game_02(List<List<int>> decks) {
  var deck_a =ListQueue<int>.from(decks[0]);
  var deck_b =ListQueue<int>.from(decks[1]);

  var archive = List<String>();
    
  while (deck_a.isNotEmpty && deck_b.isNotEmpty) {
    final decks_hash = get_lists_hash(deck_a.toList(), deck_b.toList());
    if (archive.contains(decks_hash)) {
      return [deck_a, ListQueue.from([])];
    }
    archive.add(decks_hash);

    final a = deck_a.removeFirst();
    final b = deck_b.removeFirst();
    if (a <= deck_a.length && b <= deck_b.length) {
      final result_deck = play_game_02([deck_a.toList().sublist(0, a), deck_b.toList().sublist(0, b)]);
      if (result_deck[1].length == 0) {
        deck_a.add(a);
        deck_a.add(b);
      } else {
        deck_b.add(b);
        deck_b.add(a);
      }
    } else {
      if (a > b) {
        deck_a.add(a);
        deck_a.add(b);
      } else {
        deck_b.add(b);
        deck_b.add(a);
      }
    }
  }
  return [deck_a, deck_b];
}

// a simple way to hash our list of cards
get_lists_hash(deck_a, deck_b) {
  return [deck_a, deck_b]
    .map((e) => e
      .map((e) => e
        .toString()
        .padLeft(2, '0'))
        .reduce((a, e) => a + e))
    .reduce((a, e) => '$a+$e');
}

main() {

  const path = 'input${demo ? '-demo' : ''}.txt';
  final List<List<int>> decks = File(path)
    .readAsStringSync()
    .split('\n\n')
    .map((e) => e
        .trim()
        .split('\n')
        .skip(1)
        .map((e) => int.parse(e))
        .toList())
    .toList();
  {
    // TODO: check if it's not better to pass a clone of decks
    final score = play_game(decks)
      .fold<List<int>>([], (a, e) => e.isEmpty ? a : e.toList())
      .reversed
      .mapIndexed((e, i) => e * (i + 1))
      .reduce((a, e) => a + e);

    print('part 1: $score');
  }

  {
    final score = play_game_02(decks)
      .fold<List<int>>([], (a, e) => e.isEmpty ? a : e.toList())
      .reversed
      .mapIndexed((e, i) => e * (i + 1))
      .reduce((a, e) => a + e);

    print('part 2: $score');
  }
}
