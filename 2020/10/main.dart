import 'dart:io';
import 'dart:math';

// https://stackoverflow.com/a/61452883/5239250
extension MyExtensions<T, U> on Iterable<U> {
  T foldByPairs(T initialValue, T combine(T value, U previous, U current)) {
    Iterator<U> iterator = this.iterator;
    iterator.moveNext(); // error handling omitted for brevity
    T value = initialValue;
    U previous = iterator.current;
    while (iterator.moveNext()) {
      U current = iterator.current;
      value = combine(value, previous, current);
      previous = current;
    }
    return value;
  }
}

int binom(int n, int k) {
  int result = 1;
  for (int i = 0; i < k; i++) {
    result = result * (n - i) ~/ (i + 1);
  }
  return result;
}

main() {
  final demo = false;
  final simple = false;
  final path = 'input${demo ? '-demo' : ''}${simple ? '-simple' : ''}.txt';

  final adapters = File(path)
    .readAsLinesSync()
    .map((e) => int.parse(e))
    .toList();

  adapters.sort();

  adapters.insert(0, 0);
  adapters.add(adapters.reduce(max) + 3);

	final n = adapters
		.foldByPairs({1: 0, 3: 0}, (a, p, c) {
        final d = c - p;
        if (d == 2) {
          return a;
        }
        a[d]++;
        return a;
			});

	print('part 1: $n ${n[1] * n[3]}'); // 2211

  // print(adapters);

  var previous = adapters[0];
  var buffer = [previous];
  var lists = List<List<int>>();
  for (final adapter in adapters.sublist(1)) {
    final d = adapter - previous;
    if (d == 3) {
      if (buffer.length >= 1) {
        lists.add([...buffer]);
      }
      buffer.clear();
    }
    buffer.add(adapter);
    previous = adapter;
  }
  lists.add([...buffer]);

  // print(lists.fold(0, (a, e) => max(a, e.length)));
  // max_i = 5 d==2 -> 0 d == 34

  // print(lists);
  // print(lists.map((e) => e.length));

  // [1, 2, 3, 4, 5]
  //     x
  //        x
  //           x
  //     x  x
  //        x  x
  //     x     x
  // 1 + 6
  // [1, 2, 3, 4]
  //     x
  //     x  x
  //        x
  // 1 + 3
  // [1, 2, 3]
  //     x
  // 1 + 1
  // final combinations = {5: 7, 4: 4, 3: 2, 2: 1, 1: 1};
  final combinations = {
		5: binom(3, 2) + binom(3, 1) + 1,
		4: binom(2, 2) + binom(2, 1) + 1,
		3: binom(1, 1) + 1,
		2: 1,
		1: 1,
  };
	// print(combinations);

  print(lists.fold<int>(1, (a, e) => a * combinations[e.length]));
}
