import 'dart:io';
import 'dart:math';
import 'dart:collection';

main() {
  // part 2 only

  final demo = false;
  final simple = false;
  final path = 'input${demo ? '-demo' : ''}${simple ? '-simple' : ''}.txt';

  final adapters = File(path)
    .readAsLinesSync()
    .map((e) => int.parse(e))
    .toList();

  adapters.sort();

  adapters.insert(0, 0);
  adapters.add(adapters.reduce(max) + 3);

  var n = 0;
  for (var i = 1; i < adapters.length - 2; i++) {
    if (adapters[i + 1] - adapters[i] == 1
      && adapters[i + 2] - adapters[i + 1] == 1) {
        n++;
      }

  }
  print('part 2: ${pow(2, n)}');
}
