import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'dart:math';

get_index(zone, low) {
  var bound = [0, pow(2, zone.length) - 1];
  zone.split('').forEach((c) {
    final half = (bound[1] - bound[0] + 1) ~/ 2;
    bound = c == low ? [bound[0], bound[1] - half] : [bound[0] + half, bound[1]];
    // print('$c $bound');
    });
  return bound[0];
}

int get_missing_simple(ids) {
  var prev = ids[0];
  return ids.
    sublist(1)
    .firstWhere((int e) {
      var p = prev;
      prev = e;
      return (e > p + 1);
    }) - 1;
}

get_missing_even_simpler(ids) {
  for (var i = 1; i < ids.length; i++) {
    if (ids[i] != ids[i - 1] + 1) {
      return ids[i] - 1;
    }
  }
  return -1;
}

// TODO: binary search: does not work yet.
get_missing(ids) {
  var a = 0, b = ids.length - 1;
  var mid = 0;
  // print('$a $b');
  var i = 0;
  var d = 0;
  while (b - a  > 1 && i < 5) {
    i++;
    mid = (a + b) ~/ 2;
    // print('$mid $a $b');
    // print('$mid ${ids[a]} ${ids[mid]} ${ids[b]}');
    if (ids[a] - a != ids[mid] - mid) {
      b = mid;
      d = -1;
    } else if (ids[b] - b != ids[mid] - mid) {
      a = mid;
      d = 1;
    }
    // print('$a $b');
  }
  // print('$d ${ids[mid]}');
  return ids[mid]  + d;
}

void main() async {
  final part_1 = false;
  final path = true ? 'input.txt': 'input-demo.txt';

  if (part_1) {
    final id = await File(path)
      .openRead()
      .map(utf8.decode)
      .transform(LineSplitter())
      .map<int>((l) =>
        get_index(l.substring(0, 7), 'F') * 8 + get_index(l.substring(7, 10), 'L'))
      .reduce(max);
    print(id);
  } else {
    final ids = await File(path)
      .openRead()
      .map(utf8.decode)
      .transform(LineSplitter())
      .map<int>((l) =>
        get_index(l.substring(0, 7), 'F') * 8 + get_index(l.substring(7, 10), 'L'))
      .toList();
    ids.sort();
    print(ids.sublist(0, 10));
    // print(ids.sublist(1));
    // print(get_missing([1, 2, 3, 4, 5, 6, 8]) == 7);
    // print(get_missing([11, 12, 13, 14, 15, 16, 18]) == 17);
    // print(get_missing([10, 11, 12, 13, 15, 16]) == 14);
    // print(get_missing([9, 10, 11, 12, 14, 15, 16, 17]) == 13);
    print(get_missing_simple(ids)); // 651
    print(get_missing_even_simpler(ids)); // 651
    print(get_missing(ids)); // 661

    // print(ids);
  }
}
