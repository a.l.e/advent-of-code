filename = 'input.txt' if True else 'input-demo.txt'

with open(filename) as f:
    # they're just binaries... thanks nic!
    ids = [int(''.join(map(
        lambda c: '0' if c in 'FL' else '1', line.rstrip()
      )), 2) for line in f
    ]

max_id = max(ids)
print(max_id)

min_id = max_id - len(ids)
# https://math.stackexchange.com/questions/1842152/finding-the-sum-of-numbers-between-any-two-given-numbers
sum_bounds = (max_id - min_id + 1) * (min_id + max_id) // 2
sum_ids = sum(ids)

print(sum_bounds - sum_ids);
