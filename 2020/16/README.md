```
c: MapEntry(class: [[0, 1], [4, 19]])
t: [3, 9, 18]
t: [15, 1, 5]
t: [5, 14, 9]
c: MapEntry(row: [[0, 5], [8, 19]])
t: [3, 9, 18]
t: [15, 1, 5]
t: [5, 14, 9]
c: MapEntry(seat: [[0, 13], [16, 19]])
t: [3, 9, 18]
t: [15, 1, 5]
t: [5, 14, 9]


(
  {3: [row, seat], 9: [class, row, seat], 18: [class, row, seat]},
  {15: [class, row], 1: [class, row, seat], 5: [class, row, seat]},
  {5: [class, row, seat], 14: [class, row], 9: [class, row, seat]},
  {11: [class, row, seat], 12: [class, row, seat], 13: [class, row, seat]})


column_classes: [
{departure location, departure station, departure platform, departure track, departure date, departure time, arrival location, arrival station, arrival platform, arrival track, class, price, route, seat, train, type, zone}
{departure location, departure station, departure platform, departure track, departure date, departure time, arrival location, arrival station, arrival platform, arrival track, price, route, seat, train, type, zone}
{departure track, arrival location, arrival station, arrival platform, arrival track, class, duration, price, train, type, zone}
{departure location, departure station, departure platform, departure track, departure date, departure time, arrival location, arrival station, arrival platform, arrival track, class, duration, price, route, row, seat, train, type, wagon, zone}
{departure location, departure platform, price, route, type, wagon}
{departure station, departure track, arrival station, arrival track, price, train, type, zone}
{departure station, departure platform, departure time, arrival station, arrival platform, arrival track, duration, price, route, train, type, zone}
{departure location, departure platform, departure date, departure time, arrival track, class, price, seat, train, type}
{departure location, departure station, departure platform, departure track, departure date, departure time, arrival location, arrival station, arrival platform, arrival track, price, route, row, seat, train, type, zone}
{departure location, arrival location, arrival station, arrival track, duration, price, train, type}
{departure platform, departure time, arrival location, arrival station, arrival platform, arrival track, price, route, train, type, zone}
{departure location, departure station, departure platform, departure track, departure date, departure time, arrival location, arrival station, arrival platform, arrival track, class, price, route, train, type, zone}
{departure location, departure station, departure platform, departure track, departure date, departure time, arrival location, arrival station, arrival platform, arrival track, class, duration, price, route, row, seat, train, type, wagon, zone}
{departure station, departure platform, departure track, departure time, arrival station, arrival platform, arrival track, price, route, train, type, zone}
{departure platform, departure track, arrival station, arrival platform, arrival track, price, route, train, type, zone}
{departure platform, departure track, arrival station, price, train, type, zone}
{departure station, departure platform, departure time, arrival station, arrival platform, arrival track, price, route, train, type, wagon, zone}
{departure location, departure station, departure platform, departure track, departure date, departure time, arrival location, arrival station, arrival platform, arrival track, class, duration, price, route, row, seat, train, type, wagon, zone}
{departure station, arrival station, arrival platform, arrival track, price, train, type, zone}
{departure location, departure station, departure platform, departure track, departure date, departure time, arrival location, arrival station, arrival platform, arrival track, duration, price, route, seat, train, type, zone}
]
```
