import 'dart:io';

const demo = true;
const part_2 = true;

extension Range on num {
  bool isIn(num from, num to) {
    return from <= this && this <= to;
  }
}

List<int> get_invalid_numbers(List<int> numbers, Map<String, List<List<int>>> classes) {
  return numbers.where((e) => !classes.values
    .any((c) =>  c
      .any((cc) =>
        e.isIn(cc[0], cc[1]))))
    .toList();
}


Map<int, List<String>> get_ticket_fields(List<int> numbers, Map<String, List<List<int>>> classes) {
  var result = Map<int, List<String>>();

  for (final number in numbers) {
    result[number] = List<String>();
    for (final c in classes.entries) {
      for (final v in c.value) {
        if (number.isIn(v[0], v[1])) {
          // print('$number is in $v');
          result[number].add(c.key);
          break;
        }
      }
    }
  }

  return result;

  // print('%%% ${
  // numbers.map((e) => Map.fromEntries(classes.keys
  //   // .map((k) {print('k / v: $k / ${classes[k]}'); return [];})
  //   .where((c) => classes[c]
  //     .any((cc) => e.isIn(cc[0], cc[1]))
  //     )
  //     
  //   //   fold(true, (a, cc) {print('cc / e : $c / $e'); return true; }))
  //   // // .any((c) => c
  //   //   .any((cc) => e.isIn(cc[0], cc[1]))
  //   //   )
  //   .map((e) => MapEntry(0, e))
  //   ))
  // }');
  // numbers.where((e) => classes.values
  //   .any((c) =>  c
  //     .every((cc) =>
  //       e.isIn(cc[0], cc[1]))))
  //   .toList();
  // return Map.fromEntries([MapEntry(0, ['abc'])]);
}

main() {
  final path = 'input${demo ? '-demo' : ''}${demo && part_2 ? '-02' : ''}.txt';
  final ticket_sections = File(path)
    .readAsStringSync()
    .split('\n\n');
  // print(ticket_sections);
  final classes = Map.fromEntries(ticket_sections[0]
    .split('\n')
    .map((e) {
      final f = e.split(': ');
      return MapEntry<String, List<List<int>>>(f[0], f[1]
        .split(' or ')
        .map((f) => f
          .split('-')
          .map((f) => int.parse(f))
          .toList())
        .toList()
      );
    })
  );
  print('classes: $classes');

  final my_ticket = ticket_sections[1]
    .split('\n')[1]
      .split(',')
      .map((e) => int.parse(e))
      .toList();
  // print(my_ticket);
  final nearby_tickets = ticket_sections[2]
    .trim()
    .split('\n')
    .sublist(1)
    .map((e) => e
      .split(',')
      .map((e) => int.parse(e))
      .toList())
    .toList();
  // print(nearby_tickets);

  print('part 1: ${nearby_tickets
    .map((e) => get_invalid_numbers(e, classes))
    .where((e) => e.length > 0)
    .fold<int>(0, (a, e) => a + e.reduce((a, e) => a + e))
  }');

  final valid_nearby_tickets = nearby_tickets
    .where((e) => get_invalid_numbers(e, classes).length == 0);

  print('valid_nearby_tickets: $valid_nearby_tickets');

  {
    var position_classes = Map<int, String>();
    var valid_classes_for_position = Map<int, List<String>>();
    for (final c in classes.entries) {
      print('c: $c');
      for (var i = 0; i < valid_nearby_tickets.first.length; i++) {
        var is_valid = true;
        for (final t in valid_nearby_tickets) {
          var is_in = false;
          for (var j = 0; j < 2; j++) {
            is_in |= t[i].isIn(c.value[j][0], c.value[j][1]);
          }
          is_valid &= is_in;
          print('t: $t');
        }
        print('${c.key}: $is_valid');
        if (is_valid) {
          if (!valid_classes_for_position.containsKey(i)) {
            valid_classes_for_position[i] = [c.key];
          } else {
            valid_classes_for_position[i].add(c.key);
          }
        }
      }
    }
    print(valid_classes_for_position);
    for (final e in valid_classes_for_position.entries) {
      if (e.value.length == 1) {
        position_classes[e.key] = e.value.first;
      }
    }
    print(position_classes);
  }




  // final valid_tickets_classes = all_tickets
  final valid_tickets_classes = nearby_tickets 
    .map((e) => get_ticket_fields(e, classes))
    .where((e) => e.values.every((e) => e.length != 0))
    .where((e) => e.length == my_ticket.length)
    .map((e) => e.values.toList());

  // valid_tickets_classes.forEach((e) {
  //   print('. ${e.length}');
  // });

  // print('my length: ${my_ticket.length}');
  // print('vtc: ${valid_tickets_classes.length}');
  var column_classes = List<Set<String>>();
  for (int i = 0; i < my_ticket.length; i++) {
    column_classes.add(
      valid_tickets_classes
        .map((c) => c[i].toSet())
        .reduce((a, c) => a.intersection(c))
    );
  }
  // print('column_classes: $column_classes');

  var class_counter = Map<String, int>();
  for (final c in classes.keys) {
    var counter = 0;
    for (final ticket in column_classes) {
      if (ticket.contains(c)) {
        counter++;
      }
    }
    class_counter[c] = counter;
  }

  // print(class_counter);

  var first = column_classes
    .reduce((a, e) => a.intersection(e));

  // print(first);

  // final ticket_classes = column_classes
  // .where((e) => 

  
  // print('part 2: ${valid_tickets_classes}');
}
