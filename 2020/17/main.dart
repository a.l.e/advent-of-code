import 'dart:io';
import 'package:equatable/equatable.dart';
import 'package:trotter/trotter.dart';

const demo = true;
const part_2 = true;

// https://stackoverflow.com/a/57371764/5239250
extension ExtendedIterable<E> on Iterable<E> {
  Iterable<T> mapIndexed<T>(T f(E e, int i)) {
    var i = 0;
    return this.map((e) => f(e, i++));
  }

  void forEachIndexed(void f(E e, int i)) {
    var i = 0;
    this.forEach((e) => f(e, i++));
  }
}

class Coord extends Equatable {
  var x;
  var y;
  var z;
  var w;

  Coord(this.x, this.y, this.z, this.w);

  clone() => Coord(x, y, z, w);

  @override
  toString() => '{$x, $y, $z, $w}';

  @override
  Coord operator +(o) => Coord(x + o.x, y + o.y, z + o.z, w + o.w);

  @override
  List<Object> get props => [x, y, z, w];
}

final directions = Amalgams(4, [-1, 0, 1])() // trotter
  .where((e) => part_2 || e[3] == 0)
  .where((e) => !e.every((e) => e == 0))
  .map((e) => Coord(e[0], e[1], e[2], e[3]));

count_neighbours(world, coord) {
  return directions
    .fold(0, (a, e) => a + (world.contains(coord + e) ? 1 : 0));
}

get_neighbours(coord) {
  return directions
    .fold(Set<Coord>(), (a, e) {
      a.add(coord + e);
      return a;
    });
}

clone_world(Set<Coord> world) {
  var next = Set<Coord>();
  for (final e in world) {
    next.add(e);
  }
  return next;
}

next_world(world) {
  var next = Set<Coord>();
  var inactives = Set<Coord>(); // speed up the resurection
  for (final c in world) {
    final n = count_neighbours(world, c);
    if (n == 2 || n == 3) {
      next.add(c.clone());
    }
    for (final neighbour in get_neighbours(c)) {
      if (!inactives.contains(neighbour) && !world.contains(neighbour) && !next.contains(neighbour)) {
        final n = count_neighbours(world, neighbour);
        if (n == 3) {
          next.add(neighbour.clone());
        } else {
          inactives.add(neighbour.clone());
        }
      }
    }
  }
  return(next);
}

// only works for part 1 (does not make sense for part 2)
print3d_world(world) {
  final min_max = world
    .fold(List<Coord>.from([world.first.clone(), world.first.clone()]), (a, e) {
      a[0].x = ((a[0].x < e.x) ? a[0].x : e.x);
      a[0].y = ((a[0].y < e.y) ? a[0].y : e.y);
      a[0].z = ((a[0].z < e.z) ? a[0].z : e.z);
      a[1].x = ((a[1].x > e.x) ? a[1].x : e.x);
      a[1].y = ((a[1].y > e.y) ? a[1].y : e.y);
      a[1].z = ((a[1].z > e.z) ? a[1].z : e.z);
      return a;
    });
    for (var z = min_max[0].z; z <= min_max[1].z; z++) {
      print('z: $z');
      for (var y = min_max[0].y; y <= min_max[1].y; y++) {
        for (var x = min_max[0].x; x <= min_max[1].x; x++) {

          stdout.write(world.contains(Coord(x, y, z, 0)) ? '#' : '.');
        }
        print('');
      }
      print('');
    }
}

main() {
  const path = 'input${demo ? '-demo' : ''}.txt';

  var world = Set<Coord>();
  File(path)
    .readAsLinesSync()
    .forEachIndexed((e, y) => e
      .split('')
      .forEachIndexed((e, x) {
        if (e == '#') {
          world.add(Coord(x, y, 0, 0));
        }
      }));

  print3d_world(world);

  final stopwatch = Stopwatch()..start();

  const n = 6;
  for (int i = 0; i < n; i++) {
    world = next_world(world);
  }
  // print3d_world(world);
  print(world.fold<int>(0, (a, e) => a + 1));

  print('time: ${stopwatch.elapsed}');

}
