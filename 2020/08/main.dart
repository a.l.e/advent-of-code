import 'dart:io';

class State {
  var i;
  var v;
  State(this.i, this.v);
  toString() {
    return '{i: $i, v: $v}';
  }
}

switch_command(cmd) {
  if (cmd[0] == 'jmp') {
    return ['nop', cmd[1]];
  } else if (cmd[0] == 'nop') {
    return ['jmp', cmd[1]];
  }
  return cmd;
}

void main() {
  final part_1 = true;
  final path = true ? 'input.txt': 'input-demo${part_1 ? '' : '-2'}.txt';

  final commands = File(path)
    .readAsLinesSync()
    .map((e) => e.split(' '))
    .map((e) => [e[0], int.parse(e[1])])
    .toList();

  // print(commands);

  final actions = {
    'acc': (s, v) => State(s.i + 1, s.v + v),
    'jmp': (s, v) => State(s.i + v, s.v),
    'nop': (s, v) => State(s.i + 1, s.v),
  };

  {
    var past_commands = Set<int>();
    var state = State(0, 0);
    while (!past_commands.contains(state.i)) {
      past_commands.add(state.i);
      var command = commands[state.i];
      state = actions[command[0]](state, command[1]);
    }
    print(state);
  }
  {
    final n = commands.length;
    var result = 0;
    for (var i = 0; i < n; i++) {
      var old_cmd = commands[i];
      commands[i] = switch_command(commands[i]);
      // print(commands);
      var state = State(0, 0);
      var past_commands = Set<int>();
      while (!past_commands.contains(state.i) && state.i < n) {
        past_commands.add(state.i);
        var command = commands[state.i];
        state = actions[command[0]](state, command[1]);
      }
      commands[i] = old_cmd;
      if (state.i == n) {
        result = state.v;
        break;
      }
    }
    print(result);
  }
}

