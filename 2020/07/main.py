import re

def get_bags_rules_from_file(filename):
    pattern_row = re.compile(r'(\w+? \w+?) bags contain (.+)\.$')
    pattern_contain = re.compile(r'(\d+) (\w+ \w+) bag[s]?');

    with open(filename) as f:
        rules_lines = [pattern_row.match(line.rstrip()).groups() for line in f]

    bags = {}

    for rules_line in rules_lines:
        # print(rules_lines)

        bags[rules_line[0]] = []

        if rules_line[1] == 'no other bags':
            continue
        
        for contained_bags in rules_line[1].split(', '):
            bags[rules_line[0]].append(pattern_contain.match(contained_bags).groups())
    return bags

def part1(bags):
    outer_bags = []
    stack = ['shiny gold']
    while len(stack) > 0:
        item = stack.pop()
        for bag_color, inner_bags in bags.items():
            for inner_bag in inner_bags:
                if item == inner_bag[1]:
                    if bag_color not in outer_bags:
                        outer_bags.append(bag_color)
                    if inner_bag not in stack:
                        stack.append(bag_color)
    return len(outer_bags)

def part2(bags):
    counter = 0
    stack = ['shiny gold']
    while len(stack) > 0:
        item = stack.pop()
        for content in bags[item]:
            n_items = int(content[0])
            counter += n_items
            stack += [content[1]] * n_items
    return counter

        
def main():
    filename = 'input.txt' if True else 'input-demo.txt'
    bags = get_bags_rules_from_file(filename)
    # print(bags)

    print(part1(bags))

    print(part2(bags))


if __name__ == "__main__":
    main()
