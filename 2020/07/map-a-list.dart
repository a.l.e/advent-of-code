void main() {
  {
    final list = [[0, 1], [1, 2]]
      .map((e) => [e[0], e[1] * 2]); // [1]

    print(list);
  }

  {
    print('ef,gh'.split(',')); // [2]
  }

  {
    final list = [[0, 'ab,cd'], [1, 'ef,gh']]
      .map((e) => [e[0], e[1].split(',')]); // [3]

  }

  // map-a-list.dart:15:31: Error: The method 'split' isn't defined for the class 'Object'.
  //  - 'Object' is from 'dart:core'.
  // Try correcting the name to the name of an existing method, or defining a method named 'split'.
  //       .map((e) => [e[0], e[1].split(',')]); // [3]

}
