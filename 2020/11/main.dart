import 'dart:io';

final directions = [[-1, -1], [-1, 0], [-1, 1], [0, -1], [0, 1], [1, -1], [1, 0], [1, 1]];

get_neighbours(state, i, j, n, m) {
  var neighbours = 0;
  for (final d in directions) {
    final ii = i + d[0];
    final jj = j + d[1];
    if (ii >= 0 && ii < n && jj >= 0 && jj < m) {
      neighbours += (state[jj][ii] == '#' ? 1 : 0);
    }
  }
  return neighbours;
}

get_visibles(state, i, j, n, m) {
  var neighbours = 0;
  for (final d in directions) {
    var ii = i + d[0];
    var jj = j + d[1];
    var found = false;
    while ((!found) && ii >= 0 && ii < n && jj >= 0 && jj < m) {
      if (state[jj][ii] == '.') {
        ii = ii + d[0];
        jj = jj + d[1];
        continue;
      }
      neighbours += (state[jj][ii] == '#' ? 1 : 0);
      found = true;
    }
  }
  return neighbours;
}

clone_state(state) {
  var next = List<List<String>>();
  for (final row in state) {
    var next_row = List<String>();
    for (final cell in row) {
      next_row.add(cell);
    }
    next.add(next_row);
  }
  return next;
}

next_state(List<List<String>> state, part_1) {
  var next = clone_state(state);
  final m = state.length;
  final n = state[0].length;
  for (var j = 0; j < m; j++) {
    for (var i = 0; i < n; i++) {
      if (state[j][i] == '.') {
        continue;
      }
      final neighbours = part_1 ?
        get_neighbours(state, i, j, n, m) :
        get_visibles(state, i, j, n, m);
      if (state[j][i] == 'L') {
        if (neighbours == 0) {
          next[j][i] = '#';
        }
      } else {
        if (neighbours >= (part_1 ? 4 : 5)) {
          next[j][i] = 'L';
        }
      }
    }
  }
  return next;
}

print_seats(seats) {
  for (final row in seats) {
    print(row.join(''));
  }
  print('');
}

main() {
  final part_1 = false;
  final demo = false;
  final path = 'input${demo ? '-demo' : ''}.txt';

  var seats = File(path)
    .readAsLinesSync()
    .map<List<String>>((e) => e.split(''))
    .toList();

  var states = Set<String>();

  while (!states.contains(seats.join(''))) {
    states.add(seats.join(''));
    seats = next_state(seats, part_1);
  }
  final occupied = seats.
    fold(0, (a, row) => a +
      row.fold(0, (a, c) => a += c == '#' ? 1 : 0));
  print('occupied: $occupied');
}
