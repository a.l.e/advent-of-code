import 'dart:io';

class Ferry {
  var position = [0, 0]; // x, y
  var direction = 90; // E

  final cardinalDirections = {0: 'N', 90: 'E', 180: 'S', 270: 'W'};

  final moves = {
    'N': (f, v) => f.position[1] -= v,
    'E': (f, v) => f.position[0] += v,
    'S': (f, v) => f.position[1] += v,
    'W': (f, v) => f.position[0] -= v,
    'L': (f, v) => f.direction = (f.direction + (360 - v)) % 360,
    'R': (f, v) => f.direction = (f.direction + v) % 360,
    'F': (f, v) => f.forward(v),
  };

  move(m, v) {
    moves[m](this, v);
  }

  forward(v) {
    moves[cardinalDirections[direction]](this, v);
  }

  get manhattanDistance {
    return position[0].abs() + position[1].abs();
  }

  @override
  toString() {
    return '{$position, $direction}';
  }
}

class FerryWaypoint {
  var position = [0, 0]; // x, y
  var waypoint = [10, -1]; // x, y

  final moves = {
    'N': (f, v) => f.waypoint[1] -= v,
    'E': (f, v) => f.waypoint[0] += v,
    'S': (f, v) => f.waypoint[1] += v,
    'W': (f, v) => f.waypoint[0] -= v,
    'L': (f, v) => f.rotations[360 - v](f),
    'R': (f, v) => f.rotations[v](f),
    'F': (f, v) => f.forward(v),
  };

  final rotations = {
    0: (f) {},
    90: (f) => f.swap(-1, 1),
    180: (f) {f.waypoint[0] *= -1; f.waypoint[1] *= -1;},
    270: (f) => f.swap(1, -1),
  };

  move(m, v) {
    moves[m](this, v);
  }

  forward(v) {
    position[0] += waypoint[0] * v;
    position[1] += waypoint[1] * v;
  }

  swap(sign_x, sign_y) {
    final t = waypoint[0];
    waypoint[0] = sign_x * waypoint[1];
    waypoint[1] = sign_y * t;
  }

  get manhattanDistance {
    return position[0].abs() + position[1].abs();
  }

  @override
  toString() {
    return '{$position, $waypoint}';
  }
}

main() {
  final demo = false;
  final path = 'input${demo ? '-demo' : ''}.txt';

  final instructions = File(path)
    .readAsLinesSync()
    .map<Map<String, dynamic>>((e) => {'m': e[0], 'v':int.parse(e.substring(1))})
    .toList();

  {
    var ferry = Ferry();

    for (final instruction in instructions) {
      ferry.move(instruction['m'], instruction['v']);
    }
    print('Part 1: ${ferry.manhattanDistance}');
  }

  var ferry = FerryWaypoint();
  for (final instruction in instructions) {
    ferry.move(instruction['m'], instruction['v']);
  }
  print('Part 2: ${ferry.manhattanDistance}');
}
