import 'dart:io';
import 'dart:math';

/**
 * part 1 can be solved well with a huge regexp.
 * in part 2, too. considering that the text says to optimize
 * the solution to the given data **and** waiting for a
 * coupe of minutes is ok
 */

const demo = false;
const part_1 = true;

count_self_references(List<List<int>> value, int key) {
  return value
    .fold<int>(0, (a, e) => a + e
      .where((e) => e == key)
      .length);
}
get_self_referenced_rule(start, end, max_n) {
  var patterns = List<String>();
  // found the value by increasing the steps until AoC accepted the solution...
  // but it takes several minutes to calculate.
  for (int i = 1; i < (demo ? 5: 6); i++) {
    final pattern = '${start*i}${end*i}';
    // print('pattern: $pattern');
    patterns.add(pattern);
    
  }
  // print('patterns: $patterns');
  return '(?:' + patterns.join('|') + ')';
}

main() {
  const path = 'input${demo ? '-demo' : ''}${part_1 || !demo ? '' : '-02'}.txt';
  final input = File(path)
    .readAsStringSync()
    .split('\n\n')
    .map((e) => e.split('\n').toList())
    .toList();
  // print(input);

  var rules = Map<int, String>();
  var raw_rules = Map<int, List<List<int>>>();
  for (final line in input[0]) {
    final fields = line.split(': ');
    if (fields[1].contains('"')) {
      rules[int.parse(fields[0])] = '${fields[1][1]}';
    } else {
      raw_rules[int.parse(fields[0])] = fields[1]
        .split(' | ')
        .map((e) => e
          .split(' ')
          .map((e) => int.parse(e))
          .toList()
        ).toList();
    }
  }
  // print('rules: $rules');

  if (!part_1) {
    // if (demo) {
    //   raw_rules[6] = [[4], [4, 6]];
    //   raw_rules[7] = [[5], [5, 7, 5]];
    // } else {
      raw_rules[8] = [[42], [42, 8]];
      raw_rules[11] = [[42, 31], [42, 11, 31]];
    // }
  }
  // print('raw_rules: $raw_rules');

  final max_message_length = input[1]
    .fold<int>(0, (a, e) => max(a, e.length));
  // print('max_message_length: $max_message_length');

  while (raw_rules.length > 0) {
    var new_rules = List<int>();
    // print('---');
    for (final item in raw_rules.entries) {
      var unsolvables = item.value
        .fold<int>(0, (a, e) => a + e
          .where((e) => !rules.containsKey(e) && e != item.key)
          .length);
      // print('${item.key}: unsolvables: $unsolvables');
      if (unsolvables == 0) {

        if (part_1 || count_self_references(item.value, item.key) == 0) {
          rules[item.key] = '(?:' + item.value
            .map((e) => e
              .map((e) => rules[e])
              .join(''))
            .join('|') + ')';
          new_rules.add(item.key);
        } else {
            final start = rules[item.value[0][0]];
            final end = (item.value[1].length == 2 ? '' : rules[item.value[1][2]]);
            rules[item.key] = get_self_referenced_rule(start, end, max_message_length);
            new_rules.add(item.key);
        }
      }
    }
    for (final key in new_rules) {
      raw_rules.remove(key);
    }
    // raw_rules.clear();
  }
  // print('rules: $rules');

  // print('rules: $rules');

  final re = RegExp('^${rules[0]}\$');

  final n = input[1]
    .where((e) => re.hasMatch(e))
    .length;
  print('n: $n');

}
