import 'dart:io';
import 'dart:collection';

// for part 2 i've tried
// - to brute force... it takes 55 hours
// - find a loop a cycle, nope
// - the next step would be to take into consideration that the values autoamtically insterted are growing and all bigger than the ones at the beggining... can we skip lot of values or find a path?

const demo = false;
const part_1 = false;

next() {
}

main() {
  const input = demo ? '389125467' : '198753462';
  var cups = List.from(input
    .split('')
    .map((e) => int.parse(e)));

  final start_length = cups.length + 1;
  if (!part_1) {
    for (var i = start_length; i <= 1000000; i++) {
      cups.add(i);
    }
  }
  print('length: ${cups.length}');

  final start_cups = cups.join(',');
  
  var pickup = List<int>();
  for (var i = 0; i < (part_1 ? 100 : 10000000); i++) {
    if (i % 1000 == 0) {
      print(i);
    }
    // print(cups);
    final selection = cups.removeAt(0);
    for (var j = 0; j < 3; j++) {
      pickup.add(cups.removeAt(0));
    }
    // print('i: $i s: $selection p: $pickup');
    var max_smaller_value = cups
      .fold<int>(0, (a, e) => e < selection && a < e ? e : a);
    if (max_smaller_value == 0) {
      max_smaller_value = cups
        .reduce((a, e) => a < e ? e : a);
    }
    // print('$max_smaller_value: ${cups.indexOf(max_smaller_value)}');
    cups.insertAll(cups.indexOf(max_smaller_value) + 1, pickup);
    cups.add(selection);
    pickup.clear();
  }
  print(cups);

  final one_pos = cups.indexOf(1);
  var solution = '';
  if (one_pos < cups.length - 1) {
    solution += cups.sublist(one_pos + 1).join('');
  }
  if (one_pos > 0) {
    solution += cups.sublist(0, one_pos).join('');
  }
  print(solution);
}
