#include <vector>
#include <set>
#include <map>
#include <string>
#include <algorithm>

#include <fstream>
#include <sstream>
#include <iterator>

#include <iostream>
#include <cassert>

using Banks = std::vector<int>;
using Archive = std::set<std::string>;
using ArchiveDistance = std::map<std::string, int>;

std::string banksToString(Banks banks) {
    /*
    std::string result{};
    std::transform(banks.begin(), banks.end(), std::back_inserter(result),
        [](int c) {return '0' + c;}
    );
    */
    std::string result{};
    for (int c: banks) {
        result += std::to_string(c) + " ";
    }
    return result;
}

int reallocate(Banks banks) {
    Archive archive{};

    while (archive.insert(banksToString(banks)).second) {
        // std::cout << "++++" << std::endl;
        int position = std::distance(banks.begin(), std::max_element(banks.begin(), banks.end()));
        int n = banks.at(position);
        banks.at(position) = 0;
        for (int i = 0; i < n; i++) {
            position = (++position) % banks.size();
            banks.at(position)++;
        }
    }
    for (auto bank: archive) {
        // std::cout << "bank " << bank << std::endl;
    }
    return archive.size();
}

int reallocateDistance(Banks banks) {
    ArchiveDistance archive{};

    int i = 0;
    bool ok = true;
    int duplicatedI = 0;
    std::string duplicatedPattern = "";
    while (ok) {
        auto [duplicate, ok_] = archive.insert(std::pair<std::string, int>(banksToString(banks), i));
        i++;
        duplicatedI = (*duplicate).second;
        duplicatedPattern = (*duplicate).first;
        ok = ok_;
        if (ok) {
            // std::cout << "++++" << std::endl;
            int position = std::distance(banks.begin(), std::max_element(banks.begin(), banks.end()));
            int n = banks.at(position);
            banks.at(position) = 0;
            for (int k = 0; k < n; k++) {
                position = (++position) % banks.size();
                banks.at(position)++;
            }
        }
        std::cout << "banks " << banksToString(banks) << std::endl;
    }
    for (auto bank: archive) {
        // std::cout << "bank " << bank << std::endl;
    }
    std::cout << "duplicatedPattern " << duplicatedPattern << std::endl;
    std::cout << "duplicatedI " << duplicatedI << std::endl;
    return archive.size() - duplicatedI;
}

int reallocateDistance2(Banks banks) {
    std::map<std::vector<int>, int> archive;
    for (int count{0}; archive.emplace(banks, count).second; ++count) {
        auto max = std::max_element(banks.begin(), banks.end());
        for (int i{std::exchange(*max, 0)}; i--; ++*max) {
            if (++max == banks.end()) {
                max = banks.begin();
            }
        }
    }
    return archive.size() - archive[banks];
}

int main()
{
    std::ifstream infile{"input.txt"};
    std::vector<int> banks{std::istream_iterator<int>{infile}, {}};
    // std::cout << "banks " << banksToString(banks) << std::endl;

    assert(reallocate({0, 2, 7, 0}) == 5);
    std::cout << reallocate(banks) << std::endl;

    assert(reallocateDistance({0, 2, 7, 0}) == 4);
    std::cout << "banks " << banksToString(banks) << std::endl;
    std::cout << reallocateDistance(banks) << std::endl;

    assert(reallocateDistance2({0, 2, 7, 0}) == 4);
}
