#include <iostream>
#include <string>
#include <regex>
using namespace std;
 
int main() {
	std::regex r("(\\w+)\\s+ <->[\\s,]((?:\\w))+");
    std::string s("test <-> a,b,c,d,e,f,g");
    for(std::sregex_iterator i = std::sregex_iterator(s.begin(), s.end(), r);
                             i != std::sregex_iterator();
                             ++i)
    {
        std::smatch m = *i;
        std::cout << "Match value: " << m.str() << " at Position " << m.position() << '\n';
    }
 
	return 0;
}
