#include <iostream>
#include <string>
#include <regex>

void match()
{
	std::string pattern{"((\\w+)|: (\\w+)|, (\\w+))+"};
	std::cout << "re " << pattern << std::endl;
    std::regex regex(pattern);
    std::string line = "label: first, second, third";
    std::smatch match;
    if (std::regex_match(line, match, regex, std::regex_constants::match_any)) {
        for (auto item: match) {
            std::cout << "- " << item.str() << std::endl;
        }

    }
}

void match2()
{
	std::string pattern = "(\\w+)(?:: (\\w+))?(?:, (\\w+))*";
	std::cout << "re " << pattern << std::endl;
    std::regex regex{pattern};
    std::string line = "label: first, second, third";
    std::smatch match;
    if (std::regex_match(line, match, regex, std::regex_constants::match_any)) {
        for (auto item: match) {
            std::cout << "- " << item.str() << std::endl;
        }


    }
}

void match3(std::string line)
{
	std::string pattern = "(\\w+)(?::\\s*(\\w+)(?:,\\s*(\\w+)(?:,\\s*(\\w+))?)?)?";
	std::cout << "re " << pattern << std::endl;
	std::cout << "line " << line << std::endl;
    std::regex regex{pattern};
    std::smatch match;
    if (std::regex_match(line, match, regex, std::regex_constants::match_any)) {
        for (auto item: match) {
            std::cout << "- " << item.str() << std::endl;
        }


    }
}

void match4(std::string line)
{
	// std::string pattern = "(\\w+)(?::\\s*(\\w+))?(?:(?:,\\s*(\\w+)))*";
	std::string pattern = "(?:^(\\w+)|:\\s*(\\w+)|,\\s*(\\w+)|)+";
	std::cout << "re " << pattern << std::endl;
	std::cout << "line " << line << std::endl;
    std::regex regex{pattern};
    std::smatch match;
    if (std::regex_match(line, match, regex, std::regex_constants::match_any)) {
        for (auto item: match) {
            std::cout << "- " << item.str() << std::endl;
        }


    }
}

int main()
{
	// match();
	// match2();
	match3("label: first");
	match3("label: first, second, third");
	match3("label: first, second, third, onemore");

	match4("label: first, second, third, onemore");
}
