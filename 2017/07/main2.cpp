#include<string>
#include<vector>
#include<regex>
#include <iterator>

#include <fstream>

#include<iostream>
#include<cassert>

// using DataWeight = std::vector<std::pair<std::string, std::vector>>;

template<typename T>
struct TreeNode
{
    T value;
    std::vector<TreeNode> children;
};

using DataItem = std::tuple<int, std::vector<std::string>, std::string>; // weight, children, parent
using Data = std::map<std::string, DataItem>;

using DataTree = TreeNode<std::pair<std::string, int>>;

DataTree getTree(Data data)
{
    DataTree tree;
    for (auto item: data) {
        std::cout << item.first << std::endl;
    }
    return tree;
}


using DataPseudoTree = std::map<std::string, std::tuple<int, std::vector<std::string>, std::string>>; // key: weight, child, parent

int getWeight(Data data, DataItem item)
{
    int weight = std::get<0>(item);
    for (auto child: std::get<1>(item)) {
        std::cout << "child " << child << std::endl;
        weight += getWeight(data, data[child]);
    }
    return weight;
}

DataPseudoTree getPseudoTree(Data data)
{
    DataPseudoTree tree;
    for (auto item: data) {
        std::cout << "item.first " << std::get<0>(item) << std::endl;
        int weight = getWeight(data, std::get<1>(item));
        std::cout << "weight " << weight << std::endl;
        std::get<0>(data[std::get<0>(item)]) = weight;
    }
    for (auto item: data) {
        // tree.push_back({item.first, {item.second.first, item.second.second}});
    }
    return tree;
}

Data readValues(std::string filename)
{
    Data data;
    std::ifstream infile{filename};

	std::string pattern = "(\\w+) \\((\\d+)\\)(?:\\s*->\\s*(.+))?";
    std::regex regex(pattern);
    std::smatch match;
    for (std::string line; std::getline(infile, line);) {
        std::vector<std::string>parent, child;
        if (std::regex_match(line, match, regex, std::regex_constants::match_any)) {
			parent.push_back(match[1].str());
            std::string values = match[3].str();
			if (values != "") {

                std::regex sep(", ");
                std::sregex_token_iterator token(values.cbegin(), values.cend(), sep, -1);
                std::sregex_token_iterator end;
                for (; token != end; ++token) {
                    // std::cout << "token " << *token << std::endl;
                    child.push_back(*token);
                }
			}
        }
        // std::cout << match[2] << std::endl;
        data.insert({match[1].str(), {std::stoi(match[2].str()), child, ""}});

        // data.push_back(line);
    }

    return data;
}

int main() {
    // std::ifstream infile{filename};
    // std::vector<std::string> data{std::istream_iterator<std::string>{infile}, {}};
    Data dataTest = readValues("input-test.txt");
    // getTree(dataTest);
    getPseudoTree(dataTest);
    // assert(getRoot(dataTest) == "tknk");
    // Data data = readValues("input.txt");
    // std::cout << getRoot(data) << std::endl;
}
