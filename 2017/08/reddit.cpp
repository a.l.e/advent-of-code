#include <string>
#include <vector>
#include <map>

#include <fstream>
#include <istream>
#include <sstream>
#include <iterator>

#include <algorithm> // max
#include <functional> // greater, std::function
#include <climits> // INT_MIN

#include <iostream>

struct Line : std::string { friend std::istream& operator>>(std::istream& is, Line& line){return std::getline(is, line);}};

int main() {
    std::map<std::string, std::function<bool(int, int)>> condOpMap = {
        { "==", std::equal_to<int>() }, { "!=", std::not_equal_to<int>() },
        { ">", std::greater<int>() }, { ">=", std::greater_equal<int>() },
        { "<", std::less<int>() }, { "<=", std::less_equal<int>() }
    };
    std::vector<std::string> lines(std::istream_iterator<Line>(std::ifstream("input.txt"), {});
    std::map<std::string, int> r;
    int max2 = INT_MIN;
    for(const auto& line : lines) {
        std::vector<std::string> t(std::istream_iterator<std::string>(std::istringstream(line)), {});
        if(condOpMap[t[5]](r[t[4]], std::stoi(t[6]))) {
            max2 = std::max(r[t[0]] += std::stoi(t[2]) * (t[1] == "dec" ? -1 : 1), max2);
        }
    }
    int max1 = std::max_element(r.begin(), r.end(),
            [](auto& l, auto& r){ return l.second < r.second; })->second;
    std::cout << max1 << " " << max2 << "\n";
}
