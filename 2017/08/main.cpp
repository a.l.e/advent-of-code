#include<fstream>
#include<vector>
#include<map>
#include<utility>
#include<string>
#include<iostream>
#include<cassert>

#include<regex>

struct Line {
    std::string a;
    int inc;
    std::string b;
    std::string op;
    int compare;
};
using Code = std::vector<Line>;

struct Registers {
    std::map<std::string, int> list;
    int max{0};
};

Code parse(std::string filename)
{
    Code code;
    std::ifstream infile{filename};

	std::string pattern = "^(\\w+)\\s+(\\w{3})\\s+([-]?\\d+)\\s+if\\s+(\\w+)\\s+(\\S+)\\s([-]?\\d+).*";
    std::regex regex(pattern);
    std::smatch match;
    for (std::string line; std::getline(infile, line);) {
        if (std::regex_match(line, match, regex, std::regex_constants::match_any)) {
            code.push_back(Line{match[1].str(), (match[2].str() == "inc" ? 1 : -1) * std::stoi(match[3].str()), match[4].str(), match[5].str(), std::stoi(match[6].str())});
        }
    }
    return code;
}

bool compare(int a, std::string op, int b) {
    if (op == "<") { return a < b; }
    else if (op == ">") { return a > b; }
    else if (op == "==") { return a == b; }
    else if (op == "<=") { return a <= b; }
    else if (op == ">=") { return a >= b; }
    else if (op == "!=") { return a != b; }
}

Registers calculate(const Code& code) {
    Registers registers;
    for (auto line: code) {
        int b{registers.list.count(line.b) == 1 ? registers.list.at(line.b) : 0};
        if (compare(b, line.op, line.compare)) {
            auto [it, ok] = registers.list.insert(std::make_pair(line.a, line.inc));
            if (!ok) {
                it->second += line.inc;
            }
            registers.max = std::max(registers.max, it->second);
        }
    }
    return registers;
}

int main()
{
    auto code = parse("input-test.txt");
    auto registers = calculate(code);
    assert(std::max_element(registers.list.begin(), registers.list.end(),
        [](const std::pair<std::string, int>& a, const std::pair<std::string, int>& b) {return a.second < b.second;}
    )->second == 1);
    assert(registers.max == 10);

    code = parse("input.txt");
    registers = calculate(code);
    auto max = std::max_element(registers.list.begin(), registers.list.end(),
        [](const std::pair<std::string, int>& a, const std::pair<std::string, int>& b) {return a.second < b.second;}
    );
    std::cout << "max " << max->second << std::endl;
    std::cout << "max " << registers.max << std::endl;
}
