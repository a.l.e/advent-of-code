# Day 19: A Series of Tubes

`main.cpp` contains my _over engineered_ solution: it uses a map of coordinates to store the active tubes elements.

`main2.cpp` is inspired by a solution found on reddit and stores the map in a vector of strings, one line at time.

the second solution is much shorter and also much faster.
