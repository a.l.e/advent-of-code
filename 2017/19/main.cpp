#include<iostream>
#include<cassert>

#include<string>

#include<map>
#include<utility>

#include <fstream>

struct Position
{
    int x{0}, y{0};

    bool operator==(const Position& b)
    {
        return x == b.x && y == b.y;
    }

    Position& operator+=(Position const& p)
    {
        x += p.x;
        y += p.y;
        return *this;
    }

    Position operator+(Position const& p) const
    {
        return Position{*this} += p;
    }

   bool operator<(const Position& b) const
   {
       return (x < b.x) || ((x == b.x) && (y < b.y));
   }
};


using Maze = std::map<Position, char>;

using Movement = std::map<char, Position>;
Movement movement {
    {'n', {0, 1}},
    {'e', {1, 0}},
    {'s', {0, -1}},
    {'w', {-1, 0}}
};

Maze getMaze(std::string filename)
{
    Maze maze;
    std::ifstream infile{filename};
    int i{0};
    for (std::string line; std::getline(infile, line);) {
        // std::cout << line << std::endl;
        int j{0};
        for (char c: line) {
            if (c != ' ') {
                // std::cout << c << std::endl;
                maze.insert({{j,i}, c});
            }
            j++;
        }
        i--;
    }
    return maze;
}

Position getStart(Maze maze)
{
    for (auto p: maze) {
        if (p.first.y == 0) {
            return p.first;
        }
    }
}

char getNextDirection(Maze maze, Position current, char direction) {
    char nextDirection{direction};
    if (maze.at(current) == '+') {
        if (direction == 'n' || direction == 's') {
            char c{' '};
            try {
                c = maze.at(current + movement.at('e'));
            } catch (std::out_of_range e) {
            }

            if (c != ' ') {
                nextDirection = 'e';
            } else {
                nextDirection = 'w';
            }
        } else {
            char c{' '};
            try {
                c = maze.at(current + movement.at('n'));
            } catch (std::out_of_range e) {
            }

            if (c != ' ') {
                nextDirection = 'n';
            } else {
                nextDirection = 's';
            }
        }
    }
    return nextDirection;
}


std::pair<std::string, int> getWord(Maze maze)
{
    std::string word{};
    auto current = getStart(maze);
    char direction{'s'}; // nesw

    /*
    for (auto p: maze) {
        std::cout << p.first.x << "." << p.first.y << "=" << p.second << std::endl;
    }
    */

    char currentChar = maze.at(current);
    int steps = 0;
    while (currentChar != ' ') {
		steps++;
        if ('A' <= currentChar && currentChar <= 'Z') {
            std::cout << "c " << currentChar << std::endl;
            word += currentChar;
        } else {
            // std::cout << "e " << currentChar << std::endl;
        }

        direction = getNextDirection(maze, current, direction);
        current += movement.at(direction);

        try {
            currentChar = maze.at(current);
        } catch (std::out_of_range e) {
            currentChar = ' ';
        }
    }
    std::cout << std::endl;

    // std::cout << "word " << word << std::endl;
    return {word, steps};
}

int main()
{
    {
        auto maze = getMaze("input-test.txt");
        auto[word, steps] = getWord(maze);
        assert(word == "ABCDEF");
        assert(steps == 38);
    }
    {
        auto maze = getMaze("input.txt");
        auto[word, steps] = getWord(maze);
		std::cout << word << std::endl;
		std::cout << steps << std::endl;
    }
}
