#include<vector>
#include<string>
#include<iterator>
#include<map>
#include<utility> // pair

#include <fstream>

#include<iostream>
#include<cassert>


using Maze = std::vector<std::string>;

Maze getMaze(std::string filename)
{
    Maze maze;
    std::ifstream infile{filename};
    for (std::string line; std::getline(infile, line);) {
        maze.push_back(line);
    }
    return maze;
}

std::string getWord(Maze maze)
{
    std::string word{};

    std::map<char, std::pair<int, int>> movement{
        {'n', {0, -1}},
        {'e', {1, 0}},
        {'s', {0, 1}},
        {'w', {-1, 0}}
    };

    int i{maze.at(0).find('|')}, j{0};
    char direction{'s'}, c{'|'};

    while ((c = maze.at(j).at(i)) != ' ') {
        if ('A' <= c && c <= 'Z') {
            word += c;
        }
        if (c == '+') {
            if (direction == 'n' || direction == 's') {
                direction = (maze.at(j).at(i - 1) != ' ') ? 'w' : 'e';
            } else {
                direction = (maze.at(j - 1).at(i) != ' ') ? 'n' : 's';
            }
        }
        auto next = movement.at(direction);
        i += next.first;
        j += next.second;
    }

    std::cout << "word " << word << std::endl;

    return word;
}

int main()
{
    {
        Maze maze = getMaze("input-test.txt");
        assert(getWord(maze) == "ABCDEF");
    }
    {
        Maze maze = getMaze("input.txt");
        std::cout << getWord(maze) << std::endl;
    }
}
