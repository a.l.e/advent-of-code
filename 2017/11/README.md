# Day 11: Hex Ed

http://adventofcode.com/2017/day/11

the solution uses axial coordinates which let us calculateas

```
dx = x1 - x0
dy = y1 - y0

if sign(dx) == sign(dy)
    max(abs(dx), abs(dy))
else
    abs(dx + dy)
```

(which is the _contrary_ of the solution presented in <https://stackoverflow.com/questions/5084801/manhattan-distance-between-tiles-in-a-hexagonal-grid>)

we need two axis: the y one is n|s (which is the easiest one) and the x one is sw|ne (which is a bit more complex but we cannot do otherwise).

```
          ____                   ↑
         /    \                  n
    ____/  0,1 \____            ____     ↗
   /    \      /    \      nw  /    \  ne
  / -1,1 \____/ 1,0  \        /      \
  \      /    \      /        \      /
   \____/  0,0 \____/      sw  \____/  se
   /    \      /    \     ↗      s
  / -1,0 \____/ 1,-1 \   x       ↑
  \      /    \      /           y
   \____/ 0,-1 \____/
        \      /
         \____/
```

(as explained in https://www.redblobgames.com/grids/hexagons/)
