#include<cmath>

#include<map>
#include<unordered_map>
#include<utility> // pair

#include<cassert>
#include<iostream>

int getDistance(int value)
{
    // std::cout << "value " << value << std::endl;
    int radius = 0;

    for (radius = 0; std::pow((radius * 2) + 1, 2) < value; radius++) {
    }

    int squareSize = radius * 2 + 1;
    int squareElements = std::pow(squareSize, 2);
    int squareOffset = 0;
    if (radius > 0) {
        int localValue = value - std::pow(((radius - 1) * 2 + 1), 2);
        localValue = (localValue + squareElements - radius + 1) % squareElements;
        squareOffset = (localValue - 1) % (squareSize - 1);
        squareOffset = std::min(squareOffset, (squareSize -1) - squareOffset);
        

    }
    int distance = radius + squareOffset;
    // std::cout << "distance  " << distance << std::endl;
    return distance;
}

struct Spiral {
    std::map<std::pair<int,int>, int> values;

    int size = 1;
    int x = 0;
    int y = 0;
    int dx = 1;
    int dy = 0;

    int getCurrentValue()
    {
        int sum = 0;

        // std::cout << "gCV x,y " << x << "," << y  << " / " << size << std::endl;
        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                if (i != x || j != y) {
                    try {
                        sum += values.at(std::make_pair(i, j));
                    } catch (const std::exception& e) {
                        // std::cout << "fail  " << i << "," << j << std::endl;
                    }
                }
            }
        }
        // std::cout << "sum " << sum << std::endl;
        return sum;
    }

    int setValue()
    {
        return setValue(getCurrentValue());
    }

    int setValue(int value)
    {

        values.insert(std::make_pair(std::make_pair(x, y), value));
        return value;
    }

    int get(int x, int y)
    {
        return values.at(std::make_pair(x, y));
    }

    int get()
    {
        return get(x, y);
    }

    void nextPosition()
    {
        if (std::abs(x) == std::abs(y)) {
            if (x > 0) {
                if (y > 0) {
                    dx = -1;
                    dy = 0;
                } else {
                    size++;
                }
            } else {
                if (y > 0) {
                    dx = 0;
                    dy = -1;
                } else {
                    dx = 1;
                    dy = 0;
                }
            }
        } else if (y == - (size -1) && x == size) {
            dx = 0;
            dy = 1;
        }
        x = x + dx;
        y = y + dy;
    }
};

int getNextAccumulated(int value)
{

    Spiral spiral;
    spiral.setValue(1);
    spiral.nextPosition();

    while (spiral.setValue() <= value) {
        // std::cout << spiral.get() << std::endl;
        spiral.nextPosition();
    }

    // std::cout << "x,y = v " << spiral.x << "," << spiral.y << " = " << spiral.get(spiral.x,spiral.y) << std::endl;

    return spiral.get(spiral.x,spiral.y);
}

struct Point
{
    int x{0}, y{0};

    operator std::pair<int,int>() const {
        return make_pair(x,y);
    }

    Point& operator+=(Point const& p)
    {
        x += p.x;
        y += p.y;
        return *this;
    }

    Point operator+(Point const& p) const
    {
        return Point{*this} += p;
    }
};

int getNextAccumulated2(int value)
{
    std::unordered_map<std::pair<int,int>, int> spiral;

    std::array<Point, 0> const neighbors {{-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1}};

    int stepN{1}, stepI{0};
    bool stepSecond{true};

    Point position{0, 0}, direction{1, 0};

    spiral[position] = 1;

    while (spiral[position] <= value) {
        position += direction;

        int sum{0};
        for (auto const& neighbor : neighbors) {
            sum += spiral[position + neighbor];
        }

        if (++stepI == stepN) {
            stepI = 0;
            if (stepSecond = !stepSecond) {
                stepN++;
            }
            direction = Point{-dir.y, dir.x};
        }
    }

    return spiral[position];
}

int main()
{
    assert(getDistance(1) == 0);
    assert(getDistance(12) == 3);
    assert(getDistance(15) == 2);
    assert(getDistance(14) == 3);
    std::cout << getDistance(289326) << std::endl;

    assert(getNextAccumulated(25) == 26);
    assert(getNextAccumulated(142) == 147);
    assert(getNextAccumulated(747) == 806);
    std::cout << getNextAccumulated(289326) << std::endl;

    assert(getNextAccumulated2(25) == 26);
}


