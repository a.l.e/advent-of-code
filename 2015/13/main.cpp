#include <iostream>
#include <fstream>
#include <string>

#include <sstream>
#include <iterator>
#include <vector>

#include <map>
#include <set>

#include <algorithm>

int main()
{
    bool part_two = true;
    std::map<std::string, std::map<std::string, int>> happiness_gain{};

    std::ifstream input("input.txt");
    for(std::string line; getline(input, line);) {
        line.pop_back();
        std::istringstream iss(line);
        std::vector<std::string> tokens{std::istream_iterator<std::string>{iss}, {}};
        int happiness = (tokens[2] == "gain" ? 1 : -1) *std::stoi(tokens[3]);
        happiness_gain[tokens[0]][tokens[10]] = happiness;
    }

    std::vector<std::string> guests{};
    std::string old_key{};
    for (const auto& [key, value]: happiness_gain) {
        if (key != old_key) {
            guests.push_back(key);
        } else {
        }
        old_key = key;
    }
    if (part_two) {
        for (const auto& guest: guests) {
            happiness_gain[guest]["ale"] = 0;
            happiness_gain["ale"][guest] = 0;
        }
        guests.push_back("ale");
    }

    int max_happiness = 0;
    do {
        int happiness{0};
        auto prev = guests.back();
        for (const auto& guest: guests) {
            happiness += happiness_gain[prev][guest];
            happiness += happiness_gain[guest][prev];
            prev = guest;
        }
        max_happiness = std::max(max_happiness, happiness);
    } while (std::next_permutation(guests.begin(), guests.end()));
    std::cout << max_happiness << std::endl;
}
