#include <iostream>
#include <fstream>
#include <streambuf>
#include "json.hpp"

using json = nlohmann::json;

int reduce(const json& j) {
    int total{0};
    if (j.is_object()) {
        if (true) { // part two
            bool has_red = false;
            for (const auto& [key, value]: j.items()) {
                if (value.is_string() && value.get<std::string>() == "red") {
                    has_red = true;
                    break;

                }
            }
            if (has_red) {
                return 0;
            }
        }
        for (const auto& [key, value]: j.items()) {
            if (value.is_number()) {
                total += value.get<int>();
            } else {
                total += reduce(value);
            }
        }
    } else if (j.is_array()) {
        for (const auto& value: j) {
            if (value.is_number()) {
                total += value.get<int>();
            } else {
                total += reduce(value);
            }
        }
    }
    return total;
}

int main()
{
    {
        auto j = json::parse("[1,2,3]");
        std::cout << j[0] << std::endl;
        std::cout << reduce(j) << std::endl;
    }
    {
        auto j = json::parse("{\"a\":{\"b\":4},\"c\":-1}");
        std::cout << reduce(j) << std::endl;
    }
    {
        
        auto j = json::parse("{\"a\":[-1,1]}");
        std::cout << reduce(j) << std::endl;
    }
    {
        auto j = json::parse("[-1,{\"a\":1}]");
        std::cout << reduce(j) << std::endl;
    }
    {
        auto j = json::parse("[]");
        std::cout << reduce(j) << std::endl;
    }
    {
        auto j = json::parse("{}");
        std::cout << reduce(j) << std::endl;
    }

    {
        std::ifstream input("input.txt");
        std::string json_str((std::istreambuf_iterator<char>(input)), {});
        auto j = json::parse(json_str);
        std::cout << reduce(j) << std::endl;
    }
}
