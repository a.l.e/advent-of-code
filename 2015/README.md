# Advents of Code 2015

## Day 7

Part 1:

Reading all lines, parsed in a circular list and putting the signals in a map.

The list is parsed over and over until all items have a value and can be put in the map.

- read a file into a string
- Bitwise operators
- Circular linked list
- Split string
- Maps
- Optional

Part 2:

Read all lines and build a map with objects that can calculate the value for each wire.

Start from the required value and recursively build the value.

- I could have solved by modifying the input for b in the `input.txt` file
- Inheritance with factory
- Virtual functions
- 

## Day 9

- Permutations (implementation of the Python sample implementation in Python's itertools)
  - should probably have used `next_permutation`
- Tokenize string by spaces
- Non directed graph

## Day 12

- Reading json with <https://github.com/nlohmann/json>

## Day 13

- Permutations using `next_permutation`
- Tokenize string by spaces
- Directed graph

## Day 13

Nope.

I've skimmed through the solution and looked for a solution but there seems to be none.

Brute forcing and setting  a fxied number of recepies seems to be thonly doable way of solving this.

## Day 15

- Returning lambda returning different lambdas according to an if condition (faking a real case switch).

## Day 19

part 2 does not really seem to be _solvable_.

I got a hard time understanding and implementing the solution from redit... and even after having seen it, i'm not sure i really understood why it works.

## Day 20

I have to try it again in the future... and better understand how to find all numbers that can divide a number.


## Day 24

- Combinations (implementation of the Python sample implementation in Python's itertools)
