#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <string>
#include <unordered_map>
#include <algorithm>
#include <optional>
#include <memory>

struct Gate;

using Wires = std::unordered_map<std::string, std::unique_ptr<Gate>>;

std::vector<std::string> split(std::string string, std::string delimiter)
{
    size_t pos_start = 0, pos_end;
    std::string token;
    std::vector<std::string> tokens;

    while ((pos_end = string.find(delimiter, pos_start)) != std::string::npos) {
        token = string.substr (pos_start, pos_end - pos_start);
        pos_start = pos_end + delimiter.size();
        tokens.push_back (token);
    }

    tokens.push_back (string.substr(pos_start));
    return tokens;
}

struct Gate
{
    Gate() = delete;
    Gate(std::string a) {
        if (is_number(a)) {
            this->a = std::stoi(a);
        } else {
            in_a = a;
        }
    }
    Gate(std::string a, std::string b) {
        if (is_number(a)) {
            this->a = std::stoi(a);
        } else {
            in_a = a;
        }
        if (is_number(b)) {
            this->b = std::stoi(b);
        } else {
            in_b = b;
        }
    }
    virtual ~Gate() = default;

    static std::unique_ptr<Gate> factory(const std::vector<std::string> tokens);

    virtual int get(const Wires& wires) = 0;

    void virtual reset() {
        if (in_a) {
            a = {};
        }
        if (in_b) {
            b = {};
        }
    }

    std::optional<int> a;
    std::optional<int> b;
    std::optional<std::string> in_a;
    std::optional<std::string> in_b;

protected:
    void virtual resolve_in(const Wires& wires) {
        if (!a && in_a) {
            a = wires.at(*in_a)->get(wires);
        }
        if (!b && in_b) {
            b = wires.at(*in_b)->get(wires);
        }
    };

private:
    // if the token is a number or is a defined field, return it as an int
    bool is_number(const std::string& s) const
    {
        return !s.empty() && std::find_if(s.begin(), 
                s.end(), [](unsigned char c) { return !std::isdigit(c); }) == s.end();
    }
};

struct Gate_value : public Gate
{
    Gate_value(std::string a) : Gate(a) {};
    int get(const Wires& wires) override {
        resolve_in(wires);
        return *a;
    }
};

struct Gate_AND : public Gate
{
    Gate_AND(std::string a, std::string b) : Gate(a, b) {};
    int get(const Wires& wires) override {
        resolve_in(wires);
        return *a & *b;
    }
};

struct Gate_OR : public Gate
{
    Gate_OR(std::string a, std::string b) : Gate(a, b) {};
    int get(const Wires& wires) override {
        resolve_in(wires);
        return *a | *b;
    }
};

struct Gate_NOT : public Gate
{
    Gate_NOT(std::string a) : Gate(a) {};
    int get(const Wires& wires) override {
        resolve_in(wires);
        return 65535 - *a;
    }
};

struct Gate_LSHIFT : public Gate
{
    Gate_LSHIFT(std::string a, std::string b) : Gate(a, b) {};
    int get(const Wires& wires) override {
        resolve_in(wires);
        return *a << *b;
    }
};

struct Gate_RSHIFT : public Gate
{
    Gate_RSHIFT(std::string a, std::string b) : Gate(a, b) {};
    int get(const Wires& wires) override {
        resolve_in(wires);
        return *a >> *b;
    }
};

std::unique_ptr<Gate> Gate::factory(const std::vector<std::string> tokens)
{
    switch (tokens.size()) {
        case 1:
            return std::unique_ptr<Gate>(new Gate_value(tokens.at(0)));
        case 2:
            // it's always a NOT
            return std::unique_ptr<Gate>(new Gate_NOT(tokens.at(1)));
        break;
        case 3: {
            if (tokens.at(1) == "AND") {
                return std::unique_ptr<Gate>(new Gate_AND(tokens.at(0), tokens.at(2)));
            } else if (tokens.at(1) == "OR") {
                return std::unique_ptr<Gate>(new Gate_OR(tokens.at(0), tokens.at(2)));
            } else if (tokens.at(1) == "LSHIFT") {
                return std::unique_ptr<Gate>(new Gate_LSHIFT(tokens.at(0), tokens.at(2)));
            } else if (tokens.at(1) == "RSHIFT") {
                return std::unique_ptr<Gate>(new Gate_RSHIFT(tokens.at(0), tokens.at(2)));
            } else {
                std::cout << "unknown 3 token " << tokens.at(1) << std::endl;;
                return std::unique_ptr<Gate>(new Gate_value("42"));
            }
        } 
        default:
            std::cout << "unknown number of tokens" << std::endl;;
            return std::unique_ptr<Gate>(new Gate_value("42"));
        break;
    }
}

int main()
{
    std::vector<std::string> lines {
        "123 -> x",
        "456 -> y",
        "x AND y -> d",
        "x OR y -> e",
        "x LSHIFT 2 -> f",
        "y RSHIFT 2 -> g",
        "NOT x -> h",
        "NOT y -> i"
    };

    Wires wires{};
    std::ifstream file("input.txt");
    std::string line;
    while (std::getline(file, line)) {
    // for (const auto& line: lines) {
        auto tokens = split(line, " -> ");
        auto destination = tokens.at(1);
        tokens = split(tokens.at(0), " ");
        wires[destination] = Gate::factory(tokens);
    }

    // for (const auto& wire: wires) {
    //     std::cout << wire.first << " >> " << wire.second->get(wires) << std::endl;
    // }
    std::cout << wires.at("a")->get(wires) << std::endl;
    for (auto& wire: wires) {
        wire.second->reset();
    }
    wires.at("b")->a = 46065;
    std::cout << wires.at("a")->get(wires) << std::endl;
}
