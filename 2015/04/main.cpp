#include <iostream>
#include <array>
#include <string>

#include <limits>

#include <iomanip>
#include <sstream>

#include <openssl/md5.h>

// https://stackoverflow.com/a/38814538/5239250
// https://stackoverflow.com/a/28943536/5239250
std::string md5(const std::string &str) {
    std::array<unsigned char, MD5_DIGEST_LENGTH> result;
    MD5(reinterpret_cast<const unsigned char*>(str.data()), str.size(), result.data());

    std::ostringstream sout;
    sout<<std::hex<<std::setfill('0');
    for(auto c: result)
        sout << std::setw(2) << (int)c;

    return sout.str();
}


int main()
{
    // std::cout << md5("abcdef609043") << std::endl;
    int n = 6;
    auto zeros = std::string(n, '0');
    // std::string secret = "abcdef";
    // std::string secret = "pqrstuv";
    std::string secret = "ckczppom";
    for (unsigned int i = 0; i < std::numeric_limits<int>::max(); i++) {
        std::string combination = secret + std::to_string(i);
        auto result = md5(combination);
        if (result.substr(0, n) == zeros) {
            std::cout << i << std::endl;
            break;
        }
    }
}
