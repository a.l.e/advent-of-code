cmake_minimum_required(VERSION 3.10.0)

project(hash)

find_package(OpenSSL REQUIRED)

add_executable(hash
    main.cpp
)

target_link_libraries(hash OpenSSL::SSL)
