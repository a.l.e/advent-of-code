#include <fstream>
#include <sstream>
#include <iostream>
#include <iterator>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <string>
#include <algorithm>
#include <climits>

using List = std::vector<std::string>;
using Combinations = std::vector<List>;
using Permutations = std::vector<List>;

// def permutations(iterable, r=None):
//     # permutations('ABCD', 2) --> AB AC AD BA BC BD CA CB CD DA DB DC
//     # permutations(range(3)) --> 012 021 102 120 201 210
//     pool = tuple(iterable)
//     n = len(pool)
//     r = n if r is None else r
//     if r > n:
//         return
//     indices = list(range(n))
//     cycles = list(range(n, n-r, -1))
//     yield tuple(pool[i] for i in indices[:r])
//     while n:
//         for i in reversed(range(r)):
//             cycles[i] -= 1
//             if cycles[i] == 0:
//                 indices[i:] = indices[i+1:] + indices[i:i+1]
//                 cycles[i] = n - i
//             else:
//                 j = cycles[i]
//                 indices[i], indices[-j] = indices[-j], indices[i]
//                 yield tuple(pool[i] for i in indices[:r])
//                 break
//         else:
//             return
Permutations get_permutations(List pool, int r)
{
	Permutations result{};
    List result_item{};

	int n = pool.size();
	if (r > n) {
        return {};
    }

    std::vector<int> indices{};
    for (int i = 0; i < n; i++) {
        indices.push_back(i);
    }

    std::vector<int> cycles{};
    for (int i = n; i > n - r; i--) {
        cycles.push_back(i);
    }

    for (int i = 0; i < r; i++) {
        result_item.push_back(pool.at(indices.at(i)));
    }
	result.push_back(result_item);

    while (true) {
        // for i in reversed(range(r)):
        bool found = false;
        for (int i = n - 1; i >= 0; i--) {
            cycles.at(i) -= 1;
            if (cycles.at(i) == 0) {
                // move i element to end
                // indices[i:] = indices[i+1:] + indices[i:i+1]
                auto it = indices.begin() + i;
                std::rotate(it, it + 1, indices.end());
                cycles.at(i) = n - i;
            } else {
                auto j = cycles.at(i);
                // indices[i], indices[-j] = indices[-j], indices[i]
                auto first = std::next(indices.begin(), i);
                auto second = std::prev(indices.end(), j);
                std::iter_swap(first, second);
                result_item.clear();
                for (int i = 0; i < r; i++) {
                    result_item.push_back(pool.at(indices.at(i)));
                }
                result.push_back(result_item);
                found = true;
                break;
            }
        }
        if (!found)
            break;
    }

    return result;
}

// https://www.techiedelight.com/use-std-pair-key-std-unordered_map-cpp/
// std::map can simply use std::pair as key, std::unordered_map needs a hash function
using Route = std::pair<std::string, std::string>;
struct pair_hash
{
    template <class T1, class T2>
        std::size_t operator() (const std::pair<T1, T2> &pair) const
        {
            return std::hash<T1>()(pair.first) ^ std::hash<T2>()(pair.second);
        }
};

int main()
{
    std::unordered_map<Route, int, pair_hash> distance{};
    std::unordered_set<std::string> places{};

    std::ifstream file("input.txt");
    // std::ifstream file("input-test.txt");
    std::string line;
    while (std::getline(file, line)) {
        std::istringstream iss(line);
        std::vector<std::string> tokens{std::istream_iterator<std::string>{iss},
            std::istream_iterator<std::string>{}};
        distance.insert({std::make_pair(tokens.at(0), tokens.at(2)), std::stoi(tokens.at(4))});
        distance.insert({std::make_pair(tokens.at(2), tokens.at(0)), std::stoi(tokens.at(4))});
        places.insert(tokens.at(0));
        places.insert(tokens.at(2));
    }

    int n = places.size();

    std::vector<std::string> places_v;
    places_v.insert(places_v.end(), places.begin(), places.end());

    int min_distance{INT_MAX};
    int max_distance{0};
    for (const auto& permutation: get_permutations(places_v, n)) {
        int total_distance = 0;
        for (int i = 0; i < n - 1; i++) {
            total_distance += distance.at(std::make_pair(permutation.at(i), permutation.at(i + 1)));
        }
        min_distance = std::min({min_distance, total_distance});
        max_distance = std::max({max_distance, total_distance});
    }
    std::cout << min_distance << std::endl;
    std::cout << max_distance << std::endl;
}
