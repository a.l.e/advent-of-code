#include <algorithm>
#include <iostream>
#include <vector>

int main()
{
    std::vector<int> indices{};
    for (int i = 0; i < 5; i++) {
        indices.push_back(i);
    }
    auto first = std::next(indices.begin(), 1);
    auto second = std::prev(indices.end(), 1);
    std::iter_swap(first, second);

    for (const auto i: indices) {
        std::cout << i << std::endl;
    }

}
