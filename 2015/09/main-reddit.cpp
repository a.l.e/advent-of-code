#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <regex>
#include <algorithm>
#include <climits>

// TODO: i get a segementation fault...

int main(int argc, char* argv[])
{
    std::fstream fs(argv[1]);
    std::map<std::string, std::map<std::string, unsigned>> distances;

    std::string s;
    while (std::getline(fs, s)){
        std::smatch m;
        std::regex_match(s, m, std::regex("(.*) to (.*) = (.*)"));
        distances[m[1]][m[2]] = distances[m[2]][m[1]] = (unsigned)std::stoi(m[3]);
    }

    std::vector<std::string> cities(distances.size());
    std::transform(distances.begin(), distances.end(), cities.begin(), [](auto p){return p.first;});

    unsigned minDistance = UINT_MAX;
    do{
        unsigned currDist = 0;
        for (auto it = cities.begin(); it != (cities.end() - 1); ++it)
            currDist += distances[*it][*(it+1)];

        minDistance = std::min(currDist, minDistance);
    }while (std::next_permutation(cities.begin(), cities.end()));

    std::cout << minDistance << "\n";
}
