import sys
import itertools

places = set()
distances = {}
for line in open('input.txt'):
    source, _, destination, _, distance = line.split()
    places.add(source)
    places.add(destination)
    distances.setdefault(source, {})[destination] = int(distance)
    distances.setdefault(destination, {})[source] = int(distance)

min_path = sys.maxsize
max_path = 0

for permutation in itertools.permutations(places):
    dist = sum(map(lambda x, y: distances[x][y], permutation[:-1], permutation[1:]))
    min_path = min(min_path, dist)
    max_path = max(max_path, dist)

print(min_path, max_path)
