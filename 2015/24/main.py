import itertools
import functools
import operator

# numbers = list(range(1, 6)) + list(range(7, 12))
numbers = [1, 3, 5, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113];
groups_n = 4

n = len(numbers)
group_weight = sum(numbers) // groups_n
print('group_weight', group_weight)

for i in range(2, n - groups_n * 2 + 1):
    ways = [item for item in itertools.combinations(numbers, i) if sum(item) == group_weight]
    if len(ways) > 0:
        break

ways.sort(key = lambda x: functools.reduce(operator.mul, x, 1))

print(ways[0])
print(functools.reduce(operator.mul, ways[0], 1))
