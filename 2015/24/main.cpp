#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>

using List = std::vector<int>;
using Combinations = std::vector<List>;

/**
 translation of the python sample code of itertools.combinations()
 https://docs.python.org/3/library/itertools.html#itertools.combinations
 def combinations(iterable, r):
     # combinations('ABCD', 2) --> AB AC AD BC BD CD
     # combinations(range(4), 3) --> 012 013 023 123
     pool = tuple(iterable)
     n = len(pool)
     if r > n:
         return
     indices = list(range(r))
     yield tuple(pool[i] for i in indices)
     while True:
         for i in reversed(range(r)):
             if indices[i] != i + n - r:
                 break
         else:
             return
         indices[i] += 1
         for j in range(i+1, r):
             indices[j] = indices[j-1] + 1
         yield tuple(pool[i] for i in indices)
 */
Combinations get_combinations(List pool, int r)
{
    Combinations result{};
    List result_item{};
    auto n = pool.size();
    if (r > n)
        return {};

    std::vector<int> indices{};
    for (int i = 0; i < r; i++) {
        indices.push_back(i);
    }

    for (auto i: indices) {
        result_item.push_back(pool.at(i));
    }
    result.push_back(result_item);

    while (true) {
        bool found = false;
        int k = 0;
        for (int i = r - 1; i >= 0; i--) {
            if (indices[i] != i + n - r) {
                found = true;
                k = i;
                break;
            }

        }
        if (!found)
            break;

        indices.at(k)++;
        for (int j = k + 1; j < r; j++) {
            indices.at(j) = indices.at(j - 1) + 1;
        }

        result_item.clear();
        for (int i: indices) {
            result_item.push_back(pool.at(i));
        }
        result.push_back(result_item);
    }

    return result;
}

void print_combination(List combination) {
    std::cout << "[";
    for (auto item: combination) {
        std::cout << item << " ";
    }
    std::cout << "]" << std::endl;
}

void print_entanglement(List combination)
{
    std::cout << std::accumulate(combination.begin(), combination.end(), static_cast<long long>(1), std::multiplies<long long>()) << std::endl;
}

int main()
{
    // List list{1,2,3};
    // List numbers{1, 2, 3, 4, 5, 7, 8, 9, 10, 11};
    // List numbers{1, 2, 3, 5, 7, 13, 17, 19, 23, 29, 31, 37, 41, 43, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113};
    List numbers{1, 3, 5, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113};
    const int groups_n = 4;
    
    int n = numbers.size();
    std::cout << "n " << n << std::endl;;
    int group_weight = std::accumulate(numbers.begin(), numbers.end(), 0) / groups_n;
    std::cout << "group_weight " << group_weight << std::endl;;

    Combinations result{};
    bool found = false;
    for (int i = 2; i <= n - (groups_n * 2); i++) {
        for (const auto& item: get_combinations(numbers, i)) {
            if (std::accumulate(item.begin(), item.end(), 0) != group_weight)
                continue;

            found = true;
            result.push_back(item);
        }
        if (found)
            break;
    }

    if (result.size() == 0) {
        std::cout << "no combination found" << std::endl;
        return 0;
    }
    if (result.size() == 1) {
        print_combination(result.at(0));
        print_entanglement(result.at(0));
        return 0;
    }

    std::sort(result.begin(), result.end(), [](const auto& a, const auto& b) {
        return std::accumulate(a.begin(), a.end(), static_cast<long long>(1), std::multiplies<long long>()) <
            std::accumulate(b.begin(), b.end(), static_cast<long long>(1), std::multiplies<long long>());
    });

    print_combination(result.at(0));
    print_entanglement(result.at(0));

    // for (const auto& combination: result) {
    //     print_combination(combination);
    //     print_entanglement(combination)
    //     std::cout << std::endl;
    // }
    // [1 83 101 103 107 113 ]
    // 1: 10439961859
    // 2: 72050269
}
