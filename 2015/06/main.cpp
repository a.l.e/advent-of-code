#include <array>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <tuple>

using Grid = std::array<std::array<bool, 1000>, 1000>;
using Grid_dimmed = std::array<std::array<int, 1000>, 1000>;

std::vector<std::string> split(std::string string, char delimiter)
{
    size_t pos_start = 0, pos_end;
    std::string token;
    std::vector<std::string> tokens;

    while ((pos_end = string.find(delimiter, pos_start)) != std::string::npos) {
        token = string.substr (pos_start, pos_end - pos_start);
        pos_start = pos_end + 1;
        tokens.push_back (token);
    }

    tokens.push_back (string.substr(pos_start));
    return tokens;
}

std::tuple<int, int, int, int, bool, bool> read_line(std::string line) {
    int x_0, y_0, x_1, y_1;
    bool toggle = false;
    bool turn_on = false;
    auto tokens = split(line, ' ');
    if (line.substr(0, 6) == "toggle") {
        toggle = true;

        auto coordinates = split(tokens.at(1), ',');
        x_0 = std::stoi(coordinates.at(0));
        y_0 = std::stoi(coordinates.at(1));
        coordinates = split(tokens.at(3), ',');
        x_1 = std::stoi(coordinates.at(0));
        y_1 = std::stoi(coordinates.at(1));
    } else {
        turn_on = line.substr(5, 2) == "on";

        auto coordinates = split(tokens.at(2), ',');
        x_0 = std::stoi(coordinates.at(0));
        y_0 = std::stoi(coordinates.at(1));
        coordinates = split(tokens.at(4), ',');
        x_1 = std::stoi(coordinates.at(0));
        y_1 = std::stoi(coordinates.at(1));
    }
    // std:: cout << x_0 << " " <<  y_0 << " " <<  x_1 << " " <<  y_1 << " " << std::endl;
    return {x_0, y_0, x_1, y_1, toggle, turn_on};
}

void switch_lights(Grid& grid, int x_0, int y_0, int x_1, int y_1, bool toggle, bool turn_on)
{
    for (int j = y_0; j <= y_1;  j++) {
        for (int i = x_0; i <= x_1 ;  i++) {
            if (toggle) {
                grid[j][i] = !grid[j][i];
            } else {
                grid[j][i] = turn_on;
            }
        }
    }
}

void dimm_lights(Grid_dimmed& grid, int x_0, int y_0, int x_1, int y_1, bool toggle, bool turn_on)
{
    for (int j = y_0; j <= y_1;  j++) {
        for (int i = x_0; i <= x_1 ;  i++) {
            if (toggle) {
                grid[j][i] += 2;
            } else if (turn_on) {
                grid[j][i]++;
            } else if (grid[j][i] > 0) {
                grid[j][i]--;
            }
        }
    }
}

int main()
{
    Grid grid{};
    Grid_dimmed grid_dimmed{};
    std::ifstream file("input.txt");
    std::string line;
    while (std::getline(file, line)) {
    // {
        // std::string line = "turn on 0,0 through 999,999";
        // std::string line = "toggle 0,0 through 999,0";
        // std::string line = "turn on 0,0 through 0,0";
        // std::string line = "toggle 0,0 through 999,999";
        auto [x_0, y_0, x_1, y_1, toggle, turn_on] = read_line(line);
        switch_lights(grid, x_0, y_0, x_1, y_1, toggle, turn_on);
        dimm_lights(grid_dimmed, x_0, y_0, x_1, y_1, toggle, turn_on);
    }
    int n{0};
    int m{0};
    for (int j = 0; j < 1000;  j++) {
        for (int i = 0; i < 1000 ;  i++) {
            if (grid[j][i]) {
                n++;
            }
            m += grid_dimmed[j][i];
        }
    }
    std::cout << n << std::endl;
    std::cout << m << std::endl;
}
