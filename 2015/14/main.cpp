#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>
#include <string>
#include <vector>
#include <algorithm>

#include <unordered_map>

void part_1() {
    // int time{1000};
    int time{2503};
    int max_distance{0};
    // std::ifstream input("input-test.txt");
    std::ifstream input("input.txt");
    for (std::string line; getline(input, line);) {
        std::istringstream iss(line);
        std::vector<std::string> tokens{std::istream_iterator<std::string>{iss}, {}};
        int speed = std::stoi(tokens[3]);
        int fly_time = std::stoi(tokens[6]);
        int rest_time = std::stoi(tokens[13]);
        std::cout << speed << " " << fly_time << " " << rest_time << std::endl;
        int total_time = fly_time + rest_time;
        int cycles = time / total_time;
        int distance = cycles * speed * fly_time;
        distance += std::min(fly_time, time % total_time) * speed;
        max_distance = std::max(max_distance, distance);
    }
    std::cout << max_distance << std::endl;
}

struct Deindeer {
    Deindeer() :
        speed{speed}, burst_time{0}, rest_time{0},
        cycle_time{0} {}
    Deindeer(int speed, int burst_time, int rest_time) :
        speed{speed}, burst_time{burst_time}, rest_time{rest_time},
        cycle_time{burst_time + rest_time} {}
    int speed;
    int burst_time;
    int rest_time;
    int cycle_time;
    int distance{0};
    int points{0};
};

void part_2()
{
    std::unordered_map<std::string, Deindeer> deindeers{};
    // int time{1000};
    // std::ifstream input("input-test.txt");
    int time{2503};
    std::ifstream input("input.txt");
    for (std::string line; getline(input, line);) {
        std::istringstream iss(line);
        std::vector<std::string> tokens{std::istream_iterator<std::string>{iss}, {}};
        Deindeer deindeer{std::stoi(tokens[3]), std::stoi(tokens[6]), std::stoi(tokens[13])};
        deindeers[tokens[0]] = deindeer;
    }
    for (int i = 0; i < time; i++) {
        int max_distance = 0;
        std::vector<std::string> max_deindeers{};
        for (auto& [key, value]: deindeers) {
            if ((i % value.cycle_time) < value.burst_time) {
                value.distance += value.speed;
            }
            if (value.distance >= max_distance) {
                if (value.distance > max_distance) {
                    max_deindeers.clear();
                    max_distance = value.distance;
                }
                max_deindeers.push_back(key);
            }
        }
        for (const auto& deindeer: max_deindeers) {
            deindeers[deindeer].points++;
        }
    }
    int max_points = 0;
    for (auto& [key, value]: deindeers) {
        // std::cout << value.points << std::endl;
        max_points = std::max(max_points, value.points);
    }
    std::cout << max_points << std::endl;
}

int main()
{
    // part_1();
    part_2();
}
