# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring

def part_1(filename):
    result = 0
    numbers = {}
    symbols = {}
    current_number_position = None
    with open(filename) as f:
        for i, line in enumerate([line.rstrip() for line in f]):
            for j, c in enumerate(line):
                if c.isdigit():
                    if current_number_position is None:
                        numbers[(i, j)] = c
                        current_number_position = (i, j)
                    else:
                        numbers[current_number_position] += c
                else:
                    current_number_position = None
                    if c != '.':
                        symbols[(i, j)] = c

    for position, number in numbers.items():
        for j, _ in enumerate(number):
            if has_operator((position[0], position[1] + j), symbols):
                result += int(number)
                break


    return result

def has_operator(position, symbols):
    directions = ((-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1))
    for d in directions:
        if (position[0] + d[0], position[1] + d[1]) in symbols:
            return True
    return False

def part_2(filename):
    result = 0
    with open(filename) as f:
        schematic = []
        for line in [line.rstrip() for line in f]:
            schematic.append(line)
    for i, line in enumerate(schematic):
        for j, c in enumerate(line):
            if not c.isdigit() and c != '.':
                if len(gears :=get_gears((i, j), schematic)) == 2:
                    result += gears[0] * gears[1]

    return result

def get_gears(position, schematic):
    result = {}
    directions = ((-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1))
    for d in directions:
        i = position[0] + d[0]
        j = position[1] + d[1]
        if schematic[i][j].isdigit():
            # get the number from its beginning to the its end
            for jj in range(j, -1, -1):
                if not schematic[i][jj].isdigit():
                    start = jj + 1
                    break
            else:
                start = 0
            for jj in range(j, len(schematic[i])):
                if not schematic[i][jj].isdigit():
                    end = jj
                    break
            else:
                end = len(schematic[i])
            # a symbol can touch multiple times the same number
            result[(i, start)] = int(schematic[i][start:end])
    return list(result.values())

def main():
    assert part_1('test.txt') == 4361
    print(part_1('input.txt'))
    assert part_2('test.txt') == 467835
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()
