# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring
from dataclasses import dataclass
import itertools

@dataclass
class Point:
    x: int
    y: int

def part_1(filename, emptiness_factor = 2):
    result = 0

    galaxies = []
    rows = set()
    columns = set()
    with open(filename) as f:
        for i, line in enumerate([line.rstrip() for line in f]):
            for j, c in enumerate(line):
                if c == '#':
                    galaxies.append(Point(j, i))
                    rows.add(i)
                    columns.add(j)

    # expand the intergaltic space because of skiped rows and columns
    increment = 0
    increment_x = {}
    increment_y = {}
    for i in range(max(rows) + 1):
        if i not in rows:
            increment += emptiness_factor - 1
        else:
            increment_y[i] = increment
    increment = 0
    for i in range(max(columns) + 1):
        if i in columns:
            increment_x[i] = increment
        else:
            increment += emptiness_factor - 1

    for galaxy in galaxies:
        galaxy.x += increment_x[galaxy.x]
        galaxy.y += increment_y[galaxy.y]

    # calculate the distances
    for a, b in itertools.combinations(galaxies, 2):
        result += max(a.x, b.x) - min(a.x, b.x) + \
            max(a.y, b.y) - min(a.y, b.y)

    return result

def main():
    print(part_1('test.txt', 1))
    assert part_1('test.txt') == 374
    print(part_1('input.txt'))
    # part 2 is just part 1 with a wider gap
    assert part_1('test.txt', 10) == 1030
    assert part_1('test.txt', 100) == 8410
    print(part_1('input.txt', 1000000))

if __name__ == "__main__":
    main()
