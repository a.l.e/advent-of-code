# Advent of Code 2023

- 01: A bit harder than usual for a first day. The second part is not very hard but a bit tricky (and as the saying goes: if you have a problem and you go for regular expressions, now you have two problems).  
  All in all, a nice little exercise.  
  Topic: matching text in a string.
- 02: Once you did a U-Turn from day 1, then it's a good introduction to regular expressions.  
  Topic: Finding all matches with a regex or splitting iteratively a string... as you prefer.
- 03: I like using dicts of coordinates. It went well for part one, but in part two I had to reset my solution and work on a list of chars.  
  A bit more work to do than the previous days: the puzzle is nice, but the solution feels a bit brute forcy.
- 04: Spent too much time on trying (and finally failing) to do a double list comprehension for reading ints from each part of the line. Part two has a few tricky details, but at the end, the code is rather trivial.  
  Somehow, I find it satisfying to work hard to create code that looks trivial...
- 05: A firts solution: too slow for the input data; improved it and got the first star. Adapting it for the second part works for the test data but takes forever on the real data. Started splitting ranges: hard to debug...
  Some refactoring, a few ASCII schemas, a bit of unit testing later, and after having added multiple asserts making sure that the ranges are _sane_, here it is the solution with instant result!  
- 06: A simple for loop does the job. Easy.
- 07: Poker. Almost. In part one, those who carefully read, are ahead of the pack...  
  Part two is a bit tricky, but nothing to worry about... again, if you read EVERYTHING!  
  The highlight for the C++ implementation being the cartesian product... which I could then trash, because it's not really needed to get the solution.
- 08: In the first part, it's easy to find the path... in part two it takes forever, if you don't find the (mathematical?) pattern.
- 09: Easy. A bit boring.
- 10: Complex. Interesting. But it's really not easy to find a good way for checking if an item is inside of shape or not... Part two is not finished yet.
- 11: I was lucky enough, to choose _the proper_ data structures, when reading in the data: it made things much easier (or even easy) when calculating the distances between the galaxies on the map.
- 14: Part one is cute and can be solved with simple tricks. For part two, more effort is needed. One day, I will grow the reflex, to look for loops, when a task must be repeated 1000000000 times...
- 15: Part is easy. Really. Part is more complex to read and understand than it is to code. A good candidate for the OpenTechSchool Christmas party...

## Python

### `main()`

A small template for getting started:

```py
# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring

def part_1(filename):
    result = 0
    with open(filename) as f:
        for line in [line.rstrip() for line in f]:
            print(line)

    return result

def part_2(filename):
    result = 0
    with open(filename) as f:
        for line in [line.rstrip() for line in f]:
            print(line)

    return result

def main():
    assert part_1('test.txt') == 0
    # print(part_1('input.txt'))
    # assert part_2('test.txt') == 0
    # print(part_2('input.txt'))

if __name__ == "__main__":
    main()
```
### Read text file

#### Line by line

```py
with open(filename) as f:
    for line in [line.rstrip() for line in f]:
```

### Point
```py
from dataclasses import dataclass
@dataclass
class Point:
    x: int
    y: int
```

### Sorting with a compare function

```py
def compare_hands_counter(left, right):
    if left[1] > right[1]:
        return -1
    if left[1] < right[1]:
        return 1
    if CARDS_VALUES[left[0]] < CARDS_VALUES[right[0]]:
        return -1
    if CARDS_VALUES[left[0]] > CARDS_VALUES[right[0]]:
        return 1
    return 0

counter = sorted(Counter(sorted(hand)).items(), key=functools.cmp_to_key(compare_hands_counter))
```

## C++

### Reading lines from a file

```cpp
#include <iostream>
#include <string>
#include <fstream>

std::ifstream input;
input.open("test.txt");
for (std::string line; getline(input, line); ) {   
    std::cout << line << "\n";
}   
```
### Cartesian prodcut / combinations_with_replacement

Programmer for day 7, removed after having recognized that it's not need to get to the result...

```cpp
/**
 * cartesian product of one list, without repetition.
 * cards must be sorted.
 *
 * implementation of
 * https://docs.python.org/3/library/itertools.html#itertools.combinations_with_replacement
 * as a special case of
 * https://docs.python.org/3/library/itertools.html#itertools.product
 */
std::vector<std::string> combinations_with_replacement(const std::string& cards, int repeat) {
    std::vector<std::string> result{""};
    auto pools = std::vector<std::string>(repeat, cards);
    for (auto pool: pools) {
        std::vector<std::string> tmp{};
        for (auto& r: result) {
            for (char p: pool) {
                if (r.size() > 0 && r.at(r.size() - 1) > p) {
                    continue;
                }
                tmp.push_back(r + p);
            }
        }
        result = tmp;
    }
    return result;
}
```
