# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring
from itertools import pairwise

def part_1(filename):
    result = 0
    with open(filename) as f:
        for line in [line.rstrip() for line in f]:
            values = [int(x) for x in line.split(' ')]
            result += values[-1]
            while sum(values) != 0:
                values = [b - a for (a, b) in pairwise(values)]
                result += values[-1]

    return result

def part_2(filename):
    result = 0
    with open(filename) as f:
        for line in [line.rstrip() for line in f]:
            first_differences = []
            values = [int(x) for x in line.split(' ')]
            first_differences.append(values[0])
            while sum(values) != 0:
                values = [b - a for (a, b) in pairwise(values)]
                first_differences.append(values[0])
            b = first_differences[-1]
            for a in reversed(first_differences[0:-1]):
                b = a - b
            result += b
                
    return result

def main():
    assert part_1('test.txt') == 114
    print(part_1('input.txt'))
    assert part_2('test.txt') == 2
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()
