# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring
from itertools import permutations

def part_1(filename):
    result = 1
    with open(filename) as f:
        times = f.readline()
        times = [int(s) for s in times[times.find(':') + 1:].rstrip().split()]
        distances = f.readline()
        distances = [int(s) for s in distances[distances.find(':') + 1:].rstrip().split()]
        # print(times, distances)

        for race_time, record in zip(times, distances):
            winning = 0
            for speed in range(1, race_time):
                distance = speed * (race_time - speed)
                # print('distance', distance)
                if distance > record:
                    winning += 1
            # print('winning', winning)
            result *= winning

    return result

def part_2(filename):
    result = 0
    with open(filename) as f:
        times = f.readline()
        race_time = int(''.join(times[times.find(':') + 1:].rstrip().split()))
        distances = f.readline()
        record = int(''.join(distances[distances.find(':') + 1:].rstrip().split()))
        # print(race_time, record)

        for speed in range(1, race_time):
            distance = speed * (race_time - speed)
            if distance > record:
                result += 1

    return result

def main():
    assert part_1('test.txt') == 288
    print(part_1('input.txt'))
    assert part_2('test.txt') == 71503
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()
