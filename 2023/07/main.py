# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring
from collections import Counter
import functools

# 1: Five: AAAAA
# 2: Four: AA8AA
# 3: FullHouse: 23332
# 4: Three: TTT98
# 5: TwoPair: 23432
# 6: OnePair: A23A4
# 7: One: 23456
def get_hand_type(hand):
    counter = sorted(Counter(sorted(hand)).items(), key=lambda x: x[1], reverse=True)
    if counter[0][1] == 5:
        return '1'
    elif counter[0][1] == 4:
        return '2'
    elif counter[0][1] == 3:
        if counter[1][1] == 2:
            return '3'
        else:
            return '4'
    elif counter[0][1] == 2:
        if counter[1][1] == 2:
            return '5'
        else:
            return '6'
    else:
        return '7'

# a  b  c  d  e  f  g  h  i  j  k  l  m
# A, K, Q, J, T, 9, 8, 7, 6, 5, 4, 3, 2
CARDS_VALUES = {
    'A': 'a', 'K': 'b', 'Q': 'c', 'J': 'd', 'T': 'e',
    '9': 'f', '8': 'g', '7': 'h', '6': 'i', '5': 'j',
    '4': 'k', '3': 'l', '2': 'm'
}

def get_hand_strength(hand):
    strength = get_hand_type(hand)
    strength += ''.join(CARDS_VALUES[h[0]] for h in hand)
    return strength

# a  b  c  d  e  f  g  h  i  j  k  l  m
# A, K, Q, T, 9, 8, 7, 6, 5, 4, 3, 2, J
CARDS_VALUES_WITH_JOKER = {
    'A': 'a', 'K': 'b', 'Q': 'c', 'T': 'd',
    '9': 'e', '8': 'f', '7': 'g', '6': 'h', '5': 'i',
    '4': 'j', '3': 'k', '2': 'l',
    'J': 'm'
}

def get_hand_strength_with_joker(hand):
    """For each J in the hand, replace it with one of the other cards in the hand"""
    if 'J' not in hand or hand == 'JJJJJ':
        strength = get_hand_type(hand)
    else:
        joker_count = hand.count('J')
        cards_no_joker = [c for c in hand if c != 'J']
        strengths = [get_hand_type(''.join(cards_no_joker + list(replaced))) \
            for replaced in [c * joker_count for c in cards_no_joker]]
        strengths.sort()
        strength = strengths[0]

    return  strength + ''.join(CARDS_VALUES_WITH_JOKER[h[0]] for h in hand)

def part_1(filename):
    result = 0
    with open(filename) as f:
        hands = []
        for hand, bid in [line.rstrip().split() for line in f]:
            hands.append((hand, int(bid), get_hand_strength(hand)))
        hands.sort(key=lambda h: h[2], reverse=True)
        for i, hand in enumerate(hands):
            result += (i + 1) * hand[1]

    return result

def part_2(filename):
    result = 0
    with open(filename) as f:
        hands = []
        for hand, bid in [line.rstrip().split() for line in f]:
            hands.append((hand, int(bid), get_hand_strength_with_joker(hand)))
        hands.sort(key=lambda h: h[2], reverse=True)
        for i, hand in enumerate(hands):
            result += (i + 1) * hand[1]

    return result

def main():
    assert part_1('test.txt') == 6440
    assert part_1('input.txt') == 251927063
    print(part_1('input.txt'))
    assert part_2('test.txt') == 5905
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()
