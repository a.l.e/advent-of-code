#include <fstream>
#include <cassert>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <unordered_map>
#include <algorithm>
#include <numeric>

struct Hand {
    std::string hand;
    int bid;
    int strength;
    std::string values;
};

/**
 * reverse sorting of the hands
 */
inline bool operator<(const Hand& lhs, const Hand& rhs) {
    if (lhs.strength > rhs.strength)
       return true;
   if (rhs.strength > lhs.strength)
       return false;
   if (lhs.values > rhs.values)
       return true;
   if (rhs.values > lhs.values)
       return false;
   return false;
}

std::ostream& operator<<(std::ostream& os, const Hand& h) {
    return os << h.hand << " / " << h.bid << " / " << h.strength << " / " << h.values;
}

/**
 * count occurrences of each char in the string
 */
auto get_counter(const std::string& hand) {
    std::unordered_map<char, int> counter;
    for (const char c: hand) {
        counter[c] += 1;
    }
    return counter;
}

/**
 * rank five, four, full, three, two pairs, pair, distinct cards...
 */
int get_hand_type(std::string hand) {
    std::sort(hand.begin(), hand.end());

    std::vector<int> counter{};
    for (const auto& [_, value] : get_counter(hand)) {
        counter.push_back(value);
    }
    std::sort(counter.begin(), counter.end(), std::greater<>());
    if (counter[0] == 5) {
        return 1;
    }
    if (counter[0] == 4) {
        return 2;
    }
    if (counter[0] == 3) {
        return counter[1] == 2 ?  3 : 4;
    }
    if (counter[0] == 2) {
        return counter[1] == 2 ?  5 : 6;
    }
    return 7;
}

/**
 * before calling get_hand_type(), replace the jokers with "interesting" cards ("all of the same")
 */
int get_hand_type_with_joker(std::string hand) {
    int joker_count = std::count_if(hand.begin(), hand.end(),
        [](const char c) { return c == 'J'; });
    if (joker_count == 0 || joker_count == 5) {
        return get_hand_type(hand);
    }
    std::string cards_no_joker = "";
    for (const char c: hand) {
        if (c == 'J') continue;
        cards_no_joker += c;
    }
    std::vector<int> strengths{};
    for (char c: cards_no_joker) {
        strengths.push_back(get_hand_type(cards_no_joker + std::string(joker_count, c)));
    }
    std::sort(strengths.begin(), strengths.end());
    return strengths.at(0);
}

/**
 * get the value attached to each card in the hand
 */
std::string get_hand_values(const std::string& hand) {
    static const std::unordered_map<char, char> cards_values = {
        {'A', 'a'}, {'K', 'b'}, {'Q', 'c'}, {'J', 'd'}, {'T', 'e'},
        {'9', 'f'}, {'8', 'g'}, {'7', 'h'}, {'6', 'i'}, {'5', 'j'},
        {'4', 'k'}, {'3', 'l'}, {'2', 'm'}
    };
    std::string values{};
    for (const char c: hand) {
        values += cards_values.at(c);
    }
    return values;
}

/**
 * get the value attached to each card in the hand; the joker bing moved to the end.
 */
std::string get_hand_values_with_joker(const std::string& hand) {
    static const std::unordered_map<char, char> cards_values = {
        {'A', 'a'}, {'K', 'b'}, {'Q', 'c'},             {'T', 'd'},
        {'9', 'e'}, {'8', 'f'}, {'7', 'g'}, {'6', 'h'}, {'5', 'i'},
        {'4', 'j'}, {'3', 'k'}, {'2', 'l'},
        {'J', 'm'}, 
    };
    std::string values{};
    for (const char c: hand) {
        values += cards_values.at(c);
    }
    return values;
}

auto get_tokens_from_line(const auto& line) {
    std::istringstream iss {line};
    std::string left, right;
    std::getline(iss, left, ' ');
    std::getline(iss, right, ' ');
    return std::make_pair(left, std::stoi(right));
}

int part_1(std::string filename) {
    int result = 0;
    std::vector<Hand> hands{};
    std::ifstream input;
    input.open(filename);
    for (std::string line; getline(input, line); ) {
        const auto& [hand, bid] = get_tokens_from_line(line);
        hands.push_back(Hand{hand, bid,
            get_hand_type(hand), get_hand_values(hand)});
    }
    std::sort(hands.begin(), hands.end());
    int i{1};
    for (const auto& hand: hands) {
        // std::cout << hand << "\n";
        result += i * hand.bid;
        i++;
    }
    return result;
}

int part_2(std::string filename) {
    int result = 0;
    std::vector<Hand> hands{};
    std::ifstream input;
    input.open(filename);
    for (std::string line; getline(input, line); ) {
        const auto& [hand, bid] = get_tokens_from_line(line);
        hands.push_back({hand, bid,
            get_hand_type_with_joker(hand), get_hand_values_with_joker(hand)});
    }
    std::sort(hands.begin(), hands.end());
    int i{1};
    for (const auto& hand: hands) {
        result += i * hand.bid;
        i++;
    }
    return result;
}

int main() {
    assert(part_1("test.txt") == 6440);
    assert(part_1("input.txt") == 251927063);
    std::cout << part_1("input.txt") << "\n";
    assert(part_2("test.txt") == 5905);
    std::cout << part_2("input.txt") << "\n";
    assert(part_2("input.txt") == 255632664);
}
