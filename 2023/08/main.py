# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring
import math

def part_1(filename):
    result = 0
    world = {}
    with open(filename) as f:
        directions = f.readline().strip()
        next(f)
        for line in [line.rstrip() for line in f]:
            node = line[0:3]
            left = line[7:10]
            right = line[12:15]
            world[node] = [left, right]
            # print(node, left, right)

        # print(world)
        position = 'AAA'
        while position != 'ZZZ':
            for move in directions:
                position = world[position][0 if move == 'L' else 1]
                # print(position)
                result += 1
                if position == 'ZZZ':
                    break

    # print(result)
    return result

def part_2_slow(filename):
    result = 0
    world = {}
    positions = []
    with open(filename) as f:
        directions = f.readline().strip()
        next(f)
        for line in [line.rstrip() for line in f]:
            node = line[0:3]
            left = line[7:10]
            right = line[12:15]
            if node.endswith('A'):
                positions.append(node)
            world[node] = [left, right]

        all_xxz = False
        while not all_xxz:
            for move in directions:
                all_xxz = True
                for i, position in enumerate(positions):
                    position = world[position][0 if move == 'L' else 1]
                    if not position.endswith('Z'):
                        all_xxz = False
                    positions[i] = position
                result += 1
                if all_xxz:
                    break
    return result

def part_2(filename):
    world = {}
    positions = []
    with open(filename) as f:
        directions = f.readline().strip()
        next(f)
        for line in [line.rstrip() for line in f]:
            node = line[0:3]
            left = line[7:10]
            right = line[12:15]
            if node.endswith('A'):
                positions.append([node, 0])
            world[node] = [left, right]
        # print(positions)

        all_xxz = False
        # find the path length for each starting position individually...
        while not all_xxz:
            for move in directions:
                all_xxz = True
                for i, position in enumerate(positions):
                    if position[0].endswith('Z'):
                        continue
                    next_position = world[position[0]][0 if move == 'L' else 1]
                    positions[i] = [next_position, position[1] + 1]
                    if not next_position.endswith('Z'):
                        all_xxz = False
                if all_xxz:
                    break
    # print(positions)
    # ... then, the solution is the least common multiple!
    return math.lcm(*[p[1] for p in positions])

def main():
    assert part_1('test.txt') == 2
    assert part_1('test-02.txt') == 6
    print(part_1('input.txt'))
    assert part_2('test-03.txt') == 6
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()
