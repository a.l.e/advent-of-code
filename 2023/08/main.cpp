#include <fstream>
#include <cassert>
#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <numeric>

auto part_1(std::string filename) {
    // only solved in python...
    int result = 0;
    std::ifstream input;
    input.open(filename);
    for (std::string line; getline(input, line); ) {
    }
    return result;
}

struct Position {
    std::string node;
    int path_length;
};

struct Node {
    std::string left;
    std::string right;
};

auto part_2(std::string filename) {
    std::string directions;
    std::vector<Position> positions;
    std::unordered_map<std::string, Node> world;
    std::ifstream input;
    input.open(filename);
    getline(input, directions);
    {
        std::string _;
        getline(input, _); // skip the empty line
    }
    for (std::string line; getline(input, line); ) {
        auto node = line.substr(0, 3);
        if (node.ends_with('A')) {
            positions.push_back({node, 0});
        }
        world[node] = {line.substr(7, 3), line.substr(12, 3)};
    }

    bool all_xxz = false;
    while(!all_xxz) {
        for (const char move: directions) {
            all_xxz = true;
            for (auto& position: positions) {
                if (position.node.ends_with('Z')) {
                    continue;
                }
                position = {
                    move == 'L' ? world[position.node].left : world[position.node].right,
                    position.path_length += 1
                };
                if (!position.node.ends_with('Z')) {
                    all_xxz = false;
                }
            }
            if (all_xxz) {
                break;
            }
        }
    }

    unsigned long long result = positions.back().path_length;
    positions.pop_back();
    for (const auto& position: positions) {
        result = std::lcm(result, position.path_length);
    }

    return result;
}

int main() {
    // assert(part_1("test.txt") == 0);
    // std::cout << part_1("input.txt") << "\n";
    assert(part_2("test-03.txt") == 6);
    std::cout << part_2("input.txt") << "\n";
    assert(part_2("input.txt") == 14616363770447);
}
