#include <fstream>
#include <cassert>
#include <iostream>
#include <string>

#include <regex>
#include <iterator>
#include <unordered_map>
#include <algorithm>

int part_1(std::string filename) {
    int result = 0;

    std::unordered_map<std::string, int> max_cubes = {{"red", 12}, {"green", 13}, {"blue", 14}};
    std::regex pattern("(\\d+) (red|green|blue)");

    std::ifstream input;
    input.open(filename);
    for (std::string line; getline(input, line); ) {
        bool possible = true;

        // sregex_iterator: find all matches of the pattern in the string
        auto cubes_begin = std::sregex_iterator(line.begin(), line.end(), pattern);
        auto cubes_end = std::sregex_iterator();

        for (std::sregex_iterator i = cubes_begin; i != cubes_end; ++i) {
            std::smatch match = *i;
            if (std::stoi(match[1]) > max_cubes[match[2]]) {
                possible = false;
                break;
            }
        }
        if (possible) {
            result += std::stoi(line.substr(5, line.find(':')));
        }
    }
    return result;
}

int part_2(std::string filename) {
    int result = 0;

    std::regex pattern("(\\d+) (red|green|blue)");

    std::ifstream input;
    input.open(filename);
    for (std::string line; getline(input, line); ) {
        std::unordered_map<std::string, int> min_cubes = {{"red", 0}, {"green", 0}, {"blue", 0}};

        auto cubes_begin = std::sregex_iterator(line.begin(), line.end(), pattern);
        auto cubes_end = std::sregex_iterator();

        for (std::sregex_iterator i = cubes_begin; i != cubes_end; ++i) {
            std::smatch match = *i;
            min_cubes[match[2]] = std::max(min_cubes[match[2]], std::stoi(match[1]));
        }
        result += min_cubes["red"] * min_cubes["green"] * min_cubes["blue"];
    }
    return result;
}

int main() {
    assert(part_1("test.txt") == 8);
    std::cout << part_1("input.txt") << "\n";
    assert(part_2("test.txt") == 2286);
    std::cout << part_2("input.txt") << "\n";
}
