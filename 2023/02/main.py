# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring

import re

def part_1(filename):
    result = 0
    max_cubes = {'red': 12, 'green': 13, 'blue': 14}
    pattern = re.compile(r'(\d+) (red|green|blue)')
    with open(filename) as f:
        for line in [line.rstrip() for line in f]:
            for [n_cubes, color]  in re.findall(pattern, line):
                if max_cubes[color] < int(n_cubes):
                    break
            else:
                result += int(line[5:line.find(':')])
    return result

def part_2(filename):
    result = 0
    pattern = re.compile(r'(\d+) (red|green|blue)')
    with open(filename) as f:
        for line in [line.rstrip() for line in f]:
            min_cubes = {'red': 0, 'green': 0, 'blue': 0}
            for [n_cubes, color]  in re.findall(pattern, line):
                min_cubes[color] = max(min_cubes[color], int(n_cubes))
            result += min_cubes['red'] * min_cubes['green'] * min_cubes['blue']

    return result

def main():
    assert part_1('test.txt') == 8
    print(part_1('input.txt'))
    assert part_2('test.txt') == 2286
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()
