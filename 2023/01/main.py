# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring

def part_1(filename):
    result = 0
    with open(filename) as f:
        for line in [line.rstrip() for line in f]:
            result += next(int(x) for x in line if x.isdigit()) * 10 + \
                next(int(x) for x in reversed(line) if x.isdigit())
    return result

DIGITS = {'one': 1, 'two': 2, 'three': 3, 'four': 4, 'five': 5, \
    'six': 6, 'seven': 7, 'eight': 8, 'nine': 9 }

def part_2(filename):
    result = 0
    with open(filename) as f:
        for line in [line.rstrip() for line in f]:
            result += first_match(line) * 10 + first_match(line, reversed=True)

    return result

def first_match(line, reversed=False):
    range_iterator = range(len(line) - 1, -1, -1) if reversed else range(len(line))
    for i in range_iterator:
        if line[i].isdigit():
            return int(line[i])
        for digit, value in DIGITS.items():
            if line[i:].startswith(digit):
                return value
    return 0

# # i tried to do it with regexes: you cannot (really) find overlapping matches
# # with regexes (and the "real" data contains overlapping matches!)
# import re
# def part_2(filename):
#     result = 0
#     digits_pattern = re.compile(f'({"|".join([*DIGITS.keys()] + [str(d) for d in DIGITS.values()])})')
#     with open(filename) as f:
#         for line in [line.rstrip() for line in f]:
#             print(line)
#             matches = digits_pattern.finditer(line, overlapped=True)
#             first_match = next(matches)
#             try:
#                 *_, last_match = matches # https://stackoverflow.com/a/48232574/5239250
#             except ValueError:
#                 last_match = first_match
#             print('first', first_match.group(0))
#             print('last', last_match.group(0))
#             value = match_to_value(first_match.group(0)) * 10 + match_to_value(last_match.group(0))
#             # print(value)
#             result += value
# 
# def match_to_value(match):
#     if match in DIGITS:
#         return DIGITS[match]
#     return int(match)


def main():
    assert part_1('test.txt') == 142
    print(part_1('input.txt'))
    assert part_2('test-part-2.txt') == 281
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()
