/**
 * compile and run with
 * g++ -std=c++20 -o 01 main.cpp  && ./01
 */
#include <fstream>
#include <cassert>
#include <cctype>
#include <iostream>
#include <string>
#include <string_view>
#include <algorithm>
#include <unordered_map>

int part_1(std::string filename) {
    int result = 0;
    std::ifstream input;
    input.open(filename);
    for (std::string line; getline(input, line); ) {
        auto first = *std::find_if(line.begin(), line.end(),
                [](char c) {return std::isdigit(c);});
        auto last = *std::find_if(std::reverse_iterator(line.end()), std::reverse_iterator(line.begin()),
                [](char c) {return std::isdigit(c);});
        result +=  (first - '0') * 10 + (last - '0');
    }
    return result;
}

std::unordered_map<std::string, int> digits {
    {"one", 1}, {"two", 2}, {"three", 3},
    {"four", 4}, {"five", 5}, {"six", 6},
    {"seven", 7}, {"eight", 8}, {"nine", 9 }};

int first_match(std::string line, bool reversed = false) {
    const int start = reversed ? line.length() - 1 : 0;
    const int end = reversed ? 0 : line.length();
    const int step = reversed ? -1 : 1;
    // auto compare = [reversed](int i, int n) {return reversed ? i >= n : i < n;};
    std::string_view line_view = line;
    for (int i = start; reversed ? i >= end : i < end; i += step) {
        if (std::isdigit(line.at(i))) {
            return line.at(i) - '0';
        }
        auto from_i = line_view.substr(i);
        for (const auto& [key, value]: digits) {
            if (from_i.starts_with(key)) {
                return value;
            }
        }
    }
    return 0;
}

int part_2(std::string filename) {
    int result = 0;
    std::ifstream input;
    input.open(filename);
    for (std::string line; getline(input, line); ) {
        result += first_match(line) * 10 + first_match(line, true);
    }
    return result;
}

int main() {
    assert(part_1("test.txt") == 142);
    std::cout << part_1("input.txt") << "\n";
    assert(part_2("test-part-2.txt") == 281);
    std::cout << part_2("input.txt") << "\n";
}
