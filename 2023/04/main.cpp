#include <fstream>
#include <cassert>
#include <iostream>
#include <string>
#include <vector>

#include <sstream>
#include <iterator>
#include <utility>
#include <algorithm>
#include <cmath>

/**
 * first split  by | the part of line after the ":"
 * then split both left and right by spaces and get a list of int
 */
auto get_card_numbers(const std::string& line) {
    std::istringstream iss {line.substr(line.find(':') + 1)};
    std::string left, right;
    std::getline(iss, left, '|');
    std::getline(iss, right, '|');
    iss = std::istringstream(left);
    std::vector<int> winning{std::istream_iterator<int>(iss), {}};
    iss = std::istringstream(right);
    std::vector<int> numbers{std::istream_iterator<int>(iss), {}};
    return std::make_pair(winning, numbers);
}

int get_intersection_count(std::vector<int>& winning, std::vector<int>& numbers) {
    std::sort(winning.begin(), winning.end());
    std::sort(numbers.begin(), numbers.end());
    std::vector<int> winners;
    std::set_intersection(winning.begin(), winning.end(), numbers.begin(), numbers.end(),
        std::back_inserter(winners));
    return winners.size();
}

int part_1(std::string filename) {
    int result = 0;
    std::ifstream input;
    input.open(filename);
    for (std::string line; getline(input, line); ) {
        auto [winning, numbers] = get_card_numbers(line);
        int winners_count = get_intersection_count(winning, numbers);
        if (winners_count > 0) {
            result += std::pow(2, winners_count - 1);
        }
    }
    return result;
}

int part_2(std::string filename) {
    int result = 0;
    std::ifstream input;
    input.open(filename);
    std::vector<int> cards_amount{};
    int i = 0;
    for (std::string line; getline(input, line); i++) {
        if (i == cards_amount.size()) {
            cards_amount.push_back(1);
        }
        result += cards_amount.at(i);

        auto [winning, numbers] = get_card_numbers(line);
        int winners_count = get_intersection_count(winning, numbers);

        for (int j = i + 1; j < i + winners_count + 1; j++) {
            if (j == cards_amount.size()) {
                cards_amount.push_back(1);
            }
            cards_amount.at(j) += cards_amount.at(i);
        }

    }
    return result;
}

int main() {
    assert(part_1("test.txt") == 13);
    std::cout << part_1("input.txt") << "\n";
    assert(part_2("test.txt") == 30);
    std::cout << part_2("input.txt") << "\n";
}
