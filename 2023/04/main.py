# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring

import re

def part_1(filename):
    result = 0
    with open(filename) as f:
        for line in [line.rstrip() for line in f]:
            winning, numbers = list(line[line.find(': ') + 2:].split(' | '))
            winning = [int(a) for a in winning.split()]
            numbers = [int(a) for a in numbers.split()]
            count_winnnig = len(set(winning).intersection(set(numbers)))
            if count_winnnig > 0:
                result += 2 ** (count_winnnig - 1)

    return result

def part_2(filename):
    result = 0
    cards = {}
    with open(filename) as f:
        for i, line in enumerate(line.rstrip() for line in f):
            if i not in cards:
                cards[i] = 1

            result += cards[i]

            winning, numbers = list(line[line.find(': ') + 2:].split(' | '))
            winning = [int(a) for a in winning.split()]
            numbers = [int(a) for a in numbers.split()]

            count_winnnig = len(set(winning).intersection(set(numbers)))
            for j in range(i + 1, i + count_winnnig + 1):
                cards[j] = cards.get(j, 1) + cards[i]

    return result

def main():
    assert part_1('test.txt') == 13
    print(part_1('input.txt'))
    assert part_2('test.txt') == 30
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()
