# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring

def sum_ints(a, b):
    if b < a:
        return 0
    return (b - a + 1) * (a + b) // 2

def part_1(filename):
    result = 0
    world = []
    with open(filename) as f:
        for line in [line.rstrip() for line in f]:
            world.append(list(line))

    n = len(world)
    m = len(world[1])
    for j in range(m):
        bottom = m
        rocks_to_the_bottom = 0
        for i in range(n):
            c = world[i][j]
            if c == 'O':
                rocks_to_the_bottom += 1
            elif c == '#':
                result += sum_ints(bottom - rocks_to_the_bottom + 1, bottom)
                bottom = m - i - 1
                rocks_to_the_bottom = 0
        result += sum_ints(bottom - rocks_to_the_bottom + 1, bottom)

    return result

def tilt_north(world, rounded_rocks, cube_rocks_north):
    rounded_rocks.sort()
    for rock in rounded_rocks:
        for cube_i in cube_rocks_north[rock[1]]:
            # moving just after a cube that is smaller then us
            if rock[0] > cube_i:
                target_i = cube_i + 1
                break
        else:
            # moving to the top
            target_i = 0
        for i in range(target_i, rock[0]):
            if world[i][rock[1]] == '.':
                world[i][rock[1]] = 'O'
                world[rock[0]][rock[1]] = '.'
                rock[0] = i
                break

def tilt_west(world, rounded_rocks, cube_rocks_west):
    rounded_rocks.sort(key=lambda x: [x[1], x[0]])
    for rock in rounded_rocks:
        for cube_j in cube_rocks_west[rock[0]]:
            # moving just after a cube that is smaller then us
            if rock[1] > cube_j:
                target_j = cube_j + 1
                break
        else:
            # moving to the top
            target_j = 0
        for j in range(target_j, rock[1]):
            if world[rock[0]][j] == '.':
                world[rock[0]][j] = 'O'
                world[rock[0]][rock[1]] = '.'
                rock[1] = j
                break

def tilt_south(world, rounded_rocks, cube_rocks_south, m):
    rounded_rocks.sort(reverse=True)
    for rock in rounded_rocks:
        for cube_i in cube_rocks_south[rock[1]]:
            # moving just after a cube that is smaller then us
            if rock[0] < cube_i:
                target_i = cube_i - 1
                break
        else:
            # moving to the bottom
            target_i = m - 1
        for i in range(target_i, rock[0], -1):
            if world[i][rock[1]] == '.':
                world[i][rock[1]] = 'O'
                world[rock[0]][rock[1]] = '.'
                rock[0] = i
                break

def tilt_east(world, rounded_rocks, cube_rocks_east, n):
    rounded_rocks.sort(key=lambda x: [x[1], x[0]], reverse=True)
    for rock in rounded_rocks:
        for cube_j in cube_rocks_east[rock[0]]:
            # moving just after a cube that is smaller then us
            if rock[1] < cube_j:
                target_j = cube_j - 1
                break
        else:
            # moving to the bottom
            target_j = n - 1
        for j in range(target_j, rock[1], -1):
            if world[rock[0]][j] == '.':
                world[rock[0]][j] = 'O'
                world[rock[0]][rock[1]] = '.'
                rock[1] = j
                break

def total_load_from_string(world_string, n, m):
    load = 0
    world = [world_string[j - m: j] for j in range (m, len(world_string) + m, m)]
    # print('\n'.join(world))
    for i, line in enumerate(world):
        load += (n - i) * line.count('O')
    return load

def part_2(filename):
    result = 0
    world = []
    cube_rocks = []
    rounded_rocks = []
    with open(filename) as f:
        for i, line in enumerate(f):
            world.append(list(line.rstrip()))
            for j, c in enumerate(line.rstrip()):
                if c == '#':
                    cube_rocks.append((i, j))
                elif c== 'O':
                    rounded_rocks.append([i, j])
    n = i + 1 # not nice, but right now i'm lazy for something better
    m = j + 1

    cube_rocks_east = [[] for i in range(m)]
    cube_rocks_south = [[] for i in range(n)]
    for rock in cube_rocks:
        cube_rocks_east[rock[0]].append(rock[1])
        cube_rocks_south[rock[1]].append(rock[0])
    cube_rocks_north = [sorted(cubes, reverse=True) for cubes in cube_rocks_south]
    cube_rocks_west = [sorted(cubes, reverse=True) for cubes in cube_rocks_east]
        
    # print('\n'.join([''.join(line) for line in world]))
    # print('c_e', cube_rocks_east)
    # print('c_w', cube_rocks_west)
    # print('c_n', cube_rocks_north)
    # print('c_s', cube_rocks_south)
    # print('r', rounded_rocks)

    memory = []
    loop_start = None
    cycles_n = 1000000000
    for i in range(cycles_n):
        tilt_north(world, rounded_rocks, cube_rocks_north)
        tilt_west(world, rounded_rocks, cube_rocks_west)
        tilt_south(world, rounded_rocks, cube_rocks_south, m)
        tilt_east(world, rounded_rocks, cube_rocks_east, n)

        # keep the current world in memory as a string and look for a loop
        value = ''.join([''.join(line) for line in world])

        if value in memory:
            loop_start = memory.index(value)
            break
        memory.append(value)

    loop_length = len(memory) - loop_start
    loop_missing_cycles = (cycles_n - loop_start) % loop_length
    final_world = memory[loop_start + loop_missing_cycles - 1]

    return total_load_from_string(final_world, n, m)

def main():
    assert part_1('test.txt') == 136
    print(part_1('input.txt'))
    assert part_2('test.txt') == 64
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()
