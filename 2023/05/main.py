# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring
from dataclasses import dataclass

def part_1_slow(filename):
    """calculating all possible matches for the ids gives infinite processing time
       with the real input"""
    result = None
    maps = {}
    with open(filename) as f:
        seeds = f.readline()
        seeds = [int(s) for s in seeds[seeds.find(':') + 1:].rstrip().split()]
        # print('seeds', seeds)
        next(f)
        for line in f:
            key = line.split()[0]
            # print('k', key)
            maps[key] = {}
            while (category_range := [int(s) for s in f.readline().split()]) != []:
                destination = category_range[0]
                source = category_range[1]
                length = category_range[2]
                for i in range(length):
                    maps[key][source + i] = destination + i

    for seed in seeds:
        soil = maps['seed-to-soil'].get(seed, seed)
        fertilizer = maps['soil-to-fertilizer'].get(soil, soil)
        water = maps['fertilizer-to-water'].get(fertilizer, fertilizer)
        light = maps['water-to-light'].get(water, water)
        temperature = maps['light-to-temperature'].get(light, light)
        humidity = maps['temperature-to-humidity'].get(temperature, temperature)
        location = maps['humidity-to-location'].get(humidity, humidity)
        # print(seed, '->', location)
        result = location if result is None else min(result, location)

    return result

def part_1(filename):
    with open(filename) as f:
        seeds = f.readline()
        seeds = [int(s) for s in seeds[seeds.find(':') + 1:].rstrip().split()]
        target_ids = seeds.copy()
        # print('seeds', seeds)
        next(f)
        for _ in f:
            # print(line.rstrip())
            matched = []
            while (category_range := [int(s) for s in f.readline().split()]) != []:
                source_ids = target_ids.copy()
                destination = category_range[0]
                source = category_range[1]
                length = category_range[2]
                for i, id in enumerate(source_ids):
                    if i in matched:
                        continue
                    if source <= id < source + length:
                        target_ids[i] = id + destination - source
                        matched.append(i)
                # print(target_ids)

                # print(source, destination, length)
    return min(target_ids)

def part_2_slow(filename):
    """creating all possible ids gives infinite processing time
       with the real input."""
    with open(filename) as f:
        seed_ranges = f.readline()
        seed_ranges = [int(s) for s in seed_ranges[seed_ranges.find(':') + 1:].rstrip().split()]
        seeds = []
        for start in (it := iter(seed_ranges)):
            length = next(it)
            for seed in range(start, start + length):
                seeds.append(seed)
        target_ids = seeds.copy()
        next(f)
        for _ in f:
            matched = []
            while (category_range := [int(s) for s in f.readline().split()]) != []:
                source_ids = target_ids.copy()
                destination = category_range[0]
                source = category_range[1]
                length = category_range[2]
                for i, id in enumerate(source_ids):
                    if i in matched:
                        continue
                    if source <= id < source + length:
                        target_ids[i] = id + destination - source
                        matched.append(i)
    return min(target_ids)

@dataclass
class SeedRange:
    start: int
    length: int
    def end(self):
        return self.start + self.length - 1
    def __lt__(self, other):
        return self.start < other.start

# the new range fully enclsing the old one
# source :     |-------|
# seed   :       |---|
# dest   : |---| ^   ^
# next   :
def process_full_overlapping(seed, source, destination, destination_seeds, next_seeds):
    if source.start <= seed.start and source.end() >= seed.end():
        destination_seeds.append(\
            SeedRange(destination.start + seed.start - source.start, seed.length)
        )
        return True
    return False

# source :     |-------|
# seed   :         |-------|
# dest   : |---|   ^   ^
# next   :             |---|
def process_left_overlapping(seed, source, destination, destination_seeds, next_seeds):
    if source.start < seed.start and source.end() >= seed.start and \
      source.end() < seed.end():
        destination_seeds.append(\
            SeedRange(destination.start + seed.start - source.start, seed.length - (seed.end() - source.end()))
        )
        next_seeds.append(\
            SeedRange(source.end() + 1, seed.length - (source.end() - seed.start + 1))
        )
        assert destination_seeds[-1].length > 0
        assert next_seeds[-1].length > 0
        assert destination_seeds[-1].length + next_seeds[-1].length == seed.length
        return True
    return False

# source :     |-------|
# seed   :   |-------|
# dest   :     ^     ^   |-----|
# next   :   |-|
def process_right_overlapping(seed, source, destination, destination_seeds, next_seeds):
    if source.start > seed.start and source.start <= seed.end() and \
      source.end() >= seed.end():
        destination_seeds.append(\
            SeedRange(destination.start, seed.length - (source.start - seed.start))
        )
        next_seeds.append(\
            SeedRange(seed.start, seed.length - (seed.end() - source.start + 1))
        )
        assert destination_seeds[-1].length > 0
        assert next_seeds[-1].length > 0
        assert destination_seeds[-1].length + next_seeds[-1].length == seed.length
        return True
    return False

# source :     |-------|
# seed   :   |-------------|
# dest   :     ^       ^     |-------|
# next   :   |-|       |---|
def process_full_comprised(seed, source, destination, destination_seeds, next_seeds):
    if source.start >= seed.start and source.end() <= seed.end():
        destination_seeds.append(\
            SeedRange(destination.start, source.length)
        )
        next_seeds.append(\
            SeedRange(seed.start, source.start - seed.start)
        )
        next_seeds.append(\
            SeedRange(source.end() + 1, seed.end() - source.end())
        )
        assert destination_seeds[-1].length + next_seeds[-1].length + next_seeds[-2].length == seed.length
        return True
    return False

def part_2(filename):
    """The ranges from the input file go into seed_ranges.
    Then for each category, move the matching part of the ranges to destination_seeds
    and use next_seeds to collect the ranges that should be matched with the next
    transformation range.
    Before the next category starts, the destination_seeds are merged into the seed_ranges.
    Since the list of ranges is sorted after each step, the lowest location
    is the start of the first seed_ranges.
    Asserts make sure that the lenghts of the ranges are always sane and test.py
    makes some basic tests on the start and end values of the ranges being processed.
    """
    seed_ranges = []
    with open(filename) as f:
        seeds_line = f.readline()
        seeds_line = [int(s) for s in seeds_line[seeds_line.find(':') + 1:].rstrip().split()]
        for start in (it := iter(seeds_line)):
            length = next(it)
            seed_ranges.append(SeedRange(start, length))
        seeds_count  = sum(s.length for s in seed_ranges)
        next(f) # ignore the empty line
        for _ in f: # ignore the text description of the task
            destination_seeds = [] # the seeds with the destination ids
            # read until we get an empty line
            while (category_range := [int(s) for s in f.readline().split()]) != []:
                destination = SeedRange(category_range[0], category_range[2])
                source = SeedRange(category_range[1], category_range[2])
                next_seeds = [] # the seeds for the next matching
                for seed in seed_ranges:
                    # apply the first matching transformation rule
                    process_full_overlapping(seed, source, destination, destination_seeds, next_seeds) or \
                    process_left_overlapping(seed, source, destination, destination_seeds, next_seeds) or \
                    process_right_overlapping(seed, source, destination, destination_seeds, next_seeds) or \
                    process_full_comprised(seed, source, destination, destination_seeds, next_seeds) or \
                    next_seeds.append(SeedRange(seed.start, seed.length))

                seed_ranges = next_seeds
                seed_ranges.sort()
            seed_ranges += destination_seeds
            seed_ranges.sort()
            assert sum(s.length for s in seed_ranges) == seeds_count
    return seed_ranges[0].start

def main():
    assert part_1_slow('test.txt') == 35
    assert part_1('test.txt') == 35
    print(part_1('input.txt'))
    assert part_2_slow('test.txt') == 46
    assert part_2('test.txt') == 46
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()
