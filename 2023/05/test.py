import unittest
import main

class TestStringMethods(unittest.TestCase):

    # TODO: also test for the cases where the boundaries meet...

    def test_full_overlapping(self):
        seed = main.SeedRange(5, 2)
        source = main.SeedRange(3, 10)
        destination = main.SeedRange(5, 10)
        next_seeds = []
        destination_seeds = []
        processed = main.process_full_overlapping(seed, source, destination, destination_seeds, next_seeds)
        # print(next_seeds, destination_seeds)
        self.assertTrue(processed)
        self.assertEqual(len(next_seeds), 0)
        self.assertEqual(len(destination_seeds), 1)
        self.assertEqual(destination_seeds[0].start, 7)
        self.assertEqual(destination_seeds[0].length, 2)

        processed = main.process_left_overlapping(seed, source, destination, destination_seeds, next_seeds)
        self.assertFalse(processed)
        processed = main.process_right_overlapping(seed, source, destination, destination_seeds, next_seeds)
        self.assertFalse(processed)
        processed = main.process_full_comprised(seed, source, destination, destination_seeds, next_seeds)
        self.assertFalse(processed)

    def test_perfect_full_overlapping(self):
        seed = main.SeedRange(5, 2)
        source = main.SeedRange(5, 2)
        destination = main.SeedRange(10, 2)
        next_seeds = []
        destination_seeds = []
        processed = main.process_full_overlapping(seed, source, destination, destination_seeds, next_seeds)
        # print(next_seeds, destination_seeds)
        self.assertTrue(processed)
        self.assertEqual(len(next_seeds), 0)
        self.assertEqual(len(destination_seeds), 1)
        self.assertEqual(destination_seeds[0].start, 10)
        self.assertEqual(destination_seeds[0].length, 2)
        # other could match, but the code does not get there.

    def test_left_overlapping(self):
        seed = main.SeedRange(5, 2)
        source = main.SeedRange(3, 3)
        destination = main.SeedRange(0, 3)
        next_seeds = []
        destination_seeds = []
        main.process_left_overlapping(seed, source, destination, destination_seeds, next_seeds)
        # print(next_seeds, destination_seeds)
        self.assertEqual(len(next_seeds), 1)
        self.assertEqual(len(destination_seeds), 1)
        self.assertEqual(destination_seeds[0].start, 2)
        self.assertEqual(destination_seeds[0].length, 1)
        self.assertEqual(next_seeds[0].start, 6)
        self.assertEqual(next_seeds[0].length, 1)

        processed = main.process_full_overlapping(seed, source, destination, destination_seeds, next_seeds)
        self.assertFalse(processed)
        processed = main.process_right_overlapping(seed, source, destination, destination_seeds, next_seeds)
        self.assertFalse(processed)
        processed = main.process_full_comprised(seed, source, destination, destination_seeds, next_seeds)
        self.assertFalse(processed)

    def test_right_overlapping(self):
        seed = main.SeedRange(5, 2)
        source = main.SeedRange(6, 10)
        destination = main.SeedRange(2, 10)
        next_seeds = []
        destination_seeds = []
        main.process_right_overlapping(seed, source, destination, destination_seeds, next_seeds)
        # print(next_seeds, destination_seeds)
        self.assertEqual(len(next_seeds), 1)
        self.assertEqual(len(destination_seeds), 1)
        self.assertEqual(destination_seeds[0].start, 2)
        self.assertEqual(destination_seeds[0].length, 1)
        self.assertEqual(next_seeds[0].start, 5)
        self.assertEqual(next_seeds[0].length, 1)

        processed = main.process_full_overlapping(seed, source, destination, destination_seeds, next_seeds)
        self.assertFalse(processed)
        processed = main.process_left_overlapping(seed, source, destination, destination_seeds, next_seeds)
        self.assertFalse(processed)
        processed = main.process_full_comprised(seed, source, destination, destination_seeds, next_seeds)
        self.assertFalse(processed)

    def test_full_comprised(self):
        seed = main.SeedRange(3, 10)
        source = main.SeedRange(5, 2)
        destination = main.SeedRange(2, 2)
        next_seeds = []
        destination_seeds = []
        main.process_full_comprised(seed, source, destination, destination_seeds, next_seeds)
        self.assertEqual(len(next_seeds), 2)
        self.assertEqual(len(destination_seeds), 1)
        self.assertEqual(destination_seeds[0].start, 2)
        self.assertEqual(destination_seeds[0].length, 2)
        self.assertEqual(next_seeds[0].start, 3)
        self.assertEqual(next_seeds[0].length, 2)
        self.assertEqual(next_seeds[1].start, 7)
        self.assertEqual(next_seeds[1].length, 6)

        processed = main.process_full_overlapping(seed, source, destination, destination_seeds, next_seeds)
        self.assertFalse(processed)
        processed = main.process_left_overlapping(seed, source, destination, destination_seeds, next_seeds)
        self.assertFalse(processed)
        processed = main.process_right_overlapping(seed, source, destination, destination_seeds, next_seeds)
        self.assertFalse(processed)

if __name__ == '__main__':
    unittest.main()
