# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring
import re

def get_hash(step):
    value = 0
    for c in step:
        value += ord(c)
        value *=17
        value %= 256
    return value

def part_1(filename):
    result = 0
    with open(filename) as f:
        for step in next(f).rstrip().split(','):
            result += get_hash(step)
    return result

def part_2(filename):
    result = 0
    boxes = [{} for i in range(256)]
    with open(filename) as f:
        pattern = re.compile(r'([a-z]+)([-=])(\d*)')
        for step in next(f).rstrip().split(','):
            m = pattern.match(step)
            label, operation, focal_length = m.groups()
            focal_length = 0 if focal_length == '' else int(focal_length)
            i = get_hash(label)
            # print(step, label, i, operation, focal_length)
            if operation == '-':
                try:
                    del boxes[i][label]
                except KeyError:
                    pass
            elif operation == '=':
                boxes[i][label] = focal_length

        for i, box in enumerate(boxes):
            for j, slot in enumerate(box.values()):
                result += (i + 1) * (j + 1) * slot
                # print(i, j, result)

    return result

def main():
    assert part_1('test.txt') == 52
    assert part_1('test-02.txt') == 1320
    print(part_1('input.txt'))
    assert part_2('test-02.txt') == 145
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()
