function mod(a, b) {
  return ((a % b) + b) % b;
}

function a(input) {
  let position = {x: 0, y: 0};
  const directions = ['N', 'E', 'S', 'W']
  let direction = 0; // N

  const moves = input.split(', ');
  for (const move of moves) {
    if (move[0] == 'R') {
      direction = direction + 1;
    } else if (move[0] == 'L') {
      direction = direction - 1;
    }
    direction = mod(direction, 4); // % does not work for negative numbers

    const steps = Number(move.slice(1));

    if (directions[direction] === 'N') {
      position.y += steps;
    } else if (directions[direction] === 'E') {
      position.x += steps;
    } else if (directions[direction] === 'S') {
      position.y -= steps;
    } else if (directions[direction] === 'W') {
      position.x -= steps;
    }
  }
  // console.log(position)
  return Math.abs(position.x) + Math.abs(position.y);
}

function to_string(position) {
  return position.x + ',' + position.y;
}

function b(input) {
  let positions = new Set();
  let position = {x: 0, y: 0};
  const directions = [[1, 0], [0, 1], [-1, 0], [0, -1]]; // moving North means 1 up 0 right
  let direction = 0; // N

  // if objects are in the set, has() checks if it's the same object, not if it has
  // the same values: so we first convert the position to a string
  positions.add(to_string(position));

  const moves = input.split(', ');
  for (const move of moves) {
    if (move[0] == 'R') {
      direction = direction + 1;
    } else if (move[0] == 'L') {
      direction = direction - 1;
    }
    direction = mod(direction, 4); // % does not work for negative numbers

    const steps = Number(move.slice(1));

      for (let i = 0; i < steps; i++) {
        position.x += directions[direction][0];
        position.y += directions[direction][1];
        if (positions.has(to_string(position))) {
          return Math.abs(position.x) + Math.abs(position.y);
        }
        positions.add(to_string(position));
      }
  }
  conosole.error('no position visited twice');
}

console.assert(a('R2, L3') === 5, 'first example');
console.assert(a('R2, R2, R2') === 2, 'second example');
console.assert(a('R5, L5, R5, R3') === 12, 'third example');

console.assert(b('R8, R4, R4, R8') === 4, 'b');



var fs = require('fs');

fs.readFile('input.txt', 'utf8', function(err, data) {
  if (err) throw err;
  console.log('a:', a(data));
  console.log('b:', b(data));
});
