# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring
def part_1(filename):
    directions = ((0, 1), (1, 0), (0, -1), (-1, 0))
    direction = 0
    current = [0, 0]
    with open(filename, 'r') as f:
        for turning, distance in [(m[0], int(m[1:])) for m in f.read().rstrip().split(', ')]:
            direction += 1 if turning == 'R' else 3
            direction %= 4
            current[0] += directions[direction][0] * distance
            current[1] += directions[direction][1] * distance
    return abs(current[0]) + abs(current[1])

def part_2(filename):
    result = 0
    with open(filename) as f:
        for line in [line.rstrip() for line in f]:
            print(line)

    return result

def main():
    # assert part_1('test.txt') == 0
    print(part_1('input.txt'))
    # assert part_2('test.txt') == 0
    # print(part_2('input.txt'))

if __name__ == "__main__":
    main()

