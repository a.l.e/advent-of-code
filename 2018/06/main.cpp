#include <iostream>
#include <iterator>
#include <vector>
#include <set>
#include <map>
#include <string>
#include <algorithm>
#include <numeric>
#include <cmath>
#include <limits>

struct Coordinate {
    int x, y;
};

using Coordinates = std::vector<Coordinate>;

std::ostream& operator<<(std::ostream& os, const Coordinate& coo)
{
    return os << "(" << coo.x << ", " << coo.y << ")";
}

std::istream& operator>>(std::istream& is, Coordinate& coo)
{
    char c;
    is >> coo.x >> c >> coo.y;
    return is;
}

Coordinates& read_input(Coordinates& coordinates)
{
    std::copy(
        std::istream_iterator<Coordinate>{std::cin}, {},
        std::back_inserter(coordinates));

    return coordinates;
}

std::pair<Coordinate, Coordinate> get_bounding_box(const Coordinates& c) {

	auto x_extreme = std::minmax_element(c.begin(), c.end(),
		[](const Coordinate& lhs, const Coordinate& rhs) {
			return lhs.x < rhs.x;
		});
	auto y_extreme = std::minmax_element(c.begin(), c.end(),
		[](const Coordinate& lhs, const Coordinate& rhs) {
			return lhs.y < rhs.y;
		});
    return std::make_pair(
        Coordinate{x_extreme.first->x, y_extreme.first->y},
        Coordinate {x_extreme.second->x, y_extreme.second->y});

}

int get_closest(const Coordinates& coordinates, int x, int y)
{
    int node{0};
    int min{std::numeric_limits<int>::max()};
    int i{0};
    for (const auto& coo: coordinates) {
        int distance = std::abs(x - coo.x) + std::abs(y - coo.y);
        if (distance == 0) {
            node = i;
            min = 0;
        } else if (distance == min) {
            node = -1;
        } else if (distance < min) {
            node = i;
            min = distance;
        }
        ++i;
    }
    return node;
}

/**
 * - find the closest node from each point in the bounding box
 * - remove all nodes with an area touching the border (infinite area)
 * - find how often each node occurs
 * - find the node with the biggest area
 */
int a(const Coordinates& coordinates)
{

    auto [min, max] = get_bounding_box(coordinates);
    int n = max.x - min.x + 1;
    int m = max.y - min.y + 1;

    std::vector<int> grid(n * m);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            grid.at(j * n + i) = get_closest(coordinates, min.x + i, min.y + j);
        }
    }

    // get all nodes with an area touching the border (infinite size)
    std::set<int>border{};
    for (int i = 0; i < n; i++) {
        if (grid.at(i) != -1) {
            border.insert(grid.at(i));
        }
        if (grid.at((m - 1) * n + i) != -1) {
            border.insert(grid.at(((m - 1) * m + i)));
        }
    }
    for (int j = 0; j < m; j++) {
        if (grid.at(j * n) != -1) {
            border.insert(grid.at(j * n));
        }
        if (grid.at((j + 1) * n - 1) != -1) {
            border.insert(grid.at((j + 1) * n - 1));
        }
    }

    // remove the points that are part of infinite areas
    grid.erase(std::remove_if(grid.begin(), grid.end(),
        [&border](const int g){
            return border.find(g) != border.end();
        }), grid.end());

    // remove the shared ares (-1)
    grid.erase(std::remove(grid.begin(), grid.end(), -1), grid.end());

    // get the node with most occurrences
    std::map<int, int> occurrences{};
    for (const auto g: grid) {
        occurrences[g]++;
    }

    return std::max_element(occurrences.begin(), occurrences.end(),
        [](const auto a, const auto b) {
            return a.second < b.second;
        })->second;
}

int b(const Coordinates& coordinates)
{
    auto [min, max] = get_bounding_box(coordinates);

    int n = max.x - min.x + 1;
    int m = max.y - min.y + 1;
    std::vector<int> grid(n * m);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            int x{min.x + i};
            int y{min.y + j};
            grid.at(j * n + i) = std::accumulate(coordinates.begin(), coordinates.end(), 0,
                [x, y](int a, const auto& c) {
                    return a + std::abs(c.x - x) + std::abs(c.y - y);
                });
        }
    }

    return std::accumulate(grid.begin(), grid.end(), 0,
        [](int a, const int g) {
            return a + (g < 10000 ? 1 : 0);
        });
}

int main()
{
    Coordinates coordinates{};
    coordinates = read_input(coordinates);
    std::cout << a(coordinates) << std::endl; // 4011
    // std::cout << b(coordinates) << std::endl; // 46054
}
