#include <iostream>
#include <vector>
#include <string>
#include <numeric>

using namespace std::string_literals;

std::ostream& operator<<(std::ostream& os, std::vector<int> v)
{
    return os << std::accumulate(v.begin() + 1, v.end(), std::to_string(*v.begin()),
        [](std::string a, int i) { return a + " " + std::to_string(i); });
}

std::string a(int score_i)
{
    std::vector<int> scoreboard{3, 7};
    size_t i_1{0};
    size_t i_2{1};
    while (scoreboard.size() < score_i + 10) {
        int sum{scoreboard.at(i_1) + scoreboard.at(i_2)};
        int a{sum / 10};
        int b{sum % 10};
        if (a > 0) {
            scoreboard.push_back(a);
        }
        scoreboard.push_back(b);
        i_1 = {(i_1 + 1 + scoreboard.at(i_1)) % scoreboard.size()};
        i_2 = {(i_2 + 1 + scoreboard.at(i_2)) % scoreboard.size()};
        // std::cout << scoreboard << std::endl;
        // std::cout << "a: " << a << " b: " << b << std::endl;
    }
    std::string result{};
    for (int i = score_i; i < score_i + 10; i++) {
        // std::cout << scoreboard.at(i) << std::endl;
        result += std::to_string(scoreboard.at(i));
    }
    return result;
}

size_t b(size_t score_target)
{
    static std::vector<int> scoreboard{3, 7};
    if (scoreboard.size() == 2) {
        size_t i_1{0};
        size_t i_2{1};
        while (scoreboard.size() < 50000000) {
            int sum{scoreboard.at(i_1) + scoreboard.at(i_2)};
            if (sum >= 10) {
                scoreboard.push_back(1);
                scoreboard.push_back(sum % 10);
            } else {
                scoreboard.push_back(sum);
            }
            i_1 = {(i_1 + 1 + scoreboard.at(i_1)) % scoreboard.size()};
            i_2 = {(i_2 + 1 + scoreboard.at(i_2)) % scoreboard.size()};
        }
    }
    for (size_t i = 0; i < 50000000; i++) {
        int sum{0};
        for (int j = 0; j < 6; j++) {
            sum *= 10;
            sum += scoreboard.at(i + j);
            if (sum == score_target) {
                return i;
            }
        }
    }
    return 0;
}


int main()
{
    int score_i = 430971;
    std::cout << a(9) << std::endl;
    std::cout << a(5) << std::endl;
    std::cout << a(score_i) << std::endl; // 1420135583
    // std::cout << b(51589) << std::endl;
    // std::cout << b(92510) << std::endl;
    // std::cout << b(59414) << std::endl;
    std::cout << b(430971) << std::endl;
}
