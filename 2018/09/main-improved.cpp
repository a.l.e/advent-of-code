// with list and lambdas in c++: https://www.reddit.com/r/adventofcode/comments/a4i97s/2018_day_9_solutions/ebephjj
// seems to be a bit slower thatn my own linked list... but just ab it and it's much less code!


#include <iostream>
#include <list>
#include <vector>
#include <algorithm>

int main()
{
    int players_n{493}, marbles_n{71863 * 100};
    std::list<int> circle;
    circle.push_back(0);

    auto next = [&circle](auto i) {
        if (i == circle.end()) {
            i = circle.begin();
        }
        return ++i;
    };

    auto prev = [&circle](auto i) {
        if (i == circle.begin()) {
            i = circle.end();
        }
        return --i;
    };

    std::vector<unsigned long long> score(players_n);

    auto current = circle.begin();
    for (int i = 1; i < marbles_n; i++) {
        if (i % 23 == 0) {
            for (int j = 0; j < 7; j++) {
                current = prev(current);
            }
            score[i % players_n] += i + *current;
            current = circle.erase(current);
        } else {
            current = circle.insert(next(next(current)), i);
        }
    }

    std::cout<< *std::max_element(score.begin(), score.end()) << std::endl;
}
