// heavily inspired from py https://www.reddit.com/r/adventofcode/comments/a4i97s/2018_day_9_solutions/ebepyc7
// it's very slow (several seconds for the first step data... i don't dare to check it with the 100 times data of the second step.
// it might be possible to use a forward_list (which is a linked list) instead of a deque (an array) and it might be faster to rotate

#include <iostream>
#include <deque>
#include <unordered_map>
#include <algorithm>
#include <numeric>

std::ostream& operator<<(std::ostream& os, std::deque<int> v)
{
	os << "{";
	if (!v.empty()){
		os << std::accumulate(v.begin() + 1, v.end(), std::to_string(*v.begin()),
			[](std::string a, int i) { return a + ", " + std::to_string(i); });
	}
	return os << "}";
}

long long a(const int players_n, const int marble_n)
{
	std::unordered_map<int, long long> scores{};
	std::deque<int> circle{0};

	for (int i = 1; i <= marble_n; i++) {
        if (i % 23 == 0) {
            std::rotate(circle.rbegin(), circle.rbegin() + 7, circle.rend());
            scores[i % players_n] += i + circle.back();
            circle.pop_back();
            std::rotate(circle.begin(), circle.begin() + 1, circle.end());
        } else {
            std::rotate(circle.begin(), circle.begin() + 1, circle.end());
            circle.push_back(i);
        }
        // std::cout << circle << std::endl;
	}

	return scores.empty() ? 0 : std::max_element(scores.begin(), scores.end(),
        [](const auto& a, const auto& b) {
            return a.second < b.second;
    })->second;
}

int main()
{
    std::cout << a(9, 25) << std::endl;
    std::cout << a(493, 71863) << std::endl; // 363690 <-- not correct
}
