#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <iterator>
#include <unordered_set>

int a()
{
    int sum{0};
    for (std::string line; std::getline(std::cin, line);) {

        sum += std::stoi(line);
    }
    return sum;
}

bool is_loop(int sum)
{
    static std::unordered_set<int> m{};
    if (auto s = m.find(sum); s != m.end()) {
        return true;
    }
    m.insert(sum);
    return false;
}

int b()
{
    int sum{0};
    std::vector<int> input{};
    for (std::string line; std::getline(std::cin, line);) {
        int i{std::stoi(line)};
        sum += i;
        if (is_loop(sum)) {
            return sum;
        }
        input.push_back(i);
    }

    auto it = input.begin();
    while (true) {
        sum += *it;
        if (is_loop(sum)) {
            return sum;
        }
        std::advance(it, 1);
        if (it == input.end()) {
            it = input.begin();
        }
    }
    return 0;
}

int main()
{
    // std::cout << a() << std::endl;
    std::cout << b() << std::endl;
}
