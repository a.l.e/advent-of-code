// ... b -> q
// 
// 
// h -> a -> w
// o -> a
// q -> a
// l -> a
//      h -> w
// 
// y -> d
// 
// v -> u
// 
// k -> f
// 
// j -> x
// 
// v -> r
// 
// f -> b
// 
// g -> p
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>
#include <algorithm>
#include <numeric>

const bool demo{true};
constexpr int n_workers{demo ? 2 : 4};
constexpr int time_delay{demo ? 0 : 60};

using Data = std::vector<std::pair<char, char>>;

void read_input(Data& data)
{
    for (std::string line; getline(std::cin, line); ) {
        data.push_back(std::make_pair(line.at(5), line.at(36)));
    }
}

std::string a(const Data& data)
{
    std::string result{};

    std::unordered_map<char, std::vector<char>> instructions{};
    for (const auto& [before, after]: data) {
        instructions[before];
        instructions[after].push_back(before);
    }

    std::vector<char> ready{};
    while (!(instructions.empty() && ready.empty())) {
        for (auto it = instructions.begin(); it != instructions.end(); ) {
            if (it->second.empty()) {
                ready.push_back(it->first);
                it = instructions.erase(it);
            } else {
                ++it;
            }
        }
        std::sort(ready.begin(), ready.end());

        char current = ready.at(0);
        ready.erase(ready.begin());
        result += current;
        
        for (auto& [id, before]: instructions) {
            before.erase(std::remove(before.begin(), before.end(), current), before.end());
        }

    }
    return result;
}

std::ostream& operator<<(std::ostream& os, const std::pair<char, int>& i)
{
    return os << "(" << i.first << ":" << i.second << ")";
}

std::ostream& operator<<(std::ostream& os, const std::unordered_map<char, std::vector<char>>& m)
{
    for (const auto [id, dep]: m) {
        os << id << " [";
        if (!dep.empty()) {
            os << std::accumulate(dep.begin() + 1, dep.end(), std::string(1, *dep.begin()),
                [](std::string a, char c) {return a + ", " + c;});
        }
        os << "]";
        os << std::endl;
    }
    return os;
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T> v)
{
    os << "{";
    if (!empty(v)) {
        std::stringstream ss, ssi;
        ss << *v.begin();
        os << std::accumulate(v.begin() + 1, v.end(), ss.str(),
            [&ssi](std::string a, T i) { ssi << i; return a + ", " + ssi.str(); });
    }
    return os << "}";
}

int b(const Data& data)
{
    int time{0};

    std::string order{};

    std::unordered_map<char, std::vector<char>> instructions{};
    for (const auto& [before, after]: data) {
        instructions[before];
        instructions[after].push_back(before);
    }

    std::cout << instructions << std::endl;

    std::vector<std::pair<char, int>> ready{};
    std::vector<std::pair<char, int>> busy{};
    while (!(instructions.empty() && ready.empty() && busy.empty())) {
        ++time;

        std::cout << "---" << std::endl;
        std::vector<char> done{};
        // the workers have worked one more minute
        std::cout << busy << std::endl;
        std::for_each(busy.begin(), busy.end(),
            [](auto& i) { --(i.second); });
        for (auto it = busy.begin(); it != busy.end(); ) {
            if (it->second == 0) {
                done.push_back(it->first);
                it = busy.erase(it);
            } else {
                ++it;
            }
        }

        if (!done.empty()) {
            std::sort(done.begin(), done.end());
            for (char current: done) {
                order += current;
                for (auto& [id, before]: instructions) {
                    before.erase(std::remove(before.begin(), before.end(), current), before.end());
                }
            }
        }

        if (busy.size() >= n_workers) {
            // no free worker
            continue;
        }

        for (auto it = instructions.begin(); it != instructions.end(); ) {
            if (it->second.empty()) {
                ready.push_back({it->first, time_delay + (it->first - 'A' + 1)});
                it = instructions.erase(it);
            } else {
                ++it;
            }
        }

        std::sort(ready.begin(), ready.end());

        for (auto it = ready.begin(); it != ready.end(); ) {
            busy.push_back(*it);
            it = ready.erase(it);

            if (busy.size() >= n_workers) {
                break;
            }
        }
        
    }
    std::cout << order << std::endl;

    return time - 1;
}

int main()
{
    Data data{};
    read_input(data);
    /*
    for (const auto& d: data) {
        std::cout << d.first << " -> " << d.second << std::endl;
    }
    */
    // std::cout << a(data) << std::endl;
    std::cout << b(data) << std::endl; // ko: GKTVCNPYHRDIUJMSXFBQLOAEWZ / CAFBDE // 1266
}
