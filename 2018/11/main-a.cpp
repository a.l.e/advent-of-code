#include <iostream>
#include <cassert>
#include <array>
#include <utility>
#include <algorithm>
#include <tuple>

const int width{300};
const int height{300};
using Grid = std::array<int, width * height>;

std::ostream& operator<<(std::ostream& os, std::pair<int, int> p)
{
    return os << "[" << p.first << ", " << p.second << "]";
}

int get_power_level(int x, int y, int serial_number)
{
    int rack_id{x + 10};
    int power_level{rack_id * y};
    power_level +=  serial_number;
    power_level *= rack_id;
    power_level /= 100;
    power_level %= 10;
    return power_level - 5;
}

int square_sum(Grid grid, int x, int y)
{
    int sum{0};
    for (int i = x; i < x + 3; i++) {
        for (int j = y; j < y + 3; j++) {
            sum += grid.at(i * width + j);
        }
    }
    return sum;
}

std::tuple<int, int, int> get_max_square(const Grid& grid, Grid& squares, const int size)
{
    for (int i = 0; i < height - size; i++) {
        for (int j = 0; j < width - size; j++) {
            squares.at(i * (width - size) + j) = square_sum(grid, i, j);
        }
    }
    auto max = std::max_element(squares.begin(), squares.end());

    int x = std::distance(squares.begin(), max) % (width - 3);
    int y = std::distance(squares.begin(), max) / (width - 3);
    return {*max, x, y};
}

void fill_grid(Grid& grid, int serial_number)
{
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            grid.at(i * width + j) = get_power_level(j + 1, i + 1, serial_number);
        }
    }
}

std::pair<int, int> a(int serial_number)
{
    Grid grid{};
    fill_grid(grid, serial_number);
    std::array<int, width * height> squares{};
    auto [max, x, y] = get_max_square(grid, squares, 3);
    return  std::make_pair(x + 1, y + 1);
}

std::tuple<int, int, int> b(int serial_number)
{
}


int main()
{
    const int serial_number{9445};
    assert(get_power_level(3, 5, 8) == 4);
    assert(get_power_level(122, 79, 57) == -5);
    assert(get_power_level(217, 196, 39) == 0);
    assert(get_power_level(101, 153, 71) == 4);
    std::cout << a(18) << std::endl;
    std::cout << a(serial_number) << std::endl;
}
