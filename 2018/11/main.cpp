// a() did work, b() was too slow and did not give the rightresult... now it probably gives the correct result... but it's still way too slow...
#include <iostream>
#include <cassert>
#include <array>
#include <utility>
#include <algorithm>
#include <tuple>
#include <limits>

const int grid_width{300};
const int grid_height{300};
using Grid = std::array<int, grid_width * grid_height>;

std::ostream& operator<<(std::ostream& os, std::pair<int, int> p)
{
    return os << "[" << p.first << ", " << p.second << "]";
}

std::ostream& operator<<(std::ostream& os, std::tuple<int, int, int> t)
{
    return os << "[" << std::get<0>(t) << ", " << std::get<1>(t) << "]" << " @ " << std::get<2>(t);
}

int get_power_level(int x, int y, int serial_number)
{
    int rack_id{x + 10};
    int power_level{rack_id * y};
    power_level +=  serial_number;
    power_level *= rack_id;
    power_level /= 100;
    power_level %= 10;
    return power_level - 5;
}

void fill_grid(Grid& grid, int serial_number)
{
    for (int i = 0; i < grid_height; i++) {
        for (int j = 0; j < grid_width; j++) {
            grid.at(i * grid_width + j) = get_power_level(j + 1, i + 1, serial_number);
        }
    }
}

int square_sum(Grid grid, int x, int y, int square_width)
{
    int sum{0};
    for (int i = x; i < x + square_width; i++) {
        for (int j = y; j < y + square_width; j++) {
            sum += grid.at(i * grid_width + j);
        }
    }
    return sum;
}

std::tuple<int, int, int> get_max_square(const Grid& grid, Grid& squares, const int square_width)
{
    int max{std::numeric_limits<int>::min()};
    int max_x{0}, max_y{0};
    for (int i = 0; i < grid_height - square_width; i++) {
        for (int j = 0; j < grid_width - square_width; j++) {
            int v{square_sum(grid, i, j, square_width)};
            squares.at(i * (grid_width - square_width) + j) = v;
            if (v > max) {
                max = v;
                max_x = j;
                max_y = i;
            }

        }
    }
    return {max, max_x, max_y};
    /*
    auto max = std::max_element(squares.begin(), squares.begin() + square_width);

    int x = std::distance(squares.begin(), max) % (grid_width - square_width);
    int y = std::distance(squares.begin(), max) / (grid_width - square_width);
    return {*max, x, y};
    */
}

std::pair<int, int> a(int serial_number)
{
    Grid grid{};
    fill_grid(grid, serial_number);
    std::array<int, grid_width * grid_height> squares{};
    auto [max, x, y] = get_max_square(grid, squares, 3);
    return  std::make_pair(x + 1, y + 1);
}

std::tuple<int, int, int> b(int serial_number)
{
    Grid grid{};
    fill_grid(grid, serial_number);
    std::array<int, grid_width * grid_height> squares{};
    int max{std::numeric_limits<int>::min()};
    int max_i{0};
    int max_x{0};
    int max_y{0};
    for (int i = 1; i < 300 - 1; i++) {
        std::cout << i << std::endl;
        auto [max_value, x, y] = get_max_square(grid, squares, i);
        if (max_value > max) {
            max = max_value;
            max_i = i;
            max_x = x;
            max_y = y;
        }
    }
    /*
    std::array<int, grid_width * grid_height> psum{}, qsum{};
    psum.at(0) = grid.at(0);
    for (int i = 1; i < grid_width * grid_height; i++) {
        psum.at(i) = grid.at(i- 1) + kkk
    }
    */
    return  std::make_tuple(max_x + 1, max_y + 1, max_i);
}


int main()
{
    const int serial_number{9445};
    assert(get_power_level(3, 5, 8) == 4);
    assert(get_power_level(122, 79, 57) == -5);
    assert(get_power_level(217, 196, 39) == 0);
    assert(get_power_level(101, 153, 71) == 4);
    std::cout << a(18) << std::endl;
    std::cout << a(serial_number) << std::endl; // 233,36
    // std::cout << b(18) << std::endl; // [90, 269] = 113
    std::cout << b(serial_number) << std::endl; // [231, 107] @ 14

}
