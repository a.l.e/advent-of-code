#include <iostream>
#include <vector>
#include <string>
#include <utility>
#include <unordered_map>
#include <algorithm>
#include <numeric>

using Rules = std::unordered_map<std::string, char>;
using Pot = std::pair<int, char>;
using Pots = std::vector<Pot>;

using namespace std::string_literals;

std::ostream& operator<<(std::ostream& os, const Pots& pots) {
    os << std::accumulate(pots.begin(), pots.end(), ""s,
        [](std::string a, const auto& p) { return a + (p.first < 0 ? " " : std::to_string(p.first % 10)); }) << std::endl;
    return os << std::accumulate(pots.begin(), pots.end(), ""s,
        [](std::string a, const auto& p) { return a + p.second; });
}

void ltrim(Pots &pots)
{
    pots.erase(pots.begin(), std::find_if(pots.begin(), pots.end(),
		[](const auto& p) { return p.second == '#'; }));
}

void rtrim(Pots &pots) {
    pots.erase(std::find_if(pots.rbegin(), pots.rend(),
		[](const auto& p) { return p.second == '#'; }
		).base(), pots.end());
}

void trim(Pots &pots)
{
    ltrim(pots);
    rtrim(pots);
}


std::pair<Pots, Rules> read_input()
{
    Pots pots{};
    Rules rules{};
    std::string line;
    getline(std::cin, line);
	int i = 0;
    for (char c: line.substr(15)) {
		pots.push_back({i, c});
        ++i;
    }
    getline(std::cin, line); // ignore line
    while (getline(std::cin, line)) {
            rules[line.substr(0, 5)] = line.at(9);
    }
    return {pots, rules};
}

Pots get_evolution(Pots pots, const Rules& rules)
{
    Pots result{};
    std::string pattern {"....."};

    // add .... at the end for the last plant
    int n{(pots.back()).first};
    for (int j = 1; j < 5; j++) {
        pots.push_back({n + j, '.'});
    }

    // starts from here ..v.#
    int i{(pots.front()).first - 2};

    for (const auto p: pots) {
        pattern = pattern.substr(1) + p.second;
        // std::cout << "p >" << pattern << "< " << rules.at(pattern) << std::endl;
        result.push_back({i, rules.at(pattern)});
        i++;
    }
	trim(result);
    // std::cout << result << std::endl;

    return result;
}

int get_value(Pots pots)
{
    return std::accumulate(pots.begin(), pots.end(), 0,
        [](int a, const auto& p) { return a + (p.second == '#' ? p.first : 0); });
}

int a(Pots& pots, const Rules& rules)
{
    for (int i = 0; i < 20; i++) {
        pots = get_evolution(pots, rules);
    }

    return get_value(pots);
}

long long b(Pots& pots, const Rules& rules)
{
    int last{0};
    for (long long i = 0; i < 1000; i++) {
        // std::cout << i << std::endl;
        pots = get_evolution(pots, rules);
        int current{get_value(pots)};
        std::cout << i << ", " << current << ", " << current - last << std::endl;
        last = current;
    }

    // at 151 it gets to 1173, then it grows by 8 on each evolution
    return 1173 + (50000000000 - 152) * 8;
}

int main()
{
    auto [plants, rules] = read_input();
    std::cout << a(plants, rules) << std::endl;
    // std::cout << b(plants, rules) << std::endl; // 399999999965?
}
