#include <iostream>
#include <unordered_map>
#include <vector>

/*
struct Item {
    int id;
    int count{0};
};

int main() {
    std::unordered_map<int, Item> products;
    std::vector<std::pair<int, int>> boxes {
        {1, 4}, {2, 2}, {1, 2}, {3, 1}, {1, 5}};
    for (const auto& [id, count]: boxes) {
        auto search{products.find(id)};
        if (search !=products.end()) { 
            search->second.count += count;
        } else {
            products.insert({id, {id, count}});
        }
    }

    for (const auto& [id, product]: products) {
        std::cout << product.id << ": " << product.count << std::endl;
    }
    // 3: 1
    // 1: 11
    // 2: 2
}
*/

struct Item {
    int id;
    int count{0};

    Item() {};
    Item(int id) : id{id} {};

    Item& operator+=(const int rhs)
    {
        count += rhs;
        return *this;
    }
};

/*
int main()
{
    Item item{1, 5};
    item += 6;
    std::cout << item.id << ": " << item.count;
}
*/

int main()
{
    std::unordered_map<int, Item> products;
    std::vector<std::pair<int, int>> boxes {
        {1, 4}, {2, 2}, {1, 2}, {3, 1}, {1, 5}};
    for (const auto& [id, count]: boxes) {
        products[id] += count;
    }

    for (const auto& [id, product]: products) {
        std::cout << product.id << ": " << product.count << std::endl;
    }
}
