#include <iostream>
#include <array>
#include <string>
#include <vector>
#include <unordered_map>
#include <utility>
#include <tuple>
#include <algorithm>
#include <numeric>

using Guard = std::pair<int, std::array<int, 60>>;
using Guards = std::unordered_map<int, Guard>;

Guards& get_sleepy_guards(Guards& guards)
{
    std::vector<std::string> data{};

    for (std::string line; getline(std::cin, line); ) {
        data.push_back(line);
    }
    std::sort(data.begin(), data.end());

    int id{0};
    int start{0};

    for (const auto& line: data) {
        switch (line.at(19)) {
            case 'G': // Guard #
                id = std::stoi(line.substr(26));
            break;
            case 'w': { // wakes up
                auto time{std::stoi(line.substr(15, 2))};
                auto& current{guards[id]};
                current.first += time - start;
                auto it = current.second.begin();
                std::for_each(it + start, it + time,
                    [](int& i) { ++i; });
            } break;
            case 'f': // falls asleep
                start = std::stoi(line.substr(15, 2));
            break;
        }

    }
    return guards;
}

int a(Guards guards)
{
    const auto& [id, guard] = *std::max_element(guards.begin(), guards.end(),
        [](const auto& a, const auto& b) {
            return a.second.first < b.second.first;
        });
    const auto max_minute = std::distance(guard.second.begin(),
        std::max_element(guard.second.begin(), guard.second.end()));

    return id * max_minute;
}

int b(Guards guards)
{
    const auto& [max_id, max_minute, max_sleepers] = std::accumulate(guards.begin(), guards.end(), std::make_tuple(0, 0, 0),
        [](auto& a, const auto& guard) {
            const auto& [id, g] = guard;
            const auto& [sum, minutes] = g;
            int i_max = std::distance(
                minutes.begin(),
                std::max_element(minutes.begin(), minutes.end()));
            return minutes[i_max] > std::get<2>(a) ? std::make_tuple(id, i_max, minutes[i_max]) : a;
        });
    return max_id * max_minute;
}

int main()
{
    Guards guards;
    guards = get_sleepy_guards(guards);

    std::cout << a(guards) << std::endl;
    std::cout << b(guards) << std::endl;
}
