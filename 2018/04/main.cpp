#include <iostream>
#include <iterator>
#include <vector>
#include <string>
#include <utility>
#include <algorithm>
#include <cctype> // isdigit
#include <unordered_map>

struct Guard
{
    int id;
    int asleep_time{0};
    std::unordered_map<int, int> minutes{};

    void add_sleep(int start, int end)
    {
        asleep_time += end - start;
        for (int i = start; i < end; i++) {
            minutes[i]++;
        }
    }
};

using Guards = std::unordered_map<int, Guard>;

Guards get_guards()
{
    std::vector<std::string> data{};
    Guards guards{};

    for (std::string line; getline(std::cin, line); ) {
        data.push_back(line);
    }
    std::sort(data.begin(), data.end());
    int start_sleep{0};
    int id{0};
    // std::vector<std::pairint sleep_ranges
    for (auto line: data) {
        auto line_day{line.substr(6, 5)};
        auto line_time{std::stoi(line.substr(15, 2))};
        switch (line.at(19)) {
            case 'G': {// Guard #
                id = std::stoi(line.substr(26, 
                    std::distance(
                        line.begin() + 26,
                        std::find_if(line.begin() + 26, line.end(),
                            [](char c) {
                                return !std::isdigit(c);
                            }
                        )
                    )));
                auto search{guards.find(id)};
                if (search == guards.end()) { 
                    guards.insert({id, {id}});
                }
            } break;
            case 'w': // wakes up
                guards[id].add_sleep(start_sleep, line_time);
            break;
            case 'f': // falls asleep
                start_sleep = line_time;
            break;
        }
    }
    return guards;
}

int a()
{
    auto guards = get_guards();
    auto it_sleeper = std::max_element(guards.begin(), guards.end(),
        [](const auto& a, const auto& b) {
            return a.second.asleep_time < b.second.asleep_time;});
    std::cout << it_sleeper->first << std::endl;
    auto minutes = it_sleeper->second.minutes;
    auto it_minute = std::max_element(minutes.begin(), minutes.end(),
        [](const auto& a, const auto& b) { return a.second < b.second; });
    std::cout << it_sleeper->first << std::endl;
    std::cout << it_minute->first << std::endl;
    return it_sleeper->first * it_minute->first;
}

int b()
{
    int max_sleepers{0};
    int max_minute{0};
    int max_id{0};
    for (const auto&[id, guard]: get_guards()) {
        if (!guard.minutes.empty()) {
            auto it_minute = std::max_element(guard.minutes.begin(), guard.minutes.end(),
                [](const auto& a, const auto& b) { return a.second < b.second; });
            if (it_minute->second > max_sleepers) {
                max_sleepers = it_minute->second;
                max_minute = it_minute->first;
                max_id = id;
            }
        }
    }
    return max_id * max_minute;
}

int main()
{
    ////  std::cout << a() << std::endl; // 1487 x 34 = 50558
    std::cout << b() << std::endl; // 28198
}
