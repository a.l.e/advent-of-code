#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using Data = std::vector<std::string>;

std::string read_input()
{
    std::string line;
    getline(std::cin, line);
    return line;
}

bool are_opposite(char a, char b)
{
    return a != b && std::tolower(a) == std::tolower(b);
}

// does not work for edge cases (final string is empty?)
size_t a(std::string data)
{
    int i = 0;
    while (i < data.size() - 1) {
        if (are_opposite(data.at(i), data.at(i + 1))) {
            data.erase(data.begin() + i, data.begin() + i + 2);
            if (i > 0) {
                i--;
            }
        } else {
            i++;
        }
    }
    return data.size();
}

size_t b(std::string data)
{
    std::string letters{"abcdefghijklmnopqrstuvwxyz"};
    auto min{data.size()};
    for (auto c: letters) {
        auto patched{data};
        patched.erase(std::remove_if(
                patched.begin(),
                patched.end(),
                [c](const unsigned char cc){ return std::tolower(cc) == c; }), 
            patched.end());
        min = std::min(min, a(patched));
    }
    return min;
}

int main()
{
    auto data = read_input();
    // std::cout << a(data) << std::endl;
    std::cout << b(data) << std::endl;
}
