// based on py https://www.reddit.com/r/adventofcode/comments/a47ubw/2018_day_8_solutions/ebc7ol0
#include <iostream>
#include <vector>
#include <iterator>
#include <numeric>
#include <tuple>
#include <algorithm>

using Data = std::vector<int>;

std::tuple<int, int, std::vector<int>> parse(Data data)
{
    int children_n{data.at(0)};
    int data_n{data.at(1)};
    data.erase(data.begin(), data.begin() + 2);

    std::vector<int> scores{};
    int sum{0}, sum_partial{0}, score{0};

    for (int i = 0; i < children_n; i++) {
        std::tie(sum_partial, score, data) = parse(data);
        sum += sum_partial;
        scores.push_back(score);
    }

    int sum_meta = std::accumulate(data.begin(), data.begin() + data_n, 0);
    sum += sum_meta;

    if (children_n == 0) {
        return {sum, sum_meta, std::vector<int>{data.begin() + data_n, data.end()}};
    } else {
        int sum_score{std::accumulate(data.begin(), data.begin() + data_n, 0,
            [&data, &scores](int a, int i) {
                return a + (i > 0 && i < scores.size() ?  scores.at(i - i) : 0);
            })};
        return {sum, sum_score, std::vector<int>{data.begin() + data_n, data.end()}};
    }
}

int main()
{
    Data data{std::istream_iterator<int>{std::cin}, {}};

    auto [a, b, dummy] = parse(data);

    std::cout << a << std::endl;
    std::cout << b << std::endl;
}
