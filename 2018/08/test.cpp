#include <iostream>
#include <stack>

#include <vector>

struct A {
    A() {std::cout << "a" << std::endl;}
    int c{2};
};

std::vector<int> z() {
    std::vector<int> v{1,2,3,4,5};
    return std::vector<int>{v.begin() + 2, v.end()};

}

int main()
{
    std::stack<A> s{};
    A a{};
    s.emplace(a);
    a.c = 3;
    auto& b = s.top();
    std::cout << b.c << std::endl;

    
    std::vector<int> v{1,2,3,4,5};
    std::vector<int> w{v.begin() + 2, v.end()};
    for (auto i: w) {
        std::cout << i << std::endl;
    }

    z();
    for (auto zz: z()) {
        std::cout << zz << std::endl;
    }
}
