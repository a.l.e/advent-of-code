#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <utility>

using Map = std::vector<std::string>;

struct Cart
{
    explicit Cart (int x, int y, char direction) : x{x}, y{y}, direction{direction}, name{next_name++}{}
    int x, y;
    char direction;
	bool crashed{false};
    char name;
    inline static char next_name{'a'};
    void turn(char rotation)
    {
        if (rotation == '>') {
            if (direction == '^') {
                direction = '>';
            } else if (direction == '>') {
                direction = 'v';
            } else if (direction == 'v') {
                direction = '<';
            } else if (direction == '<') {
                direction = '^';
            }
        } else {
            if (direction == '^') {
                direction = '<';
            } else if (direction == '<') {
                direction = 'v';
            } else if (direction == 'v') {
                direction = '>';
            } else if (direction == '>') {
                direction = '^';
            }
        }
    }
    char next_crossing_turn{'<'};
    void advance_next_crossing_turn() {
        if (next_crossing_turn == '<') {
            next_crossing_turn = '+';
        } else if (next_crossing_turn == '+') {
            next_crossing_turn = '>';
        } else {
            next_crossing_turn = '<';
        }
    }
};

bool operator<(const Cart& lhs, const Cart& rhs)
{
    return lhs.crashed || (lhs.y == rhs.y ? lhs.x < rhs.x : lhs.y < rhs.y);
}

std::ostream& operator<<(std::ostream& os, const Cart& cart)
{
    return os << cart.name << ": (" << cart.x << ", " << cart.y << ") " << cart.direction << (cart.crashed ? "†" : "");
}

using Carts = std::vector<Cart>;

std::ostream& operator<<(std::ostream& os, const std::pair<int, int> p)
{
    return os << "(" << p.first << ", " << p.second << ")";
}

Map read_input()
{
    Map data{};

    for (std::string line; getline(std::cin, line); ) {
            data.push_back(line);
    }

    return data;

}

Carts get_carts(Map& map)
{
    Carts carts{};
    for (int i = 0; i < map.size(); i++) {
        for (int j = 0; j < map.at(i).size(); j++) {
            char& c{map.at(i).at(j)};
            if (c == '^' || c == 'v') {
                carts.push_back(Cart{j, i, c});
                c = '|';
            } else if (c == '>' || c == '<') {
                carts.push_back(Cart{j, i, c});
                c = '-';
            }
        }
    }
    // std::sort(carts.begin(), carts.end());
    // show_carts(carts);
    return carts;
}

void show_carts(const Carts& carts)
{
    for (const auto& cart: carts) {
        std::cout <<  cart << std::endl;
    }
    std::cout <<  "---" << std::endl;
}

void advance(Cart& cart, char movement)
{
    if (movement == '+') {
        if (cart.next_crossing_turn != '+') {
            cart.turn(cart.next_crossing_turn);
        }
        cart.advance_next_crossing_turn();
    } else if (movement == '/') {
        if (cart.direction == '^' || cart.direction == 'v') {
            cart.turn('>');
        } else {
            cart.turn('<');
        }
    } else if (movement == '\\') {
        if (cart.direction == '^' || cart.direction == 'v') {
            cart.turn('<');
        } else {
            cart.turn('>');
        }
    }

    if (movement != '|' && movement != '-') {
        if (cart.direction == '^' || cart.direction == 'v') {
            movement = '|';
        } else {
            movement = '-';
        }
    }
    if (movement == '|') {
        cart.y += (cart.direction == '^' ? - 1 : +1);
    } else if (movement == '-') {
        cart.x += (cart.direction == '<' ? - 1 : +1);
    }
}

void asciianimation_header()
{
    std::cout << "x:150" << std::endl;
    std::cout << "y:150" << std::endl;
    std::cout << "BEGIN" << std::endl;
}

void asciianimation_frame(const Map& map, const Carts& carts)
{
    std::string frame{};
    for (const auto& line: map) {
        frame += line + '\n';
    }
    
    for (const auto& cart: carts)
    {
        frame.at(cart.y * (150 + 1) + cart.x) = cart.direction;
    }

    std::cout << frame;
    std::cout << "END" << std::endl;
}

void ansilove_frame(int i, const Map& map, const Carts& carts)
{
    const int line_width = 150;
    std::string frame{};
    for (const auto& line: map) {
        frame += line + '\n';
    }
    
    for (const auto& cart: carts)
    {
        frame.at(cart.y * (line_width + 1) + cart.x) = cart.direction;
    }

    std::string i_str = std::to_string(i);
    i_str = std::string(4 - i_str.size(), '0') + i_str;

    std::ofstream file("tmp/" + i_str + ".txt");
    file << frame;
}

std::pair<int, int> a(Map map)
{
    Carts carts = get_carts(map);
    int i{0};
    while (true) {
        // std::cout << i << std::endl;
        std::sort(carts.begin(), carts.end());
        // ansilove_frame(i, map, carts);
        for (auto& cart: carts) {
            // std::cout << cart << std::endl;
            char movement = map.at(cart.y).at(cart.x);
            // std::cout << movement << std::endl;
            advance(cart, movement);
			auto it = std::adjacent_find(carts.begin(), carts.end(),
				[](const auto& lhs, const auto& rhs) { return lhs.x == rhs.x && lhs.y == rhs.y;});
			if (it != carts.end()) {
				return {it->x, it->y};
			}
        }
        ++i;
    }
    return {-1, -1};
}

std::pair<int, int> b(Map map)
{
    Carts carts = get_carts(map);
    auto carts_n{carts.size()};
    int i{0};
    while (true) {
        std::sort(carts.begin(), carts.end());
        for (auto& cart: carts) {
            if (cart.crashed) {
                continue;
            }
            char movement = map.at(cart.y).at(cart.x);
            advance(cart, movement);
            
            auto it = std::find_if(carts.begin(), carts.end(),
                [&cart](const auto& i) { return i.crashed == false && i.x == cart.x && i.y == cart.y;});
            auto it2 = std::find_if(it + 1, carts.end(),
                [&cart](const auto& i) { return i.crashed == false && i.x == cart.x && i.y == cart.y;});

			if (it2 != carts.end()) {
                carts_n -= 2;
                it->crashed = true;
                it2->crashed = true;
			}
        }
        carts.erase(std::remove_if(carts.begin(), carts.end(),
            [](const auto& c) { return c.crashed; }), carts.end());
        if (carts_n == 1) {
            auto it = std::find_if(carts.begin(), carts.end(),
                [](const auto& i) { return i.crashed == false; });
            return {it->x, it->y};
        }
        ++i;
    }
    return {0, 0};
}

int main()
{
    auto map = read_input();
    // a(map);
    // std::cout << a(map) << std::endl; // 124,130
    std::cout << b(map) << std::endl;
}
