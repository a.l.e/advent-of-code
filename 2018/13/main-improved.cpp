// inspired by https://www.reddit.com/r/adventofcode/comments/a5qd71/2018_day_13_solutions/eboleqg
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <tuple>
#include <map>

struct Coordinate
{
    int x, y;

    Coordinate& operator+=(const Coordinate& c)
    {
        x += c.x;
        y += c.y;
        return *this;
    }
};

bool operator<(const Coordinate& lhs, const Coordinate& rhs)
{
    return (lhs.y == rhs.y ? lhs.x < rhs.x : lhs.y < rhs.y);
}

bool operator==(const Coordinate& lhs, const Coordinate& rhs)
{
    return lhs.x == rhs.x && lhs.y == rhs.y;
}

std::ostream& operator<<(std::ostream& os, const Coordinate& c)
{
    return os << "(" << c.x << ", " << c.y << ")";
}

struct Cart
{
    explicit Cart (Coordinate pos, Coordinate speed) : pos{pos}, speed{speed}, name{next_name++}{}
    Coordinate pos;
    Coordinate speed;
    bool crashed{false};
    int next_crossing{0};
    char name;
    inline static char next_name{'a'};
};

bool operator<(const Cart& lhs, const Cart& rhs)
{
    return lhs.crashed || (lhs.pos < rhs.pos);
}

std::ostream& operator<<(std::ostream& os, const Cart& cart)
{
    return os << cart.name << ": " << cart.pos << cart.speed << (cart.crashed ? "†" : "");
}

using Map = std::vector<std::string>;
using Carts = std::vector<Cart>;

std::ostream& operator<<(std::ostream& os, const Carts& carts)
{
    for (const auto& cart: carts) {
        os << cart << std::endl;
    }
    return os;
}

std::map<Coordinate, char> directions {
    {{-1, 0}, '<'}, {{1, 0}, '>'}, {{0, -1}, '^'}, {{0, 1}, 'v'}};

void ansilove_frame(int i, const Map& map, const Carts& carts)
{
    const int line_width = 150;
    // const int line_width = 13;
    std::string frame{};
    for (const auto& line: map) {
        frame += line + '\n';
    }
    
    for (const auto& cart: carts)
    {
        frame.at(cart.pos.y * (line_width + 1) + cart.pos.x) = directions[cart.speed];
    }

    std::string i_str = std::to_string(i);
    i_str = std::string(4 - i_str.size(), '0') + i_str;

    std::ofstream file("tmp/" + i_str + ".txt");
    file << frame;
}


std::tuple<Map, Carts> read_input()
{
    Map map{};
    Carts carts{};

    Coordinate speed{};
    int i{0};
    for (std::string line; getline(std::cin, line); ) {
        int j{0};
        for (char& c: line) {
            if (c == '<') {
                carts.push_back(Cart{{j, i}, {-1, 0}});
                c = '-';
            } else if (c == '>') {
                carts.push_back(Cart{{j, i}, {1, 0}});
                c = '-';
            } else if (c == '^') {
                carts.push_back(Cart{{j, i}, {0, -1}});
                c = '|';
            } else if (c == 'v') {
                carts.push_back(Cart{{j, i}, {0, 1}});
                c = '|';
            }
            ++j;
        }
        map.push_back(line);
        ++i;
    }

    return {map, carts};

}

void advance(Cart& cart, char movement)
{
    auto [dx, dy] = cart.speed;
    if (movement == '\\') {
        cart.speed = {dy, dx};
    } else if (movement == '/') {
        cart.speed = {-dy, -dx};
    } else if (movement == '+') {
        if (cart.next_crossing == 0) {
            cart.speed = {dy, -dx};
        } else if (cart.next_crossing == 2) {
            cart.speed = {-dy, dx};
        }
        cart.next_crossing = (cart.next_crossing + 1) % 3;
    }
    cart.pos += cart.speed;
}

Coordinate a(Map map, Carts carts)
{
    std::sort(carts.begin(), carts.end());
    // std::cout << carts << std::endl;
    int i{0};
    while (true) {
        // ansilove_frame(i, map, carts);
        std::sort(carts.begin(), carts.end());
        for (auto& cart: carts) {
            char movement = map.at(cart.pos.y).at(cart.pos.x);
            advance(cart, movement);

            auto find_cart = [&cart](const auto& i) { return i.pos == cart.pos;};
            auto it = std::find_if(carts.begin(), carts.end(), find_cart);
            auto it2 = std::find_if(it + 1, carts.end(), find_cart);
			if (it2 != carts.end()) {
				return {it->pos.x, it->pos.y};
			}
        }
        ++i;
    }
    return {};
}

Coordinate b(Map map, Carts carts)
{
    std::sort(carts.begin(), carts.end());

    auto carts_n{carts.size()};
    // std::cout << carts << std::endl;
    while (true) {
        std::sort(carts.begin(), carts.end());
        for (auto& cart: carts) {
            if (cart.crashed) {
                continue;
            }
            char movement = map.at(cart.pos.y).at(cart.pos.x);
            advance(cart, movement);

            auto find_cart = [&cart](const auto& i) { return i.pos == cart.pos;};
            auto it = std::find_if(carts.begin(), carts.end(), find_cart);
            auto it2 = std::find_if(it + 1, carts.end(), find_cart);
			if (it2 != carts.end()) {
                carts_n -= 2;
                it->crashed = true;
                it2->crashed = true;
			}
        }

        carts.erase(std::remove_if(carts.begin(), carts.end(),
            [](const auto& c) { return c.crashed; }), carts.end());

        if (carts_n == 1) {
            return {carts.front().pos};
        }
    }
    return {};
}

int main()
{
    auto [map, carts] = read_input();
    std::cout << a(map, carts) << std::endl;
    std::cout << b(map, carts) << std::endl;
}
