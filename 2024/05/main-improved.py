# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring

from functools import cmp_to_key

def get_data(filename):
    rules = []
    updates = []
    reading_rules = True
    with open(filename, 'r') as f:
        for line in [l.rstrip() for l in f]:
            if line == '':
                reading_rules = False
                continue
            if reading_rules:
                rules.append(tuple(map(int, line.split('|'))))
            else:
                updates.append(list(map(int, line.split(','))))

    return rules, updates

def rule_respected(update, rule):
    if  rule[0] not in update or rule[1] not in update:
        return True
    return update.index(rule[0]) < update.index(rule[1])

def part_1(filename):
    """
    it's _simpler_" to check if all rules are respected, instead of
    checking if the pages are in the correct order
    inspired by https://www.reddit.com/r/adventofcode/comments/1h71eyz/comment/m0i6q5k/
    """
    result = 0

    rules, updates = get_data(filename)

    for update in updates:
        if all(rule_respected(update, rule) for rule in rules):
            result += update[len(update) // 2]

    return result

def part_2(filename):
    result = 0

    rules, updates = get_data(filename)

    def compare_page(a, b):
        for rule in rules:
            if rule == (a, b):
                return -1
            if rule == (b, a):
                return 1
        return 0

    for update in updates:
        if any(not rule_respected(update, rule) for rule in rules):
            result += sorted(update, key=cmp_to_key(compare_page))[len(update) // 2]

    return result

def main():

    assert part_1('test.txt') == 143
    print(part_1('input.txt'))
    assert part_2('test.txt') == 123
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()
