# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring

from collections import defaultdict
from functools import cmp_to_key

def part_1_fails(filename):
    """
    for some uknown (to me) reason, using the ordering pairs to sort the values
    does work for the test values, but fails on the input
    """
    result = 0

    ordering_values = set()
    ordering_pairs = []
    reading_ordering_pairs = True

    def compare(a, b):
        for pair in ordering_pairs:
            if pair == (a, b):
                return -1
            if pair == (b, a):
                return 1
        return 0

    with open(filename, 'r') as f:
        for line in [l.rstrip() for l in f]:
            if line == '':
                ordering_values = sorted(ordering_values, key=cmp_to_key(compare))
                reading_ordering_pairs = False
                continue
            if reading_ordering_pairs:
                ordering_pair = tuple(map(int, line.split('|')))
                # print(ordering_pair)
                ordering_values.update(ordering_pair)
                ordering_pairs.append(ordering_pair)
            else:
                # print(line)
                update_values = list(map(int, line.split(',')))
                update_indexes = [ordering_values.index(value) for value in update_values]
                if update_indexes == sorted(update_indexes):
                    result += update_values[(len(update_values) - 1) // 2]
    return result

def part_1(filename):
    result = 0

    before = defaultdict(list)
    after = defaultdict(list)
    reading_ordering_pairs = True
    with open(filename, 'r') as f:
        for line in [l.rstrip() for l in f]:
            if line == '':
                reading_ordering_pairs = False
                continue
            if reading_ordering_pairs:
                a, b = map(int, line.split('|'))
                before[a].append(b)
                after[b].append(a)
            else:
                update_values = list(map(int, line.split(',')))
                for i, a in enumerate(update_values[:-1]):
                    for b in update_values[i + 1:]:
                        if b in before and a in before[b]:
                            break
                        if a in after and b in after[a]:
                            break
                    else:
                        continue
                    break
                else:
                    result += update_values[(len(update_values) - 1) // 2]

    return result

def get_data(filename):
    # the key must be before the values
    before = defaultdict(list)
    # the key must be after the values
    after = defaultdict(list)
    # all the update lists
    updates = []

    reading_ordering_pairs = True
    with open(filename, 'r') as f:
        for line in [l.rstrip() for l in f]:
            if line == '':
                reading_ordering_pairs = False
                continue
            if reading_ordering_pairs:
                a, b = map(int, line.split('|'))
                before[a].append(b)
                after[b].append(a)
            else:
                updates.append(list(map(int, line.split(','))))

    return before, after, updates

def part_2(filename):
    result = 0
    before, after, updates = get_data(filename)

    def compare(a, b):
        if a in before and b in before[a]:
            return -1
        if a in after and b in after[a]:
            return 1
        if b in before and a in before[b]:
            return 1
        if b in after and a in after[b]:
            return -1
        return 0

    for update_values in updates:
        sorted_values = sorted(update_values, key=cmp_to_key(compare))
        if update_values != sorted_values:
            result += sorted_values[(len(sorted_values) - 1) // 2]

    return result

def main():

    assert part_1_fails('test.txt') == 143
    print(part_1_fails('input.txt')) # wrong value
    assert part_1('test.txt') == 143
    print(part_1('input.txt'))
    assert part_2('test.txt') == 123
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()
