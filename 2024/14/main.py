# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring

from dataclasses import dataclass
from collections import Counter
import os

@dataclass
class Coord:
    x: int
    y: int

@dataclass
class Robot:
    p: Coord
    v: Coord

def next_position(robot, width, height):
    return Coord((robot.p.x + robot.v.x) % width, (robot.p.y + robot.v.y) % height)

def robots_in_region(robots, start, end):
    result = 0
    for robot in robots:
        if start.x <= robot.p.x < end.x and start.y <= robot.p.y < end.y:
            result += 1
    # print(result)
    return result

def display(robots, width, height):
    os.system('cls' if os.name == 'nt' else 'clear')
    counter = Counter([(robot.p.x, robot.p.y) for robot in robots])
    for y in range(height):
        for x in range(width):
            c = counter[(x, y)]
            print(c if c > 0 else ' ', end='')
        print()

def part_1(filename, width, height):
    result = 1
    robots = []
    with open(filename, 'r') as f:
        for p_v in [line.rstrip().split(' ') for line in f]:
            px, py = map(int, p_v[0][2:].split(','))
            vx, vy = map(int, p_v[1][2:].split(','))
            robots.append(Robot(Coord(px, py), Coord(vx, vy)))
            # print(robots[-1])
    for i in range(100):
        for robot in robots:
            robot.p = next_position(robot, width, height)
    # 01
    # 23
    # display(robots, width, height)
    quadrants = []
    quadrants.append((Coord(0, 0), Coord(width // 2, height // 2)))
    quadrants.append((Coord(width // 2 + 1, 0), Coord(width, height // 2)))
    quadrants.append((Coord(0, height // 2 + 1), Coord(width // 2, height)))
    quadrants.append((Coord(width // 2 + 1, height // 2 + 1), Coord(width, height)))
    for quadrant in quadrants:
        result *= robots_in_region(robots, *quadrant)
    return result

def triangle_area(a, b, c):
    return abs((a.x * (b.y - c.y) + (b.x * (c.y - a.y)) + (c.x * (a.y - b.y))) / 2.0)

def coord_in_triangle(p, a, b, c):
    """Is the point p in the triangle a, b, c?"""
    area_a_b_c = triangle_area(a, b, c)
    area_a_b_p = triangle_area(a, b, p)
    area_a_c_p = triangle_area(a, c, p)
    area_b_c_p = triangle_area(b, c, p)
    return area_a_b_p + area_a_c_p + area_b_c_p == area_a_b_c

def part_2(filename, width, height):
    robots = []
    with open(filename, 'r') as f:
        for p_v in [line.rstrip().split(' ') for line in f]:
            px, py = map(int, p_v[0][2:].split(','))
            vx, vy = map(int, p_v[1][2:].split(','))
            robots.append(Robot(Coord(px, py), Coord(vx, vy)))
    #   l r  
    #  ll rr 
    # lll rrr
    triangles = {'left': (Coord(0, 0), Coord(width // 2, 0), Coord(0, height)), \
        'right': (Coord(width // 2, 0), Coord(width, 0), Coord(width, height))}
    i = 1
    while True:
        coords_in_tree = set()
        for robot in robots:
            robot.p = next_position(robot, width, height)
            if coord_in_triangle(robot.p, *triangles['left']) or coord_in_triangle(robot.p, *triangles['right']):
                coords_in_tree.add((robot.p.x, robot.p.y))
        if len(coords_in_tree) < 100:
            display(robots, width, height)
            yes_or_no = input(f"{i} - is it a tree? ({len(coords_in_tree)})")
            print(len(yes_or_no.rstrip()))
            if len(yes_or_no.rstrip()) > 0 and yes_or_no.lower()[0] == 'y':
                return i
        i += 1
    return 'No tree found ' + str(i)

def main():
    assert part_1('test.txt', 11, 7) == 12 
    assert next_position(Robot(Coord(2, 4), Coord(2, -3)), 11, 7) == Coord(4, 1)
    assert next_position(Robot(Coord(4, 1), Coord(2, -3)), 11, 7) == Coord(6, 5)
    print(part_1('input.txt', 101, 103)) # 233232000
    # assert part_2('test.txt') == 0
    print(part_2('input.txt', 101, 103))

if __name__ == "__main__":
    main()
