# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring
def part_1(filename):
    result = 0
    left = []
    right = []
    with open(filename, 'r') as f:
        for line in [line.rstrip().split() for line in f]:
            left.append(int(line[0]))
            right.append(int(line[1]))
    left.sort()
    right.sort()
    for a, b in zip(left, right):
        result += abs(a - b)
    return result

def part_2(filename):
    result = 0
    left = []
    right = []
    with open(filename, 'r') as f:
        for line in [line.rstrip().split() for line in f]:
            left.append(int(line[0]))
            right.append(int(line[1]))
    for a in left:
        result += a * right.count(a)
    return result

def main():
    assert part_1('test.txt') == 11
    print(part_1('input.txt'))
    assert part_2('test.txt') == 31
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()
