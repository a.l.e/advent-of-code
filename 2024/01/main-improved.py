# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring
# from operator import sub
def part_1(filename):
    """
    the sum of the differences is the difference of the sums...
    """
    left = 0;
    right = 0;

    # with open(filename, 'r') as f:
    #     for a, b in [map(int, line.rstrip().split()) for line in f]:
    #         left += a
    #         right += b

    with open(filename, 'r') as f:
        left, right = map(sum ,zip(*[map(int, line.rstrip().split()) for line in f]))

    return abs(left - right)

    # i failed at doing it as a onliner map(abs, map(sub, map(sum, ...))) 

def part_2(filename):
    result = 0
    return result

def main():
    assert part_1('test.txt') == 11
    print(part_1('input.txt'))
    # assert part_2('test.txt') == 0
    # print(part_2('input.txt'))

if __name__ == "__main__":
    main()
