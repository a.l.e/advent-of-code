# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring
def part_1(filename):
    result = 0
    with open(filename, 'r') as f:
        for report in [list(map(int, line.rstrip().split())) for line in f]:
            increasing = None
            preceding = report[0]
            for level in report[1:]:
                if level == preceding:
                    break
                if increasing is None:
                    increasing = (level > preceding)
                elif increasing != (level > preceding):
                    break
                if abs(level - preceding) > 3:
                    break
                preceding = level
            else:
                result += 1
    return result

def is_safe(report):
    increasing = None
    preceding = report[0]
    for level in report[1:]:
        if level == preceding:
            return False
        if increasing is None:
            increasing = (level > preceding)
        elif increasing != (level > preceding):
            return False
        if abs(level - preceding) > 3:
            return False
        preceding = level
    return True

def part_2(filename):
    result = 0
    with open(filename, 'r') as f:
        for report in [list(map(int, line.rstrip().split())) for line in f]:
            if is_safe(report):
                result += 1
            else:
                for i in range(len(report)):
                    if is_safe([v for j, v in enumerate(report) if i != j]):
                        result += 1
                        break
    return result

def main():
    assert part_1('test.txt') == 2
    print(part_1('input.txt'))
    assert part_2('test.txt') == 4
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()
