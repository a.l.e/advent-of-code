#include <cassert>
#include <string>
#include <vector>
#include <print>
#include <iostream>
#include <set>
#include <sstream>
#include <algorithm>

using Report = std::vector<int>;

Report get_report(std::string line) {
    Report report{};
    std::istringstream iss(line);
    std::string token;
    while(std::getline(iss, token, ' ')) {
        report.push_back(std::stoi(token));
    }
    return report;
}

bool is_safe(const Report& report) {
    std::set<int> distances{};
    for (int i = 1; i < report.size(); i++) {
        distances.insert(report[i - 1] - report[i]);
    }
    return
        std::all_of(distances.begin(), distances.end(), [](int d) { return d >= 1 && d <= 3; }) ||
        std::all_of(distances.begin(), distances.end(), [](int d) { return d >= -3 && d <= -1; });
}

int a() {
    int result = 0;
    std::string line;
    while (std::getline(std::cin, line)) {
        if (is_safe(get_report(line))) {
            result += 1;
        }
    }
    return result;
}

int b() {
    int result = 0;
    std::string line;
    while (std::getline(std::cin, line)) {
        Report report = get_report(line);
        for (int i = 0; i < report.size(); i++) {
            Report shorter{};
            for (int j = 0; j < report.size(); j++) {
                if (i != j) {
                    shorter.push_back(report[j]);
                }
            }
            if (is_safe(shorter)) {
                result += 1;
                break;
            }
        }
    }
    return result;
}

int main(int argc, char *argv[]) {
    std::string part = argc == 3 && std::string(argv[1]) == "-part" ? argv[2] : "";

    if (part == "") {
        std::println("Usage: {0} -part a|b [< input.txt]", argv[0]);
    }

    if (part == "a") {
        std::println("{0}", a());
    } else if (part == "b") {
        std::println("{0}", b());
    }
}
