# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring

# with improvements from:
# https://www.reddit.com/r/adventofcode/comments/1h4ncyr/comment/m0041k3/
# https://git.sr.ht/~kwshi/advent-of-code/tree/main/item/python/2024/02.py

def parse(filename):
    with open(filename, 'r') as f:
        for line in f:
            yield [*(map(int, line.rstrip().split()))]

def is_safe(row):
    inc = {row[i + 1] - row[i] for i in range(len(row) - 1)}
    return inc <= {1, 2, 3} or inc <= {-1, -2, -3}

def part_1(filename):
    return sum(is_safe(report) for report in parse(filename))

def part_2(filename):
    return sum([any([is_safe(report[:i] + report[i + 1:])
        for i in range(len(report))])
            for report in parse(filename)])

def main():
    assert part_1('test.txt') == 2
    print(part_1('input.txt'))
    assert part_2('test.txt') == 4
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()
