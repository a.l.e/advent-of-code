# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring

from itertools import product
from operator import mul, add

def concat(a, b):
    return int(str(a)+str(b))

# inspired by https://www.reddit.com/r/adventofcode/comments/1h8l3z5/comment/m0u3k4l/
# not really faster with the indexes
def can_match_target_index(target, operands, available_operators):
    n = len(operands)
    for operators in product(available_operators, repeat=n - 1):
        result = operands[0]
        for i in range(n - 1):
            result = operators[i](result, operands[i + 1])
            if result > target:
                break
        if result == target:
            return True
    return False

def can_match_target(target, operands, available_operators):
    n = len(operands)
    for operators in product(available_operators, repeat=n - 1):
        result = operands[0]
        for operand, operator in zip(operands[1:], operators):
            result = operator(result, operand)
            if result > target:
                break
        if result == target:
            return True
    return False


def part_1(filename):
    result = 0
    with open(filename, 'r') as f:
        for target, *operands in [map(int, line.rstrip().replace(':', '').split()) for line in f]:
            if can_match_target(target, operands, [mul, add]):
                result += target
    return result

def part_2(filename):
    result = 0
    with open(filename, 'r') as f:
        for target, *operands in [map(int, line.rstrip().replace(':', '').split()) for line in f]:
            if can_match_target(target, operands, [mul, add, concat]):
                result += target
    return result

def main():
    assert part_1('test.txt') == 3749
    print(part_1('input.txt'))
    assert part_2('test.txt') == 11387
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()

