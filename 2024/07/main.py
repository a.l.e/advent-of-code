# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring

from itertools import product
import math

def op_sum(a, b):
    return a + b

def op_mul(a, b):
    return a * b

def op_concat(a, b):
    if b == 0:
        return a * 10
    log = math.floor(math.log10(b))
    return int(a * 10 ** (1 + log) + b)

def can_match_target(target, operands, operators):
    for operators in operators:
        try_operands = list(operands)
        result = try_operands.pop(0)
        for operator in operators:
            result = operator(result, try_operands.pop(0))
        if result == target:
            return True
    return False

def part_1(filename):
    result = 0
    with open(filename, 'r') as f:
        for target, *operands in [map(int, line.rstrip().replace(':', '').split()) for line in f]:
            operator_choices = product([op_sum, op_mul], repeat=len(operands) - 1)
            if can_match_target(target, operands, operator_choices):
                result += target
    return result

def part_2(filename):
    result = 0
    with open(filename, 'r') as f:
        for target, *operands in [map(int, line.rstrip().replace(':', '').split()) for line in f]:
            operator_choices = product([op_sum, op_mul, op_concat], repeat=len(operands) - 1)
            if can_match_target(target, operands, operator_choices):
                result += target
    return result

def main():
    assert part_1('test.txt') == 3749
    print(part_1('input.txt'))
    assert part_2('test.txt') == 11387
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()
