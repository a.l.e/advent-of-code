# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring

from collections import defaultdict

def part_1(line, blinking=25):
    result = 0
    stones = list(map(int, [stone for stone in line.split()]))
    # print(stones, len(stones))
    for i in range(blinking):
        next_generation = []
        for stone in stones:
            if stone == 0:
                next_generation.append(1)
            elif len(stone_string := str(stone)) % 2 == 0:
                next_generation.append(int(stone_string[:len(stone_string) // 2]))
                next_generation.append(int(stone_string[len(stone_string) // 2:]))
            else:
                next_generation.append(stone * 2024)
        stones = next_generation
        # print(stones, len(stones))

    return len(stones)

def part_2(line, blinking=25):
    result = 0
    stones = defaultdict(int)
    for stone in map(int, [stone for stone in line.split()]):
        stones[stone] = 1
    # print(stones, len(stones))
    for i in range(blinking):
        next_generation = defaultdict(int)
        for stone, stone_count in stones.items():
            if stone == 0:
                next_generation[1] += stone_count
            elif len(stone_string := str(stone)) % 2 == 0:
                next_generation[int(stone_string[:len(stone_string) // 2])] += stone_count
                next_generation[int(stone_string[len(stone_string) // 2:])] += stone_count
            else:
                next_generation[stone * 2024] += stone_count
        stones = next_generation
        # print(stones, len(stones))

    return sum(stones.values())

def main():
    with open('test.txt', 'r') as f:
        assert part_1(f.readline().rstrip(), 1) == 7
    with open('test-02.txt', 'r') as f:
        assert part_1(f.readline().rstrip(), 6) == 22
    with open('test-02.txt', 'r') as f:
        assert part_1(f.readline().rstrip()) == 55312
    with open('input.txt', 'r') as f:
        print(part_1(f.readline().rstrip()))
    with open('test-02.txt', 'r') as f:
        assert part_2(f.readline().rstrip(), 6) == 22
    with open('test-02.txt', 'r') as f:
        assert part_2(f.readline().rstrip()) == 55312
    with open('input.txt', 'r') as f:
        assert part_2(f.readline().rstrip()) == 194482
    with open('input.txt', 'r') as f:
        print(part_2(f.readline().rstrip(), 75))

if __name__ == "__main__":
    main()
