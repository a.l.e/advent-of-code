# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring

from collections import defaultdict

def part_1(filename):
    obstructions = set() # {(x,y),...}
    guard_position = None # (x,y)
    guard_direction = None # 0123 -> NWSE
    positions = set() # {(x,y),...}
    width = 0
    height = 0
    directions = ((0, -1), (1, 0), (0, 1), (-1, 0)) # NWSE

    with open(filename, 'r') as f:
        for y, line in enumerate(f):
            height += 1
            width = len(line) - 1
            for x, c in enumerate(line.rstrip()):
                if c == '#':
                    obstructions.add((x, y))
                elif c == '^':
                    guard_position = (x, y)
                    guard_direction = 0
    # print('=', guard_position, (width, height))

    while 0 <= guard_position[0] < width and 0 <= guard_position[1] < height:
        positions.add(guard_position)
        next_position = (guard_position[0] + directions[guard_direction][0], \
            guard_position[1] + directions[guard_direction][1])
        # print('->', next_position)
        if next_position in obstructions:
            guard_direction = (guard_direction + 1) % 4
        else:
            guard_position = next_position

    return len(positions)

def get_data(filename):
    guard_position = None # (x,y)
    guard_direction = None # 0123 -> NWSE
    obstructions = set() # {(x,y),...}
    width = 0
    height = 0
    with open(filename, 'r') as f:
        for y, line in enumerate(f):
            height += 1
            width = len(line) - 1
            for x, c in enumerate(line.rstrip()):
                if c == '#':
                    obstructions.add((x, y))
                elif c == '^':
                    guard_position = (x, y)
                    guard_direction = 0
    return guard_position, guard_direction, obstructions, width, height

def get_next_position(position, direction):
    return (position[0] + direction[0], \
        position[1] + direction[1])

def get_path(position, obstructions, width, height):
    """
    Return the path as a set of coordinates and a boolean stating
    if it finished on a cycle (True) or by exiting the room (False)
    """
    positions = defaultdict(set) # {(x,y):[],...}
    directions = ((0, -1), (1, 0), (0, 1), (-1, 0)) # NWSE
    direction = 0

    while 0 <= position[0] < width and 0 <= position[1] < height:
        if position in positions and direction in positions[position]:
            return list(positions.keys()), True
        positions[position].add(direction)
        while get_next_position(position, directions[direction]) in obstructions:
            direction = (direction + 1) % 4
        position = get_next_position(position, directions[direction])

    return list(positions.keys()), False

def part_2(filename):
    count_cycles = 0

    guard_start_position, guard_start_direction, obstructions, width, height = get_data(filename)

    path, _ = get_path(guard_start_position, obstructions, width, height)
    path.remove(guard_start_position)
    for new_obstruction in path:
        obstructions.add(new_obstruction)
        _, is_cycle = get_path(guard_start_position, obstructions, width, height)
        if is_cycle:
            count_cycles += 1
        obstructions.remove(new_obstruction)
        
    return count_cycles

def main():
    assert part_1('test.txt') == 41
    print(part_1('input.txt'))
    assert part_2('test.txt') == 6
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()
