# Advent of Code 2024

For each day (I've solved), `main.py` contains the code, I've written to solve the puzzle.

`main-improved.py`, when it exists, contains an improved solution, inspired by reading [the solutions in the Reddit thread](https://www.reddit.com/r/adventofcode/).

Sometimes, there is also a `main.cpp` with a solution in C++.

- 01: read a file with two int per line (fancy one-lines in main-improved).
- 02: read list of ints per line; check for order and distance.
- 03: regexp on a piece of (corrupted) code.
- 04: diagonals in a 2D-array
- 05: rules for validating the order in a list, and sorting with a custom compare function
- 06: move in a grid and turn when hitting an obstacle (and find cycles)
- 07: apply operators to operands and check if the result matches a target
- 08: some distance calculations in a 2D map
- 10: find tree-lie paths in a 2D map (by using dictionaries...)
- 11: game of life 1D, with caching of the values
- 12:
- 13:
- 14: robots moving around in a 2D map and detecting a christmas tree
- 15: pushing around boxes in a 2D map (with shapes bigger than 1)
- 16: shortest path with weights in a 2D map

## Python helpers

### `main()`

A small template for getting started:

```py
# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring

def part_1(filename):
    result = 0
    with open(filename) as f:
        for line in [line.rstrip() for line in f]:
            print(line)

    return result

def part_2(filename):
    result = 0
    with open(filename) as f:
        for line in [line.rstrip() for line in f]:
            print(line)

    return result

def main():
    assert part_1('test.txt') == 0
    # print(part_1('input.txt'))
    # assert part_2('test.txt') == 0
    # print(part_2('input.txt'))

if __name__ == "__main__":
    main()
```

### Read text file

#### Line by line

```py
with open(filename) as f:
    for line in [line.rstrip() for line in f]:
```

#### Single line

```py
with open(filename, 'r') as f:
    return f.read().rstrip()
```

```py
# abc, def, ghi
for m in f.read().rstrip().split(', '):
    pass
```

```py
# L1, R2, L3, R5
for m in [(m[0], int(m[1:])) for m in f.read().rstrip().split(', ')]:
    pass
```

#### Line by line with a different meaning for the first number

```
12: 1 2 3 4
21: 1 2
```

```py
with open(filename, 'r') as f:
    for target, *operands in [map(int, line.rstrip().replace(':', '').split()) for line in f]:
        pass
```

### Point

```py
from dataclasses import dataclass
@dataclass
class Point:
    x: int
    y: int
```

### Sorting with a compare function

```py
def compare_hands_counter(left, right):
    if left[1] > right[1]:
        return -1
    if left[1] < right[1]:
        return 1
    if CARDS_VALUES[left[0]] < CARDS_VALUES[right[0]]:
        return -1
    if CARDS_VALUES[left[0]] > CARDS_VALUES[right[0]]:
        return 1
    return 0

counter = sorted(Counter(sorted(hand)).items(), key=functools.cmp_to_key(compare_hands_counter))
```
