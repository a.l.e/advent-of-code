# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring

from collections import defaultdict

def get_data(filename):
    levels = defaultdict(set) # {d: ((x, y), ...)}
    with open(filename, 'r') as f:
        for y, line in enumerate(f):
            for x, d in enumerate(int(c) for c in line.rstrip()):
                levels[d].add((x, y))
    return levels

def get_reachable_endpoints(level, paths):
    result = defaultdict(set)
    directions = ((0, -1), (1, 0), (0, 1), (-1, 0))
    for coordinates in level:
        for direction in directions:
            neighbour = (coordinates[0] + direction[0], coordinates[1] + direction[1])
            if neighbour in paths:
                result[coordinates].update(paths[neighbour])
    return result

def part_1(filename):
    result = 0
    levels = get_data(filename)
    paths = defaultdict(set)
    for level in levels[9]:
        paths[level].add(level)
    for i in range(9)[::-1]:
        paths = get_reachable_endpoints(levels[i], paths)
    return sum(len(nines) for nines in paths.values())

def get_trails(level, paths):
    result = defaultdict(list)
    directions = ((0, -1), (1, 0), (0, 1), (-1, 0))
    for coordinates in level:
        for direction in directions:
            neighbour = (coordinates[0] + direction[0], coordinates[1] + direction[1])
            if neighbour in paths:
                result[coordinates] += [path + [coordinates] for path in paths[neighbour]]
    return result

def part_2(filename):
    result = 0
    levels = get_data(filename)
    paths = defaultdict(list) # {(x, y): [[(9, 9), (8, 8), ...], [...]]
    for level in levels[9]:
        paths[level] = [[level]]
    for i in range(9)[::-1]:
        paths = get_trails(levels[i], paths)
    return sum(len(trails) for trails in paths.values())

def main():
    assert part_1('test.txt') == 1
    assert part_1('test-02.txt') == 2
    assert part_1('test-03.txt') == 4
    assert part_1('test-04.txt') == 36
    print(part_1('input.txt'))
    assert part_2('test-05.txt') == 3
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()
