# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring

def get_occurrences(line):
    return line.count('XMAS') + line.count('SAMX')

def part_1(filename):
    result = 0

    with open(filename, 'r') as f:
        text = [line.rstrip() for line in f]
    n = len(text)

    # horizontal
    for line in text:
        result += get_occurrences(line)
    # vertical
    for i in range(n):
        line = ""
        for j in range(n):
            line += text[j][i]
        # print(line)
        result += get_occurrences(line)

    # diagonals starting in the first row
    for i in range(n):
        diagonal_right = ""
        diagonal_left = ""
        for j, k in zip(range(n - i), range(i, n)):
            diagonal_right += text[j][k]
            diagonal_left += text[j][n - 1 - k]
        result += get_occurrences(diagonal_right)
        result += get_occurrences(diagonal_left)
    # diagonals starting in the first/last column (except the first one)
    for i in range(1, n):
        diagonal_right = ""
        diagonal_left = ""
        for j, k in zip(range(n - i), range(i, n)):
            diagonal_right += text[k][j]
            diagonal_left += text[k][n - 1 - j]
        result += get_occurrences(diagonal_right)
        result += get_occurrences(diagonal_left)

    return result

def part_2(filename):
    result = 0

    with open(filename, 'r') as f:
        text = [line.rstrip() for line in f]
    n = len(text)

    for i in range(n - 2):
        for j in range(n - 2):
            right_diagonal = ""
            left_diagonal = ""
            for k in range(3):
                right_diagonal += text[i + k][j + k]
                left_diagonal += text[i + 2 - k][j + k]
            if (right_diagonal == 'MAS' or right_diagonal == 'SAM') and \
                (left_diagonal == 'MAS' or left_diagonal == 'SAM'):
                    result += 1
    return result

def main():
    assert part_1('test.txt') == 18
    print(part_1('input.txt'))
    assert part_2('test.txt') == 9
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()
