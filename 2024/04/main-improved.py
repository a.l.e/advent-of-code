import itertools

# improved by going in every direction from "each" coordinates

def part_1(filename):
    result = 0

    with open(filename, 'r') as f:
        text = [line.rstrip() for line in f]
    n = len(text)

    word = 'XMAS'
    directions = list(itertools.product([-1, 0, 1], repeat=2))
    directions.remove((0, 0))
    for i in range(n):
        for j in range(n):
            for direction in directions:
                for k, c in enumerate(word):
                    x = j + direction[0] * k
                    y = i + direction[1] * k
                    if 0 <= x < n and 0 <= y < n:
                        if text[y][x] != c:
                            break
                    else:
                        break
                else:
                    result += 1
    return result

def part_2(filename):
    result = 0

    with open(filename, 'r') as f:
        text = [line.rstrip() for line in f]
    n = len(text)

    for i in range(1, n - 1):
        for j in range(1, n - 1):
            if text[i][j] == 'A' and \
               text[i - 1][j - 1] + text[i + 1][j + 1]  in ('MS', 'SM') and \
               text[i + 1][j - 1] + text[i - 1][j + 1] in ('MS', 'SM'):
                    result += 1

    return result

def main():
    assert part_1('test.txt') == 18
    print(part_1('input.txt'))
    assert part_2('test.txt') == 9
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()
