# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring

def part_1(filename):
    result = 0
    with open(filename, 'r') as f:
        for report in [list(map(int, line.rstrip().split())) for line in f]:
            pass
    return result

def part_2(filename):
    result = 0
    return result

def main():
    assert part_1('test.txt') == 0
    print(part_1('input.txt'))
    assert part_2('test.txt') == 0
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()
