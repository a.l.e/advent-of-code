# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring

def part_1(filename_or_program, read_file=True):
    if read_file:
        with open(filename_or_program, 'r') as f:
            program = ''.join(f.readlines())
    else:
        program = filename_or_program

    result = 0
    start = 0
    while True:
        start = program.find('mul(', start)
        if start < 0:
            break
        end = program.find(')', start)
        mul = program[start + 4:end]
        if ',' not in mul:
            start += 4
            continue
        a, b = mul.split(',', maxsplit=1)
        if not a.isdigit() or not b.isdigit():
            start += 4
            continue
        result += int(a) * int(b)
        start = end
    return result


def part_2(filename):
    result = 0
    with open(filename, 'r') as f:
        program = ''.join(f.readlines())

    # remove the parts between don't() and do()
    start = 0
    end = 0
    while True:
        start = program.find("don't()", start)
        if start < 0:
            break
        end = program.find('do()', start)
        # if no do() is found, go to the end of the string
        if end < 0:
            end = len(program)
        else:
            end += 4
        program = program[:start] + program[end:]

    return part_1(program, read_file=False)

def main():
    assert part_1('test.txt') == 161
    print(part_1('input.txt'))
    assert part_2('test-2.txt') == 48
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()
