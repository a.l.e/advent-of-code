# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring

import re
from operator import mul

# inspired by https://www.reddit.com/r/adventofcode/comments/1h5frsp/comment/m05of6n/ 
def part_2(filename):
    result = 0
    enabled = True

    with open(filename, 'r') as f:
        program = ''.join(f.readlines())

    for a, b, do, dont in re.findall(r"mul\((\d+),(\d+)\)|(do\(\))|(don't\(\))", program):
        if do or dont:
            enabled = bool(do)
        elif enabled:
            result += int(a) * int(b)
    return result

# inspired by https://www.reddit.com/r/adventofcode/comments/1h5obsr/2024_day_3_regular_expressions_go_brrr/
def part_2_b(filename):
    result = 0
    enabled = True

    with open(filename, 'r') as f:
        program = ''.join(f.readlines())

    for match in re.findall(r"mul\(\d+,\d+\)|do\(\)|don't\(\)", program):
        match match:
            case 'do()':
                enabled = True
            case "don't()":
                enabled = False
            case _:
                if enabled:
                    a, b = map(int, match[4:-1].split(','))
                    result += a * b

    return result

def main():
    # assert part_1('test.txt') == 161
    # print(part_1('input.txt'))
    assert part_2('test-2.txt') == 48
    print(part_2('input.txt'))
    assert part_2_b('test-2.txt') == 48
    print(part_2_b('input.txt'))

if __name__ == "__main__":
    main()
