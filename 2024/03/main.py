# pylint: disable=unspecified-encoding, missing-module-docstring, missing-function-docstring

import re

def part_1(filename):
    result = 0
    with open(filename, 'r') as f:
        program = ''.join(f.readlines())
    pattern = re.compile(r'mul\((\d+),(\d+)\)', re.MULTILINE)
    for match in pattern.finditer(program):
        result += int(match.group(1)) * int(match.group(2))
    return result

def part_2(filename):
    result = 0
    with open(filename, 'r') as f:
        program = ''.join(f.readlines())

    pattern_do = re.compile(r'do(?:n\'t)?\(\)', re.MULTILINE)
    pattern_mul = re.compile(r'mul\((\d+),(\d+)\)', re.MULTILINE)

    do_active = True
    span_start = 0
    program_spans = []
    for match_do in pattern_do.finditer(program):
        if do_active:
            program_spans.append((span_start, match_do.span(0)[0]))
        do_active = (match_do.group(0) == 'do()')
        span_start = match_do.span(0)[1]
    if do_active:
        program_spans.append((span_start, len(program)))

    for span in program_spans:
        for match_mul in pattern_mul.finditer(program[span[0]:span[1]]):
            result += int(match_mul.group(1)) * int(match_mul.group(2))
        
    return result

def main():
    assert part_1('test.txt') == 161
    print(part_1('input.txt'))
    assert part_2('test-2.txt') == 48
    print(part_2('input.txt'))

if __name__ == "__main__":
    main()
